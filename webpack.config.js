const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
	plugins: [new MiniCssExtractPlugin()],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /nodule_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
		]
	}
}