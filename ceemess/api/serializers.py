from django.contrib.auth import get_user_model, authenticate, login, logout
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import authenticate 
from django.contrib.auth.models import User 
# User = get_user_model()

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, ParseError
from rest_framework.relations import HyperlinkedIdentityField
from rest_framework.reverse import reverse
from rest_framework.fields import CurrentUserDefault

from blog.models import BlogSettings, BlogImage, BlogVideo, BlogPhotoAlbum, BlogPhotoAlbumPhoto, BlogMusicAlbum, BlogMusicAlbumSong, BlogCategory, BlogTag, BlogCredit, BlogLink, BlogPost, BlogPhotoPrintSize
from userprofile.models import UserProfile

# LOGIN AND USER
class UserPublicSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False, allow_blank=True, read_only=True)

    class Meta:
        model = User
        fields = ['username',]

class UserSerializer(serializers.ModelSerializer):

	class Meta:
		model = User
		fields = ('id', 'username', 'email')

class LoginSerializer(serializers.Serializer):
	username = serializers.CharField()
	password = serializers.CharField()

	def validate(self, data):
		user = authenticate(**data)
		if user and user.is_active:
			return user
		raise serializers.ValidationError("Incorrect Credentials")

class ChangePasswordSerializer(serializers.Serializer):
	model = User

	old_password = serializers.CharField(required=True)
	new_password = serializers.CharField(required=True)

class ResetPasswordRequestSerializer(serializers.Serializer):
	username = serializers.CharField()
	email = serializers.CharField()

class ResetPasswordSerializer(serializers.Serializer):
	username = serializers.CharField()
	password = serializers.CharField()

class RegisterSerializer(serializers.ModelSerializer):

	class Meta:
		model = User
		fields = ('username', 'email', 'password')
		extra_kwargs = {'password': {'write_only': True}}

	def create(self, validated_data):
		if User.objects.filter(username__iexact=validated_data['username']).exists():
				raise serializers.ValidationError("A user with this username already exists")
		if User.objects.filter(email__iexact=validated_data['email']).exists():
			raise serializers.ValidationError("A user with this email address already exists")
		user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])
		user.set_password(validated_data['password'])
		user.save()
		return user 










# FRONT END USE
class BlogSettingsSerializer(serializers.ModelSerializer):
	slug        			= serializers.SlugField(max_length=50, required=False, read_only=True)
	image  						= serializers.ImageField(read_only=True)
	thumbnail 				= serializers.ImageField(read_only=True)
	created_by  			= UserSerializer(read_only=True)

	class Meta:
		model 					= BlogSettings
		fields 					= ['name', 'slug', 'image', 'thumbnail', 'show_blog', 'default_blog', 'show_portfolio',
												'default_portfolio', 'show_photos', 'default_photos', 'show_music', 'default_music', 
												'show_videos', 'default_videos', 'show_images', 'default_images', 'description', 
												'show_creator', 'created_by']

class BlogLikeSerializer(serializers.ModelSerializer):
	class Meta:
		model 					= User
		fields 					= '__all__'

class BlogCreditSerializer(serializers.ModelSerializer):
	class Meta:
		model 					= BlogCredit
		fields 					= '__all__'

class BlogLinkSerializer(serializers.ModelSerializer):
	class Meta:
		model 					= BlogLink
		fields 					= ['description', 'link', 'social_media', 'slug']


class BlogCategorySerializer(serializers.ModelSerializer):
	class Meta:
		model 					= BlogCategory
		fields 					= ['category', 'slug']


class BlogTagSerializer(serializers.ModelSerializer):
	class Meta:
		model 					= BlogTag
		fields 					= ['tag', 'slug']


class BlogMusicAlbumSongSerializer(serializers.ModelSerializer):
	thumbnail 				= serializers.ImageField(read_only=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model 					= BlogMusicAlbumSong
		fields 					= ['title', 'slug', 'description', 'image', 'thumbnail', 'soundcloud_embed', 'youtube_url',
												'audio_file', 'created_by', 'created_at']

class BlogMusicAlbumSerializer(serializers.ModelSerializer):
	album_songs  			= BlogMusicAlbumSongSerializer(many=True, allow_null=True, read_only=True)
	thumbnail 				= serializers.ImageField(read_only=True)
	category 					= BlogCategorySerializer(read_only=True)
	tags 							= BlogTagSerializer(read_only=True, many=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model 					= BlogMusicAlbum
		fields 					= ['band_name', 'title', 'slug', 'description', 'image', 'thumbnail', 'album_songs',
												'tags', 'category', 'public', 'created_by', 'created_at']

	def get_album_songs(self, obj):
		obj.album_songs = BlogMusicAlbumSong.objects.filter(album=self)
		return obj

class BlogPhotoPrintSizeSerializer(serializers.ModelSerializer):
	class Meta:
			model 				= BlogPhotoPrintSize
			fields 				= '__all__'

class BlogPhotoAlbumPhotoSerializer(serializers.ModelSerializer):
	thumbnail 				= serializers.ImageField(read_only=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model 					= BlogPhotoAlbumPhoto 
		fields 					= ['title', 'slug', 'image', 'thumbnail', 'created_by', 'created_at']

class BlogPhotoAlbumSerializer(serializers.ModelSerializer):
	album_photos  		= BlogPhotoAlbumPhotoSerializer(many=True, allow_null=True)
	category 					= BlogCategorySerializer(read_only=True)
	tags 							= BlogTagSerializer(read_only=True, many=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model 					= BlogPhotoAlbum
		fields 					= ['title', 'slug', 'description', 'category', 'tags', 'public', 'album_photos', 
												'created_by', 'created_at']

	def get_album_photos(self, obj):
		obj.album_photos = BlogPhotoAlbumPhoto.objects.filter(album=self)
		return obj

class BlogVideoSerializer(serializers.ModelSerializer):
	thumbnail 				= serializers.ImageField()
	category 					= BlogCategorySerializer(read_only=True)
	tags 							= BlogTagSerializer(read_only=True, many=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model 					= BlogVideo
		fields 					= ['title', 'slug', 'description', 'image', 'thumbnail', 'youtube_id',
												'vimeo_id', 'category', 'tags', 'public', 'created_by', 'created_at']

class BlogImageSerializer(serializers.ModelSerializer):
	image   					= serializers.ImageField(read_only=True)
	thumbnail 				= serializers.ImageField(read_only=True)
	category 					= BlogCategorySerializer(read_only=True)
	tags 							= BlogTagSerializer(read_only=True, many=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model 					= BlogImage
		fields 					= ['title', 'slug', 'description', 'caption', 'image', 'thumbnail', 
												'category', 'tags', 'public', 'created_by', 'created_at']

class BlogPostSerializer(serializers.ModelSerializer):
	thumbnail 				= serializers.ImageField()
	category 					= BlogCategorySerializer(read_only=True)
	tags 							= BlogTagSerializer(read_only=True, many=True)
	credits 					= BlogCreditSerializer(read_only=True, many=True)
	links 						= BlogLinkSerializer(read_only=True, many=True)
	image_highlight 	= BlogImageSerializer(read_only=True)
	image_gallery			= BlogImageSerializer(read_only=True, many=True)
	video_highlight		= BlogVideoSerializer(read_only=True)
	videos 						= BlogVideoSerializer(read_only=True, many=True)
	photo_album				= BlogPhotoAlbumSerializer(read_only=True)
	music_album				= BlogMusicAlbumSerializer(read_only=True)
	created_by  			= UserPublicSerializer(read_only=True)

	class Meta:
		model       		= BlogPost
		fields 					= ['blog', 'portfolio', 'title', 'slug', 'body', 'image_highlight', 'video_highlight',
											'image', 'image_gallery', 'photo_album', 'music_album', 'videos', 'sticky', 'category',
											'tags', 'credits', 'links', 'created_at', 'thumbnail', 'created_by']










# ADMIN USE
class DashboardSettingsSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	image  					= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 				= BlogSettings
		fields 				= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		image = self.context['image']
		created_by = self.context['request'].user
		blog_settings = BlogSettings.objects.create(created_by=created_by, image=image, **validated_data)
		return blog_settings

class DashboardPostSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	image  					= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	category 				= BlogCategorySerializer(read_only=True)
	tags 						= BlogTagSerializer(read_only=True, many=True)
	credits 				= BlogCreditSerializer(read_only=True, many=True)
	links 					= BlogLinkSerializer(read_only=True, many=True)
	image_highlight = BlogImageSerializer(read_only=True)
	image_gallery		= BlogImageSerializer(read_only=True, many=True)
	video_highlight	= BlogVideoSerializer(read_only=True)
	videos 					= BlogVideoSerializer(read_only=True, many=True)
	photo_album			= BlogPhotoAlbumSerializer(read_only=True)
	music_album			= BlogMusicAlbumSerializer(read_only=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)
	likes 					= BlogLikeSerializer(many=True, read_only=True)

	class Meta:
		model       = BlogPost
		fields      = '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		created_by = self.context['request'].user
		category = self.context['category']
		image = self.context['image']
		image_highlight = self.context['image_highlight']
		video_highlight = self.context['video_highlight']
		photo_album = self.context['photo_album']
		music_album = self.context['music_album']
		published_at = None
		if validated_data['published']:
			published_at = timezone.now()
		post = BlogPost.objects.create(image_highlight=image_highlight, video_highlight=video_highlight, photo_album=photo_album, music_album=music_album, created_by=created_by, category=category, image=image, published_at=published_at, **validated_data)
		for tag in self.context['tags']:
			post.tags.add(tag)
		for video in self.context['videos']:
			post.videos.add(video)
		for image in self.context['images']:
			post.image_gallery.add(image)
		for link in self.context['links']:
			post.links.add(link)
		return post

class DashboardPhotoAlbumPhotoSerializer(serializers.ModelSerializer):
	owner         = serializers.SerializerMethodField(read_only=True)
	album 				= BlogPhotoAlbumSerializer(read_only=True)
	image 				= serializers.ImageField(read_only=True)
	thumbnail 		= serializers.ImageField(read_only=True)
	slug        	= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  	= UserSerializer(read_only=True)
	tags 					= BlogTagSerializer(read_only=True, many=True)
	print_sizes 	= BlogPhotoPrintSizeSerializer(read_only=True, many=True)

	class Meta:
		model 		= BlogPhotoAlbumPhoto 
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		created_by = self.context['request'].user
		image = None
		if self.context['image'] != '':
			image = self.context['image']
		album = BlogPhotoAlbum.objects.filter(slug=self.context['album-slug'])[0]
		return BlogPhotoAlbumPhoto.objects.create(album=album, image=image, created_by=created_by, **validated_data)

class DashboardPhotoAlbumSerializer(serializers.ModelSerializer):
	owner         = serializers.SerializerMethodField(read_only=True)
	album_photos  = DashboardPhotoAlbumPhotoSerializer(many=True, allow_null=True, read_only=True)
	category 			= BlogCategorySerializer(read_only=True)
	tags 					= BlogTagSerializer(read_only=True, many=True)
	slug        	= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  	= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogPhotoAlbum
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def get_album_photos(self, obj):
		obj.album_photos = BlogPhotoAlbumPhoto.objects.filter(album=self)
		return obj

	def create(self, validated_data):
		created_by = self.context['request'].user
		category = self.context['category']
		album = BlogPhotoAlbum.objects.create(created_by=created_by, category=category, **validated_data)
		for tag in self.context['tags']:
			album.tags.add(tag)
		return album

class DashboardMusicAlbumSongSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	album 					= BlogMusicAlbumSerializer(read_only=True)
	image 					= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	audio_file 			= serializers.FileField(read_only=True)
	tags 						= BlogTagSerializer(read_only=True, many=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogMusicAlbumSong
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		created_by = self.context['request'].user
		album = BlogMusicAlbum.objects.filter(slug=self.context['album-slug'])[0]
		image = None
		if self.context['image'] != '':
			image = self.context['image']
		audio_file = None
		if self.context['audio_file'] != '':
			audio_file = self.context['audio_file']
		return BlogMusicAlbumSong.objects.create(created_by=created_by, image=image, audio_file=audio_file, album=album, **validated_data)

class DashboardMusicAlbumSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	album_songs 		= DashboardMusicAlbumSongSerializer(many=True, allow_null=True, read_only=True)
	image   				= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	category 				= BlogCategorySerializer(read_only=True)
	tags 						= BlogTagSerializer(read_only=True, many=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogMusicAlbum
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def get_album_songs(self, obj):
		obj.album_songs = BlogMusicAlbumSong.objects.filter(album=self)
		return obj

	def create(self, validated_data):
		created_by = self.context['request'].user
		category = self.context['category']
		image = self.context['image']
		album = BlogMusicAlbum.objects.create(created_by=created_by, category=category, image=image, **validated_data)
		for tag in self.context['tags']:
			album.tags.add(tag)
		return album

class DashboardVideoSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	image   				= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	category 				= BlogCategorySerializer(read_only=True)
	tags 						= BlogTagSerializer(read_only=True, many=True)
	credits					= BlogCreditSerializer(read_only=True, many=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogVideo
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		created_by = self.context['request'].user
		category = self.context['category']
		image = self.context['image']
		video = BlogVideo.objects.create(created_by=created_by, category=category, image=image, **validated_data)
		for tag in self.context['tags']:
			video.tags.add(tag)
		return video

class DashboardImageSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	image   				= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	category 				= BlogCategorySerializer(read_only=True)
	tags 						= BlogTagSerializer(read_only=True, many=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogImage
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data, *args, **kwargs):
		created_by = self.context['request'].user
		category = self.context['category']
		uploaded_image = self.context['image']
		image = BlogImage.objects.create(created_by=created_by, category=category, image=uploaded_image, **validated_data)
		for tag in self.context['tags']:
			image.tags.add(tag)
		return image

class DashboardLinkSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogLink
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		return BlogLink.objects.create(**validated_data)

class DashboardCategorySerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogCategory
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		return BlogCategory.objects.create(**validated_data)

class DashboardTagSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)

	class Meta:
		model 		= BlogTag
		fields 		= '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		return BlogTag.objects.create(**validated_data)








# IMPORT TEST POSTS
class DashboardPostImportSerializer(serializers.ModelSerializer):
	owner           = serializers.SerializerMethodField(read_only=True)
	image  					= serializers.ImageField(read_only=True)
	thumbnail 			= serializers.ImageField(read_only=True)
	category 				= BlogCategorySerializer(read_only=True)
	tags 						= BlogTagSerializer(read_only=True, many=True)
	credits 				= BlogCreditSerializer(read_only=True, many=True)
	links 					= BlogLinkSerializer(read_only=True, many=True)
	image_highlight = BlogImageSerializer(read_only=True)
	image_gallery		= BlogImageSerializer(read_only=True, many=True)
	video_highlight	= BlogVideoSerializer(read_only=True)
	videos 					= BlogVideoSerializer(read_only=True, many=True)
	photo_album			= BlogPhotoAlbumSerializer(read_only=True)
	music_album			= BlogMusicAlbumSerializer(read_only=True)
	slug        		= serializers.SlugField(max_length=50, required=False, read_only=True)
	created_by  		= UserSerializer(read_only=True)
	likes 					= BlogLikeSerializer(many=True, read_only=True)

	class Meta:
		model       = BlogPost
		fields      = '__all__'

	def get_owner(self, obj):
		request = self.context['request']
		if request.user.is_authenticated:
			if obj.created_by == request.user:
				return True
		return False

	def create(self, validated_data):
		created_by 	= self.context['request'].user
		post = BlogPost.objects.create(created_by=created_by, **validated_data)
		return post




