from django.urls import path, re_path
from knox import views as knox_views

from . import views

from .views import (
	BlogSettingsAPIView,
	DashboardSettingsAdminAPIView,
	DashboardSettingsEditAdminAPIView,

	BlogPostListAPIView,
	StickyBlogPostListAPIView,
	BlogPostDetailAPIView,
	CategoryListAPIView,
	CategoryPostListAPIView, 
	TagListAPIView,
	TagPostListAPIView,
	SearchListAPIView,

	PortfolioPostListAPIView,
	StickyPortfolioPostListAPIView,
	PortfolioCategoryPostListAPIView, 
	PortfolioTagPostListAPIView,
	PortfolioSearchListAPIView,

	PhotoAlbumListAPIView,
	PhotoAlbumCategoryPostListAPIView, 
	PhotoAlbumTagPostListAPIView,
	PhotoAlbumSearchListAPIView,

	MusicAlbumListAPIView,
	MusicAlbumCategoryPostListAPIView, 
	MusicAlbumTagPostListAPIView,
	MusicAlbumSearchListAPIView,

	VideoListAPIView,
	VideoCategoryPostListAPIView, 
	VideoTagPostListAPIView,
	VideoSearchListAPIView,

	ImageListAPIView,
	ImageCategoryPostListAPIView, 
	ImageTagPostListAPIView,
	ImageSearchListAPIView,

	SocialMediaLinksAPIView,

	LoginAPIView,
	UserAPIView,
	UsersAPIView, 
	ChangePasswordAPI,
	ResetPasswordRequestAPI, 
	ResetPasswordAPI,
	RegisterAPI,

	DashboardPostsAPIView,
	DashboardPostsAllAPIView, 
	DashboardPhotoAlbumsAPIView,
	DashboardPhotoAlbumsAllAPIView,
	DashboardMusicAlbumsAPIView,
	DashboardMusicAlbumsAllAPIView,
	DashboardVideosAPIView,
	DashboardVideosAllAPIView,
	DashboardImagesAPIView,
	DashboardImagesAllAPIView,
	DashboardCategoriesAPIView,
	DashboardTagsAPIView,
	DashboardLinksAPIView,

	PostsCreateAPIView,
	PostsImportAPIView, 
	PhotoAlbumsCreateAPIView,
	PhotosCreateAPIView, 
	MusicAlbumsCreateAPIView,
	TracksCreateAPIView, 
	VideosCreateAPIView,
	ImagesCreateAPIView,
	LinksCreateAPIView,
	CategoriesCreateAPIView,
	TagsCreateAPIView,

	PostEditDeleteAPIView,
	PhotoAlbumEditDeleteAPIView, 
	PhotoEditDeleteAPIView, 
	MusicAlbumEditDeleteAPIView, 
	TrackEditDeleteAPIView,
	VideoEditDeleteAPIView,
	ImageEditDeleteAPIView, 
	LinkEditDeleteAPIView, 
	CategoryEditDeleteAPIView,
	TagEditDeleteAPIView
)

urlpatterns = [
	path('settings/', BlogSettingsAPIView.as_view(), name='blog-settings'),
	path('admin/settings/', DashboardSettingsAdminAPIView.as_view(), name='dashboard-settings'),
	path('admin/settings/<str:slug>/update/', DashboardSettingsEditAdminAPIView.as_view(), name='dashboard-settings-update'),

	# ACCOUNTS
	path('login/', LoginAPIView.as_view(), name='login'),
	path('user/', UserAPIView.as_view(), name='user'),
	path('users/', UsersAPIView.as_view(), name='users'),
	path('change-password/', ChangePasswordAPI.as_view(), name='change-password'),
	path('reset-password/request/', ResetPasswordRequestAPI.as_view(), name='reset-password-request'),
	path('reset-password/<str:username>/<str:code>/', views.reset_password_finish, name='reset-password-finish'),
	path('register/', RegisterAPI.as_view(), name='register'),
	# path('verify-account/', )
	path('logout/', knox_views.LogoutView.as_view(), name='logout'),

	# BLOG
	path('', BlogPostListAPIView.as_view(), name='blog-post-list'),
	path('blog/', BlogPostListAPIView.as_view(), name='blog-post-list'),
	path('blog/sticky/', StickyBlogPostListAPIView.as_view(), name='sticky-blog-post-list'),
	path('blog/v/<str:slug>/', BlogPostDetailAPIView.as_view(), name='blog-detail-view'),
	path('categories/', CategoryListAPIView.as_view(), name='category-list'),
	path('categories/v/<str:slug>/', CategoryPostListAPIView.as_view(), name='category-post-list'),
	path('tags/', TagListAPIView.as_view(), name='tag-list'),
	path('tags/v/<str:slug>/', TagPostListAPIView.as_view(), name='tag-post-list'),
	path('search/<str:search>', SearchListAPIView.as_view(), name='search-list'),

	path('portfolio/', PortfolioPostListAPIView.as_view(), name='portfolio-post-list'),
	path('portfolio/sticky/', StickyPortfolioPostListAPIView.as_view(), name='sticky-portfolio-post-list'),
	path('portfolio/categories/v/<str:slug>/', PortfolioCategoryPostListAPIView.as_view(), name='portfolio-category-post-list'),
	path('portfolio/tags/v/<str:slug>/', PortfolioTagPostListAPIView.as_view(), name='tportfolio-tag-post-list'),
	path('portfolio/search/<str:search>', PortfolioSearchListAPIView.as_view(), name='portfolio-search-list'),

	path('photos/', PhotoAlbumListAPIView.as_view(), name='photo-album-list'),
	path('photos/categories/v/<str:slug>/', PhotoAlbumCategoryPostListAPIView.as_view(), name='photo-album-category-post-list'),
	path('photos/tags/v/<str:slug>/', PhotoAlbumTagPostListAPIView.as_view(), name='photo-album-tag-post-list'),
	path('photos/search/<str:search>', PhotoAlbumSearchListAPIView.as_view(), name='photo-album-search-list'),

	path('music/', MusicAlbumListAPIView.as_view(), name='music-album-list'),
	path('music/categories/v/<str:slug>/', MusicAlbumCategoryPostListAPIView.as_view(), name='music-album-category-post-list'),
	path('music/tags/v/<str:slug>/', MusicAlbumTagPostListAPIView.as_view(), name='music-album-tag-post-list'),
	path('music/search/<str:search>', MusicAlbumSearchListAPIView.as_view(), name='music-album-search-list'),

	path('videos/', VideoListAPIView.as_view(), name='video-list'),
	path('videos/categories/v/<str:slug>/', VideoCategoryPostListAPIView.as_view(), name='video-category-post-list'),
	path('videos/tags/v/<str:slug>/', VideoTagPostListAPIView.as_view(), name='video-tag-post-list'),
	path('videos/search/<str:search>', VideoSearchListAPIView.as_view(), name='video-search-list'),

	path('images/', ImageListAPIView.as_view(), name='image-list'),
	path('images/categories/v/<str:slug>/', ImageCategoryPostListAPIView.as_view(), name='image-category-post-list'),
	path('images/tags/v/<str:slug>/', ImageTagPostListAPIView.as_view(), name='image-tag-post-list'),
	path('images/search/<str:search>', ImageSearchListAPIView.as_view(), name='image-search-list'),

	path('links/social-media/', SocialMediaLinksAPIView.as_view(), name='social-media-links'),

	# LIST
	path('admin/dashboard/posts/', DashboardPostsAPIView.as_view(), name='dashboard-blog-posts'),
	path('admin/dashboard/posts/all/', DashboardPostsAllAPIView.as_view(), name='dashboard-blog-posts-all'),
	path('admin/dashboard/albums/photos/', DashboardPhotoAlbumsAPIView.as_view(), name='dashboard-photo-albums'),
	path('admin/dashboard/albums/photos/all/', DashboardPhotoAlbumsAllAPIView.as_view(), name='dashboard-photo-albums-all'),
	path('admin/dashboard/albums/music/', DashboardMusicAlbumsAPIView.as_view(), name='dashboard-music-albums'),
	path('admin/dashboard/albums/music/all/', DashboardMusicAlbumsAllAPIView.as_view(), name='dashboard-music-albums-all'),
	path('admin/dashboard/videos/', DashboardVideosAPIView.as_view(), name='dashboard-videos'),
	path('admin/dashboard/videos/all/', DashboardVideosAllAPIView.as_view(), name='dashboard-videos-all'),
	path('admin/dashboard/images/', DashboardImagesAPIView.as_view(), name='dashboard-images'),
	path('admin/dashboard/images/all/', DashboardImagesAllAPIView.as_view(), name='dashboard-images-all'),
	path('admin/dashboard/links/', DashboardLinksAPIView.as_view(), name='dashboard-links'),
	path('admin/dashboard/categories/', DashboardCategoriesAPIView.as_view(), name='dashboard-categories'),
	path('admin/dashboard/tags/', DashboardTagsAPIView.as_view(), name='dashboard-tags'),

	# CREATE
	path('admin/posts/create/', PostsCreateAPIView.as_view(), name='posts-create'),
	path('admin/albums/photos/create/', PhotoAlbumsCreateAPIView.as_view(), name='photos-albums-create'),
	path('admin/photos/create/', PhotosCreateAPIView.as_view(), name='photos-create'),
	path('admin/albums/music/create/', MusicAlbumsCreateAPIView.as_view(), name='music-albums-create'),
	path('admin/tracks/create/', TracksCreateAPIView.as_view(), name='tracks-create'),
	path('admin/videos/create/', VideosCreateAPIView.as_view(), name='videos-create'),
	path('admin/images/create/', ImagesCreateAPIView.as_view(), name='images-create'),
	path('admin/links/create/', LinksCreateAPIView.as_view(), name='links-create'),
	path('admin/categories/create/', CategoriesCreateAPIView.as_view(), name='categories-create'),
	path('admin/tags/create/', TagsCreateAPIView.as_view(), name='tags-create'),

	# EDIT & DELETE
	path('admin/posts/<str:slug>/update/', PostEditDeleteAPIView.as_view(), name='posts-update'),
	path('admin/albums/photos/<str:slug>/update/', PhotoAlbumEditDeleteAPIView.as_view(), name='photo-albums-update'),
	path('admin/photos/<str:slug>/update/', PhotoEditDeleteAPIView.as_view(), name='photos-update'),
	path('admin/albums/music/<str:slug>/update/', MusicAlbumEditDeleteAPIView.as_view(), name='music-albums-update'),
	path('admin/tracks/<str:slug>/update/', TrackEditDeleteAPIView.as_view(), name='tracks-update'),
	path('admin/videos/<str:slug>/update/', VideoEditDeleteAPIView.as_view(), name='videos-update'),
	path('admin/images/<str:slug>/update/', ImageEditDeleteAPIView.as_view(), name='images-update'),
	path('admin/links/<str:slug>/update/', LinkEditDeleteAPIView.as_view(), name='links-update'),
	path('admin/categories/<str:slug>/update/', CategoryEditDeleteAPIView.as_view(), name='categories-update'),
	path('admin/tags/<str:slug>/update/', TagEditDeleteAPIView.as_view(), name='tags-update'),

	path('admin/posts/import/', PostsImportAPIView.as_view(), name='posts-import'),
]










