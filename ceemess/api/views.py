from __future__ import unicode_literals
from django.conf import settings 
from django.contrib.auth.models import User
from django.db.models import Q, Max, Count
from django.core.mail import EmailMessage, send_mail 
# from django.db.models import Avg, Max, Min, Sum

from django.shortcuts import get_object_or_404, render, HttpResponseRedirect
from django.template.context_processors import csrf
from django.core.mail import EmailMessage

from django.template.loader import get_template

from django.utils.dateparse import parse_datetime
import pytz

import datetime
import time
import decimal
from django.utils import timezone 
import copy
import json

import csv
from io import TextIOWrapper

from rest_framework import generics, permissions, pagination, status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, ParseError, NotFound
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.parsers import FileUploadParser, MultiPartParser, JSONParser, FormParser
from knox.models import AuthToken 
from knox.auth import TokenAuthentication

from PIL import Image

from geopy.distance import geodesic

from .permissions import IsOwnerOrReadOnly, IsOwner, IsReadOnly
from .serializers import BlogSettingsSerializer, DashboardSettingsSerializer, BlogPostSerializer, BlogCategorySerializer, BlogTagSerializer, BlogPhotoAlbumSerializer, BlogPhotoAlbumPhotoSerializer, BlogVideoSerializer, BlogMusicAlbumSerializer, BlogMusicAlbumSongSerializer, BlogImageSerializer, BlogLinkSerializer, UserSerializer, LoginSerializer, DashboardPostSerializer, DashboardPhotoAlbumSerializer, DashboardPhotoAlbumPhotoSerializer, DashboardMusicAlbumSerializer, DashboardMusicAlbumSongSerializer, DashboardVideoSerializer, DashboardImageSerializer, DashboardCategorySerializer, DashboardTagSerializer, DashboardLinkSerializer, ChangePasswordSerializer, ResetPasswordRequestSerializer, ResetPasswordSerializer, RegisterSerializer, DashboardPostImportSerializer

from .utils import unique_email_code_generator
# from .push_notifications import send_push_message

from django.views.generic import DetailView

from blog.models import BlogSettings, BlogPost, BlogCategory, BlogTag, BlogPhotoAlbum, BlogPhotoAlbumPhoto, BlogVideo, BlogMusicAlbum, BlogMusicAlbumSong, BlogImage, BlogLink
from userprofile.models import UserProfile

# PAGINATION
class BlogPostPagination(pagination.PageNumberPagination):
	page_size = 10
	page_size_query_param = 'size'
	max_page_size = 200

	def get_paginated_response(self, data):
		author = False
		user = self.request.user 
		if user.is_authenticated:
			author = True

		context = {
			'next': self.get_next_link(),
			'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'author': author,
			'results': data
		}
		return Response(context)

class PortfolioPostPagination(pagination.PageNumberPagination):
	page_size = 20
	page_size_query_param = 'size'
	max_page_size = 200

	def get_paginated_response(self, data):
		author = False
		user = self.request.user 
		if user.is_authenticated:
			author = True

		context = {
			'next': self.get_next_link(),
			'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'author': author,
			'results': data
		}
		return Response(context)











# LOGIN 
class LoginAPIView(generics.GenericAPIView):
	serializer_class = LoginSerializer
	permission_classes = (AllowAny,)

	def post(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data
		return Response({
			"user": UserSerializer(user, context=self.get_serializer_context()).data,
			"token": AuthToken.objects.create(user)[1]
		})

# USER 
class UserAPIView(generics.RetrieveAPIView):
	permission_classes 	= [permissions.IsAuthenticated]
	serializer_class = UserSerializer

	def get_object(self):
		return self.request.user

class UsersAPIView(generics.ListAPIView):
	permission_classes 	= [permissions.IsAuthenticated]
	serializer_class = UserSerializer

	def get_queryset(self, *args, **kwargs):
		return User.objects.all()

class ChangePasswordAPI(generics.UpdateAPIView):
	"""
	An endpoint for changing password.
	"""
	serializer_class = ChangePasswordSerializer
	model = User
	permission_classes = [permissions.IsAuthenticated]

	def get_object(self, queryset=None):
		obj = self.request.user
		return obj

	def update(self, request, *args, **kwargs):
		self.object = self.get_object()
		serializer = self.get_serializer(data=request.data)

		if serializer.is_valid():
			# Check old password
			if not self.object.check_password(serializer.data.get("old_password")):
				return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
			# set_password also hashes the password that the user will get
			self.object.set_password(serializer.data.get("new_password"))
			self.object.save()
			response = {
				'status': 'success',
				'code': status.HTTP_200_OK,
				'message': 'Password updated successfully',
				'data': []
			}

			return Response(response)

		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ResetPasswordRequestAPI(generics.GenericAPIView):
	serializer_class = ResetPasswordRequestSerializer
	permission_classes = [permissions.AllowAny,]

	def post(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		data = request.data 
		if UserProfile.objects.filter(user__username=data['username'], user__email=data['email']).exists():
			profile = UserProfile.objects.get(user__username=data['username'], user__email=data['email'])
			profile.code = unique_email_code_generator(profile)
			profile.save()
			subject = "Reset Your Password." 
			message = "You have requested to reset your password. Please visit %s/api/reset-password/%s/%s/ to reset your password." % (data['url'], profile.user.username, profile.code)
			from_email = settings.EMAIL_HOST_USER 
			to_list = [profile.user.email, settings.EMAIL_HOST_USER]
			send_mail(subject, message, from_email, to_list, fail_silently=True)
			return Response(serializer.data, status=status.HTTP_200_OK)
		return Response({"incorrect_credentials": ["Wrong Username or Email."]}, status=status.HTTP_400_BAD_REQUEST)

class ResetPasswordAPI(generics.GenericAPIView):
	serializer_class = ResetPasswordSerializer 
	permission_classes = [permissions.AllowAny,]

	def post(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		data = request.data 
		if UserProfile.objects.filter(user__username=data['username'], code=data['code']).exists():
			profile = UserProfile.objects.get(user__username=data['username'], code=data['code'])
			user = profile.user 
			user.password = make_password(data['password'])
			user.save()
			profile.code = unique_email_code_generator(profile)
			profile.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		return Response(serializer.data, status=status.HTTP_404_NOT_FOUND)

class RegisterAPI(generics.GenericAPIView):
	serializer_class = RegisterSerializer
	permission_classes = [permissions.IsAuthenticated,]

	def post(self, request, *args, **kwargs):
		serializer = RegisterSerializer(data=request.data, context={'request': request})
		serializer.is_valid(raise_exception=True)
		user = serializer.save()
		profile = UserProfile.objects.create(user=user)
		
		# verify_url = "%s/api/verify-account/%s/%s/" % (data['url'], user.username, profile.code)
		# verify_message = "Click %s to verify your email address." % (verify_url)
		# template = get_template('contact_template.txt')
		# context = {
		# 	'name': user.username,
		# 	'email': user.email,
		# 	'message': verify_message,
		# }
		# content = template.render(context)
		# email = EmailMessage(
		# 	"New Account",
		# 	content,
		# 	'',
		# 	[user.email],
		# 	headers = {'Reply-to': user.email
		# })
		# email.send()

		return Response({
			"user": UserSerializer(user, context=self.get_serializer_context()).data,
			# "token": AuthToken.objects.create(user)[1]
			})

def profile_verify(request, username=1, code=1):
	success = False
	if UserProfile.objects.filter(user__username=username).exists():
		profile = UserProfile.objects.get(user__username=username)
		if profile.code == code:
			profile.verified = True
			profile.save()
			success = True
	args = {'username': username, 'code': code, 'success': success}
	return render(request, 'profile_verify.html', args)

def reset_password_finish(request, username=None, code=None):
	if request.POST:
		form_username 	= request.POST.get('username', '')
		form_code 		= request.POST.get('code', '')
		form_password	= request.POST.get('password', '')
		if UserProfile.objects.filter(user__username=username).exists():
			profile 	= UserProfile.objects.get(user__username=username)
			success 	= False
			if profile.code == form_code:
				success = True
				user 	= profile.user 
				user.set_password(form_password)
				user.save()
		args = {'success': success}
		return render(request, 'reset_password_finish.html', args)
	else:
		if UserProfile.objects.filter(user__username=username).exists():
			profile 	= UserProfile.objects.get(user__username=username)
			success 	= False
			if profile.code == code:
				success = True
		args = {'username': username, 'code': code, 'success': success}
		args.update(csrf(request))
		return render(request, 'reset_password_verify.html', args)













# BLOOG SETTINGS
class BlogSettingsAPIView(generics.ListAPIView):
	serializer_class    = BlogSettingsSerializer
	permission_classes 	= [IsReadOnly]

	def get_queryset(self, *args, **kwargs):
		return BlogSettings.objects.all()

class DashboardSettingsAdminAPIView(generics.ListCreateAPIView):
	serializer_class    = DashboardSettingsSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogSettings.objects.all()

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy() 

		serializer = DashboardSettingsSerializer(data=data, context={'request': request, 'image': data['image']})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DashboardSettingsEditAdminAPIView(generics.RetrieveUpdateDestroyAPIView):
	serializer_class    = DashboardSettingsSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogSettings.objects.all()

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogSettings.objects.get(slug=self.kwargs.get("slug"))
		instance.name 				= self.request.data.get('name')
		instance.description 		= self.request.data.get('description')

		instance.show_blog = False
		if data['show_blog'] == 'true':
			instance.show_blog = True

		instance.default_blog = False
		if data['default_blog'] == 'true':
			instance.default_blog = True

		instance.show_portfolio = False
		if data['show_portfolio'] == 'true':
			instance.show_portfolio = True

		instance.default_portfolio = False
		if data['default_portfolio'] == 'true':
			instance.default_portfolio = True

		instance.show_photos = False
		if data['show_photos'] == 'true':
			instance.show_photos = True

		instance.default_photos = False
		if data['default_photos'] == 'true':
			instance.default_photos = True

		instance.show_music = False
		if data['show_music'] == 'true':
			instance.show_music = True

		instance.default_music = False
		if data['default_music'] == 'true':
			instance.default_music = True

		instance.show_videos = False
		if data['show_videos'] == 'true':
			instance.show_videos = True

		instance.default_videos = False
		if data['default_videos'] == 'true':
			instance.default_videos = True

		instance.show_images = False
		if data['show_images'] == 'true':
			instance.show_images = True

		instance.default_images = False
		if data['default_images'] == 'true':
			instance.default_images = True

		instance.show_creator = False
		if data['show_creator'] == 'true':
			instance.show_creator = True

		if data['image'] != '':
			instance.image = data['image']

		instance.save()

		serializer = DashboardSettingsSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)








# BLOG VIEW		
class BlogPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), blog=True, sticky=False)
		else:
			return BlogPost.objects.filter(published=True, blog=True, sticky=False)

class StickyBlogPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), blog=True, sticky=True)
		else:
			return BlogPost.objects.filter(published=True, blog=True, sticky=True)

class BlogPostDetailAPIView(generics.RetrieveAPIView):
	serializer_class    = BlogPostSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsReadOnly]

	def get_object(self):
		if BlogPost.objects.filter(slug=self.kwargs.get("slug")).exists():
			return BlogPost.objects.filter(slug=self.kwargs.get("slug"))[0]
		else:
			raise NotFound()

class PortfolioPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), portfolio=True, sticky=False)
		else:
			return BlogPost.objects.filter(published=True, portfolio=True, sticky=False)

class StickyPortfolioPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), portfolio=True, sticky=True)
		else:
			return BlogPost.objects.filter(published=True, portfolio=True, sticky=True)

class CategoryListAPIView(generics.ListAPIView):
	serializer_class 	= BlogCategorySerializer
	permission_classes 	= [IsReadOnly]

	def get_queryset(self, *args, **kwargd):
		return BlogCategory.objects.all()

class CategoryPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), category__slug=self.kwargs.get("slug"))
		else:
			return BlogPost.objects.filter(published=True, category__slug=self.kwargs.get("slug"))

class TagListAPIView(generics.ListAPIView):
	serializer_class 	= BlogTagSerializer
	permission_classes 	= [IsReadOnly]

	def get_queryset(self, *args, **kwargd):
		return BlogTag.objects.all()

class TagPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), tags__slug=self.kwargs.get("slug"))
		else:
			return BlogPost.objects.filter(published=True, tags__slug=self.kwargs.get("slug"))

class PhotoAlbumListAPIView(generics.ListAPIView):
	serializer_class    = BlogPhotoAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPhotoAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)))
		else:
			return BlogPhotoAlbum.objects.filter(public=True)

class VideoListAPIView(generics.ListAPIView):
	serializer_class    = BlogVideoSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogVideo.objects.filter((Q(public=True) | Q(created_by=self.request.user)))
		else:
			return BlogVideo.objects.filter(public=True)

class MusicAlbumListAPIView(generics.ListAPIView):
	serializer_class    = BlogMusicAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogMusicAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)))
		else:
			return BlogMusicAlbum.objects.filter(public=True)

class ImageListAPIView(generics.ListAPIView):
	serializer_class    = BlogImageSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogImage.objects.filter((Q(public=True) | Q(created_by=self.request.user)))
		else:
			return BlogImage.objects.filter(public=True)

class SearchListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(title__icontains=self.kwargs.get("search")) | Q(body__icontains=self.kwargs.get("search")) & (Q(published=True) | Q(created_by=self.request.user))), blog=True, sticky=True)
		else:
			return BlogPost.objects.filter(Q(title__icontains=self.kwargs.get("search")) | Q(body__icontains=self.kwargs.get("search")), blog=True, published=True)

class PortfolioCategoryPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), category__slug=self.kwargs.get("slug"), portfolio=True)
		else:
			return BlogPost.objects.filter(category__slug=self.kwargs.get("slug"), portfolio=True, published=True)

class PortfolioTagPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), tags__slug=self.kwargs.get("slug"), portfolio=True)
		else:
			return BlogPost.objects.filter(tags__slug=self.kwargs.get("slug"), portfolio=True, published=True)

class PortfolioSearchListAPIView(generics.ListAPIView):
	serializer_class    = BlogPostSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= PortfolioPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPost.objects.filter((Q(published=True) | Q(created_by=self.request.user)), Q(title__icontains=self.kwargs.get("search")) | Q(body__icontains=self.kwargs.get("search")), portfolio=True)
		else:
			return BlogPost.objects.filter(Q(title__icontains=self.kwargs.get("search")) | Q(body__icontains=self.kwargs.get("search")), portfolio=True, published=True)

class PhotoAlbumCategoryPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPhotoAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPhotoAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)), category__slug=self.kwargs.get("slug"))
		else:
			return BlogPhotoAlbum.objects.filter(category__slug=self.kwargs.get("slug"), public=True)

class PhotoAlbumTagPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogPhotoAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPhotoAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)), tags__slug=self.kwargs.get("slug"))
		else:
			return BlogPhotoAlbum.objects.filter(tags__slug=self.kwargs.get("slug"), public=True)

class PhotoAlbumSearchListAPIView(generics.ListAPIView):
	serializer_class    = BlogPhotoAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogPhotoAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)), Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")))
		else:
			return BlogPhotoAlbum.objects.filter(Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")), public=True)

class MusicAlbumCategoryPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogMusicAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogMusicAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)), category__slug=self.kwargs.get("slug"))
		else:
			return BlogMusicAlbum.objects.filter(category__slug=self.kwargs.get("slug"), public=True)

class MusicAlbumTagPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogMusicAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogMusicAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)), tags__slug=self.kwargs.get("slug"))
		else:
			return BlogMusicAlbum.objects.filter(tags__slug=self.kwargs.get("slug"), public=True)

class MusicAlbumSearchListAPIView(generics.ListAPIView):
	serializer_class    = BlogMusicAlbumSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogMusicAlbum.objects.filter((Q(public=True) | Q(created_by=self.request.user)), Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")))
		else:
			return BlogMusicAlbum.objects.filter(Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")), public=True)

class VideoCategoryPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogVideoSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogVideo.objects.filter((Q(public=True) | Q(created_by=self.request.user)), category__slug=self.kwargs.get("slug"))
		else:
			return BlogVideo.objects.filter(category__slug=self.kwargs.get("slug"), public=True)

class VideoTagPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogVideoSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogVideo.objects.filter((Q(public=True) | Q(created_by=self.request.user)), tags__slug=self.kwargs.get("slug"))
		else:
			return BlogVideo.objects.filter(tags__slug=self.kwargs.get("slug"), public=True)

class VideoSearchListAPIView(generics.ListAPIView):
	serializer_class    = BlogVideoSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogVideo.objects.filter((Q(public=True) | Q(created_by=self.request.user)), Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")))
		else:
			return BlogVideo.objects.filter(Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")), public=True)

class ImageCategoryPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogImageSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogImage.objects.filter((Q(public=True) | Q(created_by=self.request.user)), category__slug=self.kwargs.get("slug"))
		else:
			return BlogImage.objects.filter(category__slug=self.kwargs.get("slug"), public=True)

class ImageTagPostListAPIView(generics.ListAPIView):
	serializer_class    = BlogImageSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogImage.objects.filter((Q(public=True) | Q(created_by=self.request.user)), tags__slug=self.kwargs.get("slug"))
		else:
			return BlogImage.objects.filter(tags__slug=self.kwargs.get("slug"), public=True)

class ImageSearchListAPIView(generics.ListAPIView):
	serializer_class    = BlogImageSerializer
	permission_classes 	= [IsReadOnly]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		user = None
		if self.request.user.is_authenticated:
			return BlogImage.objects.filter((Q(public=True) | Q(created_by=self.request.user)), Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search") | Q(caption__icontains=self.kwargs.get("search"))))
		else:
			return BlogImage.objects.filter(Q(title__icontains=self.kwargs.get("search")) | Q(description__icontains=self.kwargs.get("search")) | Q(caption__icontains=self.kwargs.get("search")), public=True)

class SocialMediaLinksAPIView(generics.ListAPIView):
	serializer_class    = BlogLinkSerializer
	permission_classes 	= [IsReadOnly]

	def get_queryset(self, *args, **kwargs):
		return BlogLink.objects.filter(social_media=True)






# DASHBOARD
class DashboardPostsAPIView(generics.ListAPIView):
	serializer_class    = DashboardPostSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogPost.objects.filter(created_by=self.request.user)

class DashboardPostsAllAPIView(generics.ListAPIView):
	serializer_class    = DashboardPostSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogPost.objects.filter(created_by=self.request.user)

class DashboardPhotoAlbumsAPIView(generics.ListAPIView):
	serializer_class 	= DashboardPhotoAlbumSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogPhotoAlbum.objects.filter(created_by=self.request.user)

class DashboardPhotoAlbumsAllAPIView(generics.ListAPIView):
	serializer_class 	= DashboardPhotoAlbumSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogPhotoAlbum.objects.filter(created_by=self.request.user)

class DashboardMusicAlbumsAPIView(generics.ListAPIView):
	serializer_class 	= DashboardMusicAlbumSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogMusicAlbum.objects.filter(created_by=self.request.user)

class DashboardMusicAlbumsAllAPIView(generics.ListAPIView):
	serializer_class 	= DashboardMusicAlbumSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogMusicAlbum.objects.filter(created_by=self.request.user)

class DashboardVideosAPIView(generics.ListAPIView):
	serializer_class 	= DashboardVideoSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogVideo.objects.filter(created_by=self.request.user)

class DashboardVideosAllAPIView(generics.ListAPIView):
	serializer_class 	= DashboardVideoSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogVideo.objects.filter(created_by=self.request.user)

class DashboardImagesAPIView(generics.ListAPIView):
	serializer_class 	= DashboardImageSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogImage.objects.filter(created_by=self.request.user)

class DashboardImagesAllAPIView(generics.ListAPIView):
	serializer_class 	= DashboardImageSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogImage.objects.filter(created_by=self.request.user)

class DashboardLinksAPIView(generics.ListAPIView):
	serializer_class 	= DashboardLinkSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogLink.objects.filter(created_by=self.request.user)

class DashboardCategoriesAPIView(generics.ListAPIView):
	serializer_class 	= DashboardCategorySerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogCategory.objects.filter(created_by=self.request.user)

class DashboardTagsAPIView(generics.ListAPIView):
	serializer_class 	= DashboardTagSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogTag.objects.filter(created_by=self.request.user)










# CREATE EDIT DELETE
class PostsCreateAPIView(generics.ListCreateAPIView):
	serializer_class    = DashboardPostSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogPost.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		if data['image_highlight'] != '':
			image_highlight = BlogImage.objects.filter(slug=data['image_highlight'].replace("'",''))[0]
		else:
			image_highlight = None
		if data['video_highlight'] != '':
			video_highlight = BlogVideo.objects.filter(slug=data['video_highlight'].replace("'",''))[0]
		else:
			video_highlight = None
		if data['photo_album'] != '':
			photo_album = BlogPhotoAlbum.objects.filter(slug=data['photo_album'].replace("'",''))[0]
		else:
			photo_album = None
		if data['music_album'] != '':
			music_album = BlogMusicAlbum.objects.filter(slug=data['music_album'].replace("'",''))[0]
		else:
			music_album = None

		videos = []
		if data['videos'] != '':
			for video in data['videos'].replace('[','').replace("'",'').replace(']','').split(','):
				videos.append(BlogVideo.objects.filter(slug=video)[0])
		images = []
		if data['images'] != '':
			for image in data['images'].replace('[','').replace("'",'').replace(']','').split(','):
				images.append(BlogImage.objects.filter(slug=image)[0])
		links = []
		if data['links'] != '':
			for link in data['links'].replace('[','').replace("'",'').replace(']','').split(','):
				links.append(BlogLink.objects.filter(slug=link)[0])

		if data['category'] != '':
			category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		else:
			category = None
		tags = []
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				tags.append(BlogTag.objects.filter(slug=tag)[0])

		serializer = DashboardPostSerializer(data=data, context={'request': request, 'tags': tags, 'category': category, 'image': data['image'], 'images': images, 'videos': videos, 'links': links, 'image_highlight': image_highlight, 'video_highlight': video_highlight, 'photo_album': photo_album, 'music_album': music_album})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PostEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogPost.objects.all()
	serializer_class    = DashboardPostSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogPost.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogPost.objects.get(slug=self.kwargs.get("slug"))
		instance.title = self.request.data.get('title')
		instance.body = self.request.data.get('body')

		if data['image'] != '':
			instance.image = data['image']

		if data['image_highlight'] != '':
			instance.image_highlight = BlogImage.objects.filter(slug=data['image_highlight'].replace("'",''))[0]
		else:
			instance.image_highlight = None
		if data['video_highlight'] != '':
			instance.video_highlight = BlogVideo.objects.filter(slug=data['video_highlight'].replace("'",''))[0]
		else:
			instance.video_highlight = None
		if data['photo_album'] != '':
			instance.photo_album = BlogPhotoAlbum.objects.filter(slug=data['photo_album'].replace("'",''))[0]
		else:
			instance.photo_album = None
		if data['music_album'] != '':
			instance.music_album = BlogMusicAlbum.objects.filter(slug=data['music_album'].replace("'",''))[0]
		else:
			instance.music_album = None

		instance.videos.clear()
		if data['videos'] != '':
			for video in data['videos'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.videos.add(BlogVideo.objects.filter(slug=video)[0])
		instance.image_gallery.clear()
		if data['images'] != '':
			for image in data['images'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.image_gallery.add(BlogImage.objects.filter(slug=image)[0])
		instance.links.clear()
		if data['links'] != '':
			for link in data['links'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.links.add(BlogLink.objects.filter(slug=link)[0])

		instance.blog = False
		if data['blog'] == 'true':
			instance.blog = True

		instance.blog_sticky = False
		if data['blog_sticky'] == 'true':
			instance.blog_sticky = True

		instance.portfolio = False
		if data['portfolio'] == 'true':
			instance.portfolio = True

		instance.portfolio_sticky = False
		if data['portfolio_sticky'] == 'true':
			instance.portfolio_sticky = True

		instance.published = False
		if data['published'] == 'true':
			instance.published = True

		instance.category = None
		if data['category'] != '':
			instance.category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		instance.tags.clear()
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.tags.add(BlogTag.objects.filter(slug=tag)[0])
		instance.save()

		serializer = DashboardPostSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

class PhotoAlbumsCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardPhotoAlbumSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogPhotoAlbum.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		if data['category'] != '':
			category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		else:
			category = None
		tags = []
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				tags.append(BlogTag.objects.filter(slug=tag)[0])

		serializer = DashboardPhotoAlbumSerializer(data=data, context={'request': request, 'tags': tags, 'category': category})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PhotoAlbumEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogPhotoAlbum.objects.all()
	serializer_class    = DashboardPhotoAlbumSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogPhotoAlbum.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogPhotoAlbum.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.title = self.request.data.get('title')
		instance.description = self.request.data.get('description')
		instance.public = False
		if data['public'] == 'true':
			instance.public = True
		instance.category = None
		if data['category'] != '':
			instance.category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		instance.tags.clear()
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.tags.add(BlogTag.objects.filter(slug=tag)[0])
		instance.save()

		serializer = DashboardPhotoAlbumSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

class PhotosCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardPhotoAlbumPhotoSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogPhotoAlbumPhoto.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy() 

		serializer = DashboardPhotoAlbumPhotoSerializer(data=data, context={'request': request, 'album-slug': data['album-slug'], 'image': data['image']})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PhotoEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogPhotoAlbumPhoto.objects.all()
	serializer_class    = DashboardPhotoAlbumPhotoSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogPhotoAlbumPhoto.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogPhotoAlbumPhoto.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.title = self.request.data.get('title')
		if data['image'] != '':
			instance.image = data['image']
		instance.save()

		serializer = DashboardPhotoAlbumPhotoSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

class MusicAlbumsCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardMusicAlbumSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogMusicAlbum.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		if data['category'] != '':
			category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		else:
			category = None
		tags = []
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				tags.append(BlogTag.objects.filter(slug=tag)[0])

		serializer = DashboardMusicAlbumSerializer(data=data, context={'request': request, 'tags': tags, 'category': category, 'image': data['image']})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class MusicAlbumEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogMusicAlbum.objects.all()
	serializer_class    = DashboardMusicAlbumSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogMusicAlbum.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogMusicAlbum.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.band_name = data['band_name']
		instance.title = data['title']
		instance.description = data['description']
		if data['image'] != '':
			instance.image = data['image']
		instance.public = False
		if data['public'] == 'true':
			instance.public = True
		instance.category = None
		if data['category'] != '':
			instance.category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		instance.tags.clear()
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.tags.add(BlogTag.objects.filter(slug=tag)[0])
		instance.save()

		serializer = DashboardMusicAlbumSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

class TracksCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardMusicAlbumSongSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogMusicAlbumSong.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		serializer = DashboardMusicAlbumSongSerializer(data=data, context={'request': request, 'audio_file': data['audio_file'], 'image': data['image'], 'album-slug': data['album-slug']})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class TrackEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogMusicAlbumSong.objects.all()
	serializer_class    = DashboardMusicAlbumSongSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogMusicAlbumSong.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogMusicAlbumSong.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.title = self.request.data.get('title')
		instance.description = self.request.data.get('description')
		if data['image'] != '':
			instance.image = data['image']
		instance.youtube_url = self.request.data.get('youtube_url')
		instance.soundcloud_embed = self.request.data.get('soundcloud_embed')
		if data['audio_file'] != '':
			instance.audio_file = data['audio_file']
		instance.save()

		serializer = DashboardMusicAlbumSongSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

class VideosCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardVideoSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogVideo.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		if data['category'] != '':
			category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		else:
			category = None
		tags = []
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				tags.append(BlogTag.objects.filter(slug=tag)[0])

		serializer = DashboardVideoSerializer(data=data, context={'request': request, 'tags': tags, 'category': category, 'image': data['image']})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class VideoEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogVideo.objects.all()
	serializer_class    = DashboardVideoSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogVideo.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogVideo.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.title = self.request.data.get('title')
		instance.description = self.request.data.get('description')
		instance.youtube_id = self.request.data.get('youtube_id')
		instance.vimeo_id = self.request.data.get('vimeo_id')
		instance.public = False
		if data['public'] == 'true':
			instance.public = True
		if data['image'] != '':
			instance.image = data['image']
		instance.category = None
		if data['category'] != '':
			instance.category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		instance.tags.clear()
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.tags.add(BlogTag.objects.filter(slug=tag)[0])
		instance.save()

		serializer = DashboardVideoSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

class ImagesCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardImageSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogImage.objects.filter(created_by=self.request.user)

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		if data['category'] != '':
			category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		else:
			category = None
		tags = []
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				tags.append(BlogTag.objects.filter(slug=tag)[0])

		serializer = DashboardImageSerializer(data=data, context={'request': request, 'tags': tags, 'category': category, 'image': data['image']})
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ImageEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogImage.objects.all()
	serializer_class    = DashboardImageSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogImage.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogImage.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.title = self.request.data.get('title')
		instance.description = self.request.data.get('description')
		instance.caption = self.request.data.get('caption')
		instance.public = False
		if data['public'] == 'true':
			instance.public = True
		if data['image'] != '':
			instance.image = data['image']
		instance.category = None
		if data['category'] != '':
			instance.category = BlogCategory.objects.filter(slug=data['category'].replace("'",''))[0]
		instance.tags.clear()
		if data['tags'] != '':
			for tag in data['tags'].replace('[','').replace("'",'').replace(']','').split(','):
				instance.tags.add(BlogTag.objects.filter(slug=tag)[0])
		instance.save()

		serializer = DashboardImageSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

	def perform_update(self, serializer):
		serializer.save()

	def partial_update(self, request, *args, **kwargs):
		kwargs['partial'] = True
		return self.update(request, *args, **kwargs)

class LinksCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardLinkSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogLink.objects.filter(created_by=self.request.user)

	def perform_create(self, serializer):
		serializer.save(created_by=self.request.user)

class LinkEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogLink.objects.all()
	serializer_class    = DashboardLinkSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogLink.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogLink.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.description = self.request.data.get("description")
		instance.link = self.request.data.get("link")
		instance.social_media = False
		if data['social_media'] == "true":
			instance.social_media = True
		instance.save()

		serializer = DashboardLinkSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

	def perform_update(self, serializer):
		serializer.save()

	def partial_update(self, request, *args, **kwargs):
		kwargs['partial'] = True
		return self.update(request, *args, **kwargs)

class CategoriesCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardCategorySerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogCategory.objects.filter(created_by=self.request.user)

	def perform_create(self, serializer):
		serializer.save(created_by=self.request.user)

class CategoryEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogCategory.objects.all()
	serializer_class    = DashboardCategorySerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogCategory.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogCategory.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.category = self.request.data.get("category")
		instance.save()

		serializer = DashboardCategorySerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

	def perform_update(self, serializer):
		serializer.save()

	def partial_update(self, request, *args, **kwargs):
		kwargs['partial'] = True
		return self.update(request, *args, **kwargs)

class TagsCreateAPIView(generics.ListCreateAPIView):
	serializer_class 	= DashboardTagSerializer
	permission_classes 	= [IsOwner]

	def get_queryset(self, *args, **kwargs):
		return BlogTag.objects.filter(created_by=self.request.user)

	def perform_create(self, serializer):
		serializer.save(created_by=self.request.user)

class TagEditDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset            = BlogTag.objects.all()
	serializer_class    = DashboardTagSerializer
	lookup_field        = 'slug'
	permission_classes  = [IsOwner]

	def get_object(self):
		return BlogTag.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

	def update(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		partial = kwargs.pop('partial', False)
		instance = BlogTag.objects.get(slug=self.kwargs.get("slug"), created_by=self.request.user)
		instance.tag = self.request.data.get("tag")
		instance.save()

		serializer = DashboardTagSerializer(instance, data=data, partial=partial, context={'request': request})
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(instance, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			instance._prefetched_objects_cache = {}

		return Response(serializer.data)

	def perform_update(self, serializer):
		serializer.save()

	def partial_update(self, request, *args, **kwargs):
		kwargs['partial'] = True
		return self.update(request, *args, **kwargs)








class PostsImportAPIView(generics.ListCreateAPIView):
	serializer_class    = DashboardPostSerializer
	permission_classes 	= [IsOwner]
	pagination_class 	= BlogPostPagination

	def get_queryset(self, *args, **kwargs):
		return BlogPost.objects.all()

	def post(self, request, *args, **kwargs):
		if request.FILES:
			data = request.data
		else:
			data = request.data.copy()

		csv_file = TextIOWrapper(data['csv'].file, encoding='ascii', errors='replace')
		import_csv = csv.reader(csv_file)
		next(import_csv)
		counter = 0
		for line in import_csv:
			if line:
				data['title'] = line[0]
				data['body'] = line[1]
				# date_field 	= line[2].split('+')
				# data['created_at'] 	= datetime.datetime.strptime(date_field[0], '%Y-%m-%d %H:%M:%S.%f')
				data['published'] = line[3]

				serializer	= DashboardPostImportSerializer(data=data, context={'request': request})
				if serializer.is_valid(raise_exception=True):
					counter += 1
					serializer.save()

		if serializer.is_valid(raise_exception=True):
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




		# 		counter += 1
		# 		import_data = BlogPost()
		# 		import_data.title = line[0]
		# 		import_data.body = line[1]
		# 		import_data.created_by = self.request.user
		# 		created_at = line[2].split('+')
		# 		import_data.created_at = datetime.datetime.strptime(created_at[0], '%Y-%m-%d %H:%M:%S.%f')
		# 		import_data.published = line[3]
		# 		import_data.save()

		# return BlogPost.objects.filter(created_by=self.request.user)[:counter]

		# serializer = DashboardPostSerializer(data=data, context={'request': request})
		# serializer = DashboardPostSerializer(data=data, context={'request': request, 'tags': tags, 'category': category, 'image': data['image'], 'images': images, 'videos': videos, 'links': links, 'image_highlight': image_highlight, 'video_highlight': video_highlight, 'photo_album': photo_album, 'music_album': music_album})
		# if serializer.is_valid(raise_exception=True):
		# 	serializer.save()
		# 	return Response(serializer.data, status=status.HTTP_201_CREATED)
		# return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)







