import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import Alerts from './components/common/Alerts';
import { Provider } from 'react-redux';
import store from './store';
import { styles } from './Styles';

import NavBar from './components/NavBar';
import Landing from './components/Landing';
import Blog from './components/blog/Blog';
import Portfolio from './components/portfolio/Portfolio';
import Categories from './components/category/Categories';
import Tags from './components/tag/Tags';
import AboutMe from './components/AboutMe';
import Contact from './components/Contact';
import Videos from './components/videos/Videos';
import Login from './components/admin/Login';
import Admin from './components/admin/Admin';
import Photos from './components/photos/Photos';
import Music from './components/music/Music';
import Images from './components/images/Images';

import { loadUser } from './actions/admin';
import { getBlogSettings } from './actions/common';

const alertOptions = {
  timeout: 3000,
  position: 'top center'
};

class App extends Component {
  constructor() {
    super();
    this.state = {
      showLanding: true, 
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false,
      aboutMeDescription: '',
      aboutMeImage: null,
      showCreator: false
    }
  }

  handleShowCreator = (showCreator) => {
    this.setState({showCreator: showCreator});
  }

  handleGetAboutMeImage = (aboutMeImage) => {
    this.setState({aboutMeImage: aboutMeImage});
  }

  handleGetAboutMeDescription = (aboutMeDescription) => {
    this.setState({aboutMeDescription: aboutMeDescription});
  }

  handleShowBlog = () => {
    this.setState({
      showBlog: true,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowPortfolio = () => {
    this.setState({
      showBlog: false,
      showPortfolio: true,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowCategories = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: true, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowTags = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: true, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowAboutMe = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: true, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowContact = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: true,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowVideos = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: true,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowLogin = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: true,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowMusic = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: true,
      showPhotos: false,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowPhotos = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: true,
      showImages: false,
      showAdmin: false
    });
  }

  handleShowImages = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: true,
      showAdmin: false
    });
  }

  handleShowAdmin = () => {
    this.setState({
      showBlog: false,
      showPortfolio: false,
      showCategories: false, 
      showTags: false, 
      showAboutMe: false, 
      showContact: false,
      showVideos: false,
      showLogin: false,
      showMusic: false,
      showPhotos: false,
      showImages: false,
      showAdmin: true
    });
  }

  componentDidMount() {
    store.dispatch(loadUser());
    store.dispatch(getBlogSettings());
  }

  render() {
    const { showBlog, showPortfolio, showCategories, showTags, showAboutMe, showContact, showVideos, showLogin, showMusic, showPhotos, showImages, showAdmin, aboutMeDescription, aboutMeImage, showCreator } = this.state;

    return (
      <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...alertOptions}>
          <div style={styles.appContainer}>
            <Alerts />
            <NavBar viewBlog={showBlog} showBlog={this.handleShowBlog} showPortfolio={this.handleShowPortfolio} showCategories={this.handleShowCategories} showTags={this.handleShowTags} showAboutMe={this.handleShowAboutMe} showContact={this.handleShowContact} showVideos={this.handleShowVideos} showLogin={this.handleShowLogin} showMusic={this.handleShowMusic} showPhotos={this.handleShowPhotos} showImages={this.handleShowImages} showAdmin={this.handleShowAdmin} aboutMeDescription={this.handleGetAboutMeDescription} aboutMeImage={this.handleGetAboutMeImage} showCreator={this.handleShowCreator} />
            <div />
            {showBlog && 
              <Blog showCreator={showCreator} />
            }
            {showPortfolio && 
              <Portfolio />
            }
            {showCategories && 
              <Categories />
            }
            {showTags && 
              <Tags />
            }
            {showAboutMe && 
              <AboutMe aboutMeImage={aboutMeImage} aboutMeDescription={aboutMeDescription} />
            }
            {showContact && 
              <Contact />
            }
            {showVideos && 
              <Videos />
            }
            {showLogin && 
              <Login showAdmin={this.handleShowAdmin} />
            }
            {showMusic && 
              <Music />
            }
            {showPhotos && 
              <Photos />
            }
            {showImages && 
              <Images />
            }
            {showAdmin && 
              <Admin showBlog={this.handleShowBlog} />
            }
          </div>
        </AlertProvider>
      </Provider>
     );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));

