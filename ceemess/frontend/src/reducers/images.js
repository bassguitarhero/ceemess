import { 
	GET_IMAGES,
	LOADING_IMAGES,
	GET_MORE_IMAGES,
	LOADING_MORE_IMAGES,
	CLEAR_IMAGES,

	GET_CATEGORY_IMAGES,
	GET_MORE_CATEGORY_IMAGES,
	LOADING_CATEGORY_IMAGES,
	CLEAR_CATEGORY_IMAGES,

	GET_TAG_IMAGES,
	GET_MORE_TAG_IMAGES,
	LOADING_TAG_IMAGES,
	CLEAR_TAG_IMAGES,

	GET_SEARCH_IMAGES,
	GET_MORE_SEARCH_IMAGES,
	LOADING_SEARCH_IMAGES,
	CLEAR_SEARCH_IMAGES
} from '../actions/types';

const initialState = {
	images: null,
	loadingImages: true,
	imagesNext: null,

	categoryImages: null,
	loadingCategoryImages: true,
	categoryImagesNext: null,

	tagImages: null,
	loadingTagImages: true,
	tagImagesNext: null,

	searchImages: null,
	loadingSearchImages: true,
	searchImagesNext: null
}

export default function(state=initialState, action) {
	switch(action.type) {

		case 'GET_IMAGES':
			return {
				...state,
				images: action.payload.results,
				imagesNext: action.payload.next
			};

		case 'LOADING_IMAGES':
			return {
				...state,
				loadingImages: action.payload
			};

		case 'GET_MORE_IMAGES':
			return {
				...state,
				images: [...state.images, ...action.payload.results],
				imagesNext: action.payload.next
			};

		case 'CLEAR_IMAGES':
			return {
				...state,
				images: null,
				loadingImages: true,
				imagesNext: null
			}




		case GET_CATEGORY_IMAGES:
			return {
				...state,
				categoryImages: action.payload.results,
				categoryImagesNext: action.payload.next
			};

		case GET_MORE_CATEGORY_IMAGES:
			return {
				...state,
				categoryImages: [...state.categoryImages, ...action.payload.results],
				categoryImagesNext: action.payload.next
			};

		case LOADING_CATEGORY_IMAGES:
			return {
				...state,
				loadingCategoryImages: action.payload
			};

		case CLEAR_CATEGORY_IMAGES:
			return {
				...state,
				categoryImages: null,
				loadingCategoryImages: true,
				categoryImagesNext: null
			};




		case GET_TAG_IMAGES:
			return {
				...state,
				tagImages: action.payload.results,
				tagImagesNext: action.payload.next
			};

		case GET_MORE_TAG_IMAGES:
			return {
				...state,
				tagImages: [...state.tagImages, ...action.payload.results],
				tagImagesNext: action.payload.next
			};

		case LOADING_TAG_IMAGES:
			return {
				...state,
				loadingTagImages: action.payload
			};

		case CLEAR_TAG_IMAGES:
			return {
				...state,
				tagImages: null,
				loadingTagImages: true,
				tagImagesNext: null
			};




		case GET_SEARCH_IMAGES:
			return {
				...state,
				searchImages: action.payload.results,
				searchImagesNext: action.payload.next
			};

		case GET_MORE_SEARCH_IMAGES:
			return {
				...state,
				searchImages: [...state.searchImages, ...action.payload.results],
				searchImagesNext: action.payload.next
			};

		case LOADING_SEARCH_IMAGES:
			return {
				...state,
				loadingSearchImages: action.payload
			};

		case CLEAR_SEARCH_IMAGES:
			return {
				...state,
				searchImages: null,
				loadingSearchImages: true,
				searchImagesNext: null
			};

	default:
			return state;
	}
}
