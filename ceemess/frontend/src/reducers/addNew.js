import { 
	CREATE_NEW_POST,
	CREATE_NEW_PHOTO_ALBUM,
	CREATE_NEW_PHOTO,
	CREATE_NEW_MUSIC_ALBUM,
	CREATE_NEW_TRACK, 
	CREATE_NEW_VIDEO,
	CREATE_NEW_IMAGE,
	CREATE_NEW_LINK,
	CREATE_NEW_CATEGORY,
	CREATE_NEW_TAG,

	NEW_POST_FAIL,
	NEW_PHOTO_ALBUM_FAIL,
	NEW_PHOTO_FAIL,
	NEW_MUSIC_ALBUM_FAIL,
	NEW_TRACK_FAIL,
	NEW_VIDEO_FAIL,
	NEW_IMAGE_FAIL,
	NEW_LINK_FAIL,
	NEW_CATEGORY_FAIL,
	NEW_TAG_FAIL
} from '../actions/types';

const initialState = {
	newPost: null,
	newPhotoAlbum: null,
	newPhoto: null,
	newMusicAlbum: null,
	newTrack: null,
	newVideo: null,
	newImage: null,
	newLink: null,
	newCategory: null,
	newTag: null,

	newPostFail: null,
	newPhotoAlbumFail: null,
	newPhotoFail: null,
	newMusicAlbumFail: null,
	newTrackFail: null,
	newVideoFail: null,
	newImageFail: null,
	newLinkFail: null,
	newCategoryFail: null,
	newTagFail: null
}

export default function(state=initialState, action) {
	switch(action.type) {
		case CREATE_NEW_POST:
			return {
				...state,
				newPost: action.payload
			};

		case CREATE_NEW_PHOTO_ALBUM:
			return {
				...state,
				newPhotoAlbum: action.payload
			};

		case CREATE_NEW_PHOTO:
			return {
				...state,
				newPhoto: action.payload
			};

		case CREATE_NEW_MUSIC_ALBUM:
			return {
				...state,
				newMusicAlbum: action.payload
			};

		case CREATE_NEW_TRACK:
			return {
				...state,
				newTrack: action.payload
			};

		case CREATE_NEW_VIDEO:
			return {
				...state,
				newVideo: action.payload
			};

		case CREATE_NEW_IMAGE:
			return {
				...state,
				newImage: action.payload
			};

		case CREATE_NEW_LINK:
			return {
				...state,
				newLink: action.payload
			};

			case CREATE_NEW_CATEGORY:
			return {
				...state,
				newCategory: action.payload
			};

		case CREATE_NEW_TAG:
			return {
				...state,
				newTag: action.payload
			};






		case NEW_POST_FAIL:
			return {
				...state,
				newPostFail: action.payload
			};

		case NEW_PHOTO_ALBUM_FAIL:
			return {
				...state,
				newPhotoAlbumFail: action.payload
			};

		case NEW_PHOTO_FAIL:
			return {
				...state,
				newPhotoFail: action.payload
			};

		case NEW_MUSIC_ALBUM_FAIL:
			return {
				...state,
				newMusicAlbumFail: action.payload
			};

		case NEW_TRACK_FAIL:
			return {
				...state,
				newTrackFail: action.payload
			};

		case NEW_VIDEO_FAIL:
			return {
				...state,
				newVideoFail: action.payload
			};

		case NEW_IMAGE_FAIL:
			return {
				...state,
				newImageFail: action.payload
			};

		case NEW_LINK_FAIL:
			return {
				...state,
				newLinkFail: action.payload
			};

		case NEW_CATEGORY_FAIL:
			return {
				...state,
				newCategoryFail: action.payload
			};

		case NEW_TAG_FAIL:
			return {
				...state,
				newTagFail: action.payload
			};

	default:
			return state;
	}
}
