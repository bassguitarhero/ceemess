import { 
	USER_LOADING, 
	AUTH_ERROR, 
	USER_LOADED, 
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	LOGOUT_SUCCESS,
  PASSWORD_RESET_REQUEST,
  GET_USERS,
  LOADING_USERS,
  CREATE_NEW_USER,
  NO_USER
} from '../actions/types';

const initialState = {
	token: localStorage.getItem("token"),
  isAuthenticated: false,
  isLoading: false,
  user: null,
  passwordResetReceived: null,
  users: null,
  loadingUsers: true,
  newUser: null
}

export default function(state=initialState, action) {
	switch(action.type) {
		case USER_LOADING: 
			return {
        ...state,
        isLoading: true
      };

    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false,
        user: action.payload
      };

		case LOGIN_SUCCESS: 
			localStorage.setItem("token", action.payload.token);
			return {
				...state,
				...action.payload,
        isAuthenticated: true,
        isLoading: false
			};

		case AUTH_ERROR:
    case LOGIN_FAIL:
    case LOGOUT_SUCCESS:
      localStorage.removeItem("token");
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        isLoading: false
      };

    case PASSWORD_RESET_REQUEST:
      return {
        ...state,
        passwordResetReceived: action.payload
      };

    case GET_USERS: 
      return {
        ...state,
        users: action.payload
      };

    case LOADING_USERS:
      return {
        ...state,
        loadingUsers: action.payload
      };

    case CREATE_NEW_USER:
      return {
        ...state,
        newUser: action.payload
      };

    case NO_USER: 
      return {
        ...state,
        isAuthenticated: false,
        isLoading: false
      }

		default:
			return state;
	}
}