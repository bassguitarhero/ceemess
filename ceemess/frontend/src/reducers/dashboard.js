import { 
	GET_DASHBOARD_SETTINGS,
	LOADING_DASHBOARD_SETTINGS,
	EDIT_DASHBOARD_SETTINGS, 

	GET_DASHBOARD_POSTS,
	GET_DASHBOARD_POSTS_NEXT,
	LOADING_DASHBOARD_POSTS,
	GET_MORE_DASHBOARD_POSTS,
	GET_DASHBOARD_POSTS_ALL, 
	LOADING_DASHBOARD_POSTS_ALL,

	GET_DASHBOARD_PHOTO_ALBUMS,
	GET_DASHBOARD_PHOTO_ALBUMS_NEXT,
	LOADING_DASHBOARD_PHOTO_ALBUMS,
	GET_MORE_DASHBOARD_PHOTO_ALBUMS,
	GET_DASHBOARD_PHOTO_ALBUMS_ALL,
	LOADING_DASHBOARD_PHOTO_ALBUMS_ALL,

	GET_DASHBOARD_MUSIC_ALBUMS,
	GET_DASHBOARD_MUSIC_ALBUMS_NEXT,
	LOADING_DASHBOARD_MUSIC_ALBUMS,
	GET_MORE_DASHBOARD_MUSIC_ALBUMS,
	GET_DASHBOARD_MUSIC_ALBUMS_ALL,
	LOADING_DASHBOARD_MUSIC_ALBUMS_ALL,

	GET_DASHBOARD_VIDEOS,
	GET_DASHBOARD_VIDEOS_NEXT,
	LOADING_DASHBOARD_VIDEOS,
	GET_MORE_DASHBOARD_VIDEOS,
	GET_DASHBOARD_VIDEOS_ALL,
	LOADING_DASHBOARD_VIDEOS_ALL,

	GET_DASHBOARD_IMAGES,
	GET_DASHBOARD_IMAGES_NEXT,
	LOADING_DASHBOARD_IMAGES,
	GET_MORE_DASHBOARD_IMAGES,
	GET_DASHBOARD_IMAGES_ALL,
	LOADING_DASHBOARD_IMAGES_ALL,

	GET_DASHBOARD_CATEGORIES,
	LOADING_DASHBOARD_CATEGORIES,

	GET_DASHBOARD_TAGS,
	LOADING_DASHBOARD_TAGS,

	GET_DASHBOARD_LINKS,
	LOADING_DASHBOARD_LINKS,

	EDIT_POST,
	DELETE_POST,

	EDIT_PHOTO_ALBUM,
	DELETE_PHOTO_ALBUM,
	UPLOAD_PHOTO,
	EDIT_PHOTO,
	DELETE_PHOTO,

	EDIT_MUSIC_ALBUM,
	DELETE_MUSIC_ALBUM,
	UPLOAD_SONG,
	EDIT_SONG,
	DELETE_SONG,

	EDIT_VIDEO,
	DELETE_VIDEO,

	EDIT_IMAGE,
	DELETE_IMAGE,

	EDIT_LINK,
	DELETE_LINK,

	EDIT_CATEGORY,
	DELETE_CATEGORY,

	EDIT_TAG,
	DELETE_TAG,

	CHANGE_PASSWORD,
	PASSWORD_ERROR,

	UPLOAD_CSV,

	EDIT_POST_FAIL,
	DELETE_POST_FAIL,

	EDIT_PHOTO_ALBUM_FAIL,
	DELETE_PHOTO_ALBUM_FAIL,

	UPLOAD_PHOTO_FAIL,
	EDIT_PHOTO_FAIL,
	DELETE_PHOTO_FAIL,

	EDIT_MUSIC_ALBUM_FAIL,
	DELETE_MUSIC_ALBUM_FAIL,

	UPLOAD_TRACK_FAIL,
	EDIT_SONG_FAIL,
	DELETE_SONG_FAIL,

	EDIT_VIDEO_FAIL,
	DELETE_VIDEO_FAIL,

	EDIT_IMAGE_FAIL,
	DELETE_IMAGE_FAIL,

	EDIT_LINK_FAIL,
	DELETE_LINK_FAIL,

	EDIT_CATEGORY_FAIL,
	DELETE_CATEGORY_FAIL,

	EDIT_TAG_FAIL,
	DELETE_TAG_FAIL
} from '../actions/types';

const initialState = {
	dashboardSettings: null,
	loadingDashboardSettings: true,
	editedDashboardSettings: null,

	dashboardBlogPosts: null,
	loadingDashboardBlogPosts: true,
	dashboardBlogPostsNext: null,
	dashboardBlogPostsAll: null,
	loadingDashboardBlogPostsAll: true,

	dashboardPhotoAlbums: null,
	dashboardPhotoAlbumsNext: null,
	loadingDashboardPhotoAlbums: true,
	dashboardPhotoAlbumsAll: null,
	loadingDashboardPhotoAlbumsAll: true,

	dashboardMusicAlbums: null,
	dashboardMusicAlbumsNext: null,
	loadingDashboardMusicAlbums: true,
	dashboardMusicAlbumsAll: null,
	loadingDashboardMusicAlbumsAll: true,

	dashboardVideos: null,
	dashboardVideosNext: null,
	loadingDashboardVideos: true,
	dashboardVideosAll: null,
	loadingDashboardVideosAll: true,

	dashboardImages: null,
	dashboardImagesNext: null,
	loadingDashboardImages: true,
	dashboardImagesAll: null,
	loadingDashboardImagesAll: true,

	dashboardCategories: null,
	loadingDashboardCategories: true,

	dashboardTags: null,
	loadingDashboardTags: true,

	dashboardLinks: null,
	loadingDashboardLinks: true,

	editedPost: null,
	deletedPost: null,

	editedPhotoAlbum: null,
	deletedPhotoAlbum: null,
	uploadedPhoto: null,
	editedPhoto: null,
	deletedPhoto: null,

	editedMusicAlbum: null,
	deletedMusicAlbum: null,
	uploadedSong: null,
	editedSong: null,
	deletedSong: null,

	editedVideo: null,
	deletedVideo: null,

	editedImage: null,
	deletedImage: null,

	editedLink: null,
	deletedLink: null,

	editedCategory: null,
	deletedCategory: null,

	editedTag: null,
	deletedTag: null,

	passwordChange: null,
	passwordError: null,

	csvUploaded: null,

	editedPostFail: null,
	deletedPostFail: null,

	editedPhotoAlbumFail: null,
	deletedPhotoAlbumFail: null,
	uploadedPhotoFail: null,
	editedPhotoFail: null,
	deletedPhotoFail: null,

	editedMusicAlbumFail: null,
	deletedMusicAlbumFail: null,
	uploadedSongFail: null,
	editedSongFail: null,
	deletedSongFail: null,

	editedVideoFail: null,
	deletedVideoFail: null,

	editedImageFail: null,
	deletedImageFail: null,

	editedLinkFail: null,
	deletedLinkFail: null,

	editedCategoryFail: null,
	deletedCategoryFail: null,

	editedTagFail: null,
	deletedTagFail: null,
}

export default function(state=initialState, action) {
	switch(action.type) {
		case GET_DASHBOARD_SETTINGS: 
			return {
				...state,
				dashboardSettings: action.payload
			};

		case LOADING_DASHBOARD_SETTINGS:
			return {
				...state,
				loadingDashboardSettings: action.payload
			};

		case EDIT_DASHBOARD_SETTINGS:
			return {
				...state,
				editedDashboardSettings: action.payload
			};

		case GET_DASHBOARD_POSTS:
			return {
				...state,
				dashboardBlogPosts: action.payload
			};

		case GET_DASHBOARD_POSTS_NEXT:
			return {
				...state,
				dashboardBlogPostsNext: action.payload
			};

		case LOADING_DASHBOARD_POSTS:
			return {
				...state,
				loadingDashboardBlogPosts: action.payload
			};

		case GET_MORE_DASHBOARD_POSTS:
			return {
				...state,
				dashboardBlogPosts: [...state.dashboardBlogPosts, ...action.payload]
			};

		case GET_DASHBOARD_POSTS_ALL:
			return {
				...state,
				dashboardBlogPostsAll: action.payload
			};

		case LOADING_DASHBOARD_POSTS_ALL:
			return {
				...state,
				loadingDashboardBlogPostsAll: action.payload
			};

		case GET_DASHBOARD_PHOTO_ALBUMS:
			return {
				...state,
				dashboardPhotoAlbums: action.payload
			};

		case GET_DASHBOARD_PHOTO_ALBUMS_NEXT:
			return {
				...state,
				dashboardPhotoAlbumsNext: action.payload
			};

		case LOADING_DASHBOARD_PHOTO_ALBUMS:
			return {
				...state,
				loadingDashboardPhotoAlbums: action.payload
			};

		case GET_MORE_DASHBOARD_PHOTO_ALBUMS:
			return {
				...state,
				dashboardPhotoAlbums: [...state.dashboardPhotoAlbums, ...action.payload]
			};

		case GET_DASHBOARD_PHOTO_ALBUMS_ALL:
			return {
				...state,
				dashboardPhotoAlbumsAll: action.payload
			};

		case LOADING_DASHBOARD_PHOTO_ALBUMS_ALL:
			return {
				...state,
				loadingDashboardPhotoAlbumsAll: action.payload
			};

		case GET_DASHBOARD_MUSIC_ALBUMS:
			return {
				...state,
				dashboardMusicAlbums: action.payload
			};

		case GET_DASHBOARD_MUSIC_ALBUMS_NEXT:
			return {
				...state,
				dashboardMusicAlbumsNext: action.payload
			};

		case LOADING_DASHBOARD_MUSIC_ALBUMS:
			return {
				...state,
				loadingDashboardMusicAlbums: action.payload
			};

		case GET_MORE_DASHBOARD_MUSIC_ALBUMS:
			return {
				...state,
				dashboardMusicAlbums: [...state.dashboardMusicAlbums, ...action.payload]
			};

		case GET_DASHBOARD_MUSIC_ALBUMS_ALL:
			return {
				...state,
				dashboardMusicAlbumsAll: action.payload
			};

		case LOADING_DASHBOARD_MUSIC_ALBUMS_ALL:
			return {
				...state,
				loadingDashboardMusicAlbumsAll: action.payload
			};

		case GET_DASHBOARD_VIDEOS:
			return {
				...state,
				dashboardVideos: action.payload
			};

		case GET_DASHBOARD_VIDEOS_NEXT:
			return {
				...state,
				dashboardVideosNext: action.payload
			};

		case LOADING_DASHBOARD_VIDEOS:
			return {
				...state,
				loadingDashboardVideos: action.payload
			};

		case GET_MORE_DASHBOARD_VIDEOS:
			return {
				...state,
				dashboardVideos: [...state.dashboardVideos, ...action.payload]
			};

		case GET_DASHBOARD_VIDEOS_ALL:
			return {
				...state,
				dashboardVideosAll: action.payload
			};

		case LOADING_DASHBOARD_VIDEOS_ALL:
			return {
				...state,
				loadingDashboardVideosAll: action.payload
			};

		case GET_DASHBOARD_IMAGES:
			return {
				...state,
				dashboardImages: action.payload
			};

		case GET_DASHBOARD_IMAGES_NEXT:
			return {
				...state,
				dashboardImagesNext: action.payload
			};

		case LOADING_DASHBOARD_IMAGES:
			return {
				...state,
				loadingDashboardImages: action.payload
			};

		case GET_MORE_DASHBOARD_IMAGES:
			return {
				...state,
				dashboardImages: [...state.dashboardImages, ...action.payload]
			};

		case GET_DASHBOARD_IMAGES_ALL:
			return {
				...state,
				dashboardImagesAll: action.payload
			};

		case LOADING_DASHBOARD_IMAGES_ALL:
			return {
				...state,
				loadingDashboardImagesAll: action.payload
			};

		case GET_DASHBOARD_LINKS:
			return {
				...state,
				dashboardLinks: action.payload
			};

		case LOADING_DASHBOARD_LINKS:
			return {
				...state,
				loadingDashboardLinks: action.payload
			};

		case GET_DASHBOARD_CATEGORIES:
			return {
				...state,
				dashboardCategories: action.payload
			};

		case LOADING_DASHBOARD_CATEGORIES:
			return {
				...state,
				loadingDashboardCategories: action.payload
			};

		case GET_DASHBOARD_TAGS:
			return {
				...state,
				dashboardTags: action.payload
			};

		case LOADING_DASHBOARD_TAGS:
			return {
				...state,
				loadingDashboardTags: action.payload
			};




		case EDIT_POST:
			return {
				...state,
				editedPost: action.payload
			};

		case DELETE_POST:
			return {
				...state,
				deletedPost: action.payload,
			};

		case EDIT_PHOTO_ALBUM:
			return {
				...state,
				editedPhotoAlbum: action.payload
			};

		case DELETE_PHOTO_ALBUM:
			return {
				...state,
				deletedPhotoAlbum: action.payload,
			};

		case UPLOAD_PHOTO:
			return {
				...state,
				uploadedPhoto: action.payload
			};

		case EDIT_PHOTO:
			return {
				...state,
				editedPhoto: action.payload
			};

		case DELETE_PHOTO:
			return {
				...state,
				deletedPhoto: action.payload,
			};

		case EDIT_MUSIC_ALBUM:
			return {
				...state,
				editedMusicAlbum: action.payload
			};

		case DELETE_MUSIC_ALBUM:
			return {
				...state,
				deletedMusicAlbum: action.payload,
			};

		case UPLOAD_SONG:
			return {
				...state,
				uploadedSong: action.payload
			};

		case EDIT_SONG:
			return {
				...state,
				editedSong: action.payload
			};

		case DELETE_SONG:
			return {
				...state,
				deletedSong: action.payload,
			};

		case EDIT_VIDEO:
			return {
				...state,
				editedVideo: action.payload
			};

		case DELETE_VIDEO:
			return {
				...state,
				deletedVideo: action.payload,
			};

		case EDIT_IMAGE:
			return {
				...state,
				editedImage: action.payload
			};

		case DELETE_IMAGE:
			return {
				...state,
				deletedImage: action.payload,
			};

		case EDIT_LINK:
			return {
				...state,
				editedLink: action.payload
			};

		case DELETE_LINK:
			return {
				...state,
				deletedLink: action.payload,
			};

		case EDIT_CATEGORY:
			return {
				...state,
				editedCategory: action.payload
			};

		case DELETE_CATEGORY:
			return {
				...state,
				deletedCategory: action.payload,
			};

		case EDIT_TAG:
			return {
				...state,
				editedTag: action.payload
			};

		case DELETE_TAG:
			return {
				...state,
				deletedTag: action.payload,
			};

		case CHANGE_PASSWORD:
			return {
				...state,
				passwordChange: action.payload
			};

		case PASSWORD_ERROR:
			return {
				...state,
				passwordError: action.payload
			};

		case UPLOAD_CSV:
			return {
				...state,
				csvUploaded: action.payload
			}





		case EDIT_POST_FAIL:
			return {
				...state,
				editedPostFail: action.payload
			};

		case DELETE_POST_FAIL:
			return {
				...state,
				deletedPostFail: action.payload,
			};

		case EDIT_PHOTO_ALBUM_FAIL:
			return {
				...state,
				editedPhotoAlbumFail: action.payload
			};

		case DELETE_PHOTO_ALBUM_FAIL:
			return {
				...state,
				deletedPhotoAlbumFail: action.payload,
			};

		case UPLOAD_PHOTO_FAIL:
			return {
				...state,
				uploadedPhotoFail: action.payload
			};

		case EDIT_PHOTO_FAIL:
			return {
				...state,
				editedPhotoFail: action.payload
			};

		case DELETE_PHOTO_FAIL:
			return {
				...state,
				deletedPhotoFail: action.payload,
			};

		case EDIT_MUSIC_ALBUM_FAIL:
			return {
				...state,
				editedMusicAlbumFail: action.payload
			};

		case DELETE_MUSIC_ALBUM_FAIL:
			return {
				...state,
				deletedMusicAlbumFail: action.payload,
			};

		case UPLOAD_TRACK_FAIL:
			return {
				...state,
				uploadedSongFail: action.payload
			};

		case EDIT_SONG_FAIL:
			return {
				...state,
				editedSongFail: action.payload
			};

		case DELETE_SONG_FAIL:
			return {
				...state,
				deletedSongFail: action.payload,
			};

		case EDIT_VIDEO_FAIL:
			return {
				...state,
				editedVideoFail: action.payload
			};

		case DELETE_VIDEO_FAIL:
			return {
				...state,
				deletedVideoFail: action.payload,
			};

		case EDIT_IMAGE_FAIL:
			return {
				...state,
				editedImageFail: action.payload
			};

		case DELETE_IMAGE_FAIL:
			return {
				...state,
				deletedImageFail: action.payload,
			};

		case EDIT_LINK_FAIL:
			return {
				...state,
				editedLinkFail: action.payload
			};

		case DELETE_LINK_FAIL:
			return {
				...state,
				deletedLinkFail: action.payload,
			};

		case EDIT_CATEGORY_FAIL:
			return {
				...state,
				editedCategoryFail: action.payload
			};

		case DELETE_CATEGORY_FAIL:
			return {
				...state,
				deletedCategoryFail: action.payload,
			};

		case EDIT_TAG_FAIL:
			return {
				...state,
				editedTagFail: action.payload
			};

		case DELETE_TAG_FAIL:
			return {
				...state,
				deletedTagFail: action.payload,
			};

	default:
			return state;
	}
}