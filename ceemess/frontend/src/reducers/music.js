import { 
	GET_MUSIC,
	LOADING_MUSIC,
	GET_MORE_MUSIC,
	LOADING_MORE_MUSIC,
	CLEAR_MUSIC,

	GET_CATEGORY_MUSIC,
	GET_MORE_CATEGORY_MUSIC,
	LOADING_CATEGORY_MUSIC,
	CLEAR_CATEGORY_MUSIC,

	GET_TAG_MUSIC,
	GET_MORE_TAG_MUSIC,
	LOADING_TAG_MUSIC,
	CLEAR_TAG_MUSIC,

	GET_SEARCH_MUSIC,
	GET_MORE_SEARCH_MUSIC,
	LOADING_SEARCH_MUSIC,
	CLEAR_SEARCH_MUSIC
} from '../actions/types';

const initialState = {
	musicAlbums: null,
	loadingMusicAlbums: true,
	musicAlbumsNext: null,

	categoryMusicAlbums: null,
	loadingCategoryMusicAlbums: true,
	categoryMusicAlbumsNext: null,

	tagMusicAlbums: null,
	loadingTagMusicAlbums: true,
	tagMusicAlbumsNext: null,

	searchMusicAlbums: null,
	loadingSearchMusicAlbums: true,
	searchMusicAlbumsNext: null
}

export default function(state=initialState, action) {
	switch(action.type) {

		case 'GET_MUSIC':
			return {
				...state,
				musicAlbums: action.payload.results,
				musicAlbumsNext: action.payload.next
			};

		case 'LOADING_MUSIC':
			return {
				...state,
				loadingMusicAlbums: action.payload
			};

		case 'GET_MORE_MUSIC':
			return {
				...state,
				musicAlbums: [...state.musicAlbums, ...action.payload.results],
				musicAlbumsNext: action.payload.next
			};

		case 'CLEAR_MUSIC':
			return {
				...state,
				musicAlbums: null,
				loadingMusicAlbums: true,
				musicAlbumsNext: null
			}




		case GET_CATEGORY_MUSIC:
			return {
				...state,
				categoryMusicAlbums: action.payload.results,
				categoryMusicAlbumsNext: action.payload.next
			};

		case LOADING_CATEGORY_MUSIC:
			return {
				...state,
				loadingCategoryMusicAlbums: action.payload
			};

		case GET_MORE_CATEGORY_MUSIC:
			return {
				...state,
				categoryMusicAlbums: [...state.categoryMusicAlbums, ...action.payload.results],
				categoryMusicAlbumsNext: action.payload.next
			};

		case CLEAR_CATEGORY_MUSIC:
			return {
				...state,
				categoryMusicAlbums: null,
				loadingCategoryMusicAlbums: true,
				categoryMusicAlbumsNext: null
			}



		case GET_TAG_MUSIC:
			return {
				...state,
				tagMusicAlbums: action.payload.results,
				tagMusicAlbumsNext: action.payload.next
			};

		case LOADING_TAG_MUSIC:
			return {
				...state,
				loadingTagMusicAlbums: action.payload
			};

		case GET_MORE_TAG_MUSIC:
			return {
				...state,
				tagMusicAlbums: [...state.tagMusicAlbums, ...action.payload.results],
				tagMusicAlbumsNext: action.payload.next
			};

		case CLEAR_TAG_MUSIC:
			return {
				...state,
				tagMusicAlbums: null,
				loadingTagMusicAlbums: true,
				tagMusicAlbumsNext: null
			}



		case GET_SEARCH_MUSIC:
			return {
				...state,
				searchMusicAlbums: action.payload.results,
				searchMusicAlbumsNext: action.payload.next
			};

		case LOADING_SEARCH_MUSIC:
			return {
				...state,
				loadingSearchMusicAlbums: action.payload
			};

		case GET_MORE_SEARCH_MUSIC:
			return {
				...state,
				searchMusicAlbums: [...state.searchMusicAlbums, ...action.payload.results],
				searchMusicAlbumsNext: action.payload.next
			};

		case CLEAR_SEARCH_MUSIC:
			return {
				...state,
				searchMusicAlbums: null,
				loadingSearchMusicAlbums: true,
				searchMusicAlbumsNext: null
			}

	default:
			return state;
	}
}
