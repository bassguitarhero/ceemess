import { 
	GET_VIDEOS,
	LOADING_VIDEOS,
	GET_MORE_VIDEOS,
	LOADING_MORE_VIDEOS,
	CLEAR_VIDEOS,

	GET_CATEGORY_VIDEOS,
	GET_MORE_CATEGORY_VIDEOS,
	LOADING_CATEGORY_VIDEOS,
	CLEAR_CATEGORY_VIDEOS,

	GET_TAG_VIDEOS,
	GET_MORE_TAG_VIDEOS,
	LOADING_TAG_VIDEOS,
	CLEAR_TAG_VIDEOS,

	GET_SEARCH_VIDEOS,
	GET_MORE_SEARCH_VIDEOS,
	LOADING_SEARCH_VIDEOS,
	CLEAR_SEARCH_VIDEOS
} from '../actions/types';

const initialState = {
	videos: null,
	loadingVideos: true,
	videosNext: null,

	categoryVideos: null,
	loadingCategoryVideos: true,
	categoryVideosNext: null,

	tagVideos: null,
	loadingTagVideos: true,
	tagVideosNext: null,

	searchVideos: null,
	loadingSearchVideos: true,
	searchVideosNext: null
}

export default function(state=initialState, action) {
	switch(action.type) {

		case 'GET_VIDEOS':
			return {
				...state,
				videos: action.payload.results,
				videosNext: action.payload.next
			};

		case 'LOADING_VIDEOS':
			return {
				...state,
				loadingVideos: action.payload
			};

		case 'GET_MORE_VIDEOS':
			return {
				...state,
				videos: [...state.videos, ...action.payload.results],
				videosNext: action.payload.next
			};

		case 'CLEAR_VIDEOS':
			return {
				...state,
				videos: null,
				loadingVideos: true,
				videosNext: null
			}



		case GET_CATEGORY_VIDEOS:
			return {
				...state,
				categoryVideos: action.payload.results,
				categoryVideosNext: action.payload.next
			};

		case LOADING_CATEGORY_VIDEOS:
			return {
				...state,
				loadingCategoryVideos: action.payload
			};

		case GET_MORE_CATEGORY_VIDEOS:
			return {
				...state,
				categoryVideos: [...state.categoryVideos, ...action.payload.results],
				categoryVideosNext: action.payload.next
			};

		case CLEAR_CATEGORY_VIDEOS:
			return {
				...state,
				categoryVideos: null,
				loadingCategoryVideos: true,
				categoryVideosNext: null
			}


		case GET_TAG_VIDEOS:
			return {
				...state,
				tagVideos: action.payload.results,
				tagVideosNext: action.payload.next
			};

		case LOADING_TAG_VIDEOS:
			return {
				...state,
				loadingTagVideos: action.payload
			};

		case GET_MORE_TAG_VIDEOS:
			return {
				...state,
				tagVideos: [...state.tagVideos, ...action.payload.results],
				tagVideosNext: action.payload.next
			};

		case CLEAR_TAG_VIDEOS:
			return {
				...state,
				tagVideos: null,
				loadingTagVideos: true,
				tagVideosNext: null
			}



		case GET_SEARCH_VIDEOS:
			return {
				...state,
				searchVideos: action.payload.results,
				searchVideosNext: action.payload.next
			};

		case LOADING_SEARCH_VIDEOS:
			return {
				...state,
				loadingSearchVideos: action.payload
			};

		case GET_MORE_SEARCH_VIDEOS:
			return {
				...state,
				searchVideos: [...state.searchVideos, ...action.payload.results],
				searchVideosNext: action.payload.next
			};

		case CLEAR_SEARCH_VIDEOS:
			return {
				...state,
				searchVideos: null,
				loadingSearchVideos: true,
				searchVideosNext: null
			}






	default:
			return state;
	}
}
