import { 
	GET_BLOG_SETTINGS,
	LOADING_BLOG_SETTINGS,

	GET_CATEGORIES,
	LOADING_CATEGORIES,
	CLEAR_CATEGORIES,

	GET_TAGS,
	LOADING_TAGS,
	CLEAR_TAGS,

	GET_SOCIAL_MEDIA_LINKS,
	LOADING_SOCIAL_MEDIA_LINKS,

	REBUILD_NAV_BAR
} from '../actions/types';

const initialState = {
	blogSettings: null,
	loadingBlogSettings: true, 

	categories: null,
	loadingCategories: true,

	tags: null,
	loadingTags: true,

	socialMediaLinks: null,
	loadingSocialMediaLinks: true,

	rebuildNavBar: null
}

export default function(state=initialState, action) {
	switch(action.type) {
		case GET_BLOG_SETTINGS: 
			return {
				...state,
				blogSettings: action.payload
			}

		case LOADING_BLOG_SETTINGS:
			return {
				...state,
				loadingBlogSettings: action.payload
			}

		case GET_CATEGORIES:
			return {
				...state,
				categories: action.payload
			};

		case LOADING_CATEGORIES:
			return {
				...state,
				loadingCategories: action.payload
			};

		case CLEAR_CATEGORIES:
			return {
				...state,
				categories: null,
				loadingCategories: true,
			};

		case GET_TAGS:
			return {
				...state,
				tags: action.payload
			};

		case LOADING_TAGS:
			return {
				...state,
				loadingTags: action.payload
			};

		case CLEAR_TAGS:
			return {
				...state,
				tags: null,
				loadingTags: true,
			};

		case GET_SOCIAL_MEDIA_LINKS:
			return {
				...state,
				socialMediaLinks: action.payload
			};

		case LOADING_SOCIAL_MEDIA_LINKS: 
			return {
				...state,
				loadingSocialMediaLinks: action.payload
			};

		case REBUILD_NAV_BAR:
			return {
				...state,
				rebuildNavBar: action.payload
			}

	default:
			return state;
	}
}
