import { GET_ERRORS } from '../actions/types';

const initialState = {
	msg: {},
	status: null
};

export default function(state = initialState, action) {
	switch(action.type){
		case GET_ERRORS:
			console.log('Error');
			console.log('Message: ', action.payload.msg);
			console.log('Status: ', action.payload.status);
			return {
				msg: action.payload.msg,
				status: action.payload.status
			};
		default:
			return state;
	}
}