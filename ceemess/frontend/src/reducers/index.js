import { combineReducers } from 'redux';
import addNew from './addNew';
import admin from './admin';
import blog from './blog';
import common from './common';
import dashboard from './dashboard';
import errors from './errors';
import images from './images';
import messages from './messages';
import music from './music';
import photos from './photos';
import portfolio from './portfolio';
import videos from './videos';

export default combineReducers({
	addNew, 
	admin,
	blog,
	common,
	dashboard,
	errors,
	images,
	messages,
	music,
	photos,
	portfolio,
	videos
});