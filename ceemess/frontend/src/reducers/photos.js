import { 
	GET_PHOTOS,
	LOADING_PHOTOS,
	GET_MORE_PHOTOS,
	LOADING_MORE_PHOTOS,
	CLEAR_PHOTOS,

	GET_CATEGORY_PHOTOS,
	GET_MORE_CATEGORY_PHOTOS,
	LOADING_CATEGORY_PHOTOS,
	CLEAR_CATEGORY_PHOTOS,

	GET_TAG_PHOTOS,
	GET_MORE_TAG_PHOTOS,
	LOADING_TAG_PHOTOS,
	CLEAR_TAG_PHOTOS,

	GET_SEARCH_PHOTOS,
	GET_MORE_SEARCH_PHOTOS,
	LOADING_SEARCH_PHOTOS,
	CLEAR_SEARCH_PHOTOS
} from '../actions/types';

const initialState = {
	photoAlbums: null,
	loadingPhotoAlbums: true,
	photoAlbumsNext: null,

	categoryPhotoAlbums: null,
	loadingCategoryPhotoAlbums: true,
	categoryPhotoAlbumsNext: null,

	tagPhotoAlbums: null,
	loadingTagPhotoAlbums: true,
	tagPhotoAlbumsNext: null,

	searchPhotoAlbums: null,
	loadingSearchPhotoAlbums: true,
	searchPhotoAlbumsNext: null
}

export default function(state=initialState, action) {
	switch(action.type) {

		case GET_PHOTOS:
			return {
				...state,
				photoAlbums: action.payload.results,
				photoAlbumsNext: action.payload.next
			};

		case LOADING_PHOTOS:
			return {
				...state,
				loadingPhotoAlbums: action.payload
			};

		case GET_MORE_PHOTOS:
			return {
				...state,
				photoAlbums: [...state.photoAlbums, ...action.payload.results],
				photoAlbumsNext: action.payload.next
			};

		case CLEAR_PHOTOS:
			return {
				...state,
				photoAlbums: null,
				loadingPhotoAlbums: true,
				photoAlbumsNext: null
			}


		case GET_CATEGORY_PHOTOS:
			return {
				...state,
				categoryPhotoAlbums: action.payload.results,
				categoryPhotoAlbumsNext: action.payload.next
			};

		case LOADING_CATEGORY_PHOTOS:
			return {
				...state,
				loadingCategoryPhotoAlbums: action.payload
			};

		case GET_MORE_CATEGORY_PHOTOS:
			return {
				...state,
				categoryPhotoAlbums: [...state.categoryPhotoAlbums, ...action.payload.results],
				categoryPhotoAlbumsNext: action.payload.next
			};

		case CLEAR_CATEGORY_PHOTOS:
			return {
				...state,
				categoryPhotoAlbums: null,
				loadingCategoryPhotoAlbums: true,
				categoryPhotoAlbumsNext: null
			}



		case GET_TAG_PHOTOS:
			return {
				...state,
				tagPhotoAlbums: action.payload.results,
				tagPhotoAlbumsNext: action.payload.next
			};

		case LOADING_TAG_PHOTOS:
			return {
				...state,
				loadingTagPhotoAlbums: action.payload
			};

		case GET_MORE_TAG_PHOTOS:
			return {
				...state,
				tagPhotoAlbums: [...state.tagPhotoAlbums, ...action.payload.results],
				tagPhotoAlbumsNext: action.payload.next
			};

		case CLEAR_TAG_PHOTOS:
			return {
				...state,
				tagPhotoAlbums: null,
				loadingTagPhotoAlbums: true,
				tagPhotoAlbumsNext: null
			}



		case GET_SEARCH_PHOTOS:
			return {
				...state,
				searchPhotoAlbums: action.payload.results,
				searchPhotoAlbumsNext: action.payload.next
			};

		case LOADING_SEARCH_PHOTOS:
			return {
				...state,
				loadingSearchPhotoAlbums: action.payload
			};

		case GET_MORE_SEARCH_PHOTOS:
			return {
				...state,
				searchPhotoAlbums: [...state.searchPhotoAlbums, ...action.payload.results],
				searchPhotoAlbumsNext: action.payload.next
			};

		case CLEAR_SEARCH_PHOTOS:
			return {
				...state,
				searchPhotoAlbums: null,
				loadingSearchPhotoAlbums: true,
				searchPhotoAlbumsNext: null
			}

	default:
			return state;
	}
}
