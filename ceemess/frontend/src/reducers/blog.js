import {
	GET_STICKY_BLOG_POSTS,
	LOADING_STICKY_BLOG_POSTS,

	GET_BLOG_POSTS,
	LOADING_BLOG_POSTS,
	GET_MORE_BLOG_POSTS,
	LOADING_MORE_BLOG_POSTS,
	CLEAR_BLOG_POSTS,

	GET_BLOG_POST,
	LOADING_BLOG_POST,

	GET_CATEGORY_POSTS,
	LOADING_CATEGORY_POSTS,
	GET_MORE_CATEGORY_POSTS,
	CLEAR_CATEGORY_POSTS,

	GET_TAG_POSTS,
	LOADING_TAG_POSTS,
	GET_MORE_TAG_POSTS,
	CLEAR_TAG_POSTS,

	GET_SEARCH_POSTS, 
	LOADING_SEARCH_POSTS, 
	GET_MORE_SEARCH_POSTS, 
	LOADING_MORE_SEARCH_POSTS
} from '../actions/types';

const initialState = {	
	stickyBlogPosts: null,
	loadingStickyBlogPosts: true, 

	blogPosts: null,
	loadingBlogPosts: true,
	blogPostsNext: null,
	blogPostsPrevious: null,

	loadingMoreBlogPosts: true,

	blogPost: null,
	loadingBlogPost: true,

	categoryPosts: null,
	loadingCategoryPosts: true,
	categoryPostsNext: null,

	tagPosts: null,
	loadingTagPosts: true,
	tagPostsNext: null,

	searchPosts: null,
	loadingSearchPosts: true,
	searchPostsNext: null
}

export default function(state=initialState, action) {
	switch(action.type) {
		case GET_STICKY_BLOG_POSTS:
			return {
				...state,
				stickyBlogPosts: action.payload.results,
			};

		case LOADING_STICKY_BLOG_POSTS:
			return {
				...state,
				loadingStickyBlogPosts: action.payload
			};

		case GET_BLOG_POSTS:
			return {
				...state,
				blogPosts: action.payload.results,
				blogPostsNext: action.payload.next,
				blogPostsPrevious: action.payload.previous
			};

		case LOADING_BLOG_POSTS:
			return {
				...state,
				loadingBlogPosts: action.payload
			};

		case GET_MORE_BLOG_POSTS:
			return {
				...state,
				blogPosts: [...state.blogPosts, ...action.payload.results],
				blogPostsNext: action.payload.next,
				blogPostsPrevious: action.payload.previous,
			};

		case LOADING_MORE_BLOG_POSTS:
			return {
				...state,
				loadingMoreBlogPosts: action.payload
			};

		case GET_BLOG_POST:
			return {
				...state,
				blogPost: action.payload
			};

		case LOADING_BLOG_POST:
			return {
				...state,
				loadingBlogPost: action.payload
			};

		case CLEAR_BLOG_POSTS:
			return {
				...state,
				blogPosts: null,
				loadingBlogPosts: true,
				blogPostsNext: null,
				blogPostsPrevious: null,
				stickyBlogPosts: null,
				loadingStickyBlogPosts: true,
			};

		case GET_CATEGORY_POSTS:
			return {
				...state,
				categoryPosts: action.payload.results,
				categoryPostsNext: action.payload.next
			};

		case LOADING_CATEGORY_POSTS:
			return {
				...state,
				loadingCategoryPosts: action.payload
			};

		case GET_MORE_CATEGORY_POSTS:
			return {
				...state,
				categoryPosts: [...state.categoryPosts, ...action.payload.results],
				categoryPostsNext: action.payload.next
			};

		case CLEAR_CATEGORY_POSTS:
			return {
				...state,
				categoryPosts: null,
				loadingCategoryPosts: true,
				categoryPostsNext: null
			}

		case GET_TAG_POSTS:
			return {
				...state,
				tagPosts: action.payload.results,
				tagPostsNext: action.payload.next
			};

		case LOADING_TAG_POSTS:
			return {
				...state,
				loadingTagPosts: action.payload
			};

		case GET_MORE_TAG_POSTS:
			return {
				...state,
				tagPosts: [...state.tagPosts, ...action.payload.results],
				tagPostsNext: action.payload.next
			};

		case CLEAR_TAG_POSTS:
			return {
				...state,
				tagPosts: null,
				loadingTagPosts: true,
				tagPostsNext: null
			}

		case GET_SEARCH_POSTS:
			return {
				...state,
				searchPosts: action.payload.results,
				searchPostsNext: action.payload.next
			};

		case LOADING_SEARCH_POSTS:
			return {
				...state,
				loadingSearchPosts: action.payload
			};

		case GET_MORE_SEARCH_POSTS:
			return {
				...state,
				searchPosts: [...state.searchPosts, ...action.payload.results],
				searchPostsNext: action.payload.next
			};

		case LOADING_MORE_SEARCH_POSTS:
			return {
				...state,
				loadingMoreSearchPosts: action.payload
			};

		default:
			return state;
	}
}











