import { 
	GET_PORTFOLIO_POSTS,
	LOADING_PORTFOLIO_POSTS,
	GET_STICKY_PORTFOLIO_POSTS,
	LOADING_STICKY_PORTFOLIO_POSTS,
	GET_MORE_PORTFOLIO_POSTS, 
	LOADING_MORE_PORTFOLIO_POSTS,
	CLEAR_PORTFOLIO_POSTS,

	GET_CATEGORY_PORTFOLIO,
	GET_MORE_CATEGORY_PORTFOLIO,
	LOADING_CATEGORY_PORTFOLIO,
	CLEAR_CATEGORY_PORTFOLIO,

	GET_TAG_PORTFOLIO,
	GET_MORE_TAG_PORTFOLIO,
	LOADING_TAG_PORTFOLIO,
	CLEAR_TAG_PORTFOLIO,

	GET_SEARCH_PORTFOLIO,
	GET_MORE_SEARCH_PORTFOLIO,
	LOADING_SEARCH_PORTFOLIO,
	CLEAR_SEARCH_PORTFOLIO
} from '../actions/types';

const initialState = {
	portfolioPosts: [],
	loadingPortfolioPosts: true,
	portfolioPostsNext: null,
	portfolioPostsPrevious: null,

	stickyPortfolioPosts: null,
	loadingStickyPortfolioPosts: true,

	portfolioPost: null, 
	loadingPortfolioPost: true,

	categoryPortfolio: null,
	loadingCategoryPortfolio: true,
	categoryPortfolioNext: null,

	tagPortfolio: null,
	loadingTagPortfolio: true,
	tagPortfolioNext: null,

	searchPortfolio: null,
	loadingSearchPortfolio: true,
	searchPortfolioNext: null
}

export default function(state=initialState, action) {
	switch(action.type) {

		case GET_STICKY_PORTFOLIO_POSTS:
			return {
				...state,
				portfolioPosts: [...action.payload.results, ...state.portfolioPosts],
			};

		case LOADING_STICKY_PORTFOLIO_POSTS:
			return {
				...state,
				loadingStickyPortfolioPosts: action.payload
			};

		case GET_PORTFOLIO_POSTS:
			return {
				...state,
				portfolioPosts: [...state.portfolioPosts, ...action.payload.results],
				portfolioPostsNext: action.payload.next,
				portfolioPostsPrevious: action.payload.previous
			};

		case LOADING_PORTFOLIO_POSTS:
			return {
				...state,
				loadingPortfolioPosts: action.payload
			};

		// case GET_PORTFOLIO_POST:
		// 	return {
		// 		...state,
		// 		portfolioPost: action.payload
		// 	};

		// case LOADING_PORTFOLIO_POST:
		// 	return {
		// 		...state,
		// 		loadingPortfolioPost: action.payload
		// 	};

		case GET_MORE_PORTFOLIO_POSTS:
			return {
				...state,
				portfolioPosts: [...state.portfolioPosts, ...action.payload.results],
				portfolioPostsNext: action.payload.next,
				portfolioPostsPrevious: action.payload.previous
			};

		case CLEAR_PORTFOLIO_POSTS:
			return {
				...state,
				portfolioPosts: [],
				loadingPortfolioPosts: true,
				portfolioPostsNext: null,
				portfolioPostsPrevious: null,
			};




		case GET_CATEGORY_PORTFOLIO:
			return {
				...state,
				categoryPortfolio: action.payload.results,
				categoryPortfolioNext: action.payload.next
			};

		case LOADING_CATEGORY_PORTFOLIO:
			return {
				...state,
				loadingCategoryPortfolio: action.payload
			};

		case GET_MORE_CATEGORY_PORTFOLIO:
			return {
				...state,
				categoryPortfolio: [...state.categoryPortfolio, ...action.payload.results],
				categoryPortfolioNext: action.payload.next,
			};

		case CLEAR_CATEGORY_PORTFOLIO:
			return {
				...state,
				categoryPortfolio: null,
				loadingCategoryPortfolio: true,
				categoryPortfolioNext: null,
			};



		case GET_TAG_PORTFOLIO:
			return {
				...state,
				tagPortfolio: action.payload.results,
				tagPortfolioNext: action.payload.next
			};

		case LOADING_TAG_PORTFOLIO:
			return {
				...state,
				loadingTagPortfolio: action.payload
			};

		case GET_MORE_TAG_PORTFOLIO:
			return {
				...state,
				tagPortfolio: [...state.tagPortfolio, ...action.payload.results],
				tagPortfolioNext: action.payload.next,
			};

		case CLEAR_TAG_PORTFOLIO:
			return {
				...state,
				tagPortfolio: null,
				loadingTagPortfolio: true,
				tagPortfolioNext: null,
			};



		case GET_SEARCH_PORTFOLIO:
			return {
				...state,
				searchPortfolio: action.payload.results,
				searchPortfolioNext: action.payload.next
			};

		case LOADING_SEARCH_PORTFOLIO:
			return {
				...state,
				loadingSearchPortfolio: action.payload
			};

		case GET_MORE_SEARCH_PORTFOLIO:
			return {
				...state,
				searchPortfolio: [...state.searchPortfolio, ...action.payload.results],
				searchPortfolioNext: action.payload.next,
			};

		case CLEAR_SEARCH_PORTFOLIO:
			return {
				...state,
				searchPortfolio: null,
				loadingSearchPortfolio: true,
				searchPortfolioNext: null,
			};

	default:
			return state;
	}
}