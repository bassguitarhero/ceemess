import axios from 'axios';
import { returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	GET_IMAGES,
	LOADING_IMAGES,
	GET_MORE_IMAGES,
	LOADING_MORE_IMAGES,
	CLEAR_IMAGES,

	GET_CATEGORY_IMAGES,
	GET_MORE_CATEGORY_IMAGES,
	LOADING_CATEGORY_IMAGES,
	CLEAR_CATEGORY_IMAGES,

	GET_TAG_IMAGES,
	GET_MORE_TAG_IMAGES,
	LOADING_TAG_IMAGES,
	CLEAR_TAG_IMAGES,

	GET_SEARCH_IMAGES,
	GET_MORE_SEARCH_IMAGES,
	LOADING_SEARCH_IMAGES,
	CLEAR_SEARCH_IMAGES
} from './types';

export const getImages = () => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_IMAGES,
				payload: res.data
			});
			dispatch({
				type: LOADING_IMAGES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreImages = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_IMAGES,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_IMAGES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearImages = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMAGES
	})
}

export const getCategoryImages = (category) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/categories/v/${category.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_CATEGORY_IMAGES,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_IMAGES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreCategoryImages = (category, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/categories/v/${category.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_CATEGORY_IMAGES,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategoryImages = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORY_IMAGES
	})
}

export const getTagImages = (tag) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/tags/v/${tag.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_TAG_IMAGES,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_IMAGES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreTagImages = (tag, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/tags/v/${tag.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_TAG_IMAGES,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTagImages = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAG_IMAGES
	})
}

export const getSearchImages = (searchText) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/search/${searchText}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_SEARCH_IMAGES,
				payload: res.data
			});
			dispatch({
				type: LOADING_SEARCH_IMAGES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreSearchImages = (searchText, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/images/search/${searchText}?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_SEARCH_IMAGES,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearSearchImages = () => (dispatch) => {
	dispatch({
		type: CLEAR_SEARCH_IMAGES
	})
}









