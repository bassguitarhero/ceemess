import axios from 'axios';
import { createMessage, returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	USER_LOADING, 
	AUTH_ERROR, 
	USER_LOADED, 
	ERROR_401, 
	LOGIN_SUCCESS,
	// LOGIN_FAIL,
	LOGOUT_SUCCESS,
	NO_USER,
	PASSWORD_RESET_REQUEST,
	GET_USERS,
	LOADING_USERS,
	CREATE_NEW_USER,

	REBUILD_NAV_BAR
} from './types';

export const loadUser = () => (dispatch, getState) => {
	dispatch({type: USER_LOADING});

	const token = getState().admin.token;
	if (token !== null && token !== undefined) {

		axios.get(URL + '/api/user/', tokenConfig(getState))
			.then(res => {
				if (res.data.code === 401) {
					dispatch({
						type: AUTH_ERROR
					})
				} else {
					dispatch({
						type: USER_LOADED,
						payload: res.data
					});
				}
			})
			.catch(err => {
				dispatch({
					type: AUTH_ERROR
				});
				if (err.response.status === 401) {
					dispatch({
						type: ERROR_401
					});
				}
				dispatch(returnErrors(err.response.data, err.response.status))
			});
	} else {
		dispatch({
			type: NO_USER
		})
	}
};

export const clearUser = () => (dispatch) => {
	dispatch({type: USER_LOADING});
}

export const login = (username, password) => (dispatch) => {
	// Request Headers
	 const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  // Request Body
  const data = JSON.stringify({ username, password });

  axios
    .post(URL + '/api/login/', data, config)
    .then(res => {
    	dispatch(createMessage({loginSuccessful: "Login Successful"}));
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      });
      dispatch({
      	type: REBUILD_NAV_BAR,
      	payload: true
      })
    })
    .catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const logout = () => (dispatch, getState) => {
  axios
    .post(URL + '/api/logout/', null, tokenConfig(getState))
    .then(res => {
    	dispatch(createMessage({logoutSuccessful: "Logout Successful"}));
      dispatch({
        type: LOGOUT_SUCCESS
      });
      dispatch({
      	type: REBUILD_NAV_BAR,
      	payload: true
      })
    })
    .catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
};

export const requestPasswordReset = (username, email) => (dispatch) => {
	let domain = window.location.href;
	let website_url = domain.substring(0, domain.length - 1);
	
	var formData = new FormData();
	formData.set('username', username);
	formData.append('email', email);
	formData.append('url', website_url);

	const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

	axios
		.post(URL + '/api/reset-password/request/', formData, config)
		.then(res => {
			dispatch(createMessage({requestSuccessful: "Password Reset Request Successful"}));
			dispatch({
				type: PASSWORD_RESET_REQUEST,
				payload: res.data
			})
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearPasswordResetRequest = () => (dispatch) => {
	dispatch({
		type: PASSWORD_RESET_REQUEST,
		payload: null
	});
}


export const getUsers = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/users/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_USERS,
				payload: res.data
			});
			dispatch({
				type: LOADING_USERS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearUsers = () => (dispatch) => {
	dispatch({
		type: GET_USERS,
		payload: null
	});
	dispatch({
		type: LOADING_USERS,
		payload: true
	})
}

export const createNewUser = (newUser) => (dispatch, getState) => {
	let domain = window.location.href;
	let website_url = domain.substring(0, domain.length - 1);

	var formData = new FormData();
	formData.set('username', newUser.username);
	formData.append('email', newUser.email);
	formData.append('password', newUser.password);
	formData.append('url', website_url);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/register/', formData, config)
		.then(res => {
			dispatch(createMessage({userCreated: "New User successfully created"}));
			dispatch({
				type: CREATE_NEW_USER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetCreateNewUser = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_USER,
		payload: null
	})
}


























