import axios from 'axios';
import { returnErrors } from "./messages";

import { 
	GET_BLOG_SETTINGS,
	LOADING_BLOG_SETTINGS,

	GET_CATEGORIES,
	LOADING_CATEGORIES,
	CLEAR_CATEGORIES,

	GET_TAGS,
	LOADING_TAGS,
	CLEAR_TAGS,

	GET_SOCIAL_MEDIA_LINKS,
	LOADING_SOCIAL_MEDIA_LINKS,

	REBUILD_NAV_BAR
} from './types';

export const URL = '';

export const getBlogSettings = () => (dispatch) => {
	axios
		.get(URL + '/api/settings/')
		.then(res => {
			dispatch({
				type: GET_BLOG_SETTINGS,
				payload: res.data
			});
			dispatch({
				type: LOADING_BLOG_SETTINGS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearBlogSettings = () => (dispatch) => {
	dispatch({
		type: GET_BLOG_SETTINGS,
		payload: null
	});
	dispatch({
		type: LOADING_BLOG_SETTINGS,
		payload: true
	});
}

export const getCategories = () => (dispatch) => {
	axios 
		.get(URL + `/api/categories/`)
		.then(res => {
			dispatch({
				type: GET_CATEGORIES,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORIES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategories = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORIES
	});
}

export const getTags = () => (dispatch) => {
	axios 
		.get(URL + `/api/tags/`)
		.then(res => {
			dispatch({
				type: GET_TAGS,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAGS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTags = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAGS
	});
}


export const getSocialMediaLinks = () => (dispatch) => {
	axios
		.get(URL + '/api/links/social-media/')
		.then(res => {
			dispatch({
				type: GET_SOCIAL_MEDIA_LINKS,
				payload: res.data
			});
			dispatch({
				type: LOADING_SOCIAL_MEDIA_LINKS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearSocialMediaLinks = () => (dispatch) => {
	dispatch({
		type: GET_SOCIAL_MEDIA_LINKS,
		payload: null
	});
	dispatch({
		type: LOADING_SOCIAL_MEDIA_LINKS,
		payload: true
	});
}

export const resetRebuildNavBar = () => (dispatch) => {
	dispatch({
		type: REBUILD_NAV_BAR,
		payload: null
	})
}

// Setup config with token 
export const tokenConfig = getState => {
	// Get token from state
	const token = getState().admin.token;
	// Headers
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}
	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}
	return config;
};









