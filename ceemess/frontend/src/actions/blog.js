import axios from 'axios';
import { returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	GET_STICKY_BLOG_POSTS,
	LOADING_STICKY_BLOG_POSTS,

	GET_BLOG_POSTS,
	LOADING_BLOG_POSTS,
	CLEAR_BLOG_POSTS,

	GET_MORE_BLOG_POSTS,
	LOADING_MORE_BLOG_POSTS,

	GET_BLOG_POST,
	LOADING_BLOG_POST,

	GET_CATEGORY_POSTS,
	LOADING_CATEGORY_POSTS,
	GET_MORE_CATEGORY_POSTS,
	CLEAR_CATEGORY_POSTS,

	GET_TAG_POSTS,
	LOADING_TAG_POSTS,
	GET_MORE_TAG_POSTS, 
	CLEAR_TAG_POSTS,

	GET_SEARCH_POSTS, 
	LOADING_SEARCH_POSTS, 
	GET_MORE_SEARCH_POSTS, 
	LOADING_MORE_SEARCH_POSTS
} from './types';

export const getStickyBlogPosts = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/blog/sticky/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_STICKY_BLOG_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_STICKY_BLOG_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getBlogPosts = () => (dispatch, getState) => {
	axios 
		.get(URL + '/api/blog/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_BLOG_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_BLOG_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreBlogPosts = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/blog/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_BLOG_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_BLOG_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getBlogPost = (slug) => (dispatch, getState) => {
	axios
		.get(URL + `/api/blog/v/${slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_BLOG_POST,
				payload: res.data
			});
			dispatch({
				type: LOADING_BLOG_POST,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearBlogPosts = () => (dispatch) => {
	dispatch({
		type: CLEAR_BLOG_POSTS
	});
}

export const clearBlogPost = () => (dispatch) => {
	dispatch({
		type: GET_BLOG_POST,
		payload: null
	});
	dispatch({
		type: LOADING_BLOG_POST,
		payload: true
	})
}

export const getCategoryPosts = (category) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/categories/v/${category.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_CATEGORY_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreCategoryPosts = (category, page) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/categories/v/${category.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_CATEGORY_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategoryPosts = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORY_POSTS
	})
}

export const getTagPosts = (tag) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/tags/v/${tag.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_TAG_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTagPosts = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAG_POSTS
	});
}

export const getMoreTagPosts = (tag, page) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/tags/v/${tag.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_TAG_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getSearchPosts = (searchText) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/search/${searchText}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_SEARCH_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_SEARCH_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreSearchPosts = (searchText, page) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/search/${searchText}?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_SEARCH_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_SEARCH_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}









