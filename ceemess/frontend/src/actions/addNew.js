import axios from 'axios';
import { createMessage, returnErrors } from "./messages";
import { URL } from './common';

import { 
	CREATE_NEW_POST,
	CREATE_NEW_PHOTO_ALBUM,
	CREATE_NEW_PHOTO,
	CREATE_NEW_MUSIC_ALBUM,
	CREATE_NEW_TRACK, 
	CREATE_NEW_VIDEO,
	CREATE_NEW_IMAGE,
	CREATE_NEW_LINK,
	CREATE_NEW_CATEGORY,
	CREATE_NEW_TAG,

	NEW_POST_FAIL,
	NEW_PHOTO_ALBUM_FAIL,
	NEW_PHOTO_FAIL,
	NEW_MUSIC_ALBUM_FAIL,
	NEW_TRACK_FAIL,
	NEW_VIDEO_FAIL,
	NEW_IMAGE_FAIL,
	NEW_LINK_FAIL,
	NEW_CATEGORY_FAIL,
	NEW_TAG_FAIL
} from './types';

export const createNewPost = (post) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', post.title);
	formData.append('body', post.body);
	formData.append('blog', post.blog);
	formData.append('blog_sticky', post.blogSticky);
	formData.append('portfolio', post.portfolio);
	formData.append('portfolio_sticky', post.portfolioSticky);
	formData.append('published', post.publish);
	if (post.image !== null) {
		formData.append('image', post.image);
	} else {
		formData.append('image', '');
	}
	if (post.imageHighlight !== null) {
		formData.append('image_highlight', post.imageHighlight.slug);
	} else {
		formData.append('image_highlight', '');
	}
	if (post.videoHighlight !== null) {
		formData.append('video_highlight', post.videoHighlight.slug);
	} else {
		formData.append('video_highlight', '');
	}
	if (post.photoAlbum !== null) {
		formData.append('photo_album', post.photoAlbum.slug);
	} else {
		formData.append('photo_album', '');
	}
	if (post.musicAlbum !== null) {
		formData.append('music_album', post.musicAlbum.slug);
	} else {
		formData.append('music_album', '');
	}
	if (post.category !== null) {
		formData.append('category', post.category.slug);
	} else {
		formData.append('category', '');
	}
	if (post.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < post.tags.length; i++) {
			tagsList.push(post.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}
	// videos
	if (post.videos.length > 0) {
		let videosList = [];
		for (var i = 0; i < post.videos.length; i++) {
			videosList.push(post.videos[i].slug);
		}
		formData.append('videos', videosList);
	} else {
		formData.append('videos', []);
	}
	// images
	if (post.images.length > 0) {
		let imagesList = [];
		for (var i = 0; i < post.images.length; i++) {
			imagesList.push(post.images[i].slug);
		}
		formData.append('images', imagesList);
	} else {
		formData.append('images', []);
	}
	// links
	if (post.links.length > 0) {
		let linksList = [];
		for (var i = 0; i < post.links.length; i++) {
			linksList.push(post.links[i].slug);
		}
		formData.append('links', linksList);
	} else {
		formData.append('links', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/posts/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newPostCreated: "New Post successfully created"}));
			dispatch({
				type: CREATE_NEW_POST,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_POST_FAIL,
				payload: true
			})
		});
}

export const clearNewPost = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_POST,
		payload: null
	})
}

export const clearNewPostFail = () => (dispatch) => {
	dispatch({
		type: NEW_POST_FAIL,
		payload: null
	})
}

export const createNewPhotoAlbum = (album) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', album.title);
	formData.append('description', album.description);
	formData.append('public', album.publicAlbum);
	if (album.category !== null) {
		formData.append('category', album.category.slug);
	} else {
		formData.append('category', '');
	}
	if (album.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < album.tags.length; i++) {
			tagsList.push(album.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/albums/photos/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newPhotoAlbumCreated: "New Photo Album successfully created"}));
			dispatch({
				type: CREATE_NEW_PHOTO_ALBUM,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_PHOTO_ALBUM_FAIL,
				payload: true
			})
		});
}

export const clearNewPhotoAlbum = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_PHOTO_ALBUM,
		payload: null
	})
}

export const clearNewPhotoAlbumFail = () => (dispatch) => {
	dispatch({
		type: NEW_PHOTO_ALBUM_FAIL,
		payload: null
	})
}

export const createNewPhoto = (photo) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', photo.title);
	formData.append('image', photo.photo);
	formData.append('album-slug', photo.slug);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = `<span style="font-size: 20, font-weight: 'bold'">Uploading... ` + percentCompleted + `%</span>`;
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/photos/create/', formData, config)
		.then(res => {
			dispatch({
				type: CREATE_NEW_PHOTO,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_PHOTO_FAIL,
				payload: true
			});
		});
}

export const clearNewPhoto = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_PHOTO,
		payload: null
	});
}

export const clearNewPhotoFail = () => (dispatch) => {
	dispatch({
		type: NEW_PHOTO_FAIL,
		payload: null
	});
}

export const createNewMusicAlbum = (album) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('band_name', album.bandName);
	formData.append('title', album.title);
	formData.append('description', album.description);
	if (album.image !== null && album.image !== undefined) {
		formData.append('image', album.image);
	} else {
		formData.append('image', '');
	}
	formData.append('public', album.publicAlbum);
	if (album.category !== null) {
		formData.append('category', album.category.slug);
	} else {
		formData.append('category', '');
	}
	if (album.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < album.tags.length; i++) {
			tagsList.push(album.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/albums/music/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newMusicAlbumCreated: "New Music Album successfully created"}));
			dispatch({
				type: CREATE_NEW_MUSIC_ALBUM,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_MUSIC_ALBUM_FAIL,
				payload: true
			});
		});
}

export const clearNewMusicAlbum = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_MUSIC_ALBUM,
		payload: null
	})
}

export const clearNewMusicAlbumFail = () => (dispatch) => {
	dispatch({
		type: NEW_MUSIC_ALBUM_FAIL,
		payload: null
	});
}

export const createNewTrack = (track) => (dispatch, getState) => {
	var formData = new FormData();
	formData.append('title', track.title);
	formData.append('description', track.description);
	formData.append('soundcloud_embed', track.soundcloud);
	if (track.image !== null) {
		formData.append('image', track.image);
	} else {
		formData.append('image', '');
	}
	if (track.audio !== null) {
		formData.append('audio_file', track.audio);
	} else {
		formData.append('audio_file', '');
	}
	formData.append('youtube_url', track.youtube_url);
	formData.append('album-slug', track.slug);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/tracks/create/', formData, config)
		.then(res => {
			dispatch({
				type: CREATE_NEW_TRACK,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_TRACK_FAIL,
				payload: true
			});
		});
}

export const clearNewTrack = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_TRACK,
		payload: null
	});
}

export const clearNewTrackFail = () => (dispatch) => {
	dispatch({
		type: NEW_TRACK_FAIL,
		payload: null
	});
}

export const createNewVideo = (video) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', video.title);
	formData.append('description', video.description);
	formData.append('youtube_id', video.youtubeID);
	formData.append('vimeo_id', video.vimeoID);
	formData.append('image', video.image);
	formData.append('public', video.publicVideo);
	if (video.category !== null) {
		formData.append('category', video.category.slug);
	} else {
		formData.append('category', '');
	}
	if (video.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < video.tags.length; i++) {
			tagsList.push(video.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/videos/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newVideoCreated: "New Video successfully created"}));
			dispatch({
				type: CREATE_NEW_VIDEO,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_VIDEO_FAIL,
				payload: true
			});
		});
}

export const clearNewVideo = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_VIDEO,
		payload: null
	});
}

export const clearNewVideoFail = () => (dispatch) => {
	dispatch({
		type: NEW_VIDEO_FAIL,
		payload: null
	});
}

export const createNewImage = (image) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', image.title);
	formData.append('description', image.description);
	formData.append('caption', image.caption);
	formData.append('image', image.file);
	formData.append('public', image.publicImage);
	if (image.category !== null) {
		formData.append('category', image.category.slug);
	} else {
		formData.append('category', '');
	}
	if (image.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < image.tags.length; i++) {
			tagsList.push(image.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/images/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newImageCreated: "New Image successfully created"}));
			dispatch({
				type: CREATE_NEW_IMAGE,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_IMAGE_FAIL,
				payload: true
			});
		});
}

export const clearNewImage = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_IMAGE,
		payload: null
	});
}

export const clearNewImageFail = () => (dispatch) => {
	dispatch({
		type: NEW_IMAGE_FAIL,
		payload: null
	});
}

export const createNewLink = (link) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('description', link.name);
	formData.append('link', link.linkURL);
	formData.append('social_media', link.socialMediaLink);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/links/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newLinkCreated: "New Link successfully created"}));
			dispatch({
				type: CREATE_NEW_LINK,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_LINK_FAIL,
				payload: true
			});
		});
}

export const clearNewLink = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_LINK,
		payload: null
	});
}

export const clearNewLinkFail = () => (dispatch) => {
	dispatch({
		type: NEW_LINK_FAIL,
		payload: null
	});
}

export const createNewCategory = (category) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('category', category.name);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/categories/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newCategoryCreated: "New Category successfully created"}));
			dispatch({
				type: CREATE_NEW_CATEGORY,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_CATEGORY_FAIL,
				payload: true
			});
		});
}

export const clearNewCategory = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_CATEGORY,
		payload: null
	});
}

export const clearNewCategoryFail = () => (dispatch) => {
	dispatch({
		type: NEW_CATEGORY_FAIL,
		payload: null
	});
}

export const createNewTag = (tag) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('tag', tag.name);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/tags/create/', formData, config)
		.then(res => {
			dispatch(createMessage({newTagCreated: "New Tag successfully created"}));
			dispatch({
				type: CREATE_NEW_TAG,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: NEW_TAG_FAIL,
				payload: true
			});
		});
}

export const clearNewTag = () => (dispatch) => {
	dispatch({
		type: CREATE_NEW_TAG,
		payload: null
	});
}

export const clearNewTagFail = () => (dispatch) => {
	dispatch({
		type: NEW_TAG_FAIL,
		payload: null
	});
}














