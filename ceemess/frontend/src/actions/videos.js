import axios from 'axios';
import { returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	GET_VIDEOS,
	LOADING_VIDEOS,
	GET_MORE_VIDEOS,
	LOADING_MORE_VIDEOS,
	CLEAR_VIDEOS,

	GET_CATEGORY_VIDEOS,
	GET_MORE_CATEGORY_VIDEOS,
	LOADING_CATEGORY_VIDEOS,
	CLEAR_CATEGORY_VIDEOS,

	GET_TAG_VIDEOS,
	GET_MORE_TAG_VIDEOS,
	LOADING_TAG_VIDEOS,
	CLEAR_TAG_VIDEOS,

	GET_SEARCH_VIDEOS,
	GET_MORE_SEARCH_VIDEOS,
	LOADING_SEARCH_VIDEOS,
	CLEAR_SEARCH_VIDEOS
} from './types';

export const getVideos = () => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_VIDEOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_VIDEOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreVideos = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_VIDEOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_VIDEOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearVideos = () => (dispatch) => {
	dispatch({
		type: CLEAR_VIDEOS
	});
}

export const getCategoryVideos = (category) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/categories/v/${category.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_CATEGORY_VIDEOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_VIDEOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreCategoryVideos = (category, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/categories/v/${category.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_CATEGORY_VIDEOS,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategoryVideos = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORY_VIDEOS
	})
}

export const getTagVideos = (tag) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/tags/v/${tag.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_TAG_VIDEOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_VIDEOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreTagVideos = (tag, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/tags/v/${tag.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_TAG_VIDEOS,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTagVideos = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAG_VIDEOS
	})
}

export const getSearchVideos = (searchText) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/search/${searchText}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_SEARCH_VIDEOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_SEARCH_VIDEOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreSearchVideos = (searchText, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/videos/search/${searchText}?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_SEARCH_VIDEOS,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearSearchVideos = () => (dispatch) => {
	dispatch({
		type: CLEAR_SEARCH_VIDEOS
	})
}








