import axios from 'axios';
import { URL, tokenConfig} from './common';
import { createMessage, returnErrors } from "./messages";

import { 
	GET_DASHBOARD_SETTINGS,
	LOADING_DASHBOARD_SETTINGS,
	EDIT_DASHBOARD_SETTINGS, 


	GET_DASHBOARD_POSTS,
	GET_DASHBOARD_POSTS_NEXT,
	LOADING_DASHBOARD_POSTS,
	GET_MORE_DASHBOARD_POSTS,
	GET_DASHBOARD_POSTS_ALL, 
	LOADING_DASHBOARD_POSTS_ALL,

	GET_DASHBOARD_PHOTO_ALBUMS,
	GET_DASHBOARD_PHOTO_ALBUMS_NEXT,
	LOADING_DASHBOARD_PHOTO_ALBUMS,
	GET_MORE_DASHBOARD_PHOTO_ALBUMS,
	GET_DASHBOARD_PHOTO_ALBUMS_ALL,
	LOADING_DASHBOARD_PHOTO_ALBUMS_ALL,

	GET_DASHBOARD_MUSIC_ALBUMS,
	GET_DASHBOARD_MUSIC_ALBUMS_NEXT,
	LOADING_DASHBOARD_MUSIC_ALBUMS,
	GET_MORE_DASHBOARD_MUSIC_ALBUMS,
	GET_DASHBOARD_MUSIC_ALBUMS_ALL,
	LOADING_DASHBOARD_MUSIC_ALBUMS_ALL,

	GET_DASHBOARD_VIDEOS,
	GET_DASHBOARD_VIDEOS_NEXT,
	LOADING_DASHBOARD_VIDEOS,
	GET_MORE_DASHBOARD_VIDEOS,
	GET_DASHBOARD_VIDEOS_ALL,
	LOADING_DASHBOARD_VIDEOS_ALL,

	GET_DASHBOARD_IMAGES,
	GET_DASHBOARD_IMAGES_NEXT,
	LOADING_DASHBOARD_IMAGES,
	GET_MORE_DASHBOARD_IMAGES,
	GET_DASHBOARD_IMAGES_ALL,
	LOADING_DASHBOARD_IMAGES_ALL,

	GET_DASHBOARD_CATEGORIES,
	LOADING_DASHBOARD_CATEGORIES,

	GET_DASHBOARD_TAGS,
	LOADING_DASHBOARD_TAGS,

	GET_DASHBOARD_LINKS,
	LOADING_DASHBOARD_LINKS,

	EDIT_POST,
	DELETE_POST,

	EDIT_PHOTO_ALBUM,
	DELETE_PHOTO_ALBUM,
	UPLOAD_PHOTO,
	EDIT_PHOTO,
	DELETE_PHOTO,

	EDIT_MUSIC_ALBUM,
	DELETE_MUSIC_ALBUM,
	UPLOAD_SONG,
	EDIT_SONG,
	DELETE_SONG,

	EDIT_VIDEO,
	DELETE_VIDEO,

	EDIT_IMAGE,
	DELETE_IMAGE,

	EDIT_LINK,
	DELETE_LINK,

	EDIT_CATEGORY,
	DELETE_CATEGORY,

	EDIT_TAG,
	DELETE_TAG,

	CHANGE_PASSWORD,
	PASSWORD_ERROR,

	UPLOAD_CSV,

	EDIT_POST_FAIL,
	DELETE_POST_FAIL,

	EDIT_PHOTO_ALBUM_FAIL,
	DELETE_PHOTO_ALBUM_FAIL,

	UPLOAD_PHOTO_FAIL,
	EDIT_PHOTO_FAIL,
	DELETE_PHOTO_FAIL,

	EDIT_MUSIC_ALBUM_FAIL,
	DELETE_MUSIC_ALBUM_FAIL,

	UPLOAD_TRACK_FAIL,
	EDIT_SONG_FAIL,
	DELETE_SONG_FAIL,

	EDIT_VIDEO_FAIL,
	DELETE_VIDEO_FAIL,

	EDIT_IMAGE_FAIL,
	DELETE_IMAGE_FAIL,

	EDIT_LINK_FAIL,
	DELETE_LINK_FAIL,

	EDIT_CATEGORY_FAIL,
	DELETE_CATEGORY_FAIL,

	EDIT_TAG_FAIL,
	DELETE_TAG_FAIL
} from './types';

export const getDashboardSettings = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/settings/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_SETTINGS,
				payload: res.data[0]
			});
			dispatch({
				type: LOADING_DASHBOARD_SETTINGS,
				payload: false
			})
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardSettings = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_SETTINGS,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_SETTINGS,
		payload: true
	})
}

export const editDashboardSettings = (settingsFile) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('name', settingsFile.name);
	formData.append('description', settingsFile.description);
	if (settingsFile.file !== null) {
		formData.append('image', settingsFile.file);
	} else {
		formData.append('image', '');
	}
	formData.append('show_blog', settingsFile.showBlog);
	formData.append('default_blog', settingsFile.defaultBlog);
	formData.append('show_portfolio', settingsFile.showPortfolio);
	formData.append('default_portfolio', settingsFile.defaultPortfolio);
	formData.append('show_photos', settingsFile.showPhotoAlbums);
	formData.append('default_photos', settingsFile.defaultPhotoAlbums);
	formData.append('show_music', settingsFile.showMusicAlbums);
	formData.append('default_music', settingsFile.defaultMusicAlbums);
	formData.append('show_videos', settingsFile.showVideos);
	formData.append('default_videos', settingsFile.defaultVideos);
	formData.append('show_images', settingsFile.showImages);
	formData.append('default_images', settingsFile.defaultImages);
	formData.append('show_creator', settingsFile.showCreator);
	formData.append('slug', settingsFile.slug);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	if (settingsFile.dashboardSettings !== undefined && settingsFile.dashboardSettings !== null) {
		axios
			.put(URL + `/api/admin/settings/${settingsFile.slug}/update/`, formData, config)
			.then(res => {
				dispatch(createMessage({saveSuccessful: "Changes saved."}));
				dispatch({
					type: EDIT_DASHBOARD_SETTINGS,
					payload: res.data
				});
			})
			.catch(err => 
				dispatch(returnErrors(err.response.data, err.response.status))
			);
	} else {
		axios
			.post(URL + `/api/admin/settings/`, formData, config)
			.then(res => {
				dispatch(createMessage({saveSuccessful: "Changes saved."}));
				dispatch({
					type: EDIT_DASHBOARD_SETTINGS,
					payload: res.data
				});
			})
			.catch(err => 
				dispatch(returnErrors(err.response.data, err.response.status))
			);
	}
}

export const clearEditDashboardSettings = () => (dispatch) => {
	dispatch({
		type: EDIT_DASHBOARD_SETTINGS,
		payload: null
	})
}

export const getDashboardBlogPosts = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/posts/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_POSTS,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_POSTS_NEXT,
				payload: res.data.next
			});
			dispatch({
				type: LOADING_DASHBOARD_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreDashboardBlogPosts = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/admin/dashboard/posts/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_DASHBOARD_POSTS,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_POSTS_NEXT,
				payload: res.data.next
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardBlogPosts = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_POSTS,
		payload: null
	});
	dispatch({
		type: GET_DASHBOARD_POSTS_NEXT,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_POSTS,
		payload: true
	})
}

export const getDashboardBlogPostsAll = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/posts/all/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_POSTS_ALL,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_POSTS_ALL,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardBlogPostsAll = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_POSTS_ALL,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_POSTS_ALL,
		payload: true
	});
}



export const getDashboardPhotoAlbums = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/albums/photos/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_PHOTO_ALBUMS,
				payload: res.data.results
			});
			dispatch({
				type:  GET_DASHBOARD_PHOTO_ALBUMS_NEXT,
				payload:  res.data.next
			});
			dispatch({
				type: LOADING_DASHBOARD_PHOTO_ALBUMS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreDashboardPhotoAlbums = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/admin/dashboard/albums/photos/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_DASHBOARD_PHOTO_ALBUMS,
				payload: res.data.results
			});
			dispatch({
				type:  GET_DASHBOARD_PHOTO_ALBUMS_NEXT,
				payload:  res.data.next
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardPhotoAlbums = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_PHOTO_ALBUMS,
		payload: null
	});
	dispatch({
		type:  GET_DASHBOARD_PHOTO_ALBUMS_NEXT,
		payload:  null
	});
	dispatch({
		type: LOADING_DASHBOARD_PHOTO_ALBUMS,
		payload: true
	})
}

export const getDashboardPhotoAlbumsAll = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/albums/photos/all/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_PHOTO_ALBUMS_ALL,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_PHOTO_ALBUMS_ALL,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardPhotoAlbumsAll = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_PHOTO_ALBUMS_ALL,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_PHOTO_ALBUMS_ALL,
		payload: true
	})
}

export const getDashboardMusicAlbums = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/albums/music/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_MUSIC_ALBUMS,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_MUSIC_ALBUMS_NEXT,
				payload: res.data.next
			});
			dispatch({
				type: LOADING_DASHBOARD_MUSIC_ALBUMS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreDashboardMusicAlbums = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/admin/dashboard/albums/music/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_DASHBOARD_MUSIC_ALBUMS,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_MUSIC_ALBUMS_NEXT,
				payload: res.data.next
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardMusicAlbums = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_MUSIC_ALBUMS,
		payload: null
	});
	dispatch({
		type: GET_DASHBOARD_MUSIC_ALBUMS_NEXT,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_MUSIC_ALBUMS,
		payload: true
	})
}

export const getDashboardMusicAlbumsAll = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/albums/music/all/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_MUSIC_ALBUMS_ALL,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_MUSIC_ALBUMS_ALL,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardMusicAlbumsAll = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_MUSIC_ALBUMS_ALL,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_MUSIC_ALBUMS_ALL,
		payload: true
	})
}

export const getDashboardVideos = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/videos/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_VIDEOS,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_VIDEOS_NEXT,
				payload: res.data.next
			});
			dispatch({
				type: LOADING_DASHBOARD_VIDEOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreDashboardVideos = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/admin/dashboard/videos/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_DASHBOARD_VIDEOS,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_VIDEOS_NEXT,
				payload: res.data.next
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardVideos = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_VIDEOS,
		payload: null
	});
	dispatch({
		type: GET_DASHBOARD_VIDEOS_NEXT,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_VIDEOS,
		payload: true
	})
}

export const getDashboardVideosAll = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/videos/all/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_VIDEOS_ALL,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_VIDEOS_ALL,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardVideosAll = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_VIDEOS_ALL,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_VIDEOS_ALL,
		payload: true
	})
}

export const getDashboardImages = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/images/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_IMAGES,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_IMAGES_NEXT,
				payload: res.data.next
			});
			dispatch({
				type: LOADING_DASHBOARD_IMAGES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreDashboardImages = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/admin/dashboard/images/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_DASHBOARD_IMAGES,
				payload: res.data.results
			});
			dispatch({
				type: GET_DASHBOARD_IMAGES_NEXT,
				payload: res.data.next
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardImages = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_IMAGES,
		payload: null
	});
	dispatch({
		type: GET_DASHBOARD_IMAGES_NEXT,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_IMAGES,
		payload: true
	})
}

export const getDashboardImagesAll = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/images/all/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_IMAGES_ALL,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_IMAGES_ALL,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardImagesAll = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_IMAGES_ALL,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_IMAGES_ALL,
		payload: true
	})
}

export const getDashboardLinks = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/links/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_LINKS,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_LINKS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardLinks = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_LINKS,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_LINKS,
		payload: true
	})
}

export const getDashboardCategories = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/categories/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_CATEGORIES,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_CATEGORIES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardCategories = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_CATEGORIES,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_CATEGORIES,
		payload: true
	})
}

export const getDashboardTags = () => (dispatch, getState) => {
	axios
		.get(URL + '/api/admin/dashboard/tags/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_TAGS,
				payload: res.data
			});
			dispatch({
				type: LOADING_DASHBOARD_TAGS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearDashboardTags = () => (dispatch) => {
	dispatch({
		type: GET_DASHBOARD_TAGS,
		payload: null
	});
	dispatch({
		type: LOADING_DASHBOARD_TAGS,
		payload: true
	})
}











export const editPost = (post) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', post.title);
	formData.append('body', post.body);
	formData.append('blog', post.blog);
	formData.append('blog_sticky', post.blogSticky);
	formData.append('portfolio', post.portfolio);
	formData.append('portfolio_sticky', post.portfolioSticky);
	formData.append('published', post.publish);
	if (post.file !== null) {
		formData.append('image', post.file);
	} else {
		formData.append('image', '');
	}
	if (post.imageHighlight !== null) {
		formData.append('image_highlight', post.imageHighlight.slug);
	} else {
		formData.append('image_highlight', '');
	}
	if (post.videoHighlight !== null) {
		formData.append('video_highlight', post.videoHighlight.slug);
	} else {
		formData.append('video_highlight', '');
	}
	if (post.photoAlbum !== null) {
		formData.append('photo_album', post.photoAlbum.slug);
	} else {
		formData.append('photo_album', '');
	}
	if (post.musicAlbum !== null) {
		formData.append('music_album', post.musicAlbum.slug);
	} else {
		formData.append('music_album', '');
	}
	if (post.category !== null) {
		formData.append('category', post.category.slug);
	} else {
		formData.append('category', '');
	}
	if (post.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < post.tags.length; i++) {
			tagsList.push(post.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}
	// videos
	if (post.videos.length > 0) {
		let videosList = [];
		for (var i = 0; i < post.videos.length; i++) {
			videosList.push(post.videos[i].slug);
		}
		formData.append('videos', videosList);
	} else {
		formData.append('videos', []);
	}
	// images
	if (post.images.length > 0) {
		let imagesList = [];
		for (var i = 0; i < post.images.length; i++) {
			imagesList.push(post.images[i].slug);
		}
		formData.append('images', imagesList);
	} else {
		formData.append('images', []);
	}
	// links
	if (post.links.length > 0) {
		let linksList = [];
		for (var i = 0; i < post.links.length; i++) {
			linksList.push(post.links[i].slug);
		}
		formData.append('links', linksList);
	} else {
		formData.append('links', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/posts/${post.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_POST,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_POST_FAIL,
				payload: true
			});
		});
}

export const clearEditPost = () => (dispatch) => {
	dispatch({
		type: EDIT_POST,
		payload: null
	})
}

export const clearEditPostFail = () => (dispatch) => {
	dispatch({
		type: EDIT_POST_FAIL,
		payload: null
	})
}

export const deletePost = (post) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: post.slug
	}

	axios
		.delete(URL + `/api/admin/posts/${post.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_POST,
				payload: post
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_POST_FAIL,
				payload: true
			});
		});
}

export const clearDeletePost = () => (dispatch) => {
	dispatch({
		type: DELETE_POST,
		payload: null
	});
}

export const clearDeletePostFail = () => (dispatch) => {
	dispatch({
		type: DELETE_POST_FAIL,
		payload: null
	});
}

export const editPhotoAlbum = (album) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', album.title);
	formData.append('description', album.description);
	formData.append('public', album.publicAlbum);
	if (album.category !== null) {
		formData.append('category', album.category.slug);
	} else {
		formData.append('category', '');
	}
	if (album.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < album.tags.length; i++) {
			tagsList.push(album.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/albums/photos/${album.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_PHOTO_ALBUM,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_PHOTO_ALBUM_FAIL,
				payload: true
			})
		});
}

export const clearEditPhotoAlbum = () => (dispatch)  => {
	dispatch({
		type: EDIT_PHOTO_ALBUM,
		payload: null
	});
}

export const clearEditPhotoAlbumFail = () => (dispatch) => {
	dispatch({
		type: EDIT_PHOTO_ALBUM_FAIL,
		payload: null
	});
}

export const deletePhotoAlbum = (album) => (dispatch, getState) => {
	console.log('Album: ', album);
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: album.slug
	}

	axios
		.delete(URL + `/api/admin/albums/photos/${album.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_PHOTO_ALBUM,
				payload: album
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_PHOTO_ALBUM_FAIL,
				payload: true
			});
		});
}

export const clearDeletePhotoAlbum = () => (dispatch) => {
	dispatch({
		type: DELETE_PHOTO_ALBUM,
		payload: null
	});
}

export const clearDeletePhotoAlbumFail = () => (dispatch) => {
	dispatch({
		type: DELETE_PHOTO_ALBUM_FAIL,
		payload: null
	});
}

export const uploadPhoto = (photo) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', photo.title);
	if (photo.file !== null && photo.file !== undefined) {
		formData.append('image', photo.file);
	} else {
		formData.append('image', '');
	}
	formData.append('album-slug', photo.albumSlug);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = `<span style="font-size: 20, font-weight: 'bold'">Uploading... ` + percentCompleted + `%</span>`;
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/photos/create/', formData, config)
		.then(res => {
			dispatch({
				type: UPLOAD_PHOTO,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: UPLOAD_PHOTO_FAIL,
				payload: true
			});
		});
}

export const clearUploadPhoto = () => (dispatch) => {
	dispatch({
		type: UPLOAD_PHOTO,
		payload: null
	});
}

export const clearUploadPhotoFail = () => (dispatch) => {
	dispatch({
		type: UPLOAD_PHOTO_FAIL,
		payload: null
	});
}

export const editPhoto = (photo) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', photo.title);
	formData.append('image', photo.file);
	formData.append('slug', photo.slug)

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = `<span style="font-size: 20, font-weight: 'bold'">Uploading... ` + percentCompleted + `%</span>`;
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/photos/${photo.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_PHOTO,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_PHOTO_FAIL,
				payload: true
			});
		});
}

export const clearEditPhoto = () => (dispatch) => {
	dispatch({
		type: EDIT_PHOTO,
		payload: null
	});
}

export const clearEditPhotoFail = () => (dispatch) => {
	dispatch({
		type: EDIT_PHOTO_FAIL,
		payload: null
	});
}

export const deletePhoto = (photo) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: photo.slug
	}

	axios
		.delete(URL + `/api/admin/photos/${photo.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_PHOTO,
				payload: photo
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_PHOTO_FAIL,
				payload: true
			});
		});
}

export const clearDeletePhoto = () => (dispatch) => {
	dispatch({
		type: DELETE_PHOTO,
		payload: null
	});
}

export const clearDeletePhotoFail = () => (dispatch) => {
	dispatch({
		type: DELETE_PHOTO_FAIL,
		payload: null
	});
}

export const editMusicAlbum = (album) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('band_name', album.bandName);
	formData.append('title', album.title);
	formData.append('description', album.description);
	if (album.file !== null) {
		formData.append('image', album.file);
	} else {
		formData.append('image', '');
	}
	formData.append('public', album.publicAlbum);
	if (album.category !== null) {
		formData.append('category', album.category.slug);
	} else {
		formData.append('category', '');
	}
	if (album.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < album.tags.length; i++) {
			tagsList.push(album.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/albums/music/${album.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_MUSIC_ALBUM,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_MUSIC_ALBUM_FAIL,
				payload: true
			});
		});
}

export const clearEditMusicAlbum = () => (dispatch) => {
	dispatch({
		type: EDIT_MUSIC_ALBUM,
		payload: null
	});
}

export const clearEditMusicAlbumFail = () => (dispatch) => {
	dispatch({
		type: EDIT_MUSIC_ALBUM_FAIL,
		payload: null
	});
}

export const deleteMusicAlbum = (album) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: album.slug
	}

	axios
		.delete(URL + `/api/admin/albums/music/${album.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_MUSIC_ALBUM,
				payload: album
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_MUSIC_ALBUM_FAIL,
				payload: true
			});
		});
}

export const clearDeleteMusicAlbum = () => (dispatch) => {
	dispatch({
		type: DELETE_MUSIC_ALBUM,
		payload: null
	});
}

export const clearDeleteMusicAlbumFail = () => (dispatch) => {
	dispatch({
		type: DELETE_MUSIC_ALBUM_FAIL,
		payload: null
	});
}

export const uploadMusicTrack = (track) => (dispatch, getState) => {
	var formData = new FormData();
	formData.append('title', track.title);
	formData.append('description', track.description);
	formData.append('soundcloud_embed', track.soundcloud);
	if (track.file !== null) {
		formData.append('image', track.file);
	} else {
		formData.append('image', '');
	}
	if (track.audio !== null) {
		formData.append('audio_file', track.audio);
	} else {
		formData.append('audio_file', '');
	}
	formData.append('youtube_url', track.youtube_url);
	formData.append('album-slug', track.albumSlug);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/admin/tracks/create/', formData, config)
		.then(res => {
			console.log('Res: ', res);
			dispatch({
				type: UPLOAD_SONG,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: UPLOAD_TRACK_FAIL,
				payload: true
			});
		});
}

export const clearUploadMusicTrack = () => (dispatch) => {
	dispatch({
		type: UPLOAD_SONG,
		payload: null
	});
}

export const clearUploadMusicTrackFail = () => (dispatch) => {
	dispatch({
		type: UPLOAD_TRACK_FAIL,
		payload: null
	});
}

export const editMusicTrack = (track) => (dispatch, getState) => {
	console.log('Track:  ', track);
	var formData = new FormData();
	formData.append('title', track.title);
	formData.append('description', track.description);
	formData.append('soundcloud_embed', track.soundcloud);
	if (track.file !== null && track.file !== undefined) {
		formData.append('image', track.file);
	} else {
		formData.append('image', '');
	}
	if (track.audio !== null) {
		formData.append('audio_file', track.audio);
	} else {
		formData.append('audio_file', '');
	}
	formData.append('youtube_url', track.youtube_url);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/tracks/${track.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_SONG,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_SONG_FAIL,
				payload: true
			});
		});
}

export const clearEditMusicTrack = () => (dispatch) => {
	dispatch({
		type: EDIT_SONG,
		payload: null
	});
}

export const clearEditMusicTrackFail = () => (dispatch) => {
	dispatch({
		type: EDIT_SONG_FAIL,
		payload: null
	});
}

export const deleteMusicTrack = (track) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: track.slug
	}

	axios
		.delete(URL + `/api/admin/tracks/${track.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_SONG,
				payload: track
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_SONG_FAIL,
				payload: true
			});
		});
}

export const clearDeleteMusicTrack = () => (dispatch) => {
	dispatch({
		type: DELETE_SONG,
		payload: null
	});
}

export const clearDeleteMusicTrackFail = () => (dispatch) => {
	dispatch({
		type: DELETE_SONG_FAIL,
		payload: null
	});
}

export const editVideo = (video) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', video.title);
	formData.append('description', video.description);
	formData.append('youtube_id', video.youtubeID);
	formData.append('vimeo_id', video.vimeoID);
	if (video.file !== null) {
		formData.append('image', video.file);
	} else {
		formData.append('image', '');
	}
	formData.append('public', video.publicVideo);
	if (video.category !== null) {
		formData.append('category', video.category.slug);
	} else {
		formData.append('category', '');
	}
	if (video.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < video.tags.length; i++) {
			tagsList.push(video.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/videos/${video.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_VIDEO,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_VIDEO_FAIL,
				payload: true
			});
		});
}

export const clearEditVideo = () => (dispatch) => {
	dispatch({
		type: EDIT_VIDEO,
		payload: null
	});
}

export const clearEditVideoFail = () => (dispatch) => {
	dispatch({
		type: EDIT_VIDEO_FAIL,
		payload: null
	});
}

export const deleteVideo = (video) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: video.slug
	}

	axios
		.delete(URL + `/api/admin/videos/${video.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_VIDEO,
				payload: video
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_VIDEO_FAIL,
				payload: true
			});
		});
}

export const clearDeleteVideo = () => (dispatch) => {
	dispatch({
		type: DELETE_VIDEO,
		payload: null
	});
}

export const clearDeleteVideoFail = () => (dispatch) => {
	dispatch({
		type: DELETE_VIDEO_FAIL,
		payload: null
	});
}

export const editImage = (image) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('title', image.title);
	formData.append('description', image.description);
	formData.append('caption', image.caption);
	if (image.file !== null) {
		formData.append('image', image.file);
	} else {
		formData.append('image', '');
	}	
	formData.append('public', image.publicImage);
	if (image.category !== null) {
		formData.append('category', image.category.slug);
	} else {
		formData.append('category', '');
	}
	if (image.tags.length > 0) {
		let tagsList = [];
		for (var i = 0; i < image.tags.length; i++) {
			tagsList.push(image.tags[i].slug);
		}
		formData.append('tags', tagsList);
	} else {
		formData.append('tags', []);
	}

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/images/${image.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_IMAGE,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_IMAGE_FAIL,
				payload: true
			});
		});
}

export const clearEditImage = () => (dispatch) => {
	dispatch({
		type: EDIT_IMAGE,
		payload: null
	});
}

export const clearEditImageFail = () => (dispatch) => {
	dispatch({
		type: EDIT_IMAGE_FAIL,
		payload: null
	});
}

export const deleteImage = (image) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: image.slug
	}

	axios
		.delete(URL + `/api/admin/images/${image.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_IMAGE,
				payload: image
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_IMAGE_FAIL,
				payload: true
			});
		});
}

export const clearDeleteImage = () => (dispatch) => {
	dispatch({
		type: DELETE_IMAGE,
		payload: null
	});
}

export const clearDeleteImageFail = () => (dispatch) => {
	dispatch({
		type: DELETE_IMAGE_FAIL,
		payload: null
	});
}

export const editLink = (link) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('description', link.name);
	formData.set('link', link.linkURL);
	formData.set('social_media', link.socialMediaLink);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/links/${link.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_LINK,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_LINK_FAIL,
				payload: true
			});
		});
}

export const clearEditLink = () => (dispatch) => {
	dispatch({
		type: EDIT_LINK,
		payload: null
	});
}

export const clearEditLinkFail = () => (dispatch) => {
	dispatch({
		type: EDIT_LINK_FAIL,
		payload: null
	});
}

export const deleteLink = (link) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: link.slug
	}

	axios
		.delete(URL + `/api/admin/links/${link.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_LINK,
				payload: link
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_LINK_FAIL,
				payload: true
			});
		});
}

export const clearDeleteLink = () => (dispatch) => {
	dispatch({
		type: DELETE_LINK,
		payload: null
	});
}

export const clearDeleteLinkFail = () => (dispatch) => {
	dispatch({
		type: DELETE_LINK_FAIL,
		payload: null
	});
}

export const editCategory = (category) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('category', category.name);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/categories/${category.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_CATEGORY,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_CATEGORY_FAIL,
				payload: true
			});
		});
}

export const clearEditCategory = () => (dispatch) => {
	dispatch({
		type: EDIT_CATEGORY,
		payload: null
	});
}

export const clearEditCategoryFail = () => (dispatch) => {
	dispatch({
		type: EDIT_CATEGORY_FAIL,
		payload: null
	});
}

export const deleteCategory = (category) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: category.slug
	}

	axios
		.delete(URL + `/api/admin/categories/${category.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_CATEGORY,
				payload: category
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_CATEGORY_FAIL,
				payload: true
			});
		});
}

export const clearDeleteCategory = () => (dispatch) => {
	dispatch({
		type: DELETE_CATEGORY,
		payload: null
	});
}

export const clearDeleteCategoryFail = () => (dispatch) => {
	dispatch({
		type: DELETE_CATEGORY_FAIL,
		payload: null
	});
}

export const editTag = (tag) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('tag', tag.name);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/admin/tags/${tag.slug}/update/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: EDIT_TAG,
				payload: res.data
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: EDIT_TAG_FAIL,
				payload: true
			});
		});
}

export const clearEditTag = () => (dispatch) => {
	dispatch({
		type: EDIT_TAG,
		payload: null
	});
}

export const clearEditTagFail = () => (dispatch) => {
	dispatch({
		type: EDIT_TAG_FAIL,
		payload:  null
	});
}

export const deleteTag = (tag) => (dispatch, getState) => {
	const token = getState().admin.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: tag.slug
	}

	axios
		.delete(URL + `/api/admin/tags/${tag.slug}/update/`, {headers, data})
		.then(res => {
			dispatch(createMessage({deleteSuccessful: "Delete successful."}));
			dispatch({
				type: DELETE_TAG,
				payload: tag
			});
		})
		.catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: DELETE_TAG_FAIL,
				payload: true
			});
		});
}

export const clearDeleteTag = () => (dispatch) => {
	dispatch({
		type: DELETE_TAG,
		payload: null
	});
}

export const clearDeleteTagFail = () => (dispatch) => {
	dispatch({
		type: DELETE_TAG_FAIL,
		payload: null
	});
}




export const setNewPassword = (changePassword) => (dispatch, getState) => {
	var formData = new FormData();
	formData.set('old_password', changePassword.oldPassword);
	formData.append('new_password', changePassword.newPassword1)

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/change-password/`, formData, config)
		.then(res => {
			dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: CHANGE_PASSWORD,
				payload: res.data
			})
		})
		.catch(err => {
    	dispatch(returnErrors(err.response.data, err.response.status))
    	dispatch({type: PASSWORD_ERROR, payload: true})
   	});	
}

export const clearNewPassword = () => (dispatch) => {
	dispatch({
		type: CHANGE_PASSWORD,
		payload: null
	})
}

export const clearPasswordError = () => (dispatch) => {
	dispatch({
		type: PASSWORD_ERROR,
		payload: null
	})
}








export const uploadCSV = (file) => (dispatch, getState) => {
	console.log('File: ', file);
	var formData = new FormData();
	formData.set('csv', file);

  const token = getState().admin.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/admin/posts/import/`, formData, config)
		.then(res => {
			// dispatch(createMessage({saveSuccessful: "Changes saved."}));
			dispatch({
				type: UPLOAD_CSV,
				payload: res.data
			})
		})
		.catch(err => {
    	dispatch(returnErrors(err.response.data, err.response.status))
    	// dispatch({type: PASSWORD_ERROR, payload: true})
   	});	
}

export const resetUploadCSV = () => (dispatch) => {
	dispatch({
		type: UPLOAD_CSV,
		payload: null
	});
}












