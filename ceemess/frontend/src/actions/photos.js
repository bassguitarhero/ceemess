import axios from 'axios';
import { returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	GET_PHOTOS,
	LOADING_PHOTOS,
	GET_MORE_PHOTOS,
	LOADING_MORE_PHOTOS,
	CLEAR_PHOTOS,

	GET_CATEGORY_PHOTOS,
	GET_MORE_CATEGORY_PHOTOS,
	LOADING_CATEGORY_PHOTOS,
	CLEAR_CATEGORY_PHOTOS,

	GET_TAG_PHOTOS,
	GET_MORE_TAG_PHOTOS,
	LOADING_TAG_PHOTOS,
	CLEAR_TAG_PHOTOS,

	GET_SEARCH_PHOTOS,
	GET_MORE_SEARCH_PHOTOS,
	LOADING_SEARCH_PHOTOS,
	CLEAR_SEARCH_PHOTOS
} from './types';

export const getPhotoAlbums = () => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_PHOTOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_PHOTOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMorePhotoAlbums = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_PHOTOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_PHOTOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearPhotoAlbums = () => (dispatch) => {
	dispatch({
		type: CLEAR_PHOTOS
	});
}

export const getCategoryPhotos = (category) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/categories/v/${category.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_CATEGORY_PHOTOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_PHOTOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreCategoryPhotos = (category, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/categories/v/${category.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_CATEGORY_PHOTOS,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategoryPhotos = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORY_PHOTOS
	})
}

export const getTagPhotos = (tag) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/tags/v/${tag.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_TAG_PHOTOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_PHOTOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreTagPhotos = (tag, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/tags/v/${tag.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_TAG_PHOTOS,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTagPhotos = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAG_PHOTOS
	})
}

export const getSearchPhotos = (searchText) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/search/${searchText}`, tokenConfig(getState))
		.then(res  => {
			dispatch({
				type: GET_SEARCH_PHOTOS,
				payload: res.data
			});
			dispatch({
				type: LOADING_SEARCH_PHOTOS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreSearchPhotos = (searchText, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/photos/search/${searchText}?page=${page}`, tokenConfig(getState))
		.then(res  => {
			dispatch({
				type: GET_MORE_SEARCH_PHOTOS,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearSearchPhotos = () => (dispatch) => {
	dispatch({
		type: CLEAR_SEARCH_PHOTOS
	})
}









