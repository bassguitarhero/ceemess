import axios from 'axios';
import { returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	GET_PORTFOLIO_POSTS,
	LOADING_PORTFOLIO_POSTS,
	GET_STICKY_PORTFOLIO_POSTS,
	LOADING_STICKY_PORTFOLIO_POSTS,
	GET_MORE_PORTFOLIO_POSTS, 
	LOADING_MORE_PORTFOLIO_POSTS,
	CLEAR_PORTFOLIO_POSTS,

	GET_CATEGORY_PORTFOLIO,
	GET_MORE_CATEGORY_PORTFOLIO,
	LOADING_CATEGORY_PORTFOLIO,
	CLEAR_CATEGORY_PORTFOLIO,

	GET_TAG_PORTFOLIO,
	GET_MORE_TAG_PORTFOLIO,
	LOADING_TAG_PORTFOLIO,
	CLEAR_TAG_PORTFOLIO,

	GET_SEARCH_PORTFOLIO,
	GET_MORE_SEARCH_PORTFOLIO,
	LOADING_SEARCH_PORTFOLIO,
	CLEAR_SEARCH_PORTFOLIO
} from './types';

export const getPortfolioPosts = () => (dispatch, getState) => {
	axios 
		.get(URL + '/api/portfolio/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_PORTFOLIO_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_PORTFOLIO_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getStickyPortfolioPosts = () => (dispatch, getState) => {
	axios 
		.get(URL + '/api/portfolio/sticky/', tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_STICKY_PORTFOLIO_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_STICKY_PORTFOLIO_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMorePortfolioPosts = (page) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/portfolio/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_PORTFOLIO_POSTS,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_PORTFOLIO_POSTS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearPortfolioPosts = () => (dispatch) => {
	dispatch({
		type: CLEAR_PORTFOLIO_POSTS,
	});
}

export const getCategoryPortfolio = (category) => (dispatch, getState) => {
	axios
		.get(URL + `/api/portfolio/categories/v/${category.slug}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_CATEGORY_PORTFOLIO,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_PORTFOLIO,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreCategoryPortfolio = (category, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/portfolio/categories/v/${category.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_CATEGORY_PORTFOLIO,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategoryPortfolio = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORY_PORTFOLIO
	})
}

export const getTagPortfolio = (tag) => (dispatch, getState) => {
	axios
		.get(URL + `/api/portfolio/tags/v/${tag.slug}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_TAG_PORTFOLIO,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_PORTFOLIO,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreTagPortfolio = (tag, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/portfolio/tags/v/${tag.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_TAG_PORTFOLIO,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTagPortfolio = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAG_PORTFOLIO
	})
}

export const getSearchPortfolio = (searchText) => (dispatch, getState) => {
	axios
		.get(URL + `/api/portfolio/search/${searchText}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_SEARCH_PORTFOLIO,
				payload: res.data
			});
			dispatch({
				type: LOADING_SEARCH_PORTFOLIO,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreSearchPortfolio = (searchText, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/portfolio/search/${searchText}?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_SEARCH_PORTFOLIO,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearSearchPortfolio = () => (dispatch) => {
	dispatch({
		type: CLEAR_SEARCH_PORTFOLIO
	})
}
















