import axios from 'axios';
import { returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
	GET_MUSIC,
	LOADING_MUSIC,
	GET_MORE_MUSIC,
	LOADING_MORE_MUSIC,
	CLEAR_MUSIC,

	GET_CATEGORY_MUSIC,
	GET_MORE_CATEGORY_MUSIC,
	LOADING_CATEGORY_MUSIC,
	CLEAR_CATEGORY_MUSIC,

	GET_TAG_MUSIC,
	GET_MORE_TAG_MUSIC,
	LOADING_TAG_MUSIC,
	CLEAR_TAG_MUSIC,

	GET_SEARCH_MUSIC,
	GET_MORE_SEARCH_MUSIC,
	LOADING_SEARCH_MUSIC,
	CLEAR_SEARCH_MUSIC
} from './types';

export const getMusicAlbums = () => (dispatch, getState) => {
	axios
		.get(URL + `/api/music/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MUSIC,
				payload: res.data
			});
			dispatch({
				type: LOADING_MUSIC,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreMusicAlbums = (page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/music/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_MUSIC,
				payload: res.data
			});
			dispatch({
				type: LOADING_MORE_MUSIC,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearMusicAlbums = () => (dispatch) => {
	dispatch({
		type: CLEAR_MUSIC
	});
}

export const getCategoryMusic = (category) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/music/categories/v/${category.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_CATEGORY_MUSIC,
				payload: res.data
			});
			dispatch({
				type: LOADING_CATEGORY_MUSIC,
				payload: false
			})
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreCategoryMusic = (category, page) => (dispatch, getState) => {
	axios 
		.get(URL + `/api/music/categories/v/${category.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_CATEGORY_MUSIC,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearCategoryMusic = () => (dispatch) => {
	dispatch({
		type: CLEAR_CATEGORY_MUSIC
	})
}

export const getTagMusic = (tag) => (dispatch, getState) => {
	axios
		.get(URL + `/api/music/tags/v/${tag.slug}/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_TAG_MUSIC,
				payload: res.data
			});
			dispatch({
				type: LOADING_TAG_MUSIC,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreTagMusic = (tag, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/music/tags/v/${tag.slug}/?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_TAG_MUSIC,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearTagMusic = () => (dispatch) => {
	dispatch({
		type: CLEAR_TAG_MUSIC
	})
}

export const getSearchMusic = (searchText) => (dispatch, getState) => {
	axios
		.get(URL + `/api/music/search/${searchText}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_SEARCH_MUSIC,
				payload: res.data
			});
			dispatch({
				type: LOADING_SEARCH_MUSIC,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const getMoreSearchMusic = (searchText, page) => (dispatch, getState) => {
	axios
		.get(URL + `/api/music/search/${searchText}?page=${page}`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_MORE_SEARCH_MUSIC,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const clearSearchMusic = () => (dispatch) => {
	dispatch({
		type: CLEAR_SEARCH_MUSIC
	})
}









