import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import PortfolioLink from './PortfolioLink';
import PortfolioPostMusicAlbum from './PortfolioPostMusicAlbum';
import PortfolioPostPhotoAlbum from './PortfolioPostPhotoAlbum';
import PortfolioVideo from './PortfolioVideo';

class PortfolioPost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPortfolioPost: true,
			showPortfolioPhoto: false,
			photo: null
		}
	}

	handleGetTagPortfolio = (tag) => {
		this.props.closePortfolioPost();
		this.props.viewTagPortfolio(tag);
	}

	handleGetCategoryPortfolio = (category) => {
		this.props.closePortfolioPost();
		this.props.viewCategoryPortfolio(category);
	}

	handleClosePortfolioPhoto = () => {
		this.setState({
			showPortfolioPost: true,
			showPortfolioPhoto: false,
			photo: null
		});
	}

	handleShowPortfolioPhoto = (photo) => {
		this.setState({
			showPortfolioPost: false,
			showPortfolioPhoto: true,
			photo: photo
		});
	}

	handleClosePortfolioPost = () => {
		this.props.closePortfolioPost();
	}

	componentDidMount() {
		// console.log('Post: ', this.props.portfolioPost);
	}

	render() {
		function formatDateTime(s) {
			var fields 			= s.split('T');
			var date 				= fields[0];
			var time 				= fields[1];
			var dateFields 	= date.split('-');
			var timeFields 	= time.split(':');
			return dateFields[1]+'/'+dateFields[1]+'/'+dateFields[0]+', '+timeFields[0]+':'+timeFields[1];
		}

		const { showPortfolioPost, showPortfolioPhoto, photo } = this.state;
		const { portfolioPost } = this.props;

		return (
			<div>
				{showPortfolioPost && 
					<Grid container>
						{portfolioPost.image_highlight !== null ?
							<Grid item sm={3} xs={12}>
								<div style={styles.imageContainer}>
									<img src={portfolioPost.image_highlight.thumbnail} alt={portfolioPost.image_highlight.caption} style={styles.image} />
								</div>
							</Grid> : 
							<Grid item sm={3} xs={12}>
								<div style={styles.imageContainer}>
									<img src={portfolioPost.thumbnail} alt="Post Thumbnail" style={styles.image} />
								</div>
							</Grid>
						}
						<Grid container item sm={9} xs={12}>
							<div style={styles.post}>
								<Grid container item xs={12}>
									<Grid item xs={10}>
										<div style={styles.titleContainer}>
											<span style={styles.titleText}>{portfolioPost.title}</span>
										</div>
									</Grid>
									<Grid item xs={2} style={{textAlign: 'right'}}>
										<Button onClick={this.handleClosePortfolioPost} style={styles.closeButton}>X</Button>
									</Grid>
								</Grid>
								<Grid item xs={12}>
									<div style={styles.bodyContainer}>
										<p style={styles.bodyText}>{portfolioPost.body}</p>
									</div>
								</Grid>
								{portfolioPost.photo_album !== null && 
									<Grid item xs={12}>
										<PortfolioPostPhotoAlbum selectPortfolioPostPhoto={this.handleShowPortfolioPhoto} portfolioPostPhotoAlbum={portfolioPost.photo_album.album_photos} />
									</Grid>
								}
								{portfolioPost.music_album !== null &&
									<Grid item xs={12}>
										<PortfolioPostMusicAlbum portfolioPostMusicAlbum={portfolioPost.music_album.album_songs} />
									</Grid>
								}
								{portfolioPost.links.length > 0 && 
									<div>
										{portfolioPost.links.map((link, index) => (
											<PortfolioLink key={index} link={link} />
										))}
									</div>
								}
								{portfolioPost.videos.length > 0 &&
									<div>
										{portfolioPost.videos.map((video, index) => (
											<PortfolioVideo key={index} video={video} />
										))}
									</div>
								}
								<div style={styles.publishedContainer}>
									<span style={styles.publishedAt}>
										Published: {formatDateTime(portfolioPost.created_at)}
									</span>
								</div>
								<Grid container item xs={12}>
									<Grid item xs={6}>
											<Button onClick={this.handleGetCategoryPortfolio.bind(this, portfolioPost.category)} style={styles.categoryButton}>{portfolioPost.category.category}</Button>
									</Grid>
									<Grid item xs={6} style={styles.tagsContainer}>
										{portfolioPost.tags.map((tag, index) => (
											<Button key={index} onClick={this.handleGetTagPortfolio.bind(this, tag)} style={styles.tagButton}>{tag.tag}</Button>
										))}
									</Grid>
								</Grid>
							</div>
						</Grid>
					</Grid>
				}
				{showPortfolioPhoto && 
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={photo.image} alt={photo.title} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{photo.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleClosePortfolioPhoto} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

export default PortfolioPost;












