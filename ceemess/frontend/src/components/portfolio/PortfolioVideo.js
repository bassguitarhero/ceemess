import React, { Component } from 'react';
import { styles } from '../../Styles';

class PortfolioVideo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			landscapeMode: true,
			windowWidth: '',
			windowHeight: ''
		}
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		console.log('Video: ', this.props.video);
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { video } = this.props;
		const { landscapeMode } = this.state;

		return (
			<div>
				{(video.youtube_id !== '' && video.youtube_id !== null) && 
					<div style={styles.videoContainer}>
						{landscapeMode ? 
							<iframe src={`https://www.youtube.com/embed/${video.youtube_id}`}
								title="YouTube Landscape Mode"
	              frameBorder='0'
	              allow='autoplay; encrypted-media'
	              allowFullScreen
	              width="640" 
								height="360" 
	            /> : 
	            <iframe src={`https://www.youtube.com/embed/${video.youtube_id}`}
	            	title="YouTube Portrait Mode"
	              frameBorder='0'
	              allow='autoplay; encrypted-media'
	              allowFullScreen
	              width="100%"
	              height="360" 
	            />
						}
							
					</div>
				}
				{(video.vimeo_id !== '' && video.vimdeo_id != null) &&
					<div style={styles.videoContainer}>
						{landscapeMode ? 
							<iframe src={`https://player.vimeo.com/video/${video.vimeo_id}`}
								title="Vimeo Landscape Mode"
								width="640" 
								height="360" 
								frameBorder="0" 
								allow="autoplay; fullscreen" 
								allowFullScreen 
							/> :
							<iframe src={`https://player.vimeo.com/video/${video.vimeo_id}`}
								title="Vimeo Portrait Mode"
								width="100%" 
								frameBorder="0" 
								allow="autoplay; fullscreen" 
								allowFullScreen 
							/>
						}
          </div>
				}
			</div>
		);
	}
}

export default PortfolioVideo;




