import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class PortfolioPostInline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			landscapeMode: '',
			windowWidth: '',
			windowHeight: '',
			imageHeight: '180px'
		}
	}

	handleShowPortfolioPost = (post) => {
		this.props.showPortfolioPost(post);
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false, imageHeight: '150px'});
		} else {
			this.setState({landscapeMode: true, imageHeight: '180px'});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { portfolioPost } = this.props;

		return (
			<div>
				{(portfolioPost.image_highlight !== null) ? 
					<div>
						<Button onClick={this.handleShowPortfolioPost.bind(this, portfolioPost)}>
							<img src={portfolioPost.image_highlight.thumbnail} alt={portfolioPost.image_highlight.caption} style={styles.image} />
						</Button>
					</div> :
					<div>
						{portfolioPost.image !== null ? 
							<Button onClick={this.handleShowPortfolioPost.bind(this, portfolioPost)}>
								<img src={portfolioPost.thumbnail} alt="Post Thumbnail" style={styles.image} />
							</Button> :
							<div>
								No Image
							</div>
						}
					</div>
				}
			</div>
		);
	}
}

export default PortfolioPostInline;




