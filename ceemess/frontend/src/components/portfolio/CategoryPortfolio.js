import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import PortfolioPost from './PortfolioPost';
import PortfolioPostInline from './PortfolioPostInline';

import { getMoreCategoryPortfolio, clearCategoryPortfolio, getTagPortfolio, getCategoryPortfolio } from '../../actions/portfolio';

class CategoryPortfolio extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPortfolio: true,
			showPost: false,
			post: null
		}
	}

	handleShowPortfolioPost = (post) => {
		this.setState({
			portfolioPost: post,
			showPost: false,
			showPortfolio: true
		});
	}

	handleGetTagPortfolio = (tag) => {
		this.props.getTagPortfolio(tag);
		this.props.viewTagPortfolio(tag);
	}

	handleGetCategoryPortfolio = (category) => {
		this.props.getCategoryPortfolio(category);
		this.props.viewCategoryPortfolio(category);
	}

	handleLoadMorePortfolio = () => {
		const { category, categoryPortfolioNext } = this.props;
		var fields = categoryPortfolioNext.split('=');
		this.props.getMoreCategoryPortfolio(category, fields[1]);
	}

	handleClosePost = () => {
		this.setState({
			showPortfolio: true,
			showPost: false,
			post: null
		})
	}

	handleShowPost = (post) => {
		this.setState({
			post: post
		});
	}

	componentDidMount() {

	}

	componentDidUpdate(lastState) {
		if (this.state.post !== null && (lastState.post === null || lastState.post === undefined) && this.state.showPost === false) {
			this.setState({
				showPortfolio: false,
				showPost: true
			})
		}
	}

	componentWillUnmount() {
		this.props.clearCategoryPortfolio();
	}

	render() {
		const { categoryPortfolio, loadingCategoryPortfolio, categoryPortfolioNext } = this.props;
		const { category } = this.props;
		const { showPortfolio, showPost, post } = this.state;

		return (
			<div>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Category: {category.category}</span></div>
				</Grid>
				{showPost && 
					<div>
						<PortfolioPost closePortfolioPost={this.handleClosePost} portfolioPost={post} viewCategoryPortfolio={this.handleGetCategoryPortfolio} viewTagPortfolio={this.handleGetTagPortfolio} />
					</div>
				}
				{showPortfolio && 
					<div>
						{loadingCategoryPortfolio === false ? 
							<Grid container>
								{categoryPortfolio.map((post, index) => (
									<Grid key={index} item md={3} sm={4} xs={6}>
										<PortfolioPostInline key={index} showPortfolioPost={this.handleShowPost} portfolioPost={post} />
									</Grid>
								))}
								<Grid item xs={12}>
									{categoryPortfolioNext ? 
										<div style={styles.loadMoreContainer}>
											<Button style={styles.loadMoreButton} onClick={this.handleLoadMorePortfolio}>
												Load More
											</Button>
										</div> :
										<div style={styles.loadMoreContainer}>
											<span style={styles.text}>...</span>
										</div>
									}
								</Grid>
							</Grid> :
							<Grid container style={{flex: 1}}>
								<Grid md={2} item />
				        <Grid container item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Portfolio</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		categoryPortfolio: state.portfolio.categoryPortfolio,
		loadingCategoryPortfolio: state.portfolio.loadingCategoryPortfolio,
		categoryPortfolioNext: state.portfolio.categoryPortfolioNext
	}
}

export default connect(mapStateToProps, { getMoreCategoryPortfolio, clearCategoryPortfolio, getTagPortfolio, getCategoryPortfolio })(CategoryPortfolio);












