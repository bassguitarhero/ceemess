import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { getPortfolioPosts, getStickyPortfolioPosts, getMorePortfolioPosts, clearPortfolioPosts, getTagPortfolio, getCategoryPortfolio } from '../../actions/portfolio';

import PortfolioPostInline from './PortfolioPostInline';
import PortfolioPost from './PortfolioPost';

class PortfolioHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			landscapeMode: '',
			windowWidth: '',
			windowHeight: '',
			showPortfolioHome: true,
			showPortfolioPost: false,
			portfolioPost: null
		}
	}

	handleGetTagPortfolio = (tag) => {
		this.props.getTagPortfolio(tag);
		this.props.viewTagPortfolio(tag);
	}

	handleGetCategoryPortfolio = (category) => {
		this.props.getCategoryPortfolio(category);
		this.props.viewCategoryPortfolio(category);
	}

	handleGetMorePortfolioPosts = () => {
		const { portfolioPostsNext } = this.props;
		let fields = portfolioPostsNext.split('=');
		this.props.getMorePortfolioPosts(fields[1]);
	}

	handleClosePortfolioPost = () => {
		this.setState({
			portfolioPost: null,
			showPortfolioHome: true,
			showPortfolioPost: false
		});
	}

	handleShowPortfolioPost = (post) => {
		this.setState({
			portfolioPost: post,
			showPortfolioHome: false,
			showPortfolioPost: true
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.props.getPortfolioPosts();
		this.props.getStickyPortfolioPosts();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		this.props.clearPortfolioPosts();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { showPortfolioHome, showPortfolioPost, portfolioPost } = this.state;
		const { portfolioPosts, loadingPortfolioPosts, loadingStickyPortfolioPosts, portfolioPostsNext } = this.props;
		return (
			<div style={styles.container}>
				<Grid container>
					{showPortfolioHome && 
						<Grid container>
		       		{(loadingPortfolioPosts === false && loadingStickyPortfolioPosts === false) ?
		       			<Grid container item xs={12}>
		       				{portfolioPosts.map((post, index) => (
		       					<Grid key={index} item md={3} sm={4} xs={6}>
		       						<PortfolioPostInline key={index} portfolioPost={post} showPortfolioPost={this.handleShowPortfolioPost} />
		       					</Grid>
		       				))}
		       				<Grid item xs={12}>
		       					{portfolioPostsNext ? 
			       					<div style={styles.loadMoreContainer}>
												<Button style={styles.loadMoreButton} onClick={this.handleGetMorePortfolioPosts}>Load More</Button>
											</div> :
											<div style={styles.loadMoreContainer}>
												...
											</div>
			       				}
		       				</Grid>
		       			</Grid> :
		       			<div style={styles.textContainer}>
		       				<span style={styles.text}>Loading Portfolio Posts</span> <Dots />
		       			</div>
		       		}
		        </Grid>
					}
					{showPortfolioPost && 
						<PortfolioPost portfolioPost={portfolioPost} closePortfolioPost={this.handleClosePortfolioPost} viewCategoryPortfolio={this.handleGetCategoryPortfolio} viewTagPortfolio={this.handleGetTagPortfolio} />
					}
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		portfolioPosts: state.portfolio.portfolioPosts,
		loadingPortfolioPosts: state.portfolio.loadingPortfolioPosts,

		portfolioPostsNext: state.portfolio.portfolioPostsNext,
		portfolioPostsPrevious: state.portfolio.portfolioPostsPrevious,

		loadingStickyPortfolioPosts: state.portfolio.loadingStickyPortfolioPosts
	}
}

export default connect(mapStateToProps, { getPortfolioPosts, getStickyPortfolioPosts, getMorePortfolioPosts, clearPortfolioPosts, getTagPortfolio, getCategoryPortfolio })(PortfolioHome);












