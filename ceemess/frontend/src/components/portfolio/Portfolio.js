import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import PortfolioHome from './PortfolioHome';
import CategoryPortfolio from './CategoryPortfolio';
import TagPortfolio from './TagPortfolio';
import SearchPortfolio from './SearchPortfolio';
import PortfolioSidebar from './PortfolioSidebar';

class Portfolio extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPortfolioHome: true,
			showCategoryPortfolio: false,
			showTagPortfolio: false,
			showSearchPortfolio: false,
			category: null,
			tag: null,
			searchText: ''
		}
	}

	handleCloseCategoryPortfolio = () => {
		this.setState({
			category: null,
			showPortfolioHome: true,
			showCategoryPortfolio: false,
			showTagPortfolio: false,
			showSearchPortfolio: false,
		})
	}

	handleShowCategoryPortfolio = (category) => {
		this.setState({
			category: category,
			showPortfolioHome: false,
			showCategoryPortfolio: true,
			showTagPortfolio: false,
			showSearchPortfolio: false,
		})
	}

	handleCloseTagPortfolio = () => {
		this.setState({
			tag: null,
			showPortfolioHome: true,
			showCategoryPortfolio: false,
			showTagPortfolio: false,
			showSearchPortfolio: false,
		})
	}

	handleShowTagPortfolio = (tag) => {
		this.setState({
			tag: tag,
			showPortfolioHome: false,
			showCategoryPortfolio: false,
			showTagPortfolio: true,
			showSearchPortfolio: false,
		})
	}

	handleCloseSearchPortfolio = () => {
		this.setState({
			searchText: '',
			showPortfolioHome: true,
			showCategoryPortfolio: false,
			showTagPortfolio: false,
			showSearchPortfolio: false,
		})
	}

	handleShowSearchPortfolio = (searchText) => {
		this.setState({
			searchText: searchText,
			showPortfolioHome: false,
			showCategoryPortfolio: false,
			showTagPortfolio: false,
			showSearchPortfolio: true,
		})
	}

	render() {
		const { showPortfolioHome, showCategoryPortfolio, showTagPortfolio, showSearchPortfolio } = this.state;
		const { category, tag, searchText } = this.state;

		return (
			<div style={styles.container}>
				<Grid container>
          <Grid item md={1} />
          <Grid container item md={8} xs={12} spacing={2}>
						{showPortfolioHome && 
							<PortfolioHome viewCategoryPortfolio={this.handleShowCategoryPortfolio} viewTagPortfolio={this.handleShowTagPortfolio} />
						}
						{showCategoryPortfolio && 
							<CategoryPortfolio viewCategoryPortfolio={this.handleShowCategoryPortfolio} viewTagPortfolio={this.handleShowTagPortfolio} closeCategoryPortfolio={this.handleCloseCategoryPortfolio} category={category} />
						}
						{showTagPortfolio && 
							<TagPortfolio viewCategoryPortfolio={this.handleShowCategoryPortfolio} viewTagPortfolio={this.handleShowTagPortfolio} closeTagPortfolio={this.handleCloseTagPortfolio} tag={tag} />
						}
						{showSearchPortfolio && 
							<SearchPortfolio viewCategoryPortfolio={this.handleShowCategoryPortfolio} viewTagPortfolio={this.handleShowTagPortfolio} closeSearchPortfolio={this.handleCloseSearchPortfolio} searchText={searchText} />
						}
					</Grid>
					<Grid item xs={2}>
       			<PortfolioSidebar viewCategoryPortfolio={this.handleShowCategoryPortfolio} viewTagPortfolio={this.handleShowTagPortfolio} viewSearchPortfolio={this.handleShowSearchPortfolio} />
       		</Grid>
				</Grid>
			</div>
		);
	}
}

export default Portfolio;












