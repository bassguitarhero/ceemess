import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import ImagesHome from './ImagesHome';
import CategoryImages from './CategoryImages';
import TagImages from './TagImages';
import SearchImages from './SearchImages';
import ImagesSidebar from './ImagesSidebar';

class Images extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showImagesHome: true,
			showCategoryImages: false,
			showTagImages: false,
			showSearchImages: false,
			category: null,
			tag: null,
			searchText: ''
		}
	}

	handleCloseCategoryImages = () => {
		this.setState({
			category: null,
			showImagesHome: true,
			showCategoryImages: false,
			showTagImages: false,
			showSearchImages: false,
		})
	}

	handleShowCategoryImages = (category) => {
		this.setState({
			category: category,
			showImagesHome: false,
			showCategoryImages: true,
			showTagImages: false,
			showSearchImages: false,
		})
	}

	handleCloseTagImages = () => {
		this.setState({
			tag: null,
			showImagesHome: true,
			showCategoryImages: false,
			showTagImages: false,
			showSearchImages: false,
		})
	}

	handleShowTagImages = (tag) => {
		this.setState({
			tag: tag,
			showImagesHome: false,
			showCategoryImages: false,
			showTagImages: true,
			showSearchImages: false,
		})
	}

	handleShowSearchImages = (searchText) => {
		this.setState({
			searchText: searchText,
			showImagesHome: false,
			showCategoryImages: false,
			showTagImages: false,
			showSearchImages: true,
		})
	}

	handleCloseSearchImages = () => {
		this.setState({
			searchText: '',
			showImagesHome: true,
			showCategoryImages: false,
			showTagImages: false,
			showSearchImages: false,
		})
	}

	render() {
		const { showImagesHome, showCategoryImages, showTagImages, showSearchImages } = this.state;
		const { category, tag, searchText } = this.state;

		return (
			<div>
				<Grid container style={{flex: 1}}>
					<Grid md={1} item />
	        <Grid container md={8} item xs style={{flex: 1}}>
	        	{showImagesHome && 
	        		<ImagesHome viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} />
	        	}
	        	{showCategoryImages && 
	        		<CategoryImages viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} close={this.handleCloseCategoryImages} category={category} />
	        	}
	        	{showTagImages && 
	        		<TagImages viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} close={this.handleCloseTagImages} tag={tag} />
	        	}
	        	{showSearchImages && 
	        		<SearchImages viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} close={this.handleCloseSearchImages} searchText={searchText} />
	        	}
	        </Grid>
					<Grid container md={2} item xs style={{flex: 1}}>
						<ImagesSidebar viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} viewSearchImages={this.handleShowSearchImages} />
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Images;











