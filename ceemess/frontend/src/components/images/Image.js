import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class Image extends Component {
	handleShowCategoryImages = (category) => {
		this.props.viewCategoryImages(category);
	}

	handleShowTagImages = (tag) => {
		this.props.viewTagImages(tag);
	}

	handleCloseImage = () => {
		this.props.closeImage();
	}

	render() {
		const { image } = this.props;

		return (
			<div>
				<div style={styles.imageContainer}>
					<img src={image.image} alt={image.caption} style={styles.image} />
				</div>
				<div style={styles.captionContainer}>
					<span style={styles.captionText}>{image.caption}</span>
				</div>
				<div style={styles.closeImagePlaceHolder}>
					<div style={styles.closeImagePosition}>
						<div style={styles.closeImageCircle}>
							<Button onClick={this.handleCloseImage} style={styles.closeImageText}> X </Button>
						</div>
					</div>
				</div>
				<Grid container>
					<Grid item sm={6} xs={12}>
						{image.category !== null && 
							<Button style={styles.categoryButton} onClick={this.handleShowCategoryImages.bind(this, image.category)}>{image.category.category}</Button>
						}
					</Grid>
					<Grid item sm={6} xs={12} style={styles.tagsContainer}>
						{image.tags.map((tag, index) => (
							<Button key={index} style={styles.tagButton} onClick={this.handleShowTagImages.bind(this, tag)} >{tag.tag}</Button>
						))}
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Image;












