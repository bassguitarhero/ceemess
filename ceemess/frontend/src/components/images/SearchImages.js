import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import ImageInline from './ImageInline';
import Image from './Image';

import { getMoreSearchImages, clearSearchImages, getCategoryImages, getTagImages } from '../../actions/images';

class SearchImages extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showImages: true,
			showImage: false,
			image: null,
		}
	}

	handleShowCategoryImages = (category) => {
		this.props.getCategoryImages(category);
		this.props.viewCategoryImages(category);
	}

	handleShowTagImages = (tag) => {
		this.props.getTagImages(tag);
		this.props.viewTagImages(tag);
	}

	handleLoadMoreImages = () => {
		const { searchText, searchImagesNext } = this.props;
		var fields = searchImagesNext.split('=');
		this.props.getMoreSearchImages(searchText, fields[1]);
	}

	handleCloseImage = () => {
		this.setState({
			showImages: true,
			showImage: false,
			image: null
		});
	}

	handleShowImage = (image) => {
		this.setState({
			image: image
		});
	}

	componentDidMount() {

	}

	componentDidUpdate(lastState) {
		if (this.state.image !== null && lastState.image === null && this.state.showImage === false) {
			this.setState({
				showImage: true,
				showImages: false
			});
		}
	}

	componentWillUnmount() {
		this.props.clearSearchImages();
	}

	render() {
		const { searchImages, loadingSearchImages, searchImagesNext } = this.props;
		const { searchText } = this.props;
		const { showImages, showImage, image } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Search: {searchText}</span></div>
				</Grid>
				{showImage && 
					<div>
						<Image closeImage={this.handleCloseImage} image={image} viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} />
					</div>
				}
				{showImages && 
					<div style={{flex: 1}}>
						{loadingSearchImages === false ? 
							<div>
								{searchImages.map((image, index) => (
									<ImageInline key={index} showImage={this.handleShowImage} image={image} viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} />
								))}
								{searchImagesNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreImages}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Images</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		searchImages: state.images.searchImages,
		loadingSearchImages: state.images.loadingSearchImages,
		searchImagesNext: state.images.searchImagesNext
	}
}

export default connect(mapStateToProps, { getMoreSearchImages, clearSearchImages, getTagImages, getCategoryImages })(SearchImages);












