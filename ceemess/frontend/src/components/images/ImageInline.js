import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class ImageInline extends Component {
	handleShowCategoryImages = (category) => {
		this.props.viewCategoryImages(category);
	}

	handleShowTagImages = (tag) => {
		this.props.viewTagImages(tag);
	}

	handleSelectImage = (image) => {
		this.props.showImage(image);
	}

	componentDidMount() {
		// console.log('Image: ', this.props.image);
	}

	render() {
		function truncateDescription(s) {
			if (s) {
				if (s.length > 400) {
					return s.substring(0, 300) + '...';
				} else {
					return s;
				}
			}
		}
		
		const { image } = this.props;

		return (
			<Grid container style={{flex: 1}}>
				<Grid item sm={3}>
					<div style={styles.imageContainer}>
						<Button onClick={this.handleSelectImage.bind(this, image)}>
							<img src={image.thumbnail} alt={image.caption} style={styles.image} />
						</Button>
					</div>
				</Grid>
				<Grid container item sm={9}>
					<Grid item xs={12}>
						<div style={styles.titleContainer}><Button style={styles.title} onClick={this.handleSelectImage.bind(this, image)}>{image.title}</Button></div>
						<div style={styles.descriptionContainer}><span style={styles.descriptionText}>{truncateDescription(image.description)}</span></div>
					</Grid>
				</Grid>
				<Grid container item xs={12}>
					<Grid item sm={6} xs={12} style={styles.categoryContainer}>
						{image.category !== null && 
							<Button style={styles.categoryButton} onClick={this.handleShowCategoryImages.bind(this, image.category)}>{image.category.category}</Button>
						}
					</Grid>
					<Grid item sm={6} xs={12} style={styles.tagsContainer}>
						{image.tags.map((tag, index) => (
							<Button key={index} style={styles.tagButton} onClick={this.handleShowTagImages.bind(this, tag)} >{tag.tag}</Button>
						))}
					</Grid>
				</Grid>
				<Grid item xs={12}><hr style={styles.bottomLine} /></Grid>
			</Grid>
		);
	}
}

export default ImageInline;













