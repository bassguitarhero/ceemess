import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import ImageInline from './ImageInline';
import Image from './Image';

import { getImages, getMoreImages, clearImages, getCategoryImages, getTagImages } from '../../actions/images';

class ImagesHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showImages: true,
			showImage: false,
			image: null,
		}
	}

	handleShowCategoryImages = (category) => {
		this.props.getCategoryImages(category);
		this.props.viewCategoryImages(category);
	}

	handleShowTagImages = (tag) => {
		this.props.getTagImages(tag);
		this.props.viewTagImages(tag);
	}

	handleLoadMoreImages = () => {
		const { imagesNext } = this.props;
		var fields = imagesNext.split('=');
		this.props.getMoreImages(fields[1]);
	}

	handleCloseImage = () => {
		this.setState({
			showImages: true,
			showImage: false,
			image: null
		});
	}

	handleShowImage = (image) => {
		this.setState({
			image: image
		});
	}

	componentDidMount() {
		this.props.getImages();
	}

	componentDidUpdate(lastState) {
		if (this.state.image !== null && lastState.image === null && this.state.showImage === false) {
			this.setState({
				showImage: true,
				showImages: false
			});
		}
	}

	componentWillUnmount() {
		this.props.clearImages();
	}

	render() {
		const { showImage, showImages, image } = this.state;
		const { loadingImages, images, imagesNext } = this.props;

		return (
			<div style={{flex: 1}}>
				{showImage && 
					<div>
						<Image closeImage={this.handleCloseImage} image={image} viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} />
					</div>
				}
				{showImages && 
					<div style={{flex: 1}}>
						{loadingImages === false ? 
							<div>
								{images.map((image, index) => (
									<ImageInline key={index} showImage={this.handleShowImage} image={image} viewCategoryImages={this.handleShowCategoryImages} viewTagImages={this.handleShowTagImages} />
								))}
								{imagesNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreImages}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Images</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		images: state.images.images,
		loadingImages: state.images.loadingImages,
		imagesNext: state.images.imagesNext
	}
}

export default connect(mapStateToProps, { getImages, getMoreImages, clearImages, getCategoryImages, getTagImages })(ImagesHome);











