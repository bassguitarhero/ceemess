import React from 'react';

const backdrop = props => <div style={props.portrait ? styles.modalPortrait : styles.modal}><div style={styles.uploadModalHeader}><span style={styles.uploadModalHeaderText}>Uploading:</span></div><div style={styles.modalText} id="uploadProgress" /></div>;

export default backdrop;

const styles = {
	modalPortrait: {
		width: '90%',
		backgroundColor: '#FFF',
		boxShadow: '0 2px 8px rgba(0, 0, 0, .3)',
		borderRadius: 25,
		position: 'fixed',
		top: '4vh',
		left: '5%',
		maxHeight: '100vh',
		minHeight: '80vh',
		overflowY: 'auto',
		overflowScrolling: "touch",
		WebkitOverflowScrolling: "touch",
		zIndex: 1000,
	},
	modal: {
		width: '60%',
		backgroundColor: '#FFF',
		boxShadow: '0 2px 8px rgba(0, 0, 0, .3)',
		borderRadius: 25,
		position: 'fixed',
		top: '4vh',
		left: '20%',
		maxHeight: '100vh',
		minHeight: '80vh',
		overflowY: 'auto',
		overflowScrolling: "touch",
		WebkitOverflowScrolling: "touch",
		zIndex: 1000,
	},
	uploadModalHeader: {
		padding: 20
	},
	uploadModalHeaderText: {
		fontSize: 20,
		fontWeight: 'bold'
	},
	modalText: {
		justifyContent: 'center',
		alignItems: 'center',
		padding: 20,
		margin: '0 auto'
	}
}