import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { withAlert } from 'react-alert';
import PropTypes from "prop-types";

class Alerts extends Component {
	static propTypes = {
		error: PropTypes.object.isRequired
	}

	componentDidUpdate(prevProps) {
		const { error, alert, message } = this.props;
		if (error !== prevProps.error) {
			if (error.msg.name) alert.error(`Name: ${error.msg.name.join()}`);
      if (error.msg.email) alert.error(`Email: ${error.msg.email.join()}`);
      if (error.msg.message)
        alert.error(`Message: ${error.msg.message.join()}`);
      if (error.msg.non_field_errors)
        alert.error(error.msg.non_field_errors.join());
      if (error.msg.username) alert.error(error.msg.username.join());
      if (error.msg && error.status) alert.error(error.status, error.msg);
      if (error.msg.old_password) alert.error(`Password: ${error.msg.old_password.join()}`)
      if (error.msg.incorrect_credentials) 
      	alert.error(`Credentials: ${error.msg.incorrect_credentials.join()}`);
		}

		if (message !== prevProps.message) {
			if (message.newPostCreated) alert.success(message.newPostCreated);
			if (message.newPhotoAlbumCreated) alert.success(message.newPhotoAlbumCreated);
			if (message.newMusicAlbumCreated) alert.success(message.newMusicAlbumCreated);
			if (message.newVideoCreated) alert.success(message.newVideoCreated);
			if (message.newImageCreated) alert.success(message.newImageCreated);
			if (message.newLinkCreated) alert.success(message.newLinkCreated);
			if (message.newCategoryCreated) alert.success(message.newCategoryCreated);
			if (message.newTagCeated) alert.success(message.newTagCreated);
			if (message.saveSuccessful) alert.success(message.saveSuccessful);
			if (message.deleteSuccessful) alert.success(message.deleteSuccessful);
			if (message.loginSuccessful) alert.success(message.loginSuccessful);
			if (message.logoutSuccessful) alert.success(message.logoutSuccessful);
			if (message.requestSuccessful) alert.success(message.requestSuccessful);
			if (message.userCreated) alert.success(message.userCreated);
		}
	}

	render() {
		return (
			<Fragment />
		);
	}
}

const mapStateToProps = state => ({
  error: state.errors,
  message: state.messages
});

export default connect(mapStateToProps)(withAlert()(Alerts));