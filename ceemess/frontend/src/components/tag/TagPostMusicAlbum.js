import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class TagPostMusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFour: []
		}
	}

	render() {
		const { blogPostMusicAlbum } = this.props;

		return (
			<div>
				{blogPostMusicAlbum.map((song, index) => (
					<div key={index}>
						{song.audio_file && 
							<Grid container spacing={2} style={styles.songContainer}>
								<Grid item sm={4} xs={12}>
									<span>{song.title}</span>
								</Grid>
								<Grid item sm={8} xs={12}>
									<audio 
			              src={song.audio_file} 
			              style={styles.audioFilePlayer}  
			              controls 
			            />
								</Grid>
							</Grid>
						}
						{song.soundcloud_embed && 
							<div>
								Sound Cloud
							</div>
						}
					</div>
				))}
			</div>
		);
	}
}

export default TagPostMusicAlbum;












