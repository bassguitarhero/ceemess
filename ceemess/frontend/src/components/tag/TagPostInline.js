import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import TagPostInlinePhotoAlbum from './TagPostInlinePhotoAlbum';
import TagPostInlineMusicAlbum from './TagPostInlineMusicAlbum';
import TagPostVideoHighlight from './TagPostVideoHighlight';
import TagLink from './TagLink';

class TagPostInline extends Component {
	handleViewBlogPhoto = (photo) => {
		this.props.showTagPhoto(photo);
	}

	handleShowTagPost = (post) => {
		this.props.showTagPost(post);
	}

	render() {
		function truncateBodyText(s) {
			if (s.length > 300) {
				return s.substring(0, 300) + '...';
			} else {
				return s
			}
		}

		function formatDateTime(s) {
			var fields 			= s.split('T');
			var date 				= fields[0];
			var time 				= fields[1];
			var dateFields 	= date.split('-');
			var timeFields 	= time.split(':');
			return dateFields[1]+'/'+dateFields[1]+'/'+dateFields[0]+', '+timeFields[0]+':'+timeFields[1];
		}

		const { post } = this.props;

		return (
			<div style={styles.container}>
				<Grid container>
					{post.image_highlight !== null ?
						<Grid item sm={3} xs={12}>
							<div style={styles.imageContainer}>
								<Button onClick={this.handleShowTagPost.bind(this, post)}><img src={post.image_highlight.thumbnail} alt={post.image_highlight.caption} style={styles.image} /></Button>
							</div>
						</Grid> : 
						<Grid item sm={3} xs={12}>
							<div style={styles.imageContainer}>
								<Button onClick={this.handleShowTagPost.bind(this, post)}><img src={post.image} alt="Post Thumbnail" style={styles.image} /></Button>
							</div>
						</Grid>
					}
					<Grid container item sm={9} xs={12}>
						<div style={styles.post}>
							<Grid item xs={12}>
								<div style={styles.titleContainer}>
									<Button onClick={this.handleShowTagPost.bind(this, post)}><span style={styles.titleText}>{post.title}</span></Button>
								</div>
							</Grid>
							<Grid item xs={12}>
								<div style={styles.bodyContainer}>
									<span style={styles.bodyText}>{truncateBodyText(post.body)}</span>
								</div>
							</Grid>
							{post.photo_album !== null && 
								<Grid item xs={12}>
									<TagPostInlinePhotoAlbum selectPhoto={this.handleViewBlogPhoto} tagPostInlinePhotoAlbumPhotos={post.photo_album.album_photos} />
								</Grid>
							}
							{post.music_album !== null &&
								<Grid item xs={12}>
									<TagPostInlineMusicAlbum tagPostInlineMusicAlbumSongs={post.music_album.album_songs} />
								</Grid>
							}
							{post.video_highlight !== null && 
								<Grid item xs={12}>
									<TagPostVideoHighlight videoYouTubeID={post.video_highlight.youtube_id} videoVimeoID={post.video_highlight.vimeo_id} />
								</Grid>
							}
							{post.links.length > 0 && 
								<div>
									{post.links.map((link, index) => (
										<TagLink key={index} link={link} />
									))}
								</div>
							}
							<div style={styles.publishedContainer}>
								<span style={styles.publishedAt}>
									Published: {formatDateTime(post.created_at)}
								</span>
							</div>
							<Grid item xs={12}>
								<div style={styles.readMoreContainer}>
									<Button style={styles.readMoreButton} onClick={this.handleShowTagPost.bind(this, post)}>Read More</Button>
								</div>
							</Grid>
							<Grid container item xs={12}>
								<Grid item xs={6}>
									{post.category !== null &&
										<Button style={styles.categoryButton}>{post.category.category}</Button>
									}
								</Grid>
								<Grid item xs={6} style={styles.tagsContainer}>
									{post.tags.map((tag, index) => (
										<Button key={index} style={styles.tagButton}>{tag.tag}</Button>
									))}
								</Grid>
							</Grid>
						</div>
					</Grid>
				</Grid>
				<hr style={styles.bottomLine} />
			</div>
		);
	}
}

export default TagPostInline;











