import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { getTagPosts } from '../../actions/blog';
import { getTags, clearTags } from '../../actions/common';

import TagPostInline from './TagPostInline';
import TagPost from './TagPost';

class Tags extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showTags: true,
			showTagPosts: false,
			showTagPost: false,
			showTagPostPhoto: false,
			tagPost: null,
			tagPostPhoto: null,
			tagName: '',
			tag: null
		}
	}

	handleGetMoreTagPosts = () => {
		const { tagPostsNext } = this.props;
		console.log('Tag Posts Next: ', tagPostsNext);
		const { tag } = this.state;
		const fields = tagPostsNext.split('=');
		this.props.getMoreTagPosts(tag, fields[0]);
	}

	handleCloseTagPostPhoto = () => {
		this.setState({
			showTagPosts: true,
			showTagPostPhoto: false,
			tagPostPhoto: null
		});
	}

	handleShowTagPostPhoto = (photo) => {
		this.setState({
			showTagPosts: false,
			showTagPostPhoto: true,
			tagPostPhoto: photo
		});
	}

	handleCloseTagPost = () => {
		this.setState({
			showTagPosts: true,
			showTagPost: false,
			tagPost: null
		});
	}

	handleShowTagPost = (post) => {
		console.log('Post: ', post);
		this.setState({
			showTagPosts: false,
			showTagPost: true,
			showTagPostPhoto: false,
			tagPost: post
		});
	}

	handleSelectTag = (tag) => {
		this.props.getTagPosts(tag);
		this.setState({
			showTagPosts: true,
			showTagPostPhoto: false,
			tagName: tag.tag,
			tag: tag
		});
	}

	componentDidMount() {
		this.props.getTags();
	}

	componentWillUnmount() {
		this.props.clearTags();
	}

	render() {
		const { tags, loadingTags, tagPosts, loadingTagPosts, tagPostsNext } = this.props;
		const { showTags, showTagPosts, showTagPost, showTagPostPhoto, tagPost, tagPostPhoto, tagName } = this.state;

		return (
			<div>
				{showTags && 
					<Grid container>
						<Grid md={2} item />
            <Grid container md={8} item xs>
							{loadingTags === false ? 
								<div style={styles.tagsContainer}>
									{tags.map((tag, index) => (
										<Button key={index} style={styles.tagButton} onClick={this.handleSelectTag.bind(this, tag)}>
											{tag.tag}
										</Button>
									))}
								</div> :
								<div style={styles.textContainer}>
									<span style={styles.text}>Loading Tags </span> <Dots />
								</div>
							}
            </Grid>
          </Grid>
				}
				{showTagPosts && 
					<Grid container>
						<Grid md={2} item />
            <Grid container md={8} item xs>
            	<div style={styles.tagTitleContainer}>
            		<span style={styles.title}>Tag: {tagName}</span>
            	</div>
            	{loadingTagPosts === false ? 
            		<div>
            			{tagPosts.map((post, index) => (
            				<TagPostInline key={index} post={post} showTagPost={this.handleShowTagPost} showTagPhoto={this.handleShowTagPostPhoto} />
            			))}
            			{tagPostsNext ? 
										<div style={styles.textContainer}><Button style={styles.loadMoreButton} onClick={this.handleGetMoreTagPosts}>Load More</Button></div> :
										<div style={styles.textContainer}><span style={styles.text}>...</span></div>
									}
            		</div> :
            		<div style={styles.loadingTagsContainer}>
            			<span style={styles.loadingTagsText}>Loading Tag Posts</span> <Dots />
            		</div>
            	}
            </Grid>
          </Grid>
				}
				{showTagPost && 
					<div>
						<TagPost closeTagPost={this.handleCloseTagPost} post={tagPost} />
					</div>
				}
				{showTagPostPhoto && 
					<div>
						<div style={styles.tagPostPhotoImageContainer}>
							<img src={tagPostPhoto.image} alt={tagPostPhoto.caption	} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.tagPostPhotoCaptionContainer}>
							<span style={styles.tagPostPhotoCaption}>{tagPostPhoto.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseTagPostPhoto} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		tags: state.common.tags,
		loadingTags: state.common.loadingTags,

		tagPosts: state.blog.tagPosts,
		loadingTagPosts: state.blog.loadingTagPosts,
		tagPostsNext: state.blog.tagPostsNext
	}
}

export default connect(mapStateToProps, { getTags, getTagPosts, clearTags })(Tags);











