import React, { Component } from 'react';
import { styles } from '../../Styles';

class TagLink extends Component {
	render() {
		const { link } = this.props;

		return (
			<div style={styles.container}>
				<a href={link.link} target="_blank" rel="noopener noreferrer" style={styles.blogLink}><span style={styles.linkText}>{link.description}</span></a>
			</div>
		);
	}
}

export default TagLink;












