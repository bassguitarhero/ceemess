import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class TagPostPhotoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}

	handleButtonPush = (photo) => {
		// console.log('Photo: ', photo);
		this.props.selectBlogPostPhoto(photo);
	}

	render() {
		const { blogPostPhotoAlbum } = this.props;

		return (
			<Grid container>
				{blogPostPhotoAlbum.reverse().map((photo, index) => (
					<Grid item xs={3} key={index}>
						<Button onClick={this.handleButtonPush.bind(this, photo)}><img src={photo.thumbnail} alt={photo.title} style={styles.inlineAlbumImage} /></Button>
					</Grid>
				))}
			</Grid>
		);
	}
}

export default TagPostPhotoAlbum;












