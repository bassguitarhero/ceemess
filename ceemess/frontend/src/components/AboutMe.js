import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { connect } from 'react-redux';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../Styles';

import { getSocialMediaLinks, clearSocialMediaLinks } from '../actions/common';

class AboutMe extends Component {
	componentDidMount() {
		this.props.getSocialMediaLinks();
	}

	componentWillUnmount() {
		this.props.clearSocialMediaLinks();
	}

	render() {
		const { socialMediaLinks, loadingSocialMediaLinks } = this.props;

		return (
			<Grid container>
				<Grid md={1} item />
				<Grid container md={2} item xs>
					{this.props.aboutMeImage !== null && 
						<div style={styles.imageContainer}>
							<img src={this.props.aboutMeImage} alt="About Me Thumbnail" style={styles.aboutMeImage} />
						</div>
					}
				</Grid>
        <Grid container md={8} item xs>
					<div style={styles.titleContainer}>
						<span style={styles.title}>About Me:</span>
					</div>
					<div style={styles.bodyTextContainer}>
						<span style={styles.bodyText}>
							{this.props.aboutMeDescription}
						</span>
					</div>
					<div style={styles.bodyTextContainer}>
						{loadingSocialMediaLinks === false ? 
							<div>
								{socialMediaLinks.map((link, index) => (
									<a key={index} href={link.link} target="_BLANK" rel="noopener noreferrer">
										<span style={styles.bodyText}>{link.description}</span>
									</a>
								))}
							</div> : 
							<div>Loading Links <Dots /></div>
						}
					</div>
				</Grid>
			</Grid>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		socialMediaLinks: state.common.socialMediaLinks,
		loadingSocialMediaLinks: state.common.loadingSocialMediaLinks
	}
}

export default connect(mapStateToProps, { getSocialMediaLinks, clearSocialMediaLinks })(AboutMe);

