import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

class Contact extends Component {
	render() {
		return (
			<div style={styles.container}>
				<Grid container>
          <Grid md={2} item />
          <Grid md={8} item xs>
          	<div style={styles.titleContainer}><span style={styles.contactTitle}>Contact Me:</span></div>
						<div style={styles.textContainer}><span style={styles.contactText}>Contact me by email: jboyce.video /at/ gmail</span></div>
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Contact;

const styles = {
	container: {
		padding: 10
	},
	titleContainer: {
		textAlign: 'center',
		padding: 10
	},
	contactTitle: {
		fontSize: 24,
		fontWeight: 'bold'
	},
	textContainer: {
		padding: 10
	},
	contactText: {
		fontSize: 18
	}
}