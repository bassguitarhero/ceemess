import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import ReactPlayer from "react-player";
import { styles } from '../../Styles';

class TrackInline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			landscapeMode: '',
			windowWidth: '',
			windowHeight: '',
		}
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { track } = this.props;
		const { landscapeMode } = this.state;

		return (
			<Grid container item xs={12} style={{padding: 10}}>
				{track.audio_file && 
					<Grid container item spacing={1} style={styles.trackContainer}>
						<Grid item sm={4} xs={12}>
							<div style={{padding: 5, marginBottom: 10}}><span>{track.title}</span></div>
						</Grid>
						<Grid item sm={8} xs={12}>
							<div style={{flex: 1}}>
								<audio 
		              src={track.audio_file} 
		              style={styles.audioFilePlayer}  
		              controls 
		            />
	            </div>
						</Grid>
					</Grid>
				}
				{track.soundcloud_embed && 
					<div style={{flex: 1, textAlign: 'center'}}>
						<Grid container item xs={12} style={{flex: 1, textAlign: 'center'}}>
							<Grid item xs={4}>
								<ReactPlayer
					        url={track.soundcloud_embed} 
					        width={landscapeMode ? "640px" : "350px"}
					        style={{textAlign: 'center', alignSelf: 'center'}}
					      />
							</Grid>
						</Grid>
					</div>
				}
				{track.youtube_url && 
					<div style={{flex: 1, textAlign: 'center'}}>
						<Grid container item xs={12} style={{flex: 1, textAlign: 'center'}}>
							<Grid item xs={4}>
								<ReactPlayer
					        url={track.youtube_url} 
					        width={landscapeMode ? "640px" : "350px"}
					        style={{textAlign: 'center', alignSelf: 'center'}}
					      />
							</Grid>
						</Grid>
					</div>
				}
			</Grid>
		);
	}
}

export default TrackInline;












