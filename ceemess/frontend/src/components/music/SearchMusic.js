import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import MusicAlbumInline from './MusicAlbumInline';
import MusicAlbum from './MusicAlbum';

import { getMoreSearchMusic, clearSearchMusic, getCategoryMusic, getTagMusic } from '../../actions/music';

class SearchMusic extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicAlbums: true,
			showMusicAlbum: false,
			album: null
		}
	}

	handleGetTagMusic = (tag) => {
		this.props.getTagMusic(tag);
		this.props.viewTagMusic(tag);
	}

	handleGetCategoryMusic = (category) => {
		this.props.getCategoryMusic(category);
		this.props.viewCategoryMusic(category);
	}

	handleLoadMoreMusic = () => {
		const { searchText, searchMusicAlbumsNext } = this.props;
		var fields = searchMusicAlbumsNext.split('=');
		this.props.getMoreSearchMusic(searchText, fields[1]);
	}

	handleCloseMusic = () => {
		this.setState({
			showMusicAlbums: true,
			showMusicAlbum: false,
			album: null
		});
	}

	handleShowMusic = (album) => {
		this.setState({
			album: album
		});
	}

	componentDidMount() {

	}

	componentDidUpdate(lastState) {
		if (this.state.album !== null && lastState.album === null && this.state.showMusicAlbum === false) {
			this.setState({
				showMusicAlbums: false,
				showMusicAlbum: true,
			});
		}
	}

	componentWillUnmount() {
		this.props.clearSearchMusic();
	}

	render() {
		const { searchMusicAlbums, loadingSearchMusicAlbums, searchMusicAlbumsNext } = this.props;
		const { searchText } = this.props;
		const { showMusicAlbums, showMusicAlbum, album } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Search: {searchText}</span></div>
				</Grid>
				{showMusicAlbum && 
					<div>
						<MusicAlbum closeAlbum={this.handleCloseMusic} album={album} />
					</div>
				}
				{showMusicAlbums && 
					<div style={{flex: 1}}>
						{loadingSearchMusicAlbums === false ? 
							<div>
								{searchMusicAlbums.map((album, index) => (
									<MusicAlbumInline key={index} showAlbum={this.handleShowMusic} album={album} viewCategoryMusic={this.handleGetCategoryMusic} viewTagMusic={this.handleGetTagMusic} />
								))}
								{searchMusicAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreMusic}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Images</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		searchMusicAlbums: state.music.searchMusicAlbums,
		loadingSearchMusicAlbums: state.music.loadingSearchMusicAlbums,
		searchMusicAlbumsNext: state.music.searchMusicAlbumsNext
	}
}

export default connect(mapStateToProps, { getMoreSearchMusic, clearSearchMusic, getCategoryMusic, getTagMusic })(SearchMusic);












