import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { getMusicAlbums, getMoreMusicAlbums, clearMusicAlbums, getCategoryMusic, getTagMusic } from '../../actions/music';

import MusicAlbumInline from './MusicAlbumInline';
import MusicAlbum from './MusicAlbum';

class MusicHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicAlbums: true,
			showMusicAlbumInline: false,
			album: null
		}
	}

	handleGetTagMusic = (tag) => {
		this.props.getTagMusic(tag);
		this.props.viewTagMusic(tag);
	}

	handleGetCategoryMusic = (category) => {
		this.props.getCategoryMusic(category);
		this.props.viewCategoryMusic(category);
	}

	handleCloseAlbum = () => {
		this.setState({
			showMusicAlbums: true,
			showMusicAlbumInline: false,
			album: null
		});
	}

	handleShowAlbum = (album) => {
		this.setState({
			album: album
		})
	}

	handleGetMoreMusicAlbums = () => {
		const { musicAlbumsNext } = this.props;
		var fields = musicAlbumsNext.split('=');
		this.props.getMoreMusicAlbums(fields[1]);
	}

	componentDidMount() {
		this.props.getMusicAlbums();
	}

	componentDidUpdate(lastState) {
		if (this.state.album != null && lastState.album == null && this.state.showMusicAlbumInline === false) {
			this.setState({
				showMusicAlbums: false,
				showMusicAlbumInline: true
			})
		}
	}

	componentWillUnmount() {
		this.props.clearMusicAlbums();
	}

	render() {
		const { musicAlbums, loadingMusicAlbums, musicAlbumsNext } = this.props;
		const { showMusicAlbums, showMusicAlbumInline, album } = this.state;

		return (
			<div style={{flex: 1}}>
				{showMusicAlbums && 
					<div style={{flex: 1}}>
						{loadingMusicAlbums === false ? 
							<div>
								{musicAlbums.map((album, index) => (
									<MusicAlbumInline key={index} showAlbum={this.handleShowAlbum} album={album} viewCategoryMusic={this.handleGetCategoryMusic} viewTagMusic={this.handleGetTagMusic} />
								))}
								{musicAlbumsNext ? 
									<div style={styles.loadMoreContainer}><Button style={styles.loadMoreButton} onClick={this.handleGetMoreMusicAlbums}>Load More</Button></div> :
									<div style={styles.loadMoreContainer}><span style={styles.loadMoreText}>...</span></div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
				        	<div style={styles.loadingContainer}>
				        		<span>Loading Music Albums</span> <Dots />
				        	</div>
				        </Grid>
				      </Grid>
						}
					</div>		
				}
				{showMusicAlbumInline && 
					<MusicAlbum album={album} closeAlbum={this.handleCloseAlbum} />
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		musicAlbums: state.music.musicAlbums,
		loadingMusicAlbums: state.music.loadingMusicAlbums,
		musicAlbumsNext: state.music.musicAlbumsNext
	}
}

export default connect(mapStateToProps, { getMusicAlbums, getMoreMusicAlbums, clearMusicAlbums, getCategoryMusic, getTagMusic })(MusicHome);












