import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import MusicAlbumInline from './MusicAlbumInline';
import MusicAlbum from './MusicAlbum';

import { getMoreCategoryMusic, clearCategoryMusic, getCategoryMusic, getTagMusic } from '../../actions/music';

class CategoryMusic extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicAlbums: true,
			showMusicAlbum: false,
			album: null
		}
	}

	handleGetTagMusic = (tag) => {
		this.props.getTagMusic(tag);
		this.props.viewTagMusic(tag);
	}

	handleGetCategoryMusic = (category) => {
		this.props.getCategoryMusic(category);
		this.props.viewCategoryMusic(category);
	}

	handleLoadMoreMusic = () => {
		const { category, categoryMusicAlbumsNext } = this.props;
		var fields = categoryMusicAlbumsNext.split('=');
		this.props.getMoreCategoryMusic(category, fields[1]);
	}

	handleCloseMusic = () => {
		this.setState({
			showMusicAlbums: true,
			showMusicAlbum: false,
			album: null
		});
	}

	handleShowMusic = (album) => {
		this.setState({
			album: album
		});
	}

	componentDidMount() {

	}

	componentDidUpdate(lastState) {
		if (this.state.album !== null && lastState.album === null && this.state.showMusicAlbum === false) {
			this.setState({
				showMusicAlbums: false,
				showMusicAlbum: true,
			});
		}
	}

	componentWillUnmount() {
		this.props.clearCategoryMusic();
	}

	render() {
		const { categoryMusicAlbums, loadingCategoryMusicAlbums, categoryMusicAlbumsNext } = this.props;
		const { category } = this.props;
		const { showMusicAlbums, showMusicAlbum, album } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Category: {category.category}</span></div>
				</Grid>
				{showMusicAlbum && 
					<div>
						<MusicAlbum closeAlbum={this.handleCloseMusic} album={album} />
					</div>
				}
				{showMusicAlbums && 
					<div style={{flex: 1}}>
						{loadingCategoryMusicAlbums === false ? 
							<div>
								{categoryMusicAlbums.map((album, index) => (
									<MusicAlbumInline key={index} showAlbum={this.handleShowMusic} album={album} viewCategoryMusic={this.handleGetCategoryMusic} viewTagMusic={this.handleGetTagMusic} />
								))}
								{categoryMusicAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreMusic}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Images</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		categoryMusicAlbums: state.music.categoryMusicAlbums,
		loadingCategoryMusicAlbums: state.music.loadingCategoryMusicAlbums,
		categoryMusicAlbumsNext: state.music.categoryMusicAlbumsNext
	}
}

export default connect(mapStateToProps, { getMoreCategoryMusic, clearCategoryMusic, getCategoryMusic, getTagMusic })(CategoryMusic);












