import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import MusicAlbumInline from './MusicAlbumInline';
import MusicAlbum from './MusicAlbum';

import { getMoreTagMusic, clearTagMusic, getCategoryMusic, getTagMusic } from '../../actions/music';

class TagMusic extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicAlbums: true,
			showMusicAlbum: false,
			album: null
		}
	}

	handleGetTagMusic = (tag) => {
		this.props.getTagMusic(tag);
		this.props.viewTagMusic(tag);
	}

	handleGetCategoryMusic = (category) => {
		this.props.getCategoryMusic(category);
		this.props.viewCategoryMusic(category);
	}

	handleLoadMoreMusic = () => {
		const { tag, tagMusicAlbumsNext } = this.props;
		var fields = tagMusicAlbumsNext.split('=');
		this.props.getMoreTagMusic(tag, fields[1]);
	}

	handleCloseMusic = () => {
		this.setState({
			showMusicAlbums: true,
			showMusicAlbum: false,
			album: null
		});
	}

	handleShowMusic = (album) => {
		this.setState({
			album: album
		});
	}

	componentDidMount() {

	}

	componentDidUpdate(lastState) {
		if (this.state.album !== null && lastState.album === null && this.state.showMusicAlbum === false) {
			this.setState({
				showMusicAlbums: false,
				showMusicAlbum: true,
			});
		}
	}

	componentWillUnmount() {
		this.props.clearTagMusic();
	}

	render() {
		const { tagMusicAlbums, loadingTagMusicAlbums, tagMusicAlbumsNext } = this.props;
		const { tag } = this.props;
		const { showMusicAlbums, showMusicAlbum, album } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Tag: {tag.tag}</span></div>
				</Grid>
				{showMusicAlbum && 
					<div>
						<MusicAlbum closeAlbum={this.handleCloseMusic} album={album} />
					</div>
				}
				{showMusicAlbums && 
					<div style={{flex: 1}}>
						{loadingTagMusicAlbums === false ? 
							<div>
								{tagMusicAlbums.map((album, index) => (
									<MusicAlbumInline key={index} showAlbum={this.handleShowMusic} album={album} viewCategoryMusic={this.handleGetCategoryMusic} viewTagMusic={this.handleGetTagMusic} />
								))}
								{tagMusicAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreMusic}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Images</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		tagMusicAlbums: state.music.tagMusicAlbums,
		loadingTagMusicAlbums: state.music.loadingTagMusicAlbums,
		tagMusicAlbumsNext: state.music.tagMusicAlbumsNext
	}
}

export default connect(mapStateToProps, { getMoreTagMusic, clearTagMusic, getCategoryMusic, getTagMusic })(TagMusic);












