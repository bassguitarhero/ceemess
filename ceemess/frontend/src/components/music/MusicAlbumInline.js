import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

import TrackInline from './TrackInline';

class MusicAlbumInline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFour: []
		}
	}

	handleGetTagMusic = (tag) => {
		this.props.viewTagMusic(tag);
	}

	handleGetCategoryMusic = (category) => {
		this.props.viewCategoryMusic(category);
	}

	handleSelectAlbum = (album) => {
		this.props.showAlbum(album);
	}

	handleGetFirstFour = () => {
		const { album } = this.props;
		var firstFour = [];

		if (album.album_songs.length > 0) {
			if (album.album_songs.length >= 4) {
				for (var i = 0; i < 4; i++) {
					if (album.album_songs[i]) {
						firstFour.push(album.album_songs[i]);
						// console.log('Track ', i, ': ', album.album_songs[i]);
					}
				}
			} else {
				for (var i = 0; i < album.album_songs.length; i++) {
					if (album.album_songs[i]) {
						firstFour.push(album.album_songs[i]);
						// console.log('Track ', i, ': ', album.album_songs[i]);
					}
				}
			}
		}
		this.setState({
			firstFour: firstFour
		})
		// console.log('First Four: ', firstFour);
	}

	componentDidMount() {
		this.handleGetFirstFour();
	}

	render() {
		function truncateDescription(s) {
			if (s.length > 400) {
				return s.substring(0, 300) + '...';
			} else {
				return s;
			}
		}

		const { album } = this.props;
		const { firstFour } = this.state;

		return (
			<Grid container style={{flex: 1}}>
				<Grid item sm={3}>
					<div style={styles.imageContainer}>
						<Button onClick={this.handleSelectAlbum.bind(this, album)}>
							<img src={album.thumbnail} alt="Music Album Thumbnail" style={styles.albumImage} />
						</Button>
					</div>
				</Grid>
				<Grid container item md={8}>
					<Grid item xs={12}>
						<div style={styles.titleContainer}><Button style={styles.inlinePostTitleText} onClick={this.handleSelectAlbum.bind(this, album)}>{album.title}</Button></div>
						<div style={styles.descriptionContainer}><span style={styles.descriptionText}>{truncateDescription(album.description)}</span></div>
					</Grid>
				</Grid>
				<Grid container item xs={12} style={{flex: 1}}>
					{firstFour.map((track, index) => (
						<TrackInline key={index} track={track} />
					))}
					{album.album_songs.length > 4 && 
						<Grid item xs={12}>
							<div style={styles.readMoreContainer}><span style={{fontSize: 16}}>{album.album_songs.length} songs</span></div>
						</Grid>
					}
					<Grid item xs={12}>
						<div style={styles.readMoreContainer}>
							<Button style={styles.readMoreButton} onClick={this.handleSelectAlbum.bind(this, album)}>Read More</Button>
						</div>
					</Grid>
				</Grid>
				<Grid container item xs={12}>
					<Grid item sm={6} xs={12} style={styles.categoryContainer}>
						{album.category !== null && 
							<Button style={styles.categoryButton} onClick={this.handleGetCategoryMusic.bind(this, album.category)}>{album.category.category}</Button>
						}
					</Grid>
					<Grid item sm={6} xs={12} style={styles.tagsContainer}>
						{album.tags.map((tag, index) => (
							<Button key={index} style={styles.tagButton} onClick={this.handleGetTagMusic.bind(this, tag)} >{tag.tag}</Button>
						))}
					</Grid>
				</Grid>
				<Grid item xs={12}><hr style={styles.bottomLine} /></Grid>
			</Grid>
		);
	}
}

export default MusicAlbumInline;












