import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import MusicHome from './MusicHome';
import CategoryMusic from './CategoryMusic';
import TagMusic from './TagMusic';
import SearchMusic from './SearchMusic';
import MusicSidebar from './MusicSidebar';

class Music extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicHome: true,
			showCategoryMusic: false,
			showTagMusic: false,
			showSearchMusic: false,
			category: null,
			tag: null,
			searchText: ''
		}
	}

	handleCloseSearchMusic = () => {
		this.setState({
			searchText: '',
			showMusicHome: true,
			showCategoryMusic: false,
			showTagMusic: false,
			showSearchMusic: false,
		})
	}

	handleShowSearchMusic = (searchText) => {
		this.setState({
			searchText: searchText,
			showMusicHome: false,
			showCategoryMusic: false,
			showTagMusic: false,
			showSearchMusic: true,
		})
	}

	handleCloseTagMusic = () => {
		this.setState({
			tag: null,
			showMusicHome: true,
			showCategoryMusic: false,
			showTagMusic: false,
			showSearchMusic: false,
		})
	}

	handleShowTagMusic = (tag) => {
		this.setState({
			tag: tag,
			showMusicHome: false,
			showCategoryMusic: false,
			showTagMusic: true,
			showSearchMusic: false,
		})
	}

	handleCloseCategoryMusic = () => {
		this.setState({
			category: null,
			showMusicHome: true,
			showCategoryMusic: false,
			showTagMusic: false,
			showSearchMusic: false,
		})
	}

	handleShowCategoryMusic = (category) => {
		this.setState({
			category: category,
			showMusicHome: false,
			showCategoryMusic: true,
			showTagMusic: false,
			showSearchMusic: false,
		})
	}

	render() {
		const { showMusicHome, showCategoryMusic, showTagMusic, showSearchMusic } = this.state;
		const { category, tag, searchText } = this.state;

		return (
			<div style={{flex: 1, padding: 10}}>
				<Grid container style={{flex: 1}}>
					<Grid md={1} item />
	        <Grid container md={8} item xs style={{flex: 1}}>
						{showMusicHome && 
							<MusicHome viewCategoryMusic={this.handleShowCategoryMusic} viewTagMusic={this.handleShowTagMusic} />
						}
						{showCategoryMusic && 
							<CategoryMusic viewCategoryMusic={this.handleShowCategoryMusic} viewTagMusic={this.handleShowTagMusic} closeCategoryMusic={this.handleCloseCategoryMusic} category={category} />
						}
						{showTagMusic && 
							<TagMusic viewCategoryMusic={this.handleShowCategoryMusic} viewTagMusic={this.handleShowTagMusic} closeTagMusic={this.handleCloseTagMusic} tag={tag} />
						}
						{showSearchMusic && 
							<SearchMusic viewCategoryMusic={this.handleShowCategoryMusic} viewTagMusic={this.handleShowTagMusic} closeSearchMusic={this.handleCloseSearchMusic} searchText={searchText} />
						}
					</Grid>
					<Grid container md={2} item xs style={{flex: 1}}>
						<MusicSidebar viewCategoryMusic={this.handleShowCategoryMusic} viewTagMusic={this.handleShowTagMusic} viewSearchMusic={this.handleShowSearchMusic} />
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Music












