import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

import TrackInline from './TrackInline';

class MusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	handleGetTagMusic = (tag) => {
		this.props.viewTagMusic(tag);
	}

	handleGetCategoryMusic = (category) => {
		this.props.viewCategoryMusic(category);
	}

	handleCloseAlbum = () => {
		this.props.closeAlbum();
	}

	handleSelectAlbum = (album) => {
		this.props.showAlbum(album);
	}

	handleGetFirstFour = () => {
		const { album } = this.props;
		const { firstFour } = this.state;

		if (album.album_songs.length > 0) {
			for (var i = 0; i < 4; i++) {
				if (album.album_songs[i]) {
					firstFour.push(album.album_songs[i]);
					// console.log('Track ', i, ': ', album.album_songs[i]);
				}
			}
		}
		// console.log('First Four: ', firstFour);
	}

	componentDidMount() {
		this.handleGetFirstFour();
	}

	render() {
		function truncateDescription(s) {
			if (s.length > 400) {
				return s.substring(0, 300) + '...';
			} else {
				return s;
			}
		}

		const { album } = this.props;

		return (

			<Grid container style={{flex: 1, padding: 10}}>
				<Grid item sm={3}>
					<div style={styles.imageContainer}>
						<img src={album.thumbnail} alt="Music Album Thumbnail" style={styles.image} />
					</div>
				</Grid>
				<Grid container item md={8}>
					<Grid container item xs={12}>
						<Grid item xs={10}>
							<div style={styles.titleContainer}><span style={styles.titleText}>{album.title}</span></div>
						</Grid>
						<Grid item xs={2}>
							<Button onClick={this.handleCloseAlbum} style={styles.closeButton}>X</Button>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.descriptionContainer}><span style={styles.descriptionText}>{truncateDescription(album.description)}</span></div>
						</Grid>
					</Grid>
					<Grid container item xs={12}>
						{album.album_songs.map((track, index) => (
							<TrackInline key={index} track={track} />
						))}
					</Grid>
				</Grid>
				<Grid container item xs={12}>
					<Grid item sm={6} xs={12}>
						{album.category !== null && 
							<Button style={styles.categoryButton} onClick={this.handleGetCategoryMusic.bind(this, album.category)}>{album.category.category}</Button>
						}
					</Grid>
					<Grid item sm={6} xs={12} style={styles.tagsContainer}>
						{album.tags.map((tag, index) => (
							<Button key={index} style={styles.tagButton} onClick={this.handleGetTagMusic.bind(this, tag)} >{tag.tag}</Button>
						))}
					</Grid>
				</Grid>
			</Grid>
		);
	}
}

export default MusicAlbum;












