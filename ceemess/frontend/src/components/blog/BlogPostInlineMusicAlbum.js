import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import TrackInline from '../music/TrackInline';

class BlogPostInlineMusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFour: []
		}
	}

	componentDidMount() {
		const { blogPostMusicAlbum } = this.props;
		const { firstFour } = this.state;
		for (var i = 0; i < 4; i++) {
			if (blogPostMusicAlbum[i] !== undefined) {
				firstFour.push(blogPostMusicAlbum[i])
			}
		}
		this.setState({
			firstFour: firstFour
		});
	}

	render() {
		const { blogPostMusicAlbum } = this.props;
		const { firstFour } = this.state;

		return (
			<div style={styles.container}>
				{firstFour.map((song, index) => (
					<TrackInline track={song} />
				))}
				{blogPostMusicAlbum.length > 4 && 
					<div style={{flex: 1, textAlign: 'center'}}>
						<span>{blogPostMusicAlbum.length} songs</span>
					</div>
				}
			</div>
		);
	}
}

export default BlogPostInlineMusicAlbum;

