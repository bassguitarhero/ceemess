import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import FiberPinIcon from '@material-ui/icons/FiberPin';
import { styles } from '../../Styles';

import BlogPostInlineImageGallery from './BlogPostInlineImageGallery';
import BlogPostInlinePhotoAlbum from './BlogPostInlinePhotoAlbum';
import BlogPostInlineMusicAlbum from './BlogPostInlineMusicAlbum';
import BlogPostVideoHighlight from './BlogPostVideoHighlight';
import BlogLink from './BlogLink';

class BlogPostInline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			landscapeMode: true,
			windowWidth: '',
			windowHeight: ''
		}
	}

	handleShowImageGalleryImage = (image) => {
		this.props.viewImage(image);
	}

	handleSelectBlogTag = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleSelectBlogCategory = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleViewBlogPhoto = (photo) => {
		this.props.viewBlogPhoto(photo);
	}

	handleViewBlogPost = (post) => {
		this.props.viewBlogPost(post);
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		function truncateBodyText(s) {
			if (s.length > 300) {
				return s.substring(0, 300) + '...';
			} else {
				return s
			}
		}

		function formatDateTime(s) {
			var fields 			= s.split('T');
			var date 				= fields[0];
			var time 				= fields[1];
			var dateFields 	= date.split('-');
			var timeFields 	= time.split(':');
			var finalTime 	= timeFields[0];
			var timePeriod = '';
			if (timeFields[0] < 12) {
				timePeriod = 'AM';
				finalTime = timeFields[0];
			} else {
				timePeriod = 'PM';
				finalTime = timeFields[0] - 12;
			}
			return dateFields[1]+'/'+dateFields[2]+'/'+dateFields[0]+', '+finalTime+':'+timeFields[1]+' '+timePeriod;
		}
		
		const { blogPost } = this.props;

		return (
			<div style={{flex: 1}}>
				<Grid container>
					{blogPost.image_highlight !== null ?
						<Grid item sm={3} xs={12}>
							<div style={styles.imageContainer}>
								<Button onClick={this.handleViewBlogPost.bind(this, blogPost)}><img src={blogPost.image_highlight.thumbnail} alt={blogPost.image_highlight.caption} style={styles.image} /></Button>
							</div>
						</Grid> : 
						<Grid item sm={3} xs={12}>
							<div style={styles.imageContainer}>
								<Button onClick={this.handleViewBlogPost.bind(this, blogPost)}><img src={blogPost.thumbnail} alt="Post Thumbnail" style={styles.image} /></Button>
							</div>
						</Grid>
					}
					<Grid container item sm={9} xs={12} style={{flex: 1}}>
						<div style={{flex: 1}}>
							<Grid container>
								<Grid item xs={11}>
									<div style={styles.titleContainer}>
										<Button onClick={this.handleViewBlogPost.bind(this, blogPost)} style={styles.inlinePostTitleText}>{blogPost.title}</Button>
									</div>
								</Grid>
								<Grid item xs={1} style={{justifyContent: 'center', alignSelf: 'center'}}>
									{blogPost.sticky && <FiberPinIcon />}
								</Grid>
							</Grid>
							<Grid item xs={12}>
								<div style={styles.bodyContainer}>
									<span style={styles.bodyText}>{truncateBodyText(blogPost.body)}</span>
								</div>
							</Grid>
							{blogPost.image_gallery !== null &&
								<Grid item xs={12}>
									<BlogPostInlineImageGallery selectImage={this.handleShowImageGalleryImage} imageGallery={blogPost.image_gallery} />
								</Grid>
							}
							{blogPost.photo_album !== null && 
								<Grid item xs={12}>
									<BlogPostInlinePhotoAlbum selectPhoto={this.handleViewBlogPhoto} blogPostPhotoAlbum={blogPost.photo_album.album_photos} />
								</Grid>
							}
							{blogPost.music_album !== null &&
								<Grid item xs={12}>
									<BlogPostInlineMusicAlbum blogPostMusicAlbum={blogPost.music_album.album_songs} />
								</Grid>
							}
							{blogPost.video_highlight !== null && 
								<Grid item xs={12}>
									<BlogPostVideoHighlight videoYouTubeID={blogPost.video_highlight.youtube_id} videoVimeoID={blogPost.video_highlight.vimeo_id} />
								</Grid>
							}
							{blogPost.links.length > 0 && 
								<div>
									{blogPost.links.map((link, index) => (
										<BlogLink key={index} link={link} />
									))}
								</div>
							}
							<div style={styles.publishedContainer}>
								{this.props.showCreator ? 
									<span style={styles.publishedAt}>
										Published: {formatDateTime(blogPost.created_at)} by {blogPost.created_by.username}
									</span> : 
									<span style={styles.publishedAt}>
										Published: {formatDateTime(blogPost.created_at)}
									</span> 
								}
							</div>
							<Grid item xs={12}>
								<div style={styles.readMoreContainer}>
									<Button style={styles.readMoreButton} onClick={this.handleViewBlogPost.bind(this, blogPost)}>Read More</Button>
								</div>
							</Grid>
							<Grid container item xs={12}>
								<Grid item sm={6} xs={12} style={styles.categoryContainer}>
									{blogPost.category !== null && 
										<Button style={styles.categoryButton} onClick={this.handleSelectBlogCategory.bind(this, blogPost.category)}>{blogPost.category.category}</Button>
									}
								</Grid>
								<Grid item sm={6} xs={12} style={styles.tagsContainer}>
									{blogPost.tags.map((tag, index) => (
										<Button key={index} style={styles.tagButton} onClick={this.handleSelectBlogTag.bind(this, tag)} >{tag.tag}</Button>
									))}
								</Grid>
							</Grid>
						</div>
					</Grid>
				</Grid>
				<hr style={styles.bottomLine} />
			</div>
		);
	}
}

export default BlogPostInline;












