import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import BlogPostPhotoAlbum from './BlogPostPhotoAlbum';
import BlogPostImageGallery from './BlogPostImageGallery';
import BlogPostMusicAlbum from './BlogPostMusicAlbum';
import BlogPostVideoHighlight from './BlogPostVideoHighlight';
import BlogLink from './BlogLink';
import BlogSidebar from './BlogSidebar';

import { getBlogPost, clearBlogPost, getCategoryPosts, getTagPosts, getSearchPosts } from '../../actions/blog';

class BlogPost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showBlogPost: true,
			showBlogPostPhoto: false,
			showImageGalleryImage: false,
			landscapeMode: '',
			windowWidth: '',
			windowHeight: '',
			photoData: '',
			image: null
		}
	}

	handleSelectBlogTag = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleSelectBlogCategory = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleViewBlogSearchPosts = () => {
		this.props.viewBlogSearchPosts();
	}

	handleGetSearchText = (searchText) => {
		this.setState({
			searchText: searchText
		});
	}

	handleCloseImageGalleryImage = () => {
		this.setState({
			image: null,
			showImageGalleryImage: false,
			showBlogPost: true
		})
	}

	handleSelectImageGalleryImage = (image) => {
		this.setState({
			image: image,
			showImageGalleryImage: true,
			showBlogPost: false
		});
	}

	handleCloseBlogPostPhoto = () => {
		this.setState({
			photoData: '',
			showBlogPost: true,
			showBlogPostPhoto: false,
			showImageGalleryImage: false,
		});
	}

	handleSelectBlogPostPhoto = (photo) => {
		this.setState({
			photoData: photo,
			showBlogPost: false,
			showBlogPostPhoto: true,
			showImageGalleryImage: false,
		});
	}

	handleCloseBlogPost = () => {
		this.props.closeBlogPost();
		// if (this.props.category !== null && this.props.category !== undefined) {
		// 	this.props.getCategoryPosts(this.props.category);
		// }
		// if (this.props.tag !== null && this.props.tag !== undefined) {
		// 	this.props.getTagPosts(this.props.tag);
		// }
		// if (this.props.searchText !== null && this.props.searchText !== undefined) {
		// 	this.props.getSearchPosts(this.props.searchText);
		// }
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {

	}

	componentWillUnmount() {
		this.props.clearBlogPost();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		function formatDateTime(s) {
			var fields 			= s.split('T');
			var date 				= fields[0];
			var time 				= fields[1];
			var dateFields 	= date.split('-');
			var timeFields 	= time.split(':');
			return dateFields[1]+'/'+dateFields[1]+'/'+dateFields[0]+', '+timeFields[0]+':'+timeFields[1];
		}

		const { showBlogPost, showBlogPostPhoto, photoData, image, showImageGalleryImage } = this.state;
		const { blogPost } = this.props;

		return (
			<div style={styles.container}>
				{showBlogPost && 
					<Grid container>
						<Grid md={1} item />
            <Grid container md={8} item xs>
							{blogPost.image_highlight !== null ?
								<Grid item sm={3} xs={12}>
									<div style={styles.imageContainer}>
										<img src={blogPost.image_highlight.thumbnail} alt={blogPost.image_highlight.caption} style={styles.image} />
									</div>
								</Grid> : 
								<Grid item sm={3} xs={12}>
									<div style={styles.imageContainer}>
										<img src={blogPost.thumbnail} alt="Post Thumbnail" style={styles.image} />
									</div>
								</Grid>
							}
							<Grid container item sm={9} xs={12}>
								<div style={styles.post}>
									<Grid container item xs={12}>
										<Grid item xs={10}>
											<div style={styles.titleContainer}>
												<span style={styles.titleText}>{blogPost.title}</span>
											</div>
										</Grid>
										<Grid item xs={2} style={{flex: 1, textAlign: 'right'}}>
											<div style={{flex: 1, textAlign: 'right'}}><Button onClick={this.handleCloseBlogPost} style={styles.closeButton}>X</Button></div>
										</Grid>
									</Grid>
									<Grid item xs={12}>
										<div style={styles.bodyContainer}>
											<span style={styles.bodyText}>{blogPost.body}</span>
										</div>
									</Grid>
									{blogPost.photo_album !== null && 
										<Grid item xs={12}>
											<BlogPostPhotoAlbum selectBlogPostPhoto={this.handleSelectBlogPostPhoto} blogPostPhotoAlbum={blogPost.photo_album.album_photos} />
										</Grid>
									}
									{blogPost.image_gallery !== null && 
										<Grid item xs={12}>
											<BlogPostImageGallery showImageGalleryImage={this.handleSelectImageGalleryImage} imageGallery={blogPost.image_gallery} />
										</Grid>
									}
									{blogPost.music_album !== null &&
										<Grid item xs={12}>
											<BlogPostMusicAlbum blogPostMusicAlbum={blogPost.music_album.album_songs} />
										</Grid>
									}
									{blogPost.video_highlight !== null && 
										<Grid item xs={12}>
											<BlogPostVideoHighlight videoYouTubeID={blogPost.video_highlight.youtube_id} videoVimeoID={blogPost.video_highlight.vimeo_id} />
										</Grid>
									}
									{blogPost.links.length > 0 && 
										<div>
											{blogPost.links.map((link, index) => (
												<BlogLink key={index} link={link} />
											))}
										</div>
									}
									<div style={styles.publishedContainer}>
										{this.props.showCreator ? 
											<span style={styles.publishedAt}>
												Published: {formatDateTime(blogPost.created_at)} by {blogPost.created_by.username}
											</span> : 
											<span style={styles.publishedAt}>
												Published: {formatDateTime(blogPost.created_at)}
											</span> 
										}
									</div>
									<Grid container item xs={12}>
										<Grid item sm={6} xs={12}>
												<Button onClick={this.handleSelectBlogCategory.bind(this, blogPost.category)} style={styles.categoryButton}>{blogPost.category.category}</Button>
										</Grid>
										<Grid item sm={6} xs={12} style={styles.tagsContainer}>
											{blogPost.tags.map((tag, index) => (
												<Button onClick={this.handleSelectBlogTag.bind(this, tag)} key={index} style={styles.tagButton}>{tag.tag}</Button>
											))}
										</Grid>
									</Grid>
								</div>
							</Grid>
						</Grid>
						<Grid item md={2} xs>
							<BlogSidebar getSearchText={this.handleGetSearchText} viewBlogSearchPosts={this.handleViewBlogSearchPosts} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} />
						</Grid>
					</Grid>
				}
				{showImageGalleryImage && 
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={image.thumbnail} alt={image.caption} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{image.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogPostPhoto} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
				{showBlogPostPhoto && 
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={photoData.thumbnail} alt={photoData.title} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{photoData.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogPostPhoto} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps, { getBlogPost, clearBlogPost, getCategoryPosts, getTagPosts, getSearchPosts })(BlogPost);












