import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { getCategories, clearCategories, getTags, clearTags } from '../../actions/common';
import { getSearchPosts, getCategoryPosts, getTagPosts } from '../../actions/blog';

class BlogSidebar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchText: '',
			landscapeMode: true,
			windowWidth: '',
			windowHeight: ''
		}
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmitSearch = () => {
		const { searchText } = this.state;
		this.props.getSearchPosts(searchText);
		this.props.viewBlogSearchPosts();
		this.props.getSearchText(searchText);
	}

	handleGetTagPosts = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleGetCategoryPosts = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.props.getCategories();
		this.props.getTags();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		this.props.clearCategories();
		this.props.clearTags();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { searchText, landscapeMode } = this.state;
		const { categories, loadingCategories } = this.props;
		const { tags, loadingTags } = this.props;

		return (
			<div style={{flex: 1}}>
				<Grid container style={{flex: 1}}>
					<Grid container item xs={12}>
						<Grid item xs={12}>
							<div style={{padding: 5}}><input onChange={this.handleChange} name="searchText" id="searchText" placeholder="Search" style={styles.searchInput} /></div>
						</Grid>
						<Grid item xs={12}>
							<div style={{padding: 5}}><Button onClick={this.handleSubmitSearch} style={landscapeMode ? styles.searchButtonWide : styles.searchButton}>Search</Button></div>
						</Grid>
					</Grid>
					<Grid item xs={12}>
						{loadingCategories === false ? 
							<div style={{padding: 5, textAlign: 'center'}}>
								<div style={{flex: 1, textAlign: 'center', padding: 5}}><span style={styles.sidebarHeaderText}>Categories:</span></div>
								{categories.map((category, index) => (
									<div key={index} style={{padding: 5}}>
										<Button onClick={this.handleGetCategoryPosts.bind(this, category)} style={styles.categoryButton}>{category.category}</Button>
									</div>
								))}
							</div> :
							<div style={{padding: 5}}>
								Loading Categories <Dots />
							</div>
						}
					</Grid>
					<Grid item xs={12}>
						{loadingTags === false ? 
							<div style={{padding: 5}}>
								<div style={{flex: 1, textAlign: 'center', padding: 5}}><span style={styles.sidebarHeaderText}>Tags:</span></div>
								{tags.map((tag, index) => (
									<div key={index} style={{flex: 1, padding: 5, float: 'left'}}>
										<Button onClick={this.handleGetTagPosts.bind(this, tag)} style={styles.tagButton}>{tag.tag}</Button>
									</div>
								))}
							</div> :
							<div style={{padding: 5}}>
								Loading Tags <Dots />
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		categories: state.common.categories,
		loadingCategories: state.common.loadingCategories,

		categoryPosts: state.blog.categoryPosts,
		loadingCategoryPosts: state.blog.loadingCategoryPosts,

		tags: state.common.tags,
		loadingTags: state.common.loadingTags,

		tagPosts: state.blog.tagPosts,
		loadingTagPosts: state.blog.loadingTagPosts
	}
}

export default connect(mapStateToProps, { getSearchPosts, getCategories, getCategoryPosts, clearCategories, getTags, getTagPosts, clearTags })(BlogSidebar);












