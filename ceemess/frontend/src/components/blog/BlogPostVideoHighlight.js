import React, { Component } from 'react';
import { styles } from '../../Styles';

class BlogPostVideoHighlight extends Component {
	constructor(props) {
		super(props);
		this.state = {
			landscapeMode: true,
			windowWidth: '',
			windowHeight: ''
		}
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {	
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { videoYouTubeID, videoVimeoID } = this.props;
		const { landscapeMode } = this.state;

		return (
			<div>
				{videoYouTubeID !== '' && 
					<div style={styles.videoContainer}>
						{landscapeMode ? 
							<iframe src={`https://www.youtube.com/embed/${videoYouTubeID}`}
								title="YouTube Landscape Mode"
	              frameBorder='0'
	              allow='autoplay; encrypted-media'
	              allowFullScreen
	              width="100%" 
								height="360" 
								maxwidth="640"
	            /> : 
	            <iframe src={`https://www.youtube.com/embed/${videoYouTubeID}`}
	            	title="YouTube Portrait Mode"
	              frameBorder='0'
	              allow='autoplay; encrypted-media'
	              allowFullScreen
	              width="100%"
	              maxwidth="640"
	              height="360" 
	            />
						}
							
					</div>
				}
				{(videoVimeoID !== '' && videoVimeoID  !== null) &&
					<div style={styles.videoContainer}>
						{landscapeMode ? 
							<iframe src={`https://player.vimeo.com/video/${videoVimeoID}`}
								title="Vimeo Landscape Mode"
								maxwidth="640"
								width="100%" 
								height="360" 
								frameBorder="0" 
								allow="autoplay; fullscreen" 
								allowFullScreen 
							/> :
							<iframe src={`https://player.vimeo.com/video/${videoVimeoID}`}
								title="Vimeo Portrait Mode"
								width="100%" 
								maxwidth="640"
								frameBorder="0" 
								allow="autoplay; fullscreen" 
								allowFullScreen 
							/>
						}
          </div>
				}
			</div>
		);
	}
}

export default BlogPostVideoHighlight;

