import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class BlogPostImageGallery extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	handleShowImageGalleryImage = (image) => {
		this.props.showImageGalleryImage(image);
	}

	componentDidMount() {

	}

	render() {
		const { imageGallery } = this.props;

		return (
			<Grid container>
				{imageGallery.map((image, index) => (
					<Grid item xs={3} key={index}>
						<Button onClick={this.handleShowImageGalleryImage.bind(this, image)}><img src={image.thumbnail} alt={image.caption} style={styles.image} /></Button>
					</Grid>
				))}
			</Grid>
		);
	}
}

export default BlogPostImageGallery;












