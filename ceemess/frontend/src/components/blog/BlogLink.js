import React, { Component } from 'react';
import { styles } from '../../Styles';

class BlogLink extends Component {
	componentDidMount() {
	// 	console.log('Link: ', this.props.link);
	}

	render() {
		const { link } = this.props;

		return (
			<div style={{padding: 10}}>
				<a href={link.link} target="_blank" rel="noopener noreferrer" style={styles.blogLink}><span style={styles.linkText}>{link.description}</span></a>
			</div>
		);
	}
}

export default BlogLink;