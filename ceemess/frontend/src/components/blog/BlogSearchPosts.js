import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import BlogPost from './BlogPost';
import BlogSidebar from './BlogSidebar';
import BlogPostInline from './BlogPostInline';

class BlogSearchPosts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showSearchPosts: true,
			showBlogPost:  false,
			showSearchPostImage: false,
			image: null,
			post: null
		}
	}

	handleCloseBlogImage = () => {
		this.setState({
			image: null,
			showSearchPosts: true,
			showBlogPost:  false,
			showSearchPostImage: false,
		});
	}

	handleViewImageGalleryImage = (image) => {
		this.setState({
			image: image,
			showSearchPosts: false,
			showBlogPost:  false,
			showSearchPostImage: true,
		});
	}

	handleSelectBlogTag = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleSelectBlogCategory = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleViewBlogSearchPosts = () => {
		this.props.viewBlogSearchPosts();
	}

	handleGetSearchText = (searchText) => {
		this.setState({
			searchText: searchText
		});
	}

	handleShowImageGalleryImage = (image) => {
		this.props.viewImage(image);
	}

	handleViewBlogTagPosts = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleViewBlogCategoryPosts = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleViewBlogPhoto = (photo) => {
		this.props.viewBlogPhoto(photo);
	}

	handleCloseBlogPost = () => {
		this.setState({
			showSearchPosts: true,
			showBlogPost: false,
			post: null
		})
	}

	handleViewBlogPost = (post) => {
		this.setState({
			showSearchPosts: false,
			showBlogPost: true,
			post: post
		})
	}

	handleCloseSearchPosts = () => {
		this.props.closeBlogSearchPosts();
	}

	render() {
		const { showSearchPosts, showBlogPost, post, showSearchPostImage, image } = this.state;
		const { searchPosts, loadingSearchPosts, searchPostsNext, searchText } = this.props;

		return (
			<div style={{flex: 1}}>
				{showSearchPosts && 
					<Grid container>
		        <Grid item md={1} />
		        <Grid item md={8} sm={12}>
							{loadingSearchPosts === false ? 
								<div>
									<Grid container item xs={12}>
										<Grid item xs={11}>
											<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Search: {searchText}</span></div>
										</Grid>
										<Grid item xs={1}>
											<Button onClick={this.handleCloseSearchPosts} style={styles.closeButton}>X</Button>
										</Grid>
									</Grid>
									{searchPosts.length > 0 ? 
										<div style={{flex: 1}}>
											{searchPosts.map((post, index) => (
												<BlogPostInline key={index} blogPost={post} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleViewBlogCategoryPosts} viewBlogTagPosts={this.handleViewBlogTagPosts} viewImage={this.handleViewImageGalleryImage} />
											))}
											{searchPostsNext ? 
												<div style={{flex: 1, textAlign: 'center'}}><Button style={styles.loadMoreButton} onClick={this.handleGetMoreSearchPosts}>Load More</Button></div> :
												<div style={{flex: 1, textAlign: 'center'}}><span>...</span></div>
											}
										</div> :
										<div>
											<span>No Posts!</span>
										</div>
									}
								</div> :
								<div style={{flex: 1, textAlign: 'center', padding: 20}}>
									<span style={{fontSize: 16}}>Loading Search Posts</span> <Dots />
								</div>	
							}
						</Grid>
						<Grid item md={2} xs>
							<BlogSidebar getSearchText={this.handleGetSearchText} viewBlogSearchPosts={this.handleViewBlogSearchPosts} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} />
						</Grid>
					</Grid>
				}
				{showBlogPost && 
					<BlogPost blogPost={post} closeBlogPost={this.handleCloseBlogPost} searchText={this.props.searchText} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} viewBlogSearchPosts={this.handleViewBlogSearchPosts}  />
				}
				{showSearchPostImage && 
					<div style={{flex: 1}}>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={image.image} alt={image.caption} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{image.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogImage} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
					
		);
	}
}

const mapStateToProps = (state) => {
	return {
		searchPosts: state.blog.searchPosts,
		loadingSearchPosts: state.blog.loadingSearchPosts,
		searchPostsNext: state.blog.searchPostsNext
	}
}

export default connect(mapStateToProps, { })(BlogSearchPosts);












