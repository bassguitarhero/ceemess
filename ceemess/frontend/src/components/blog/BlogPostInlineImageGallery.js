import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

class BlogPostInlineImageGallery extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFour: []
		}
	}

	handleSelectImage = (image) => {
		this.props.selectImage(image);
	}

	handleGetFirstFour = () => {
		const { imageGallery } = this.props;
		var firstFour = [];
		for (var i = 0; i < 4; i++) {
			if (imageGallery[i]) {
				firstFour.push(imageGallery[i]);
			}
		}
		this.setState({
			firstFour: firstFour
		});
	}

	componentDidMount() {
		this.handleGetFirstFour();
	}

	render() {
		const { firstFour } = this.state;

		return (
			<Grid container>
				{firstFour.map((image, index) => (
					<Grid item xs={3} key={index}>
						<Button onClick={this.handleSelectImage.bind(this, image)} style={styles.imageButton}>
							<img src={image.thumbnail} alt={image.caption} style={styles.image} />
						</Button>
					</Grid>
				))}
			</Grid>
		)
	}
}

export default BlogPostInlineImageGallery;












