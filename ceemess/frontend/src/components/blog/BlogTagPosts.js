import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { clearTags } from '../../actions/common';
import { getTagPosts, getMoreTagPosts, clearTagPosts } from '../../actions/blog';

import BlogPostInline from './BlogPostInline';
import BlogPost from './BlogPost';
import BlogPhoto from './BlogPhoto';
import BlogSidebar from './BlogSidebar';

class BlogTagPosts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showTagPosts: true,
			showTagPost: false,
			showCategoryPostPhoto: false, 
			showTagPostImage: false,
			tagPostPhoto: null,
			image: null,
			tagPost: null	
		}
	}

	handleCloseBlogImage = () => {
		this.setState({
			image: null,
			showTagPosts: true,
			showTagPost: false,
			showTagPostImage: false,
		});
	}

	handleViewImageGalleryImage = (image) => {
		this.setState({
			image: image,
			showTagPosts: false,
			showTagPost: false,
			showTagPostImage: true,
		});
	}

	handleSelectBlogTag = (tag) => {
		this.props.clearTagPosts()
		this.props.getTagPosts(tag);
		this.props.viewBlogTagPosts(tag);
	}

	handleSelectBlogCategory = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleViewBlogSearchPosts = () => {
		this.props.viewBlogSearchPosts();
	}

	handleGetSearchText = (searchText) => {
		this.setState({
			searchText: searchText
		});
	}

	handleCloseBlogTagPosts = () => {
		this.props.closeBlogTagPosts();
	}

	handleGetMoreTagPosts = () => {
		const { tag, tagPostsNext } = this.props;
		const fields = tagPostsNext.split('=');
		this.props.getMoreTagPosts(tag, fields[0]);
	}

	handleViewBlogTagPosts = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleViewBlogCategoryPosts = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleClosePhoto = () => {
		this.setState({
			showTagPosts: true,
			showTagPost: false,
			showTagPostPhoto: false, 
			tagPostPhoto: null,
		});
	}

	handleViewBlogPhoto = (photo) => {
		this.setState({
			showTagPosts: false,
			showTagPost: false,
			showTagPostPhoto: true, 
			tagPostPhoto: photo,
		});
	}

	handleCloseBlogPost = () => {
		this.setState({
			showTagPosts: true,
			showTagPost: false,
			showTagPostPhoto: false, 
			tagPost: null,
		});
	}

	handleViewBlogPost = (post) => {
		this.setState({
			showTagPosts: false,
			showTagPost: true,
			showTagPostPhoto: false, 
			tagPost: post
		})
	}

	componentDidMount() {
		this.props.getTagPosts(this.props.tag);
	}

	componentDidUpdate() {

	}

	componentWillUnmount() {
		this.props.clearTags();
		this.props.clearTagPosts();
	}

	render() {
		const { tag, tagPosts, loadingTagPosts, tagPostsNext } = this.props;
		const { image, showTagPosts, showTagPost, showTagPostPhoto, showTagPostImage, tagPost, tagPostPhoto } = this.state;

		return (
			<div style={{flex: 1}}>
				{showTagPosts && 
					<div style={{flex: 1}}>
						{loadingTagPosts === false ?
							<div style={{flex: 1}}>
								{tagPosts.length > 0 ? 
									<Grid container>
				            <Grid md={1} item />
				            <Grid md={8} item xs>
				            	<Grid container>
				            		<Grid item xs={11}>
						            	<div style={styles.titleContainer}>
				            				<span style={styles.title}>Tag: {tag.tag}</span>
				            			</div>	
				            		</Grid>
				            		<Grid item xs={1}>
				            			<Button onClick={this.handleCloseBlogTagPosts} style={styles.closeButton}>X</Button>
				            		</Grid>
				            	</Grid>
											{tagPosts.map((post, index) => (
												<BlogPostInline key={index} blogPost={post} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleViewBlogCategoryPosts} viewBlogTagPosts={this.handleSelectBlogTag} viewImage={this.handleViewImageGalleryImage} />
											))}
											{tagPostsNext ? 
												<div style={styles.textContainer}><Button style={styles.loadMoreButton} onClick={this.handleGetMoreTagPosts}>Load More</Button></div> :
												<div style={styles.textContainer}><span style={styles.text}>...</span></div>
											}
										</Grid>
										<Grid item md={2} xs>
											<BlogSidebar getSearchText={this.handleGetSearchText} viewBlogSearchPosts={this.handleViewBlogSearchPosts} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} />
										</Grid>
									</Grid> :
									<Grid container>
				            <Grid md={2} item />
				            <Grid md={8} item xs>
				            	<div style={styles.textContainer}>
												<span style={styles.text}>No Posts</span> <Dots />
											</div>
										</Grid>
									</Grid>
								}
							</div> : 
							<Grid container>
		            <Grid md={2} item />
		            <Grid md={8} item xs>
		            	<div style={styles.textContainer}>
		            		<span style={styles.text}>Loading Tag Posts</span> <Dots />
		            	</div>
		            </Grid>
							</Grid>
						}
					</div>
				}
				{showTagPost && 
					<div>
						<BlogPost blogPost={tagPost} closeBlogPost={this.handleCloseBlogPost} tag={this.props.tag} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} />
					</div>
				}
				{showTagPostPhoto && 
					<BlogPhoto photo={tagPostPhoto} closePhoto={this.handleClosePhoto} />
				}
				{showTagPostImage && 
					<div style={{flex: 1}}>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={image.image} alt={image.caption} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{image.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogImage} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		tagPosts: state.blog.tagPosts,
		loadingTagPosts: state.blog.loadingTagPosts,
		tagPostsNext: state.blog.tagPostNext
	}
}

export default connect(mapStateToProps, { getTagPosts, getMoreTagPosts, clearTags, clearTagPosts })(BlogTagPosts);












