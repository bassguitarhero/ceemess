import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { clearCategories } from '../../actions/common';
import { getCategoryPosts, getMoreCategoryPosts, clearCategoryPosts } from '../../actions/blog';

import BlogPostInline from './BlogPostInline';
import BlogPost from './BlogPost';
import BlogPhoto from './BlogPhoto';
import BlogSidebar from './BlogSidebar';

class BlogCategoryPosts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			image: null,
			showCategoryPosts: true,
			showCategoryPost: false,
			showCategoryPostPhoto: false, 
			showCategoryPostImage: false,
			categoryPostPhoto: null,
			categoryPost: null
		}
	}

	handleCloseBlogImage = () => {
		this.setState({
			image: null,
			showCategoryPosts: true,
			showCategoryPost: false,
			showCategoryPostImage: false,
		});
	}

	handleViewImageGalleryImage = (image) => {
		this.setState({
			image: image,
			showCategoryPosts: false,
			showCategoryPost: false,
			showCategoryPostImage: true,
		});
	}

	handleSelectBlogTag = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleSelectBlogCategory = (category) => {
		this.props.clearCategoryPosts();
		this.props.getCategoryPosts(category);
	}

	handleViewBlogSearchPosts = () => {
		this.props.viewBlogSearchPosts();
	}

	handleGetSearchText = (searchText) => {
		this.setState({
			searchText: searchText
		});
	}

	handleCloseBlogCategoryPosts = () => {
		this.props.closeBlogCategoryPosts();
	}

	handleGetMoreCategoryPosts = () => {
		const { category, categoryPostsNext } = this.props;
		const fields = categoryPostsNext.split('=');
		this.props.getMoreCategoryPosts(category, fields[1]);
	}

	handleViewBlogTagPosts = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleViewBlogCategoryPosts = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleClosePhoto = () => {
		this.setState({
			showCategoryPosts: true,
			showCategoryPost: false,
			showCategoryPostPhoto: false, 
			categoryPostPhoto: null
		})
	}

	handleViewBlogPhoto = (photo) => {
		this.setState({
			showCategoryPosts: false,
			showCategoryPost: false,
			showCategoryPostPhoto: true, 
			categoryPostPhoto: photo,
			categoryPost: null
		})
	}

	handleCloseBlogPost = () => {
		this.setState({
			showCategoryPosts: true,
			showCategoryPost: false,
			showCategoryPostPhoto: false, 
			categoryPostPhoto: null,
			categoryPost: null
		})
	}

	handleViewBlogPost = (post) => {
		this.setState({
			showCategoryPosts: false,
			showCategoryPost: true,
			showCategoryPostPhoto: false, 
			categoryPostPhoto: null,
			categoryPost: post
		})
	}

	componentDidMount() {
		this.props.getCategoryPosts(this.props.category);
	}

	componentWillUnmount() {
		this.props.clearCategories();
		this.props.clearCategoryPosts();
	}

	render() {
		const { category, categoryPosts, loadingCategoryPosts, categoryPostsNext } = this.props;
		const { image, showCategoryPosts, showCategoryPost, showCategoryPostPhoto, showCategoryPostImage, categoryPostPhoto, categoryPost } = this.state;

		return (
			<div style={{flex: 1}}>
				{showCategoryPosts && 
					<div style={{flex: 1}}>
						{loadingCategoryPosts === false ? 
							<Grid container>
		            <Grid item md={1} />
		            <Grid item md={8} sm={12}>
		            	<Grid container>
		            		<Grid item xs={11}>
				            	<div style={styles.titleContainer}>
				            		<span style={styles.title}>Category: {category.category}</span>
				            	</div>	
				            </Grid>
				            <Grid item xs={1}>
				            	<Button onClick={this.handleCloseBlogCategoryPosts} style={styles.closeButton}>X</Button>
				            </Grid>
				          </Grid>
									{categoryPosts.map((post, index) => (
										<BlogPostInline key={index} blogPost={post} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleViewBlogTagPosts} viewImage={this.handleViewImageGalleryImage} />
									))}
									{categoryPostsNext ? 
										<div style={styles.textContainer}><Button style={styles.loadMoreButton} onClick={this.handleGetMoreCategoryPosts}>Load More</Button></div> :
										<div style={styles.textContainer}><span style={styles.text}>...</span></div>
									}
								</Grid>
								<Grid item md={2} xs>
									<BlogSidebar getSearchText={this.handleGetSearchText} viewBlogSearchPosts={this.handleViewBlogSearchPosts} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} />
								</Grid>
							</Grid> :
							<Grid container style={{flex: 1}}>
		            <Grid item md={1} />
		            <Grid item md={8} sm={12}>
									<div style={styles.textContainer}><span style={styles.text}>Loading Category posts</span> <Dots /></div>
								</Grid>
							</Grid>
						}
					</div>
				}
				{showCategoryPost && 
					<BlogPost blogPost={categoryPost} closeBlogPost={this.handleCloseBlogPost} category={this.props.category} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} viewImage={this.handleViewImageGalleryImage} />
				}
				{showCategoryPostPhoto && 
					<BlogPhoto photo={categoryPostPhoto} closePhoto={this.handleClosePhoto} />
				}
				{showCategoryPostImage && 
					<div style={{flex: 1}}>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={image.image} alt={image.caption} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{image.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogImage} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		categoryPosts: state.blog.categoryPosts,
		loadingCategoryPosts: state.blog.loadingCategoryPosts,
		categoryPostsNext: state.blog.categoryPostsNext
	}
}

export default connect(mapStateToProps, { getCategoryPosts, getMoreCategoryPosts, clearCategories, clearCategoryPosts })(BlogCategoryPosts)












