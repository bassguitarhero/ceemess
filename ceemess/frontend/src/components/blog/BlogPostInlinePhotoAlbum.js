import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

class BlogPostInlinePhotoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFourPhotos: []
		}
	}

	handleSelectPhoto = (photo) => {
		this.props.selectPhoto(photo);
	}

	getFirstFourPhotoAlbumPhotos = () => {
		const { blogPostPhotoAlbum } = this.props;
		const { firstFourPhotos } = this.state;
		let reverseAlbum = blogPostPhotoAlbum.reverse();
		for (var i = 0; i < 4; i++) {
			if (reverseAlbum[i] !== undefined) {
				firstFourPhotos.push(reverseAlbum[i]);
			}
		}
		this.setState({
			firstFourPhotos: firstFourPhotos
		})
	}

	componentDidMount() {
		this.getFirstFourPhotoAlbumPhotos(this.props.blogPostPhotoAlbum);
	}

	render() {
		const { firstFourPhotos } = this.state;
		const { blogPostPhotoAlbum } = this.props;

		return (
			<Grid container>
				{firstFourPhotos.map((photo, index) => (
					<Grid item xs={3} key={index}>
						<Button onClick={this.handleSelectPhoto.bind(this, photo)} style={styles.photoButton}>
							<img src={photo.thumbnail} alt={photo.title} style={styles.inlineAlbumImage} />
						</Button>
					</Grid>
				))}
				{blogPostPhotoAlbum.length > 4 && 
					<Grid item xs={12}>
						<div style={{flex:1, padding: 10, textAlign: 'center'}}><span>{blogPostPhotoAlbum.length} photos</span></div>
					</Grid>
				}
			</Grid>
		);
	}
}

export default BlogPostInlinePhotoAlbum;












