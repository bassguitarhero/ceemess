import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import BlogSearchPosts from './BlogSearchPosts';
import BlogSidebar from './BlogSidebar';
import BlogPostInline from './BlogPostInline';
import BlogPost from './BlogPost';
import BlogPostInlinePhotoAlbumPhoto from './BlogPostInlinePhotoAlbumPhoto';

import BlogCategoryPosts from './BlogCategoryPosts';
import BlogTagPosts from './BlogTagPosts';

import { getStickyBlogPosts, getBlogPosts, getMoreBlogPosts, clearBlogPosts, getCategoryPosts, getTagPosts } from '../../actions/blog';

class BlogHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			blogPost: null,
			homePhotoData: '',
			showBlogCategoryPosts: false,
			showBlogTagPosts: false, 
			blogCategory: null,
			blogTag: null,
			image: null,
			showSearchPosts: false,
			searchText: '',
			landscapeMode: true
		}
	}

	handleGetSearchText = (searchText) => {
		this.setState({
			searchText: searchText
		});
	}

	handleCloseBlogSearchPosts = () => {
		this.setState({
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false,
			searchText: ''
		});
	}

	handleViewBlogSearchPosts = () => {
		this.props.viewBlogSearchPosts();
	}

	handleCloseBlogTagPosts = () => {
		this.setState({
			blogTag: null,
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleSelectBlogTag = (tag) => {
		this.props.viewBlogTagPosts(tag);
	}

	handleCloseBlogCategoryPosts = () => {
		this.setState({
			blogCategory: null,
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleSelectBlogCategory = (category) => {
		this.props.viewBlogCategoryPosts(category);
	}

	handleCloseBlogPhoto = () => {
		this.setState({
			homePhotoData: '',
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleViewBlogPhoto = (photo) => {
		this.props.viewBlogPhoto(photo);
	}

	handleCloseBlogPost = () => {
		this.setState({
			blogPost: '',
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleViewBlogPost = (post) => {
		this.props.viewBlogPost(post);
	}

	handleCloseBlogImage = () => {
		this.setState({
			image: null,
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleViewImageGalleryImage = (image) => {
		this.props.viewImage(image);
	}

	handleGetMoreBlogPosts = () => {
		const { blogPostsNext } = this.props;
		let fields = blogPostsNext.split('=');
		this.props.getMoreBlogPosts(fields[1]);
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.props.getStickyBlogPosts();
		this.props.getBlogPosts();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastState) {
		
	}

	componentWillUnmount() {
		this.props.clearBlogPosts();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { showBlog, showBlogHome, showBlogPost, showBlogPostInlinePhotoAlbumPhoto, blogPost, homePhotoData, showBlogCategoryPosts, blogCategory, blogTag, showBlogTagPosts, showImageGalleryImage, image, showSearchPosts, searchText, landscapeMode, showCreator } = this.state;
		const { blogPosts, loadingBlogPosts, blogPostsNext, blogPostsPrevious, loadingStickyBlogPosts, stickyBlogPosts } = this.props;

		return (
			<Grid container style={{flex: 1}}>
        <Grid item md={1} />
        <Grid item md={8} sm={12}>
					{loadingStickyBlogPosts === false ? 
						<div>
							{stickyBlogPosts.length > 0 && 
								<div>
									{stickyBlogPosts.map((blogPost, index) => (
										<BlogPostInline key={index} blogPost={blogPost} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} viewImage={this.handleViewImageGalleryImage} showCreator={this.props.showCreator} />
									))}
								</div>
							}
						</div> :
						<div>
							<Grid container>
			          <Grid md={1} item />
			          <Grid md={8} item xs>
									<div style={styles.textContainer}>
										<span style={styles.text}>Loading Sticky Posts</span> <Dots />
									</div>
								</Grid>
							</Grid>
						</div>
					}
					{loadingBlogPosts === false ? 
						<div>
							{blogPosts.length > 0 ? 
								<div>
									{blogPosts.map((blogPost, index) => (
										<BlogPostInline key={index} blogPost={blogPost} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag}  viewImage={this.handleViewImageGalleryImage} showCreator={this.props.showCreator} />
									))}
									{blogPostsNext ? 
										<div style={styles.loadMoreContainer}>
											<Button style={styles.loadMoreButton} onClick={this.handleGetMoreBlogPosts}>Load More</Button>
										</div> :
										<div style={styles.loadMoreContainer}>
											...
										</div>
									}
								</div> : 
								<div style={styles.textContainer}>
									<span style={styles.text}>No Blog Posts!</span>
								</div>
							}
						</div> : 
						<div>
							<Grid container>
			          <Grid item md={1} />
			          <Grid item md={8} sm={12}>
									<div style={styles.textContainer}>
										<span style={styles.text}>Loading Blog Posts</span> <Dots />
									</div>
								</Grid>
							</Grid>
						</div>
					}
				</Grid>
				<Grid item md={2} sm={12}>
					<BlogSidebar getSearchText={this.handleGetSearchText} viewBlogSearchPosts={this.handleViewBlogSearchPosts} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} />
				</Grid>
			</Grid>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		blogPosts: state.blog.blogPosts,
		loadingBlogPosts: state.blog.loadingBlogPosts,
		blogPostsNext: state.blog.blogPostsNext,
		blogPostsPrevious: state.blog.blogPostsPrevious,

		stickyBlogPosts: state.blog.stickyBlogPosts,
		loadingStickyBlogPosts: state.blog.loadingStickyBlogPosts,
	}
}

export default connect(mapStateToProps, { getStickyBlogPosts, getBlogPosts, getMoreBlogPosts, clearBlogPosts, getCategoryPosts, getTagPosts })(BlogHome);











