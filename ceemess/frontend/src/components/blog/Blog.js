import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import { styles } from '../../Styles';

import BlogHome from './BlogHome';
import BlogSearchPosts from './BlogSearchPosts';
import BlogPost from './BlogPost';

import BlogCategoryPosts from './BlogCategoryPosts';
import BlogTagPosts from './BlogTagPosts';

class Blog extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			blogPost: null,
			homePhotoData: '',
			showBlogCategoryPosts: false,
			showBlogTagPosts: false, 
			blogCategory: null,
			blogTag: null,
			image: null,
			showSearchPosts: false,
			searchText: '',
			landscapeMode: true
		}
	}

	handleGetSearchText = (searchText) => {
		this.setState({
			searchText: searchText
		});
	}

	handleCloseBlogSearchPosts = () => {
		this.setState({
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false,
			searchText: ''
		});
	}

	handleViewBlogSearchPosts = () => {
		this.setState({
			showBlog: true,
			showBlogHome: false,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: true 
		});
	}

	handleCloseBlogTagPosts = () => {
		this.setState({
			blogTag: null,
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false,
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleSelectBlogTag = (tag) => {
		this.setState({
			blogTag: tag,
			showBlog: true,
			showBlogHome: false,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: true,
			showSearchPosts: false 
		});
	}

	handleCloseBlogCategoryPosts = () => {
		this.setState({
			blogCategory: null,
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleSelectBlogCategory = (category) => {
		this.setState({
			blogCategory: category,
			showBlog: true,
			showBlogHome: false,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: true,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleCloseBlogPhoto = () => {
		this.setState({
			homePhotoData: '',
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleViewBlogPhoto = (photo) => {
		this.setState({
			homePhotoData: photo,
			showBlog: false,
			showBlogHome: false,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: true,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleCloseBlogPost = () => {
		this.setState({
			blogPost: '',
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleViewBlogPost = (post) => {
		this.setState({
			blogPost: post,
			showBlog: true,
			showBlogHome: false,
			showBlogPost: true,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleCloseBlogImage = () => {
		this.setState({
			image: null,
			showBlog: true,
			showBlogHome: true,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: false, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleViewImageGalleryImage = (image) => {
		this.setState({
			image: image,
			showBlog: false,
			showBlogHome: false,
			showBlogPost: false,
			showBlogPostInlinePhotoAlbumPhoto: false,
			showImageGalleryImage: true, 
			showBlogCategoryPosts: false,
			showBlogTagPosts: false,
			showSearchPosts: false 
		});
	}

	handleGetMoreBlogPosts = () => {
		const { blogPostsNext } = this.props;
		let fields = blogPostsNext.split('=');
		this.props.getMoreBlogPosts(fields[1]);
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastState) {

	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { showBlog, showBlogHome, showBlogPost, showBlogPostInlinePhotoAlbumPhoto, blogPost, homePhotoData, showBlogCategoryPosts, blogCategory, blogTag, showBlogTagPosts, showImageGalleryImage, image, showSearchPosts, searchText, landscapeMode } = this.state;

		return (
			<div>
				{showBlog && 
					<div>
						{showBlogHome && 
							<BlogHome showCreator={this.props.showCreator} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} viewImage={this.handleViewImageGalleryImage} viewBlogSearchPosts={this.handleViewBlogSearchPosts}  />
						}
						{showBlogPost && 
							<BlogPost showCreator={this.props.showCreator} searchText={searchText} blogPost={blogPost} closeBlogPost={this.handleCloseBlogPost} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} viewBlogSearchPosts={this.handleViewBlogSearchPosts} />
						}
						{showBlogCategoryPosts && 
							<BlogCategoryPosts category={blogCategory} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} closeBlogCategoryPosts={this.handleCloseBlogCategoryPosts} viewImage={this.handleViewImageGalleryImage} viewBlogSearchPosts={this.handleViewBlogSearchPosts}  />
						}
						{showBlogTagPosts && 
							<BlogTagPosts tag={blogTag} viewBlogPost={this.handleViewBlogPost} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} closeBlogTagPosts={this.handleCloseBlogTagPosts} viewImage={this.handleViewImageGalleryImage} viewBlogSearchPosts={this.handleViewBlogSearchPosts}  />
						}
						{showSearchPosts && 
							<BlogSearchPosts searchText={searchText} closeBlogSearchPosts={this.handleCloseBlogSearchPosts}  viewBlogPost={this.handleViewBlogPost} viewBlogSearchPosts={this.handleViewBlogSearchPosts} viewBlogPhoto={this.handleViewBlogPhoto} viewBlogCategoryPosts={this.handleSelectBlogCategory} viewBlogTagPosts={this.handleSelectBlogTag} viewImage={this.handleViewImageGalleryImage} />
						}
					</div>
				}	
				{showBlogPostInlinePhotoAlbumPhoto &&
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={homePhotoData.image} alt={homePhotoData.title} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{homePhotoData.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogPhoto} style={landscapeMode ? styles.closePhotoTextWide : styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
				{showImageGalleryImage && 
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={image.image} alt={image.caption} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{image.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseBlogImage} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

export default Blog;











