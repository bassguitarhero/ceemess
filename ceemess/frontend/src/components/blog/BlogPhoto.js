import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class BlogPhoto extends Component {
	handleClosePhoto = () => {
		this.props.closePhoto();
	}

	render() {
		const { photo } = this.props;

		return (
			<div>
				<div style={styles.imageContainer}>
					<img src={photo.image} alt={photo.title} style={styles.image} />
				</div>
				<div style={styles.imageCaptionContainer}>
					<span style={styles.imageCaptionText}>{photo.caption}</span>
				</div>
				<div style={styles.closePhotoPlaceHolder}>
					<div style={styles.closePhotoPosition}>
						<div style={styles.closePhotoCircle}>
							<Button onClick={this.handleClosePhoto} style={styles.closePhotoText}>X</Button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default BlogPhoto;












