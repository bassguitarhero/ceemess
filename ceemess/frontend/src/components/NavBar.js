import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Button, Grid, Select, MenuItem } from '@material-ui/core';
import { styles } from '../Styles';

import { getBlogSettings, resetRebuildNavBar } from '../actions/common'; 

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemName: '',
      navMenu: [],
      websiteName: ''
    }
  }

  handleSetShowCreator = (showCreator) => {
    this.props.showCreator(showCreator);
  }

  handleSetAboutMeImage = (aboutMeImage) => {
    this.props.aboutMeImage(aboutMeImage);
  }

  handleSetAboutMeDescription = (aboutMeDescription) => {
    this.props.aboutMeDescription(aboutMeDescription);
  }

  handleSelectHome = (e) => {
    this.setState({
      itemName: "Blog"
    })
    this.handleShowBlog();
  }

  handleItemChange = (e) => {
    this.setState({
      itemName: e.target.value
    });
  }

  handleShowBlog = () => {
    this.props.showBlog();
  }

  handleShowPortfolio = () => {
    this.props.showPortfolio();
  }

  handleShowAboutMe = () => {
    this.props.showAboutMe();
  }

  handleShowContact = () => {
    this.props.showContact();
  }

  handleShowVideos = () => {
    this.props.showVideos();
  }

  handleShowLogin = () => {
    this.props.showLogin();
  }

  handleShowMusic = () => {
    this.props.showMusic();
  }

  handleShowPhotos = () => {
    this.props.showPhotos();
  }

  handleShowImages = () => {
    this.props.showImages();
  }

  handleShowAdmin = () => {
    this.props.showAdmin();
  }

  handleRebuildNavMenu = () => {
    const { isAuthenticated, blogSettings } = this.props;
    const navMenu = [];
    var itemName = '';
    var websiteName = '';
    if (blogSettings.length > 0) {
      blogSettings.map(settings => {
        // console.log('Settings: ', settings);
        websiteName = settings.name;
        if (settings.show_blog === true) {
          navMenu.push({
            name: "Blog",
            id: "Blog",
          })
        }
        if (settings.show_portfolio === true) {
          navMenu.push({
            name: "Portfolio",
            id: "Portfolio"
          })
        }
        if (settings.show_photos === true) {
          navMenu.push({
            name: "Photos",
            id: "Photos"
          })
        }
        if (settings.show_music === true) {
          navMenu.push({
            name: "Music",
            id: "Music"
          })
        }
        if (settings.show_videos === true) {
          navMenu.push({
            name: "Videos",
            id: "Videos"
          })
        }
        if (settings.show_images === true) {
          navMenu.push({
            name: "Images",
            id: "Images"
          })
        }
        this.handleSetAboutMeDescription(settings.description);
        this.handleSetAboutMeImage(settings.thumbnail);
        this.handleSetShowCreator(settings.show_creator);
      });
      navMenu.push({
        name: "About Me",
        id: "AboutMe"
      });
      if (isAuthenticated) {
        navMenu.push({
          name: "Admin",
          id: "Admin"
        });
      } else {
        navMenu.push({
          name: "Login",
          id: "Login"
        })
      }
      if (!isAuthenticated) {
        blogSettings.map(settings => {
          if (settings.default_blog === true) {
            this.handleShowBlog();
            itemName = "Blog";
          }
          if (settings.default_portfolio === true) {
            this.handleShowPortfolio();
            itemName = "Portfolio";
          }
          if (settings.default_photos === true) {
            this.handleShowPhotos();
            itemName = "Photos";
          }
          if (settings.default_music === true) {
            this.handleShowMusic();
            itemName = "Music";
          }
          if (settings.default_videos === true) {
            this.handleShowVideos();
            itemName = "Videos";
          }
          if (settings.default_images === true) {
            this.handleShowImages();
            itemName = "Images";
          }
        })
       } else {
         this.handleShowAdmin();
         itemName = "Admin";
       }
    } else {
      if (isAuthenticated === true) {
        navMenu.push({
          name: "Admin",
          id: "Admin"
        })
        itemName = "Admin";
        this.handleShowAdmin();
      } else {
        navMenu.push({
          name: "Login",
          id: "Login"
        })
        itemName = "Login";
        this.handleShowLogin();
      }
    }
    this.setState({websiteName: websiteName, navMenu: navMenu, itemName: itemName});
  }

  handleBuildNavMenu = () => {
    const { isAuthenticated, blogSettings } = this.props;
    const navMenu = [];
    var itemName = '';
    var websiteName = '';
    if (blogSettings.length > 0) {
      blogSettings.map(settings => {
        // console.log('Settings: ', settings);
        websiteName = settings.name;
        if (settings.show_blog === true) {
          navMenu.push({
            name: "Blog",
            id: "Blog",
          })
        }
        if (settings.show_portfolio === true) {
          navMenu.push({
            name: "Portfolio",
            id: "Portfolio"
          })
        }
        if (settings.show_photos === true) {
          navMenu.push({
            name: "Photos",
            id: "Photos"
          })
        }
        if (settings.show_music === true) {
          navMenu.push({
            name: "Music",
            id: "Music"
          })
        }
        if (settings.show_videos === true) {
          navMenu.push({
            name: "Videos",
            id: "Videos"
          })
        }
        if (settings.show_images === true) {
          navMenu.push({
            name: "Images",
            id: "Images"
          })
        }
        this.handleSetAboutMeDescription(settings.description);
        this.handleSetAboutMeImage(settings.thumbnail);
        this.handleSetShowCreator(settings.show_creator);
      });
      navMenu.push({
        name: "About Me",
        id: "AboutMe"
      });
      if (isAuthenticated) {
        navMenu.push({
          name: "Admin",
          id: "Admin"
        });
      } else {
        navMenu.push({
          name: "Login",
          id: "Login"
        })
      }
      blogSettings.map(settings => {
        if (settings.default_blog === true) {
          this.handleShowBlog();
          itemName = "Blog";
        }
        if (settings.default_portfolio === true) {
          this.handleShowPortfolio();
          itemName = "Portfolio";
        }
        if (settings.default_photos === true) {
          this.handleShowPhotos();
          itemName = "Photos";
        }
        if (settings.default_music === true) {
          this.handleShowMusic();
          itemName = "Music";
        }
        if (settings.default_videos === true) {
          this.handleShowVideos();
          itemName = "Videos";
        }
        if (settings.default_images === true) {
          this.handleShowImages();
          itemName = "Images";
        }
      })
    } else {
      if (isAuthenticated === true) {
        navMenu.push({
          name: "Admin",
          id: "Admin"
        })
        itemName = "Admin";
        this.handleShowAdmin();
      } else {
        navMenu.push({
          name: "Login",
          id: "Login"
        })
        itemName = "Login";
        this.handleShowLogin();
      }
    }
    this.setState({websiteName: websiteName, navMenu: navMenu, itemName: itemName});
  }

  componentDidUpdate(lastProps, lastState) {
    // if (this.props.isAuthenticated === false && lastProps.isAuthenticated === true) {
    //   this.handleRebuildNavMenu();
    //   if (this.props.loadingBlogSettings === false) {
    //     this.handleBuildNavMenu();
    //   }
    // }
    // if (this.props.isAuthenticated === true && lastProps.isAuthenticated === false) {
    //   this.handleRebuildNavMenu();
    // }
    if (this.props.rebuildNavBar !== null && lastProps.rebuildNavBar === null) {
      // console.log('Rebuild Nav Bar');
      this.handleRebuildNavMenu();
      this.props.resetRebuildNavBar();
    }
    if (this.props.isLoading === false && lastProps.isLoading === true) {
      if (this.props.loadingBlogSettings === false) {
        this.handleBuildNavMenu();
      }
    }
    if (this.props.loadingBlogSettings === false && lastProps.loadingBlogSettings === true) {
      if (this.props.isLoading === false) {
        this.handleBuildNavMenu();
      }
    }
    if (this.state.itemName === "Blog") {
      this.handleShowBlog();
    }
    if (this.state.itemName === "Portfolio") {
      this.handleShowPortfolio();
    }
    if (this.state.itemName === "Photos") {
      this.handleShowPhotos();
    }
    if (this.state.itemName === "Music") {
      this.handleShowMusic();
    }
    if (this.state.itemName === "Videos") {
      this.handleShowVideos();
    }
    if (this.state.itemName === "Images") {
      this.handleShowImages();
    }
    if (this.state.itemName === "About Me") {
      this.handleShowAboutMe();
    }
    if (this.state.itemName === "Contact") {
      this.handleShowContact();
    }
    if (this.state.itemName === "Login") {
      this.handleShowLogin();
    }
    if (this.state.itemName === "Admin") {
      this.handleShowAdmin();
    }
  }

  render() {
    const { itemName, navMenu, websiteName } = this.state;
    return (
      <div style={styles.container}>
        <AppBar position="static" style={{ background: '#2E3B55' }}>
          <Toolbar>
            <Grid container>
              <Grid md={1} item />
              <Grid md={10} item container spacing={1} xs>
                <Grid item xs={6} style={styles.siteTitle}>
                  <Typography variant="h4" color="inherit">
                    <Button style={styles.homeButton} onClick={this.handleSelectHome}>{websiteName}</Button>
                  </Typography>
                </Grid>
                <Grid item xs={6} style={styles.rightAlign}>
                  <span style={styles.itemNameText}>View:  </span>
                  <Select
                    value={itemName} 
                    onChange={this.handleItemChange.bind(this)}
                    inputProps={{
                      name: `Sections`,
                      id: "Sections"
                    }} 
                    style={styles.itemSelect} 
                  >
                    {navMenu.map((item, index) => (
                      <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
                    ))}
                  </Select>
                </Grid>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.admin.isAuthenticated,
    isLoading: state.admin.isLoading,
    user: state.admin.user, 

    blogSettings: state.common.blogSettings,
    loadingBlogSettings: state.common.loadingBlogSettings,

    rebuildNavBar: state.common.rebuildNavBar
  }
}

export default connect(mapStateToProps, { getBlogSettings, resetRebuildNavBar })(NavBar);











