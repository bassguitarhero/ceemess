import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import VideoInline from './VideoInline';

import { getMoreTagVideos, clearTagVideos, getTagVideos, getCategoryVideos } from '../../actions/videos';

class TagVideos extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	handleGetTagVideos = (tag) => {
		this.props.getTagVideos(tag);
		this.props.viewTagVideos(tag);
	}

	handleGetCategoryVideos = (category) => {
		this.props.getCategoryVideos(category);
		this.props.viewCategoryVideos(category);
	}

	handleLoadMoreVideos = () => {
		const { tag, tagVideosNext } = this.props;
		var fields = tagVideosNext.split('=');
		this.props.getMoreTagVideos(tag, fields[1]);
	}

	componentWillUnmount() {
		this.props.clearTagVideos();
	}

	render() {
		const { tagVideos, loadingTagVideos, tagVideosNext } = this.props;
		const { tag } = this.props;

		return (
			<div>
				<Grid container>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Tag: {tag.tag}</span></div>
				</Grid>
					{loadingTagVideos === false ? 
						<div>
							{tagVideos.map((video, index) => (
								<VideoInline key={index} video={video} viewTagVideos={this.handleGetTagVideos} viewCategoryVideos={this.handleGetCategoryVideos} />
							))}
							{tagVideosNext ? 
								<div style={styles.loadMoreContainer}>
									<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreVideos}>
										Load More
									</Button>
								</div> :
								<div style={styles.loadMoreContainer}>
									<span style={styles.text}>...</span>
								</div>
							}
						</div> :
						<Grid container style={{flex: 1}}>
							<Grid md={2} item />
			        <Grid container item md={8} sm={12} style={{flex: 1}}>
								<div style={styles.loadingContainer}>
									<span style={styles.loadingText}>Loading Videos</span> <Dots />
								</div>
							</Grid>
						</Grid>
					}
				</Grid>
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		tagVideos: state.videos.tagVideos,
		loadingTagVideos: state.videos.loadingTagVideos,
		tagVideosNext: state.videos.tagVideosNext
	}
}

export default connect(mapStateToProps, { getMoreTagVideos, clearTagVideos, getTagVideos, getCategoryVideos })(TagVideos);












