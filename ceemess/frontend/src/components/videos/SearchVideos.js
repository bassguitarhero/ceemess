import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import VideoInline from './VideoInline';

import { getMoreSearchVideos, clearSearchVideos, getTagVideos, getCategoryVideos } from '../../actions/videos';

class SearchVideos extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	handleGetTagVideos = (tag) => {
		this.props.getTagVideos(tag);
		this.props.viewTagVideos(tag);
	}

	handleGetCategoryVideos = (category) => {
		this.props.getCategoryVideos(category);
		this.props.viewCategoryVideos(category);
	}

	handleLoadMoreVideos = () => {
		const { searchText, searchVideosNext } = this.props;
		var fields = searchVideosNext.split('=');
		this.props.getMoreSearchVideos(searchText, fields[1]);
	}

	componentWillUnmount() {
		this.props.clearSearchVideos();
	}

	render() {
		const { searchVideos, loadingSearchVideos, searchVideosNext } = this.props;
		const { searchText } = this.props;

		return (
			<div>
				<Grid container>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Search: {searchText}</span></div>
				</Grid>
					{loadingSearchVideos === false ? 
						<div>
							{searchVideos.map((video, index) => (
								<VideoInline key={index} video={video} viewTagVideos={this.handleGetTagVideos} viewCategoryVideos={this.handleGetCategoryVideos} />
							))}
							{searchVideosNext ? 
								<div style={styles.loadMoreContainer}>
									<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreVideos}>
										Load More
									</Button>
								</div> :
								<div style={styles.loadMoreContainer}>
									<span style={styles.text}>...</span>
								</div>
							}
						</div> :
						<Grid container style={{flex: 1}}>
							<Grid md={2} item />
			        <Grid container item md={8} sm={12} style={{flex: 1}}>
								<div style={styles.loadingContainer}>
									<span style={styles.loadingText}>Loading Videos</span> <Dots />
								</div>
							</Grid>
						</Grid>
					}
				</Grid>
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		searchVideos: state.videos.searchVideos,
		loadingSearchVideos: state.videos.loadingSearchVideos,
		searchVideosNext: state.videos.searchVideosNext
	}
}

export default connect(mapStateToProps, { getMoreSearchVideos, clearSearchVideos, getTagVideos, getCategoryVideos })(SearchVideos);












