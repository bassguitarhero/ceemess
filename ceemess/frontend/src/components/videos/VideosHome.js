import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { getVideos, getMoreVideos, clearVideos, getCategoryVideos, getTagVideos } from '../../actions/videos';

import VideoInline from './VideoInline';

class Videos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showVideos: true,
			showVideo: false,
		}
	}

	handleGetTagVideos = (tag) => {
		this.props.getTagVideos(tag);
		this.props.viewTagVideos(tag);
	}

	handleGetCategoryVideos = (category) => {
		this.props.getCategoryVideos(category);
		this.props.viewCategoryVideos(category);
	}

	handleGetMoreVideos = () => {
		const { videosNext } = this.props;
		const fields = videosNext.split('=');
		this.props.getMoreVideos(fields[1]);
	}

	componentDidMount() {
		this.props.getVideos();
	}

	componentWillUnmount() {
		this.props.clearVideos();
	}

	render() {
		const { videos, loadingVideos, videosNext } = this.props;

		return (
			<div style={styles.container}>
				{loadingVideos === false ? 
					<div style={{flex: 1}}>
						{videos.map((video, index) => (
							<VideoInline key={index} video={video} viewTagVideos={this.handleGetTagVideos} viewCategoryVideos={this.handleGetCategoryVideos} />
						))}
						{videosNext ? 
							<div style={styles.textContainer}>
								<Button style={styles.loadMoreButton} onClick={this.handleGetMoreVideos}>Load More</Button>
							</div> :
							<div style={styles.textContainer}>
								<span style={styles.text}>...</span>
							</div>
						}
					</div> :
					<div style={styles.textContainer}>
						<span style={styles.text}>Loading Videos</span> <Dots />
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		videos: state.videos.videos,
		loadingVideos: state.videos.loadingVideos,
		videosNext: state.videos.videosNext
	}
}

export default connect(mapStateToProps, { getVideos, getMoreVideos, clearVideos, getCategoryVideos, getTagVideos })(Videos);












