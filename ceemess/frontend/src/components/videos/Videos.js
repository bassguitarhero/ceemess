import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import VideosHome from './VideosHome';
import CategoryVideos from './CategoryVideos';
import TagVideos from './TagVideos';
import SearchVideos from './SearchVideos';
import VideosSidebar from './VideosSidebar';

class Videos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showVideosHome: true,
			showCategoryVideos: false,
			showTagVideos: false,
			showSearchVideos: false,
			category: null,
			tag: null,
			searchText: ''
		}
	}

	handleCloseTagVideos = () => {
		this.setState({
			tag: null,
			showVideosHome: true,
			showCategoryVideos: false,
			showTagVideos: false,
			showSearchVideos: false,
		})
	}

	handleViewTagVideos = (tag) => {
		this.setState({
			tag: tag,
			showVideosHome: false,
			showCategoryVideos: false,
			showTagVideos: true,
			showSearchVideos: false,
		})
	}

	handleCloseCategoryVideos = () => {
		this.setState({
			category: null,
			showVideosHome: true,
			showCategoryVideos: false,
			showTagVideos: false,
			showSearchVideos: false,
		})
	}

	handleViewCategoryVideos = (category) => {
		this.setState({
			category: category,
			showVideosHome: false,
			showCategoryVideos: true,
			showTagVideos: false,
			showSearchVideos: false,
		})
	}

	handleCloseSearchVideos = () => {
		this.setState({
			searchText: '',
			showVideosHome: true,
			showCategoryVideos: false,
			showTagVideos: false,
			showSearchVideos: false,
		})
	}

	handleViewSearchVideos = (searchText) => {
		this.setState({
			searchText: searchText,
			showVideosHome: false,
			showCategoryVideos: false,
			showTagVideos: false,
			showSearchVideos: true,
		})
	}

	render() {
		const { showVideosHome, showCategoryVideos, showTagVideos, showSearchVideos } = this.state;
		const { category, tag, searchText } = this.state;

		return (
			<div style={styles.container}>
				<Grid container style={{flex: 1}}>
					<Grid md={1} item />
	        <Grid container md={8} item xs style={{flex: 1}}>
						{showVideosHome && 
							<VideosHome viewTagVideos={this.handleViewTagVideos} viewCategoryVideos={this.handleViewCategoryVideos} />
						}
						{showCategoryVideos && 
							<CategoryVideos viewTagVideos={this.handleViewTagVideos} viewCategoryVideos={this.handleViewCategoryVideos} category={category} close={this.handleCloseCategoryVideos} />
						}
						{showTagVideos && 
							<TagVideos viewTagVideos={this.handleViewTagVideos} viewCategoryVideos={this.handleViewCategoryVideos} tag={tag} close={this.handleCloseTagVideos} />
						}
						{showSearchVideos && 
							<SearchVideos viewTagVideos={this.handleViewTagVideos} viewCategoryVideos={this.handleViewCategoryVideos} searchText={searchText} close={this.handleCloseSearchVideos} />
						}
					</Grid>
					<Grid container md={2} item xs style={{flex: 1}}>
						<VideosSidebar viewTagVideos={this.handleViewTagVideos} viewCategoryVideos={this.handleViewCategoryVideos} viewSearchVideos={this.handleViewSearchVideos} />
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Videos;












