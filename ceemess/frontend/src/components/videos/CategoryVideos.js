import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import VideoInline from './VideoInline';

import { getMoreCategoryVideos, clearCategoryVideos } from '../../actions/videos';

class CategoryVideos extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	handleGetTagVideos = (tag) => {
		this.props.getTagVideos(tag);
		this.props.viewTagVideos(tag);
	}

	handleGetCategoryVideos = (category) => {
		this.props.getCategoryVideos(category);
		this.props.viewCategoryVideos(category);
	}

	handleLoadMoreVideos = () => {
		const { category, categoryVideosNext } = this.props;
		var fields = categoryVideosNext.split('=');
		this.props.getMoreCategoryVideos(category, fields[1]);
	}

	componentWillUnmount() {
		this.props.clearCategoryVideos();
	}

	render() {
		const { categoryVideos, loadingCategoryVideos, categoryVideosNext } = this.props;
		const { category } = this.props;

		return (
			<div>
				<Grid container>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Category: {category.category}</span></div>
				</Grid>
					{loadingCategoryVideos === false ? 
						<div>
							{categoryVideos.map((video, index) => (
								<VideoInline key={index} video={video} viewTagVideos={this.handleGetTagVideos} viewCategoryVideos={this.handleGetCategoryVideos} />
							))}
							{categoryVideosNext ? 
								<div style={styles.loadMoreContainer}>
									<Button style={styles.loadMoreButton} onClick={this.handleLoadMoreVideos}>
										Load More
									</Button>
								</div> :
								<div style={styles.loadMoreContainer}>
									<span style={styles.text}>...</span>
								</div>
							}
						</div> :
						<Grid container style={{flex: 1}}>
							<Grid md={2} item />
			        <Grid container item md={8} sm={12} style={{flex: 1}}>
								<div style={styles.loadingContainer}>
									<span style={styles.loadingText}>Loading Videos</span> <Dots />
								</div>
							</Grid>
						</Grid>
					}
				</Grid>
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		categoryVideos: state.videos.categoryVideos,
		loadingCategoryVideos: state.videos.loadingCategoryVideos,
		categoryVideosNext: state.videos.categoryVideosNext
	}
}

export default connect(mapStateToProps, { getMoreCategoryVideos, clearCategoryVideos })(CategoryVideos);












