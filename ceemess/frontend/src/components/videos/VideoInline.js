	import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

class VideoInline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			windowWidth: '',
			windowHeight: '',
			landscapeMode: true
		}
	}

	handleGetTagVideos = (tag) => {
		this.props.viewTagVideos(tag);
	}

	handleGetCategoryVideos = (category) => {
		this.props.viewCategoryVideos(category);
	}
	
	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({landscapeMode: false});
		} else {
			this.setState({landscapeMode: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { video } = this.props;
		const { landscapeMode } = this.state;

		return (
			<div style={styles.container}>
				<Grid container spacing={1}>
					<Grid item md={3} xs={12} style={{paddingTop: 20, alignItems: 'center'}}>
						{landscapeMode ? 
							<div>
								{video.thumbnail ? 
									<div style={styles.imageContainer} >
										<img src={video.thumbnail} alt="Video Thumbnail" style={styles.videoInlineImage} />
									</div> :
									<div style={styles.imageContainer}>
										X
									</div>
								}
							</div> :
							<div>
								{video.thumbnail ? 
									<div style={styles.imageContainer}>
										<img src={video.thumbnail} alt="Video Thumbnail" style={styles.videoInlineImage} />
									</div> :
									<div style={styles.imageContainer}>
										X
									</div>
								}
							</div>
						}
					</Grid>
					<Grid container item md={9} xs={12} style={{padding: 10}}>
						<Grid item xs={12} style={styles.videoTitleContainer}>
							<span style={styles.inlinePostTitleText}>{video.title}</span>
						</Grid>
						<Grid item xs={12} style={styles.videoDescriptionContainer}>
							<span style={styles.descriptionText}>{video.description}</span>
						</Grid>
						{video.youtube_id && 
							<Grid item xs={12} style={{flex: 1, padding: 20}}>
								{landscapeMode ? 
									<iframe src={`https://www.youtube.com/embed/${video.youtube_id}`}
										title="YouTube Landscape Mode"
			              frameBorder='0'
			              allow='autoplay; encrypted-media'
			              allowFullScreen
			              width="100%" 
										height="360" 
										maxwidth="640"
			            /> : 
			            <iframe src={`https://www.youtube.com/embed/${video.youtube_id}`}
			            	title="YouTube Portrait Mode"
			              frameBorder='0'
			              allow='autoplay; encrypted-media'
			              allowFullScreen
			              width="100%"
			              maxwidth="640"
			              height="360" 
			            />
								}
								<span>&nbsp;</span>
							</Grid>
						}
						{video.vimeo_id && 
							<Grid item xs={12} style={{flex: 1, padding: 20}}>
								{landscapeMode ? 
									<iframe src={`https://player.vimeo.com/video/${video.vimeo_id}`}
										title="Vimeo Landscape Mode"
										maxwidth="640"
										width="100%" 
										height="360" 
										frameBorder="0" 
										allow="autoplay; fullscreen" 
										allowFullScreen 
									/> :
									<iframe src={`https://player.vimeo.com/video/${video.vimeo_id}`}
										title="Vimeo Portrait Mode"
										width="100%" 
										maxwidth="640"
										frameBorder="0" 
										allow="autoplay; fullscreen" 
										allowFullScreen 
									/>
								}
								<span>&nbsp;</span>
							</Grid>
						}
					</Grid>
					<Grid container item xs={12}>
						<Grid item sm={6} xs={12} style={styles.categoryContainer}>
							{video.category !== null && 
								<Button style={styles.categoryButton} onClick={this.handleGetCategoryVideos.bind(this, video.category)}>{video.category.category}</Button>
							}
						</Grid>
						<Grid item sm={6} xs={12} style={styles.tagsContainer}>
							{video.tags.map((tag, index) => (
								<Button key={index} style={styles.tagButton} onClick={this.handleGetTagVideos.bind(this, tag)} >{tag.tag}</Button>
							))}
						</Grid>
					</Grid>
					<Grid item xs={12}>
							<hr style={styles.bottomLine} />
						</Grid>
				</Grid>
			</div>
		);
	}
}

export default VideoInline;










