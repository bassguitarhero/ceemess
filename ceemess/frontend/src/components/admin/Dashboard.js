import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import Overview from './dashboard/Overview';
import Posts from './dashboard/Posts';
import PhotoAlbums from './dashboard/PhotoAlbums';
import MusicAlbums from './dashboard/MusicAlbums';
import Videos from './dashboard/Videos';
import Images from './dashboard/Images';
import Links from './dashboard/Links';
import Categories from './dashboard/Categories';
import Tags from './dashboard/Tags';

class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showOverview: true, 
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: false
		}
	}

	handleShowOverview = () => {
		this.setState({
			showOverview: true,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: false
		})
	}

	handleShowPosts = () => {
		this.setState({
			showOverview: false,
			showPosts: true,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: false
		})
	}

	handleShowPhotoAlbums = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: true,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: false
		})
	}

	handleShowMusicAlbums = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: true,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: false
		})
	}

	handleShowVideos = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: true,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: false
		})
	}

	handleShowImages = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: true,
			showLinks: false,
			showCategories: false,
			showTags: false
		})
	}

	handleShowLinks = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: true,
			showCategories: false,
			showTags: false
		})
	}

	handleShowCategories = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: true,
			showTags: false
		})
	}

	handleShowTags = () => {
		this.setState({
			showOverview: false,
			showPosts: false,
			showPhotoAlbums: false,
			showMusicAlbums: false,
			showVideos: false,
			showImages: false,
			showLinks: false,
			showCategories: false,
			showTags: true
		})
	}

	render() {
		const { showOverview, showPosts, showPhotoAlbums, showMusicAlbums, showVideos, showImages, showLinks, showCategories, showTags } = this.state;

		return (
			<div style={styles.container}>
				<Grid container>
					<Grid container item sm={10} xs={12} style={{padding: 5}}>
						{showOverview && 
							<Overview />
						}
						{showPosts && 
							<Posts />
						}
						{showPhotoAlbums && 
							<PhotoAlbums />
						}
						{showMusicAlbums && 
							<MusicAlbums />
						}
						{showVideos && 
							<Videos />
						}
						{showImages && 
							<Images />
						}
						{showLinks && 
							<Links />
						}
						{showCategories && 
							<Categories />
						}
						{showTags && 
							<Tags />
						}
					</Grid>
					<Grid container item sm={2} xs={12} style={{padding: 5}}>
						<div>
							<div style={{padding: 10}}>
								<span style={{fontSize: 18, fontWeight: 'bold'}}>View:</span>
							</div>
							<div style={{padding: 5}}>
								<Button style={showOverview ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowOverview}>Overview</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showPosts ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowPosts}>Posts</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showPhotoAlbums ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowPhotoAlbums}>Photos</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showMusicAlbums ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowMusicAlbums}>Music</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showVideos ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowVideos}>Videos</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showImages ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowImages}>Images</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showLinks ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowLinks}>Links</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showCategories ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowCategories}>Categories</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showTags ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowTags}>Tags</Button>
							</div>
						</div>
					</Grid>
				</Grid>
	    </div>
		);
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps, { })(Dashboard);












