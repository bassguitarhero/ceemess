import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import { login, requestPasswordReset, clearPasswordResetRequest } from '../../actions/admin';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			email: '',
			showLoginForm: true,
			showPasswordResetForm: false,
			showPasswordResetInstructions: false
		}
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		// if (key === 'name'){
		// 	if (value.length > 250){
		// 		alert("Your update title is too long")
		// 	}
		// }
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSwitchForms = () => {
		this.setState({
			showLoginForm: !this.state.showLoginForm,
			showPasswordResetForm: !this.state.showPasswordResetForm
		})
	}

	handleLogin = () => {
		const { username, password } = this.state;
		this.props.login(username, password);
	}

	handleSendRequest = () => {
		const { username, email } = this.state;
		this.props.requestPasswordReset(username, email);
	}

	componentDidUpdate(lastProps) {
		if  (this.props.isAuthenticated === true && lastProps.isAuthenticated === false) {
			this.props.showAdmin();
		}
		if (this.props.passwordResetReceived !== null && lastProps.passwordResetReceived === null) {
			this.setState({
				email: '',
				password: '',
				showLoginForm: true,
				showPasswordResetForm: false,
				showPasswordResetInstructions: true
			});
			this.props.clearPasswordResetRequest();
		}
	}

	render() {
		const { username, password, email, showLoginForm, showPasswordResetForm, showPasswordResetInstructions } = this.state;

		return (
			<div style={styles.container}>
				{showLoginForm && 
					<Grid container style={{flex: 1}}>
						<Grid md={2} item />
		        <Grid container md={8} item xs style={{flex: 1}}>
		        	{showPasswordResetInstructions && 
		        		<Grid container item xs={12} style={{padding: 10}}>
		        			<span>Password Reset sent. Please check your email.</span>
		        		</Grid>
		        	}
		        	<Grid container item xs={12} style={{padding: 10}}>
		        		<Grid item xs={2}>
		        			Username:
		        		</Grid>
		        		<Grid item xs={10}>
			        		<input type="text" placeholder="Username" name="username" onChange={this.handleChange} value={username} style={styles.input} />
			        	</Grid>
		        	</Grid>
		        	<Grid container item xs={12} style={{padding: 10}}>
		        		<Grid item xs={2}>
		        			Password:
		        		</Grid>
		        		<Grid item xs={10}>
			        		<input type="password" name="password" placeholder="Password" onChange={this.handleChange} value={password} style={styles.input} />
			        	</Grid>
		        	</Grid>
		        	<Grid container item xs={12} style={{padding: 10}}>
		        		<Button style={styles.loginButton} onClick={this.handleLogin}>Login</Button>
		        	</Grid>
		        	<Grid container item xs={12} style={{}}>
		        		<Button style={styles.loginButton} onClick={this.handleSwitchForms}>Forgot Password?</Button>
		        	</Grid>
		        </Grid>
		      </Grid>
		    }
		    {showPasswordResetForm && 
		    	<Grid container style={{flex: 1}}>
						<Grid md={2} item />
		        <Grid container md={8} item xs style={{flex: 1}}>
		        	<Grid container item xs={12} style={{padding: 10}}>
		        		<Grid item xs={2}>
		        			Username:
		        		</Grid>
		        		<Grid item xs={10}>
			        		<input type="text" placeholder="Username" name="username" onChange={this.handleChange} value={username} style={styles.input} />
			        	</Grid>
		        	</Grid>
		        	<Grid container item xs={12} style={{padding: 10}}>
		        		<Grid item xs={2}>
		        			Email:
		        		</Grid>
		        		<Grid item xs={10}>
			        		<input type="text" name="email" placeholder="Email" onChange={this.handleChange} value={email} style={styles.input} />
			        	</Grid>
		        	</Grid>
		        	<Grid container item xs={12} style={{padding: 10}}>
		        		<Button style={styles.loginButton} onClick={this.handleSendRequest}>Send Request</Button>
		        	</Grid>
		        	<Grid container item xs={12} style={{}}>
		        		<Button style={styles.loginButton} onClick={this.handleSwitchForms}>Cancel</Button>
		        	</Grid>
		        </Grid>
		      </Grid>
		    }
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		isAuthenticated: state.admin.isAuthenticated,
		passwordResetReceived: state.admin.passwordResetReceived
	}
}

export default connect(mapStateToProps, { login, requestPasswordReset, clearPasswordResetRequest })(Login);











