import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField, Checkbox } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { getDashboardSettings, clearDashboardSettings, editDashboardSettings, clearEditDashboardSettings } from '../../../actions/dashboard';
import { clearUser, loadUser } from '../../../actions/admin';
import { clearBlogSettings, getBlogSettings } from '../../../actions/common';

class BlogSettingsHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			slug: '',
			description: '',
			file: null,
			image: '',
			thumbnail: '',
			showBlog: true,
			defaultBlog: true,
			showPortfolio: true,
			defaultPortfolio: false,
			showPhotoAlbums: true,
			defaultPhotoAlbums: false,
			showMusicAlbums: true,
			defaultMusicAlbums: false,
			showVideos: true,
			defaultVideos: false,
			showImages: true,
			defaultImages: false,
			uploading: false,
			showCreator: false
		}
	}

	handleSelectFile = (e) => {
		this.setState({
			file: e.target.files[0]
		});
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleCheckboxChange = (e) => {
		// console.log('E: ', e);
		this.setState({
			[e.target.name]: !this.state[e.target.name]
		})
	}

	handleSubmit = () => {
		const { name, description, file, showBlog, defaultBlog, showPortfolio, defaultPortfolio, showPhotoAlbums, defaultPhotoAlbums, showMusicAlbums, defaultMusicAlbums, showVideos, defaultVideos, showImages, defaultImages, showCreator, slug } = this.state;
		const { dashboardSettings } = this.props;
		const settingsFile = { name, file, description, showBlog, defaultBlog, showPortfolio, defaultPortfolio, showPhotoAlbums, defaultPhotoAlbums, showMusicAlbums, defaultMusicAlbums, showVideos, defaultVideos, showImages, defaultImages, dashboardSettings, showCreator, slug };
		this.props.editDashboardSettings(settingsFile);
		this.setState({uploading: true});
	}

	componentDidMount() {
		this.props.getDashboardSettings();
	}

	componentDidUpdate(lastProps) {
		if (this.props.editedDashboardSettings !== null && lastProps.editedDashboardSettings === null) {
			this.props.clearEditDashboardSettings();
			this.props.clearUser();
			this.props.clearBlogSettings();
			this.props.loadUser();
			this.props.getBlogSettings();
			const { editedDashboardSettings } = this.props;
			this.setState({
				uploading: false,
				name: editedDashboardSettings.name,
				slug: editedDashboardSettings.slug,
				image: editedDashboardSettings.image,
				thumbnail: editedDashboardSettings.thumbnail,
				showBlog: editedDashboardSettings.show_blog,
				defaultBlog: editedDashboardSettings.default_blog,
				showPortfolio: editedDashboardSettings.show_portfolio,
				defaultPortfolio: editedDashboardSettings.default_portfolio,
				showPhotoAlbums: editedDashboardSettings.show_photos,
				defaultPhotoAlbums: editedDashboardSettings.default_photos,
				showMusicAlbums: editedDashboardSettings.show_music,
				defaultMusicAlbums: editedDashboardSettings.default_music,
				showVideos: editedDashboardSettings.show_videos,
				defaultVideos: editedDashboardSettings.default_videos,
				showImages: editedDashboardSettings.show_images,
				defaultImages: editedDashboardSettings.default_images,
				description: editedDashboardSettings.description,
				showCreator: editedDashboardSettings.show_creator
			});
		}
		if (this.props.loadingDashboardSettings === false && lastProps.loadingDashboardSettings === true) {
			const { dashboardSettings } = this.props;
			if (dashboardSettings !== undefined) {
				this.setState({
					name: dashboardSettings.name,
					slug: dashboardSettings.slug,
					image: dashboardSettings.image,
					thumbnail: dashboardSettings.thumbnail,
					showBlog: dashboardSettings.show_blog,
					defaultBlog: dashboardSettings.default_blog,
					showPortfolio: dashboardSettings.show_portfolio,
					defaultPortfolio: dashboardSettings.default_portfolio,
					showPhotoAlbums: dashboardSettings.show_photos,
					defaultPhotoAlbums: dashboardSettings.default_photos,
					showMusicAlbums: dashboardSettings.show_music,
					defaultMusicAlbums: dashboardSettings.default_music,
					showVideos: dashboardSettings.show_videos,
					defaultVideos: dashboardSettings.default_videos,
					showImages: dashboardSettings.show_images,
					defaultImages: dashboardSettings.default_images,
					description: dashboardSettings.description,
					showCreator: dashboardSettings.show_creator
				});
			}
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardSettings();
	}

	render() {
		const { name, description, file, image, thumbnail, showBlog, defaultBlog, showPortfolio, defaultPortfolio, showPhotoAlbums, defaultPhotoAlbums, showMusicAlbums, defaultMusicAlbums, showVideos, defaultVideos, showImages, defaultImages, showCreator, uploading } = this.state;
		const { showSettingsForm, showUserForm, showImportForm, showExportForm } = this.state;
		const { loadingDashboardSettings } = this.props;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				{loadingDashboardSettings === false ? 
					<Grid container item xs={12}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>Settings:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="name" 
								onChange={this.handleChange} 
								value={name} 
								placeholder="Name"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="About Me"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3}>
								<span style={{fontSize: 18}}>Current Image:</span>
							</Grid>
							<Grid item xs={9}>
								<img src={image} alt="" style={styles.currentImage} />
							</Grid>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								{image !== '' ? 
									<label htmlFor="file" style={{fontSize: 18}}>Replace:</label> :
									<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
								}
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="file" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Blog:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showBlog" 
								checked={showBlog} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Default:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="defaultBlog" 
								checked={defaultBlog} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Portfolio:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showPortfolio" 
								checked={showPortfolio} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Default:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="defaultPortfolio" 
								checked={defaultPortfolio} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Photo Albums:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showPhotoAlbums" 
								checked={showPhotoAlbums} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Default:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="defaultPhotoAlbums" 
								checked={defaultPhotoAlbums} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Music Albums:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showMusicAlbums" 
								checked={showMusicAlbums} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Default:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="defaultMusicAlbums" 
								checked={defaultMusicAlbums} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Videos:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showVideos" 
								checked={showVideos} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Default:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="defaultVideos" 
								checked={defaultVideos} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Images:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showImages" 
								checked={showImages} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Default:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="defaultImages" 
								checked={defaultImages} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={4} style={styles.formRow}>
							<span style={{fontSize: 18}}>Show Author:</span>
						</Grid>
						<Grid item xs={8} style={styles.formRow}>
							<Checkbox 
								name="showCreator" 
								checked={showCreator} 
								onChange={this.handleCheckboxChange} 
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Save Settings</Button>
						</Grid>
					</Grid> :
					<Grid item xs={12} style={{flex: 1, padding: 10}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Loading Settings <Dots /></span></Grid>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardSettings: state.dashboard.dashboardSettings,
		loadingDashboardSettings: state.dashboard.loadingDashboardSettings,
		editedDashboardSettings: state.dashboard.editedDashboardSettings
	}
}

export default connect(mapStateToProps, { getDashboardSettings, clearDashboardSettings, editDashboardSettings, clearEditDashboardSettings, clearUser, loadUser, clearBlogSettings, getBlogSettings })(BlogSettingsHome);












