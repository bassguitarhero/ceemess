import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField, Checkbox } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { setNewPassword, clearNewPassword, clearPasswordError } from '../../../actions/dashboard';

class UserForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			oldPassword: '',
			newPassword1: '',
			newPassword2: '',
			uploading: false
		}
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { oldPassword, newPassword1, newPassword2 } = this.state;
		const changePassword = { oldPassword, newPassword1 };
		if (newPassword1 === newPassword2) {
			this.setState({uploading: true})
			this.props.setNewPassword(changePassword);
		} else {
			alert("New password doesn't match");
		}
	}

	componentDidUpdate(lastProps) {
		if (this.props.passwordChange !== null && lastProps.passwordChange === null) {
			// console.log('passwordChange: ', this.props.passwordChange);
			this.props.clearNewPassword();
			this.setState({
				uploading: false,
				oldPassword: '',
				newPassword1: '',
				newPassword2: ''
			});
		}
		if (this.props.passwordError !== null && lastProps.passwordError === null) {
			this.props.clearPasswordError();
			this.setState({uploading: false});
			// alert("Old Password does not match");
		}
	}

	render() {
		const { oldPassword, newPassword1, newPassword2, uploading } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid container>
        	<Grid container item xs style={{alignContent: 'flex-start', alignItems: 'flex-start', flex: 1}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>Change Password:</span>
						</Grid>

						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="password" 
								name="oldPassword" 
								onChange={this.handleChange} 
								value={oldPassword} 
								placeholder="Old Password"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="password" 
								name="newPassword1" 
								onChange={this.handleChange} 
								value={newPassword1} 
								placeholder="New Password"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="password" 
								name="newPassword2" 
								onChange={this.handleChange} 
								value={newPassword2} 
								placeholder="Confirm Password"
								style={styles.inputField}
							/>
						</Grid>
						{uploading === false ? 
							<Grid item xs={12} style={styles.formRow}>
								<Button style={styles.submitButton} onClick={this.handleSubmit}>Change Password</Button>
							</Grid> :
							<div id="uploadProgress"></div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		passwordChange: state.dashboard.passwordChange,
		passwordError: state.dashboard.passwordError
	}
}

export default connect(mapStateToProps, { setNewPassword, clearNewPassword, clearPasswordError })(UserForm);












