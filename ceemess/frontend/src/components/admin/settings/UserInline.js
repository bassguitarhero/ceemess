import React from 'react';

const UserInline = (props) => (
	<div style={{paddingLeft: 10}}>
		<span>{props.user.username}</span>
	</div>
);

export default UserInline;