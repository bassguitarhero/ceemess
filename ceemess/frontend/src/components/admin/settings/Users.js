import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField, Checkbox } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UserInline from './UserInline';
import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewUser, resetCreateNewUser, getUsers, clearUsers } from '../../../actions/admin';

class Users extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			email: '',
			password: '',
			passwordConfirm: '',
			uploading: false
		}
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { username, email, password, passwordConfirm } = this.state;
		if (password === passwordConfirm) {
			const newUser = { username, email, password };
			if (window.confirm("Create New User?")) {
				this.props.createNewUser(newUser);
				this.setState({uploading: true});
			}
		}
	}

	componentDidMount() {
		this.props.getUsers();
	}

	componentDidUpdate(lastProps) {
		if (this.props.newUser !== null && lastProps.newUser === null) {
			this.props.clearUsers();
			this.props.getUsers();
			this.props.resetCreateNewUser();
			this.setState({
				username: '',
				email: '',
				password: '',
				passwordConfirm: '',
				uploading: false
			})
		}
	}

	componentWillUnmount() {
		this.props.clearUsers();
	}

	render() {
		const { username, email, password, passwordConfirm, uploading } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid container>
        	<Grid container item xs style={{alignContent: 'flex-start', alignItems: 'flex-start', flex: 1}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>Create User:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="username" 
								onChange={this.handleChange} 
								value={username} 
								placeholder="Username"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="email" 
								onChange={this.handleChange} 
								value={email} 
								placeholder="Email Address"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="password" 
								name="password" 
								onChange={this.handleChange} 
								value={password} 
								placeholder="Password"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="password" 
								name="passwordConfirm" 
								onChange={this.handleChange} 
								value={passwordConfirm} 
								placeholder="Confirm Password"
								style={styles.inputField}
							/>
						</Grid>
						{uploading === false ? 
							<Grid item xs={12} style={styles.formRow}>
								<Button style={styles.submitButton} onClick={this.handleSubmit}>Create New User</Button>
							</Grid> :
							<div id="uploadProgress"></div>
						}
						<Grid container item xs={12}>
							<Grid item xs={12} style={styles.formRow}>
								<span style={styles.formTitle}>Users:</span>
							</Grid>
							{this.props.loadingUsers === false ? 
								<Grid container>
									{this.props.users.map((user, index) => (
										<Grid item xs={12} style={{padding: 10}}><UserInline user={user} key={index} /></Grid>
									))}
								</Grid> :
								<Grid container>
									<span>Loading Users <Dots /></span>
								</Grid>
							}
						</Grid>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newUser: state.admin.newUser,
		users: state.admin.users,
		loadingUsers: state.admin.loadingUsers
	}
}

export default connect(mapStateToProps, { createNewUser, resetCreateNewUser, getUsers, clearUsers })(Users);












