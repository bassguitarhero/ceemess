import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField, Checkbox } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { uploadCSV, resetUploadCSV } from '../../../actions/dashboard';

class ImportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			file: null,
			uploading: false
		}
	}

	handleSelectFile = e => this.setState({
		file: e.target.files[0]
	});

	handleSubmit = () => {
		this.setState({uploading: true});
		this.props.uploadCSV(this.state.file);
	}

	componentDidUpdate(lastProps) {
		if (this.props.csvUploaded !== null && lastProps.csvUploaded === null) {
			console.log('CSV: ', this.props.csvUploaded);
			this.props.resetUploadCSV();
			this.setState({file: null, uploading: false});
		}
	}

	render() {
		const { file, uploading } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
					<Grid container item xs={12} style={styles.formRow}>
						<Grid item xs={3} style={{flex: 1}}>
							<label htmlFor="file" style={{fontSize: 18}}>CSV File:</label>
						</Grid>
						<Grid item xs={9} style={{flex: 1}}>
							<Input 
								type="file" 
								name="file" 
								onChange={this.handleSelectFile}
							/>
						</Grid>
					</Grid>
					<Grid item xs={12}>
						<Button style={styles.submitButton} onClick={this.handleSubmit}>Upload CSV</Button>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		csvUploaded: state.dashboard.csvUploaded
	}
}

export default connect(mapStateToProps, { uploadCSV, resetUploadCSV })(ImportForm);












