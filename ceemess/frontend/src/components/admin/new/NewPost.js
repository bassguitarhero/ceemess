import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewPost, clearNewPost, clearNewPostFail } from '../../../actions/addNew';
import { getDashboardPhotoAlbumsAll, clearDashboardPhotoAlbumsAll, getDashboardMusicAlbumsAll, clearDashboardMusicAlbumsAll, getDashboardVideosAll, clearDashboardVideosAll, getDashboardImagesAll, clearDashboardImagesAll, getDashboardLinks, clearDashboardLinks, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class NewPost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			body: '',
			image: null,
			imageHighlightList: [],
			imageHighlightName: '',
			videoHighlightList: [],
			videoHighlightName: '',
			photoAlbumList: [],
			photoAlbumName: '',
			musicAlbumList: [],
			musicAlbumName: '',
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			blog: true, 
			blogSticky: false,
			portfolio: false,
			portfolioSticky: false,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			publish: false,
			showPostForm: true,
			showVideosForm: false,
			showImagesForm: false,
			showLinksForm: false,
			photoAlbumsList: [],
			musicAlbumsList: [],
			videosList: [],
			imagesList: [],
			linksList: [],
			showVideosList: false,
			showImagesList: false,
			showLinksList: false,
			uploadingPost: false
		}
	}

	handleShowPostForm = () => {
		this.setState({
			showPostForm: true,
			showVideosForm: false,
			showImagesForm: false,
			showLinksForm: false
		})
	}

	handleShowVideosForm = () => {
		this.setState({
			showPostForm: false,
			showVideosForm: true,
			showImagesForm: false,
			showLinksForm: false
		})
	}

	handleShowImagesForm = () => {
		this.setState({
			showPostForm: false,
			showVideosForm: false,
			showImagesForm: true,
			showLinksForm: false
		})
	}

	handleShowLinksForm = () => {
		this.setState({
			showPostForm: false,
			showVideosForm: false,
			showImagesForm: false,
			showLinksForm: true
		})
	}

	handleImageHighlightChange = (e) => {
		this.setState({
			imageHighlightName: e.target.value
		})
	}

	handleVideoHighlightChange = (e) => {
		this.setState({
			videoHighlightName: e.target.value
		})
	}

	handlePhotoAlbumChange = (e) => {
		this.setState({
			photoAlbumName: e.target.value
		})
	}

	handleMusicAlbumChange = (e) => {
  	this.setState({
  		musicAlbumName: e.target.value
  	})
  }

	handleCategoryChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSelectFile = e => this.setState({
		image: e.target.files[0]
	});

	handleBlogChange = () => {
		this.setState({
			blog: !this.state.blog
		})
	}

	handleBlogStickyChange = () => {
		this.setState({
			blogSticky: !this.state.blogSticky
		})
	}

	handlePortfolioChange = () => {
		this.setState({
			portfolio: !this.state.portfolio
		})
	}

	handlePortfolioStickyChange = () => {
		this.setState({
			portfolioSticky: !this.state.portfolioSticky
		})
	}

	handlePublishChange = () => {
		this.setState({
			publish: !this.state.publish
		})
	}

	handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.id === tagsList[i].id) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSelectVideo = (video) => {
		const { videosList } = this.state;
		for (var i = 0; i < videosList.length; i++) {
			if (video.slug === videosList[i].slug) {
				videosList[i].checked = !videosList[i].checked
			}
		}
		this.setState({
			videosList: videosList
		});
	}

	handleSelectImage = (image) => {
		const { imagesList } = this.state;
		for (var i = 0; i < imagesList.length; i++) {
			if (image.slug === imagesList[i].slug) {
				imagesList[i].checked = !imagesList[i].checked
			}
		}
		this.setState({
			imagesList: imagesList
		});
	}

	handleSelectLink = (link) => {
		const { linksList } = this.state;
		for (var i = 0; i < linksList.length; i++) {
			if (link.slug === linksList[i].slug) {
				linksList[i].checked = !linksList[i].checked
			}
		}
		this.setState({
			linksList: linksList
		});
	}

	handleSubmit = () => {
		const { title, body, image, blog, blogSticky, portfolio, portfolioSticky, tagsList, categoryName, categoriesList, publish, videosList, imagesList, linksList } = this.state;
		const { imageHighlightList, videoHighlightList, photoAlbumList, musicAlbumList } = this.state;
		const { imageHighlightName, videoHighlightName, photoAlbumName, musicAlbumName } = this.state;

		let imageHighlight = null;
		for (var i = 0; i < imageHighlightList.length; i++) {
			if (imageHighlightList[i].slug === imageHighlightName && imageHighlightName !== 'select') {
				imageHighlight = imageHighlightList[i];
			}
		}
		let videoHighlight = null;
		for (var i = 0; i < videoHighlightList.length; i++) {
			if (videoHighlightList[i].slug === videoHighlightName && videoHighlightName !== 'select') {
				videoHighlight = videoHighlightList[i];
			}
		}
		let photoAlbum = null;
		for (var i = 0; i < photoAlbumList.length; i++) {
			if (photoAlbumList[i].slug === photoAlbumName && photoAlbumName !== 'select') {
				photoAlbum = photoAlbumList[i];
			}
		}
		let musicAlbum = null;
		for (var i = 0; i < musicAlbumList.length; i++) {
			if (musicAlbumList[i].slug === musicAlbumName && musicAlbumName !== 'select') {
				musicAlbum = musicAlbumList[i];
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		const videos = [];
		for (var i = 0; i < videosList.length; i++) {
			if (videosList[i].checked) {
				videos.push(videosList[i]);
			}
		}
		const images = [];
		for (var i = 0; i < imagesList.length; i++) {
			if (imagesList[i].checked) {
				images.push(imagesList[i]);
			}
		}
		const links = [];
		for (var i = 0; i < linksList.length; i++) {
			if (linksList[i].checked) {
				links.push(linksList[i]);
			}
		}
		const post = { title, body, image, imageHighlight, videoHighlight, photoAlbum, musicAlbum, category, blog, blogSticky, portfolio, portfolioSticky, tags, publish, videos, images, links };
		if (title !== '') {
			if (window.confirm("Create New Post?")) {
				this.props.createNewPost(post);
				this.setState({uploadingPost: true});
			}
		} else {
			alert("Posts must have a title.")
		}
	}

	componentDidMount() {
		this.props.getDashboardPhotoAlbumsAll();
		this.props.getDashboardMusicAlbumsAll();
		this.props.getDashboardVideosAll();
		this.props.getDashboardImagesAll();
		this.props.getDashboardLinks();
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.newPostFail !== null && lastProps.newPostFail === null) {
			this.props.clearNewPostFail();
			this.setState({uploadingPost: false});
		}
		if (this.props.loadingDashboardPhotoAlbumsAll === false && lastProps.loadingDashboardPhotoAlbumsAll === true) {
			const { dashboardPhotoAlbumsAll } = this.props;
			const photoAlbumList = [];
			photoAlbumList.push({
				id: 0,
				title: "Select:",
				slug: "select"
			});
			dashboardPhotoAlbumsAll.map(album => {
				photoAlbumList.push({
					id: album.id,
					title: album.title,
					slug: album.slug
				});
			})
			this.setState({
				photoAlbumName: 'select',
				photoAlbumList: photoAlbumList
			})
		}
		if (this.props.loadingDashboardMusicAlbumsAll === false && lastProps.loadingDashboardMusicAlbumsAll === true) {
			const { dashboardMusicAlbumsAll } = this.props;
			const musicAlbumList = [];
			musicAlbumList.push({
				id: 0,
				title: "Select:",
				slug: "select"
			});
			dashboardMusicAlbumsAll.map(album => {
				musicAlbumList.push({
					id: album.id,
					title: album.title,
					slug: album.slug
				});
			})
			this.setState({
				musicAlbumName: 'select',
				musicAlbumList: musicAlbumList
			})
		}
		if (this.props.loadingDashboardVideosAll === false && lastProps.loadingDashboardVideosAll === true) {
			const { dashboardVideosAll } = this.props;
			const videoHighlightList = [];
			videoHighlightList.push({
				id: 0,
				title: "Select:",
				slug: "select"
			});
			dashboardVideosAll.map(video => {
				videoHighlightList.push({
					id: video.id,
					title: video.title,
					slug: video.slug
				});
			})
			const videosList = [];
			dashboardVideosAll.map(video => {
				videosList.push({
					title: video.title,
					thumbnail: video.thumbnail,
					slug: video.slug,
					checked: false
				});
			})
			this.setState({
				videoHighlightName: 'select',
				videoHighlightList: videoHighlightList,
				showVideosList: true,
				videosList: videosList
			})
		}
		if (this.props.loadingDashboardImagesAll === false && lastProps.loadingDashboardImagesAll === true) {
			const { dashboardImagesAll } = this.props;
			const imageHighlightList = [];
			imageHighlightList.push({
				id: 0,
				title: "Select:",
				slug: "select"
			});
			dashboardImagesAll.map(image => {
				imageHighlightList.push({
					id: image.id,
					title: image.title,
					slug: image.slug
				});
			})
			const imagesList = [];
			dashboardImagesAll.map(image => {
				imagesList.push({
					title: image.title,
					thumbnail: image.thumbnail,
					slug: image.slug,
					checked: false
				});
			})
			this.setState({
				imageHighlightName: 'select',
				imageHighlightList: imageHighlightList,
				showImagesList: true,
				imagesList: imagesList
			})
		}
		if (this.props.loadingDashboardLinks === false && lastProps.loadingDashboardLinks === true) {
			const { dashboardLinks } = this.props;
			const linksList = [];
			dashboardLinks.map(link => {
				linksList.push({
					description: link.description,
					link: link.link,
					slug: link.slug,
					checked: false
				});
			})
			this.setState({
				showLinksList: true,
				linksList: linksList
			})
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: "select"
			});
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			this.setState({
				categoryName: 'select',
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.newPost !== null && lastProps.newPost === null) {
			this.setState({
				title: '',
				body: '',
				image: null,
				imageHighlightList: [],
				imageHighlightName: '',
				videoHighlightList: [],
				videoHighlightName: '',
				photoAlbumList: [],
				photoAlbumName: '',
				musicAlbumList: [],
				musicAlbumName: '',
				categoriesList: [],
				displayCategoriesList: false, 
				category: null,
				blog: true, 
				blogSticky: false,
				portfolio: false,
				portfolioSticky: false,
				tags: [],
				tagsList: [],
				displayTagsList: false,
				categoryName: '',
				publish: false,
				showPostForm: true,
				showVideosForm: false,
				showImagesForm: false,
				showLinksForm: false,
				photoAlbumsList: [],
				musicAlbumsList: [],
				videosList: [],
				imagesList: [],
				linksList: [],
				showVideosList: false,
				showImagesList: false,
				showLinksList: false,
				uploadingPost: false
			})
			this.props.clearDashboardPhotoAlbumsAll();
			this.props.clearDashboardMusicAlbumsAll();
			this.props.clearDashboardVideosAll();
			this.props.clearDashboardImagesAll();
			this.props.clearDashboardLinks();
			this.props.clearDashboardCategories();
			this.props.clearDashboardTags();
			this.props.clearDashboardPhotoAlbumsAll();
			this.props.clearDashboardMusicAlbumsAll();
			this.props.clearDashboardVideosAll();
			this.props.clearDashboardImagesAll();
			this.props.getDashboardLinks();
			this.props.getDashboardCategories();
			this.props.getDashboardTags();
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardPhotoAlbumsAll();
		this.props.clearDashboardMusicAlbumsAll();
		this.props.clearDashboardVideosAll();
		this.props.clearDashboardImagesAll();
		this.props.clearDashboardLinks();
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { title, body, image, imageHighlight, videoHighlight, photoAlbum, musicAlbum, blog, blogSticky, portfolio, portfolioSticky, publish } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;
		const { showPostForm, showVideosForm, showImagesForm, showLinksForm, showVideosList, videosList, showImagesList, imagesList, showLinksList, linksList } = this.state;
		const { imageHighlightList, imageHighlightName, videoHighlightList, videoHighlightName, photoAlbumList, photoAlbumName, musicAlbumList, musicAlbumName, uploadingPost } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploadingPost && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container style={styles.formTypeButtonsContainer}>
					<Grid item xs={3} style={styles.formTypeButtonContainer}>
						<Button style={showPostForm ? styles.formTypeButtonSelected : styles.formTypeButton } onClick={this.handleShowPostForm}>Post</Button>
					</Grid>
					<Grid item xs={3} style={styles.formTypeButtonContainer}>
						<Button style={showVideosForm ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowVideosForm}>Videos</Button>
					</Grid>
					<Grid item xs={3} style={styles.formTypeButtonContainer}>
						<Button style={showImagesForm ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowImagesForm}>Images</Button>
					</Grid>
					<Grid item xs={3} style={styles.formTypeButtonContainer}>
						<Button style={showLinksForm ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowLinksForm}>Links</Button>
					</Grid>
				</Grid>
				{showPostForm && 
					<Grid container style={{paddingTop: 10}}>
	        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
							<Grid item xs={12} style={styles.formRow}>
								<span style={styles.formTitle}>New Post:</span>
							</Grid>
							<Grid item xs={12} style={styles.formRow}>
								<Input 
									type="text" 
									name="title" 
									onChange={this.handleChange} 
									value={title} 
									placeholder="Title"
									style={styles.inputField}
								/>
							</Grid>
							<Grid item xs={12} style={styles.formRow}>
								<TextField 
									name="body" 
									onChange={this.handleChange} 
									value={body} 
									placeholder="Body"
									style={styles.textField}
									multiline={true}
									rows="8"
								/>
							</Grid>
							{(imageHighlightName === 'select') && 
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={3} style={{flex: 1}}>
										<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
									</Grid>
									<Grid item xs={9} style={{flex: 1}}>
										<Input 
											type="file" 
											name="image" 
											onChange={this.handleSelectFile}
										/>
									</Grid>
								</Grid>
							}
							{image === null &&
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={6} style={{flex: 1}}>
										<span style={{fontSize: 18}}>Image Highlight:</span>
									</Grid>
									<Grid item xs={6} style={{flex: 1}}>
										{this.props.loadingDashboardImagesAll === false ? 
											<Select
			                  value={imageHighlightName} 
			                  onChange={this.handleImageHighlightChange.bind(this)}
			                  inputProps={{
			                    name: `Image`,
			                    id: "Image"
			                  }} 
			                  style={styles.selectItemName} 
			                >
			                	{imageHighlightList.map((imageHighlight, index) => (
													<MenuItem value={imageHighlight.slug} key={index}>{imageHighlight.title}</MenuItem>
												))}
			                </Select> :
			               	<div><span>Loading Images <Dots /></span></div>
			              }
									</Grid>
								</Grid>
							}
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={6} style={{flex: 1}}>
									<span style={{fontSize: 18}}>Video Highlight:</span>
								</Grid>
								<Grid item xs={6} style={{flex: 1}}>
									{this.props.loadingDashboardVideosAll === false ? 
										<Select
		                  value={videoHighlightName} 
		                  onChange={this.handleVideoHighlightChange.bind(this)}
		                  inputProps={{
		                    name: `Video`,
		                    id: "Video"
		                  }} 
		                  style={styles.selectItemName} 
		                >
		                	{videoHighlightList.map((videoHighlight, index) => (
												<MenuItem value={videoHighlight.slug} key={index}>{videoHighlight.title}</MenuItem>
											))}
		                </Select> :
		                <div><span>Loading Videos <Dots /></span></div>
		              }
								</Grid>
							</Grid>
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={6} style={{flex: 1}}>
									<span style={{fontSize: 18}}>Photo Album:</span>
								</Grid>
								<Grid item xs={6} style={{flex: 1}}>
									{this.props.loadingDashboardPhotoAlbumsAll === false ? 
										<Select
		                  value={photoAlbumName} 
		                  onChange={this.handlePhotoAlbumChange.bind(this)}
		                  inputProps={{
		                    name: `Photo Album`,
		                    id: "Photo Album"
		                  }} 
		                  style={styles.selectItemName} 
		                >
		                	{photoAlbumList.map((photoAlbum, index) => (
												<MenuItem value={photoAlbum.slug} key={index}>{photoAlbum.title}</MenuItem>
											))}
		                </Select> :
		                <div><span>Loading Photo Albums <Dots /></span></div>
		              }
								</Grid>
							</Grid>
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={6} style={{flex: 1}}>
									<span style={{fontSize: 18}}>Music Album:</span>
								</Grid>
								<Grid item xs={6} style={{flex: 1}}>
									{this.props.loadingDashboardMusicAlbumsAll === false ?
										<Select
		                  value={musicAlbumName} 
		                  onChange={this.handleMusicAlbumChange.bind(this)}
		                  inputProps={{
		                    name: `Music Album`,
		                    id: "Music Album"
		                  }} 
		                  style={styles.selectItemName} 
		                >
		                	{musicAlbumList.map((musicAlbum, index) => (
												<MenuItem value={musicAlbum.slug} key={index}>{musicAlbum.title}</MenuItem>
											))}
		                </Select> :
		                <div><span>Loading Music Album <Dots /></span></div>
		              }
								</Grid>
							</Grid>
							{displayCategoriesList ? 
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={4}>
										<span style={{fontSize: 18}}>Category:</span>
									</Grid>
									<Grid item xs={8}>
										<Select
		                  value={categoryName} 
		                  onChange={this.handleCategoryChange.bind(this)}
		                  inputProps={{
		                    name: `Categories`,
		                    id: "Categories"
		                  }} 
		                  style={styles.selectItemName} 
		                >
		                	{categoriesList.map((category, index) => (
												<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
											))}
		                </Select>
	                </Grid>
								</Grid> :
								<Grid container item xs={12} style={styles.formRow}>
									<span>Loading Categories <Dots /></span>
								</Grid>
							}
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={1}>
									<Checkbox 
										name="blog" 
										checked={blog} 
										onChange={this.handleBlogChange} 
									/>
								</Grid>
								<Grid item xs={10}>
									<Button onClick={(event)=>{event.preventDefault(); this.handleBlogChange()}}>Blog</Button>
								</Grid>
							</Grid>
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={1}>
									<Checkbox 
										name="blogSticky" 
										checked={blogSticky} 
										onChange={this.handleBlogStickyChange} 
									/>
								</Grid>
								<Grid item xs={10}>
									<Button onClick={(event)=>{event.preventDefault(); this.handleBlogStickyChange()}}>Blog Sticky</Button>
								</Grid>
							</Grid>
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={1}>
									<Checkbox
										name="portfolio" 
										checked={portfolio} 
										onChange={this.handlePortfolioChange} 
									/>
								</Grid>
								<Grid item xs={10}>
									<Button onClick={(event)=>{event.preventDefault(); this.handlePortfolioChange()}}>Portfolio</Button>
								</Grid>
							</Grid>
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={1}>
									<Checkbox
										name="portfolioSticky" 
										checked={portfolioSticky} 
										onChange={this.handlePortfolioStickyChange} 
									/>
								</Grid>
								<Grid item xs={10}>
									<Button onClick={(event)=>{event.preventDefault(); this.handlePortfolioStickyChange()}}>Portfolio Sticky</Button>
								</Grid>
							</Grid>
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={1}>
									<Checkbox
										name="publish" 
										checked={publish} 
										onChange={this.handlePublishChange} 
									/>
								</Grid>
								<Grid item xs={10}>
									<Button onClick={(event)=>{event.preventDefault(); this.handlePublishChange()}}>Publish</Button>
								</Grid>
							</Grid>
							<Grid item xs={12} style={styles.formRow}>
								<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Post</Button>
							</Grid>
						</Grid>
						<Grid item md={4} style={styles.formRow}>
							<div style={{paddingBottom: 10}}>
								<span style={styles.tagsTitle}>Tags:</span>
							</div>
							{displayTagsList ? 
								<Grid container>
									{tagsList.map((tag, index) => (
										<Grid container item xs={12} style={{padding: 5}} key={index}>
											<Grid item xs={2}>
												<Checkbox 
													name={tag.tag} 
													checked={tag.checked} 
													onChange={this.handleSelectTag.bind(this, tag)}
												/>
											</Grid>
											<Grid item xs={10}>
												<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
											</Grid>
										</Grid>
									))}
								</Grid> :
								<div>
									<span>Loading Tags <Dots /></span>
								</div>
							}
						</Grid>
					</Grid>
				}
				{showVideosForm && 
					<div style={{paddingTop: 10}}>
						{showVideosList ? 
							<Grid container style={{flex: 1}}>
								<Grid item xs={12} style={styles.formRow}>
									<span style={styles.formTitle}>Videos:</span>
								</Grid>
								{videosList.map((video, index) => (
									<Grid container item xs={12} style={styles.formRow} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={video.slug} 
												checked={video.checked} 
												onChange={this.handleSelectVideo.bind(this, video)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectVideo.bind(this, video)}>{video.title}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<Grid container style={{flex: 1}}>
								<Grid item xs={12}><span>Loading Videos <Dots /></span></Grid>
							</Grid>
						}
					</div>
				}
				{showImagesForm && 
					<div style={{paddingTop: 10}}>
						{showImagesList ? 
							<Grid container style={{flex: 1}}>
								<Grid item xs={12} style={styles.formRow}>
									<span style={styles.formTitle}>Images:</span>
								</Grid>
								{imagesList.map((image, index) => (
									<Grid container item xs={12} style={styles.formRow} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={image.slug} 
												checked={image.checked} 
												onChange={this.handleSelectImage.bind(this, image)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectImage.bind(this, image)}>{image.title}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<Grid container style={{flex: 1}}>
								<Grid item xs={12}><span>Loading Images <Dots /></span></Grid>
							</Grid>
						}
					</div>
				}
				{showLinksForm && 
					<div style={{paddingTop: 10}}>
						{showLinksList ? 
							<Grid container style={{flex: 1}}>
								<Grid item xs={12} style={styles.formRow}>
									<span style={styles.formTitle}>Links:</span>
								</Grid>
								{linksList.map((link, index) => (
									<Grid container item xs={12} style={styles.formRow} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={link.slug} 
												checked={link.checked} 
												onChange={this.handleSelectLink.bind(this, link)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectLink.bind(this, link)}>{link.description}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<Grid container style={{flex: 1}}>
								<Grid item xs={12}><span>Loading Links <Dots /></span></Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newPost: state.addNew.newPost,
		newPostFail: state.addNew.newPostFail, 

		dashboardPhotoAlbumsAll: state.dashboard.dashboardPhotoAlbumsAll,
		loadingDashboardPhotoAlbumsAll: state.dashboard.loadingDashboardPhotoAlbumsAll,
		dashboardMusicAlbumsAll: state.dashboard.dashboardMusicAlbumsAll,
		loadingDashboardMusicAlbumsAll: state.dashboard.loadingDashboardMusicAlbumsAll,
		dashboardVideosAll: state.dashboard.dashboardVideosAll,
		loadingDashboardVideosAll: state.dashboard.loadingDashboardVideosAll,
		dashboardImagesAll: state.dashboard.dashboardImagesAll,
		loadingDashboardImagesAll: state.dashboard.loadingDashboardImagesAll,
		dashboardLinks: state.dashboard.dashboardLinks,
		loadingDashboardLinks: state.dashboard.loadingDashboardLinks,
		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { createNewPost, clearNewPost, clearNewPostFail, getDashboardPhotoAlbumsAll, clearDashboardPhotoAlbumsAll, getDashboardMusicAlbumsAll, clearDashboardMusicAlbumsAll, getDashboardVideosAll, clearDashboardVideosAll, getDashboardImagesAll, clearDashboardImagesAll, getDashboardLinks, clearDashboardLinks, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(NewPost);












