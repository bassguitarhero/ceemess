import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewLink, clearNewLink, clearNewLinkFail } from '../../../actions/addNew';
import { getDashboardLinks, clearDashboardLinks } from '../../../actions/dashboard';

class NewLink extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			linkURL: '',
			socialMediaLink: false,
			uploading: false
		}
	}

	handleCheckboxChange = () => {
		this.setState({
			socialMediaLink: !this.state.socialMediaLink
		})
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'linkURL'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { name, linkURL, socialMediaLink } = this.state;
		const link = { name, linkURL, socialMediaLink };
		if (name !== '' && linkURL !== '') {
			if (window.confirm("Create New Link?")) {
				this.props.createNewLink(link);
				this.setState({uploading: true});
			}
		} else {
			alert("Links must have a description and URL");
		}
	}

	componentDidMount() {
		this.props.getDashboardLinks();
	}

	componentDidUpdate(lastProps) {
		if (this.props.newLinkFail !== null && lastProps.newLinkFail === null) {
			this.props.clearNewLinkFail();
			this.setState({uploading: false});
		}
		if (this.props.newLink !== null && lastProps.newLink === null) {
			this.props.clearNewLink();
			this.props.clearDashboardLinks();
			this.props.getDashboardLinks();
			this.setState({
				name: '',
				linkURL: '',
				uploading: false
			})
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardLinks();
	}

	render() {
		const { name, linkURL, socialMediaLink, uploading } = this.state;
		const { dashboardLinks, loadingDashboardLinks } = this.props;

		return (
			<div>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>New Link:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="name" 
								onChange={this.handleChange} 
								value={name} 
								placeholder="Name"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="linkURL" 
								onChange={this.handleChange} 
								value={linkURL} 
								placeholder="URL"
								style={styles.inputField}
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={1}>
								<Checkbox 
									name="socialMediaLink" 
									checked={socialMediaLink} 
									onChange={this.handleCheckboxChange} 
								/>
							</Grid>
							<Grid item xs={10}>
								<Button onClick={this.handleCheckboxChange}>Social Media Link</Button>
							</Grid>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Link</Button>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						{!loadingDashboardLinks ? 
							<div>
								<div style={{padding: 5}}><span style={styles.tagsTitle}>Links:</span></div>
								{dashboardLinks.map((link, index) => (
									<div key={index} style={{padding: 5}}>
										<span>{link.description}</span>
									</div>
								))}
							</div> :
							<div style={{padding: 5}}>
								<span style={styles.tagsTitle}>Loading Links <Dots /></span>
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newLink: state.addNew.newLink, 
		newLinkFail: state.addNew.newLinkFail,

		dashboardLinks: state.dashboard.dashboardLinks,
		loadingDashboardLinks: state.dashboard.loadingDashboardLinks
	}
}

export default connect(mapStateToProps, { createNewLink, clearNewLink, clearNewLinkFail, getDashboardLinks, clearDashboardLinks })(NewLink);












