import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, Select, MenuItem, TextField } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewVideo, clearNewVideo, clearNewVideoFail } from '../../../actions/addNew';
import { getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class NewVideo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			youtubeID: '',
			vimeoID: '',
			description: '',
			image: null,
			category: null,
			aspectRatio: '',
			width: '',
			height: '',
			publicVideo: true,
			categoriesList: [],
			displayCategoriesList: false, 
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			uploadingVideo: false
		}
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

  handleSelectTag = (tag) => {
		// console.log('Tag: ', tag);
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.id === tagsList[i].id) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title' || key === 'youtubeID' || key === 'vimeoID'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleCheckboxChange = () => {
		this.setState({
			publicVideo: !this.state.publicVideo
		})
	}

	handleSelectFile = e => this.setState({
		image: e.target.files[0]
	});

	handleSubmit = () => {
		const { title, youtubeID, vimeoID, description, image, aspectRatio, width, height, publicVideo, categoriesList, tagsList, categoryName } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const video = { title, youtubeID, vimeoID, description, image, aspectRatio, width, height, publicVideo, category, tags };
		if (title !== '') {
			if (window.confirm("Create New Video?")) {
				this.props.createNewVideo(video);
				this.setState({uploadingVideo: true});
			}
		} else {
			alert("Video must have a title");
		}		
	}

	componentDidMount() {
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.newVideoFail !== null && lastProps.newVideoFail === null) {
			this.props.clearNewVideoFail();
			this.setState({uploadingVideo: false});
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: 'select',
			})
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			// console.log('Tags List: ', tagsList);
			this.setState({
				categoryName: 'select',
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			// console.log('Tags List: ', tagsList);
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.newVideo !== null && lastProps.newVideo === null) {
			this.setState({
				title: '',
				youtubeID: '',
				vimeoID: '',
				description: '',
				image: null,
				aspectRatio: '',
				width: '',
				height: '',
				publicVideo: true,
				categoriesList: [],
				displayCategoriesList: false, 
				category: null,
				tags: [],
				tagsList: [],
				displayTagsList: false,
				categoryName: '',
				uploadingVideo: false
			})
			this.props.clearNewVideo();
			this.props.clearDashboardCategories();
			this.props.clearDashboardTags();
			this.props.getDashboardCategories();
			this.props.getDashboardTags();
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { title, youtubeID, vimeoID, description, image, publicVideo, uploadingVideo } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;

		return (
			<div>
				{uploadingVideo && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>New Video:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="Description"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="image" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="youtubeID" 
								onChange={this.handleChange} 
								value={youtubeID} 
								placeholder="YouTube ID"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="vimeoID" 
								onChange={this.handleChange} 
								value={vimeoID} 
								placeholder="Vimeo ID"
								style={styles.inputField}
							/>
						</Grid>
						{displayCategoriesList ? 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={4}>
									<span style={{fontSize: 18}}>Category:</span>
								</Grid>
								<Grid item xs={8}>
									<Select
	                  value={categoryName} 
	                  onChange={this.handleItemChange.bind(this)}
	                  inputProps={{
	                    name: `Categories`,
	                    id: "Categories"
	                  }} 
	                  style={styles.selectItemName} 
	                >
	                	{categoriesList.map((category, index) => (
											<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
										))}
	                </Select>
                </Grid>
							</Grid> :
							<Grid item xs={12} style={styles.formRow}>
								<span>Loading Categories <Dots /></span>
							</Grid>
						}
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={1}>
								<Checkbox 
									name="publicVideo" 
									checked={publicVideo} 
									onChange={this.handleCheckboxChange} 
								/>
							</Grid>
							<Grid item xs={10}>
								<Button onClick={this.handleCheckboxChange}>Public</Button>
							</Grid>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Video</Button>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						<div>
							<span style={styles.tagsTitle}>Tags:</span>
						</div>
						{displayTagsList ? 
							<Grid container>
								{tagsList.map((tag, index) => (
									<Grid container item xs={12} style={{padding: 5}} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={tag.tag} 
												checked={tag.checked} 
												onChange={this.handleSelectTag.bind(this, tag)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<div><span>Loading Tags <Dots /></span></div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newVideo: state.addNew.newVideo,
		newVideoFail: state.addNew.newVideoFail, 

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { createNewVideo, clearNewVideo, clearNewVideoFail, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(NewVideo);












