import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewTag, clearNewTag, clearNewTagFail } from '../../../actions/addNew';
import { getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class NewTag extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			uploading: false
		}
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { name } = this.state;
		const tag = { name };
		if (name !== '') {
			if (window.confirm("Create New Tag?")) {
				this.props.createNewTag(tag);
				this.setState({uploading: true})
			}
		}
	}

	componentDidMount() {
		this.props.getDashboardTags();
	}

	componentDidUpdate(lastProps) {
		if (this.props.newTagFail !== null && lastProps.newTagFail === null) {
			this.props.clearNewTagFail();
			this.setState({uploading: false});
		}
		if (this.props.newTag !== null && lastProps.newTag === null) {
			this.props.clearNewTag();
			this.props.clearDashboardTags();
			this.props.getDashboardTags();
			this.setState({
				name: '',
				uploading: false
			})
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardTags();
	}

	render() {
		const { name, uploading } = this.state;
		const { dashboardTags, loadingDashboardTags } = this.props;

		return (
			<div>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>	
				}
				<Grid container>
        	<Grid container item xs style={{alignContent: 'flex-start', alignItems: 'flex-start', flex: 1}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>New Tag:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="name" 
								onChange={this.handleChange} 
								value={name} 
								placeholder="Name"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Tag</Button>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						{!loadingDashboardTags ? 
							<div>
								<div style={{padding: 5}}><span style={styles.tagsTitle}>Tags:</span></div>
								{dashboardTags.map((tag, index) => (
									<div key={index} style={{padding: 5}}>
										<span>{tag.tag}</span>
									</div>
								))}
							</div> :
							<div style={{padding: 5}}>
								<span style={styles.tagsTitle}>Loading Tags <Dots /></span>
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newTag: state.addNew.newTag,
		newTagFail: state.addNew.newTagFail, 

		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { createNewTag, clearNewTag, clearNewTagFail, getDashboardTags, clearDashboardTags })(NewTag);












