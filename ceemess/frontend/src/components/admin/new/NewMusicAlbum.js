import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import NewMusicAlbumTrack from './NewMusicAlbumTrack';
import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewMusicAlbum, clearNewMusicAlbum, createNewTrack, clearNewTrack } from '../../../actions/addNew';
import { clearNewMusicAlbumFail, clearNewTrackFail } from '../../../actions/addNew';
import { getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class NewMusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bandName: '',
			title: '',
			description: '',
			image: null,
			publicAlbum: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			showAlbum: true,
			showTracks: false,
			tracks: [],
			newAlbumTitle: '',
			newAlbumSlug: '',
			id: 1,
			stageTrackID: 1,
			uploadingAlbum: false,
			uploadingTracks: false
		}
	}

	handleRemoveTrack = (id) => {
		const { tracks } = this.state;
		let newTracks = tracks.filter(track => (track.id !== id));
		for (var i = 0; i < newTracks.length; i++) {
			newTracks[i].id = i + 1;
		}
		this.setState({id: newTracks.length + 1, tracks: newTracks});
	}

	uploadTrack = () => {
		const { tracks, newAlbumSlug, stageTrackID } = this.state;
		for (var i = 0; i < tracks.length; i++) {
			if (tracks[i].title === '') {
				alert("All Tracks must have a Title");
			} else {
				this.setState({uploadingTracks: true});
				for (var i = 0; i < tracks.length; i++) {
					if (tracks[i].id === stageTrackID) {
						if (tracks[i].id === 1) {
							if (window.confirm("Upload Tracks?")) {
								const track = { title: tracks[i].title, image: tracks[i].image, audio: tracks[i].audio, soundcloud: tracks[i].soundcloud, youtube_url: tracks[i].youtube_url, slug: newAlbumSlug };
								this.props.createNewTrack(track);
								this.setState({stageTrackID: stageTrackID + 1});
							}
						} else {
							const track = { title: tracks[i].title, image: tracks[i].image, audio: tracks[i].audio, soundcloud: tracks[i].soundcloud, youtube_url: tracks[i].youtube_url, slug: newAlbumSlug };
							this.props.createNewTrack(track);
							this.setState({stageTrackID: stageTrackID + 1});
						}
					}
				}
			}
		}	
	}

	handleChangeTrackText = (field, text, id) => {
		const { tracks } = this.state;
		for (var i = 0; i < tracks.length; i++) {
			if (tracks[i].id === id) {
				tracks[i][field] = text;
			}
		}
		this.setState({tracks: tracks});
	}

	handleChangeTrackImage = (image, id) => {
		const { tracks } = this.state;
		for (var i = 0; i < tracks.length; i++) {
			if (tracks[i].id === id) {
				tracks[i].image = image;
			}
		}
		this.setState({tracks:tracks});
	}

	handleChangeTrackAudio = (audio, id) => {
		const { tracks } = this.state;
		for (var i = 0; i < tracks.length; i++) {
			if (tracks[i].id === id) {
				tracks[i].audio = audio;
			}
		}
		this.setState({tracks:tracks});
	}

	addTrack = () => {
		const { tracks, id } = this.state;
		tracks.push({
			id: id,
			order: '',
			title: '',
			description: '',
			caption: '',
			image: null,
			audio: null,
			soundcloud: '',
			youtube_url: ''
		})
		this.setState({id: id + 1});
	}

	handleItemChange = (e) => {
    this.setState({
      itemName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'bandName' || key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.id === tagsList[i].id) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSubmit = () => {
		const { bandName, title, description, image, publicAlbum, categoriesList, tagsList, categoryName } = this.state;
		this.setState({uploadingAlbum: true});
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const album = { bandName, title, description, image, category, publicAlbum, tags };
		if (window.confirm("Create New Album?")) {
			this.props.createNewMusicAlbum(album);
			this.setState({uploadingAlbum: true});
		}
	}

	handleSelectFile = e => this.setState({
		image: e.target.files[0]
	});

	handleCheckboxChange = () => {
		this.setState({
			publicAlbum: !this.state.publicAlbum
		})
	}

	handleShowAlbumForm = () => {
		this.setState({
			showAlbum: true,
			showTracks: false
		})
	}

	handleShowTracksForm = () => {
		this.setState({
			showAlbum: false,
			showTracks: true
		})
	}

	componentDidMount() {
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
		this.addTrack();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.newMusicAlbumFail !== null && lastProps.newMusicAlbumFail === null) {
			this.props.clearNewMusicAlbumFail();
			this.setState({uploadingAlbum: false});
		}
		if (this.props.newTrackFail !== null && lastProps.newTrackFail === null) {
			this.props.clearNewTrackFail();
			this.setState({uploadingTracks: false});
		}
		if (this.props.newTrack !== null && lastProps.newTrack === null) {
			this.props.clearNewTrack();
			if (this.state.stageTrackID > this.state.tracks.length) {
				this.setState({uploadingTracks: false, tracks: [], newAlbumTitle: '', newAlbumSlug: '', showAlbum: true, showTracks: false});
				this.addTrack();
			} else {
				this.uploadTrack();
			}
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: "select"
			});
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			this.setState({
				categoryName: 'select',
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.newMusicAlbum !== null && lastProps.newMusicAlbum === null) {
			this.setState({
				bandName: '',
				title: '',
				description: '',
				image: null,
				publicAlbum: true,
				categoriesList: [],
				displayCategoriesList: false, 
				category: null,
				tags: [],
				tagsList: [],
				displayTagsList: false,
				categoryName: '',
				showAlbum: false,
				showTracks: true,
				newAlbumSlug: this.props.newMusicAlbum.slug,
				newAlbumTitle: this.props.newMusicAlbum.title,
				uploadingAlbum: false
			})
			this.props.clearNewMusicAlbum();
			this.props.clearDashboardCategories();
			this.props.clearDashboardTags();
			this.props.getDashboardCategories();
			this.props.getDashboardTags();
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { bandName, title, description, image, publicAlbum } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;
		const { showAlbum, showTracks, uploadingAlbum, uploadingTracks } = this.state;
		const { tracks, newAlbumTitle } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid container style={styles.formTypeButtonsContainer}>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showAlbum ? styles.formTypeButtonSelected : styles.formTypeButton } onClick={this.handleShowAlbumForm}>Album</Button>
					</Grid>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showTracks ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowTracksForm}>Tracks</Button>
					</Grid>
				</Grid>
				{showAlbum && 
					<div>
						{uploadingAlbum && 
							<div>
								<UploadModal />
								<Backdrop />
							</div>
						}
						<Grid container style={{paddingTop: 10}}>
		        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
								<Grid item xs={12} style={styles.formRow}>
									<span style={styles.formTitle}>New Music Album:</span>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<Input 
										type="text" 
										name="bandName" 
										onChange={this.handleChange} 
										value={bandName} 
										placeholder="Band Name"
										style={styles.inputField}
									/>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<Input 
										type="text" 
										name="title" 
										onChange={this.handleChange} 
										value={title} 
										placeholder="Title"
										style={styles.inputField}
									/>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<TextField 
										name="description" 
										onChange={this.handleChange} 
										value={description} 
										placeholder="Description"
										style={styles.textField}
										multiline={true}
										rows="8"
									/>
								</Grid>
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={3} style={{flex: 1}}>
										<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
									</Grid>
									<Grid item xs={9} style={{flex: 1}}>
										<Input 
											type="file" 
											name="image" 
											onChange={this.handleSelectFile}
										/>
									</Grid>
								</Grid>
								{displayCategoriesList ? 
									<Grid container item xs={12} style={styles.formRow}>
										<Grid item xs={4}>
											<span style={{fontSize: 18}}>Category:</span>
										</Grid>
										<Grid item xs={8}>
											<Select
			                  value={categoryName} 
			                  onChange={this.handleItemChange.bind(this)}
			                  inputProps={{
			                    name: `Categories`,
			                    id: "Categories"
			                  }} 
			                  style={styles.selectItemName} 
			                >
			                	{categoriesList.map((category, index) => (
													<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
												))}
			                </Select>
		                </Grid>
									</Grid> :
									<Grid item xs={12} style={styles.formRow}>
										<span>Loading Categories <Dots /></span>
									</Grid>
								}
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={1}>
										<Checkbox 
											name="publicAlbum" 
											checked={publicAlbum} 
											onChange={this.handleCheckboxChange} 
										/>
									</Grid>
									<Grid item xs={10}>
										<Button onClick={this.handleCheckboxChange}>Public</Button>
									</Grid>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Music Album</Button>
								</Grid>
							</Grid>
							<Grid item md={4} style={styles.formRow}>
								<div>
									<span style={styles.tagsTitle}>Tags:</span>
								</div>
								{displayTagsList ? 
									<Grid container>
										{tagsList.map((tag, index) => (
											<Grid container item xs={12} style={{padding: 5}} key={index}>
												<Grid item xs={2}>
													<Checkbox 
														name={tag.tag} 
														checked={tag.checked} 
														onChange={this.handleSelectTag.bind(this, tag)}
													/>
												</Grid>
												<Grid item xs={10}>
													<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
												</Grid>
											</Grid>
										))}
									</Grid> :
									<div>
										<span>Loading Tags <Dots /></span>
									</div>
								}
							</Grid>
						</Grid>
					</div>
				}
				{showTracks && 
					<div style={{paddingTop: 10}}>
						{uploadingTracks && 
							<div>
								<UploadModal />
								<Backdrop />
							</div>
						}
						<div style={{padding: 10}}><span style={{fontSize: 18, fontWeight: 'bold'}}>{newAlbumTitle}:</span></div>
						{tracks.map((track, index) => (
							<NewMusicAlbumTrack track={track} changeText={this.handleChangeTrackText} changeImage={this.handleChangeTrackImage} changeAudio={this.handleChangeTrackAudio} cancel={this.handleRemoveTrack} key={index} />
						))}
						<Grid container style={styles.formTypeButtonsContainer}>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.addTrackButton} onClick={this.addTrack}>Add Track</Button>
							</Grid>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.uploadTracksButton} onClick={this.uploadTrack}>Upload Tracks</Button>
							</Grid>
						</Grid>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newMusicAlbum: state.addNew.newMusicAlbum, 
		newTrack: state.addNew.newTrack,

		newMusicAlbumFail: state.addNew.newMusicAlbumFail,
		newTrackFail: state.addNew.newTrackFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { createNewMusicAlbum, clearNewMusicAlbum, createNewTrack, clearNewTrack, clearNewMusicAlbumFail, clearNewTrackFail, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(NewMusicAlbum);












