import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField } from '@material-ui/core';
import { styles } from '../../../Styles';

class NewPhotoAlbumPhoto extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: '',
			photoID: '',
			order: '',
			title: '',
			description: '',
			caption: '',
			image: null
		}
	}

	handleCancelPhoto = () => {
		const { photo } = this.props;
		this.props.cancel(photo.id);
	}

	handleSelectFile = (e) => {
		this.setState({
			image: e.target.files[0]
		});
		this.props.changeImage(e.target.files[0], this.props.photo.id);
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  	this.props.changeText(e.target.name, e.target.value, this.props.photo.id);
	}

	componentDidMount() {
		const { photo } = this.props;
		this.setState({
			id: photo.id,
			photoID: photo.photoID,
			title: photo.title,
			description: photo.description,
			caption: photo.caption,
			image: photo.image
		})
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.photo !== lastProps.photo) {
			this.setState({image: null});
			const { photo } = this.props;
			var image = photo.image;
			this.setState({
				id: photo.id,
				photoID: photo.photoID,
				title: photo.title,
				description: photo.description,
				caption: photo.caption,
				image: image
			})
		}
	}

	render() {
		const { order, title, description, caption, image } = this.state;
		const { photo } = this.props;
		
		return (
			<div>
				<Grid container style={{paddingBottom: 20}}>
					<Grid item xs={1} style={{textAlign: 'center', paddingTop: 30}}>{photo.photoID}</Grid>
					<Grid container item xs={11} style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={11} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={1}>
							<Button style={styles.closeButton} onClick={this.handleCancelPhoto}>X</Button>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="image" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps, { })(NewPhotoAlbumPhoto);












