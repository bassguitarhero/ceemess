import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField } from '@material-ui/core';
import { styles } from '../../../Styles';

class NewMusicAlbumTrack extends Component {
	constructor(props) {
		super(props);
		this.state = {
			order: '',
			title: '',
			description: '',
			caption: '',
			image: null,
			soundcloud: '',
			audio: null,
			youtube_url: '',
		}
	}

	handleCancelTrack = () =>  {
		this.props.cancel(this.props.track.id);
	}

	handleSelectFile = (e) => {
		this.setState({
			image: e.target.files[0]
		});
		this.props.changeImage(e.target.files[0], this.props.track.id);
	}

	handleSelectAudio = (e) => {
		this.setState({
			audio: e.target.files[0]
		});
		this.props.changeAudio(e.target.files[0], this.props.track.id);
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  	this.props.changeText(e.target.name, e.target.value, this.props.track.id);
	}

	componentDidMount() {
		const { track } = this.props;
		this.setState({
			id: track.id,
			title: track.title,
			description: track.description,
			image: track.image,
			soundcloud: track.soundcloud,
			audio: track.audio,
			youtube_url: track.youtube_url
		});
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.track !== lastProps.track) {
			const { track } = this.props;
			this.setState({
				id: track.id,
				title: track.title,
				description: track.description,
				image: track.image,
				soundcloud: track.soundcloud,
				audio: track.audio,
				youtube_url: track.youtube_url
			});
		}
	}

	render() {
		const { order, title, description, caption, image, soundcloud, audio, youtube_url } = this.state;
		const { track } = this.props;

		return (
			<div>
				<Grid container style={{paddingBottom: 20}}>
					<Grid item xs={1} style={{textAlign: 'center', paddingTop: 30}}>{track.id}</Grid>
					<Grid container item xs={11} style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={11} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={1}>
							<Button style={styles.closeButton} onClick={this.handleCancelTrack}>X</Button>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="Description"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="image" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						{(audio === null && youtube_url === '') && 
							<Grid item xs={12} style={styles.formRow}>
								<Input 
									type="text" 
									name="soundcloud" 
									onChange={this.handleChange} 
									value={soundcloud} 
									placeholder="Soundcloud URL"
									style={styles.inputField}
								/>
							</Grid>
						}
						{(soundcloud === '' && youtube_url === '') && 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={3} style={{flex: 1}}>
									<label htmlFor="file" style={{fontSize: 18}}>Audio:</label>
								</Grid>
								<Grid item xs={9} style={{flex: 1}}>
									<Input 
										type="file" 
										name="audio" 
										onChange={this.handleSelectAudio}
									/>
								</Grid>
							</Grid>
						}
						{(audio === null && soundcloud === '') && 
							<Grid item xs={12} style={styles.formRow}>
								<Input 
									type="text" 
									name="youtube_url" 
									onChange={this.handleChange} 
									value={youtube_url} 
									placeholder="YouTube URL"
									style={styles.inputField}
								/>
							</Grid>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps, { })(NewMusicAlbumTrack);












