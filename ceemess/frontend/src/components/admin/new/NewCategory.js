import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewCategory, clearNewCategory, clearNewCategoryFail } from '../../../actions/addNew';
import { getDashboardCategories, clearDashboardCategories } from '../../../actions/dashboard';

class NewCategory extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			uploading: false
		}
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { name } = this.state;
		const category = { name };
		if (name !== '') {
			if (window.confirm("Create New Category?")) {
				this.props.createNewCategory(category);
				this.setState({uploading: true})
			}
		} else {
			alert("Category must have a name");
		}
	}

	componentDidMount() {
		this.props.getDashboardCategories();
	}

	componentDidUpdate(lastProps) {
		if (this.props.newCategoryFail !== null && lastProps.newCategoryFail === null) {
			this.props.clearNewCategoryFail();
			this.setState({uploading: false});
		}
		if (this.props.newCategory !== null && lastProps.newCategory === null) {
			this.props.clearNewCategory();
			this.props.clearDashboardCategories();
			this.props.getDashboardCategories();
			this.setState({
				name: '',
				uploading: false
			})
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
	}

	render() {
		const { name, uploading } = this.state;
		const { dashboardCategories, loadingDashboardCategories } = this.props;

		return (
			<div>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>New Category:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="name" 
								onChange={this.handleChange} 
								value={name} 
								placeholder="Name"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Category</Button>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						{!loadingDashboardCategories ? 
							<div>
								<div style={{padding: 5}}><span style={styles.tagsTitle}>Categories:</span></div>
								{dashboardCategories.map((category, index) => (
									<div key={index} style={{padding: 5}}>
										<span>{category.category}</span>
									</div>
								))}
							</div> :
							<div style={{padding: 5}}>
								<span style={styles.tagsTitle}>Loading Categories <Dots /></span>
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newCategory: state.addNew.newCategory, 
		newCategoryFail: state.addNew.newCategoryFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories
	}
}

export default connect(mapStateToProps, { createNewCategory, clearNewCategory, clearNewCategoryFail, getDashboardCategories, clearDashboardCategories })(NewCategory);












