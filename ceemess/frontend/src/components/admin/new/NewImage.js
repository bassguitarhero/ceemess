import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewImage, clearNewImage, clearNewImageFail } from '../../../actions/addNew';
import { getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class NewImage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			description: '',
			caption: '',
			file: null,
			publicImage: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			uploadingImage: false
		}
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title' || key === 'caption'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSelectTag = (tag) => {
		// console.log('Tag: ', tag);
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.id === tagsList[i].id) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSelectFile = e => this.setState({
		file: e.target.files[0]
	});

	handleCheckboxChange = () => {
		this.setState({
			publicImage: !this.state.publicImage
		})
	}

	handleSubmit = () => {
		const { title, description, caption, file, publicImage, tagsList, categoriesList, categoryName } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const image = { title, description, caption, file, publicImage, category, tags };
		if (title !== '' && file !== null) {
			if (window.confirm("Create New Image?")) {
				this.props.createNewImage(image);
				this.setState({uploadingImage: true});
			}
		} else {
			alert("Images must have a Title and an Image File.");
		}
	}

	componentDidMount() {
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.newImageFail !== null && lastProps.newImageFail === null) {
			this.props.clearNewImageFail();
			this.setState({uploadingImage: false});
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: "select"
			});
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			// console.log('Tags List: ', tagsList);
			this.setState({
				categoryName: 'select',
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			// console.log('Tags List: ', tagsList);
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.newImage !== null && lastProps.newImage === null) {
			this.setState({
				title: '',
				description: '',
				caption: '',
				file: null,
				publicImage: true,
				categoriesList: [],
				displayCategoriesList: false, 
				category: null,
				tags: [],
				tagsList: [],
				displayTagsList: false,
				categoryName: '',
				uploadingImage: false
			})
			this.props.clearNewImage();
			this.props.clearDashboardCategories();
			this.props.clearDashboardTags();
			this.props.getDashboardCategories();
			this.props.getDashboardTags();
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { title, description, caption, file, publicImage, uploadingImage } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;

		return (
			<div>
				{uploadingImage && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={12} style={styles.formRow}>
							<span style={styles.formTitle}>New Image:</span>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="Description"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="caption" 
								onChange={this.handleChange} 
								value={caption} 
								placeholder="Caption"
								style={styles.inputField}
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>Image:</label>
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="file" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						{displayCategoriesList ? 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={4}>
									<span style={{fontSize: 18}}>Category:</span>
								</Grid>
								<Grid item xs={8}>
									<Select
	                  value={categoryName} 
	                  onChange={this.handleItemChange.bind(this)}
	                  inputProps={{
	                    name: `Categories`,
	                    id: "Categories"
	                  }} 
	                  style={styles.selectItemName} 
	                >
	                	{categoriesList.map((category, index) => (
											<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
										))}
	                </Select>
                </Grid>
							</Grid> :
							<Grid item xs={12} style={styles.formRow}>
								<span>Loading Categories <Dots /></span>
							</Grid>
						}
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={1}>
								<Checkbox 
									name="publicImage" 
									checked={publicImage} 
									onChange={this.handleCheckboxChange} 
								/>
							</Grid>
							<Grid item xs={10}>
								<Button onClick={this.handleCheckboxChange}>Public</Button>
							</Grid>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Image</Button>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						<div>
							<span style={styles.tagsTitle}>Tags:</span>
						</div>
						{displayTagsList ? 
							<Grid container>
								{tagsList.map((tag, index) => (
									<Grid container item xs={12} style={{padding: 5}} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={tag.tag} 
												checked={tag.checked} 
												onChange={this.handleSelectTag.bind(this, tag)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<div>
								<span>Loading Tags <Dots /></span>
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newImage: state.addNew.newImage, 
		newImageFail: state.addNew.newImageFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { createNewImage, clearNewImage, clearNewImageFail, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(NewImage);











