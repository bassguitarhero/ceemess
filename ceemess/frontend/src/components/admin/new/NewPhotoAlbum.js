import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import NewPhotoAlbumPhoto from './NewPhotoAlbumPhoto';
import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { createNewPhotoAlbum, clearNewPhotoAlbum, createNewPhoto, clearNewPhoto, clearNewPhotoAlbumFail, clearNewPhotoFail } from '../../../actions/addNew';
import { getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class NewPhotoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			description: '',
			publicAlbum: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			showAlbum: true,
			showPhotos: false,
			photos: [],
			id: 1,
			photoID: 1,
			newAlbumTitle: '',
			newAlbumSlug: '',
			stagePhotoID: 1,
			uploadingAlbum: false,
			uploadingPhotos: false
		}
	}

	uploadNewPhoto = () => {
		const { photos, newAlbumSlug, stagePhotoID } = this.state;
		for (var i = 0; i < photos.length; i++) {
			if (photos[i].title === '' || photos[i].image === null) {
				alert("Each Photo must have a title and image");
			} else {
				this.setState({uploadingPhotos: true});
				for (var i = 0; i < photos.length; i++) {
					if (photos[i].id === stagePhotoID) {
						const photo = { title: photos[i].title, photo: photos[i].image, slug: newAlbumSlug }
						if (photos[i].id === 1) {
							if (window.confirm("Upload New Photos?")) {
								this.props.createNewPhoto(photo);
								this.setState({stagePhotoID: stagePhotoID + 1});
							}
						} else {
							this.props.createNewPhoto(photo);
							this.setState({stagePhotoID: stagePhotoID + 1});
						}
					}
				}
			}
		}		
	}

	handleChangePhotoText = (field, text, id) => {
		const { photos } = this.state;
		for (var i = 0; i < photos.length; i++) {
			if (photos[i].id === id) {
				photos[i][field] = text;
			}
		}
		this.setState({photos: photos});
	}

	handleChangePhotoImage = (image, id) => {
		const { photos } = this.state;
		for (var i = 0; i < photos.length; i++) {
			if (photos[i].id === id) {
				photos[i].image = image;
			}
		}
		this.setState({photos:photos});
	}

	removePhoto = (photoID) => {
		const { photos } = this.state;
		// let newPhotos = photos.filter(photo => (photo.photoID !== photoID));
		let newPhotos = [];
		for (var i = 0; i < photos.length; i++) {
			if (photos[i].photoID !== photoID) {
				newPhotos.push({
					id: photos[i].id,
					photoID: photos[i].photoID,
					title: photos[i].title,
					image: photos[i].image
				});
			}
		}
		for (var i = 0; i < newPhotos.length; i++) {
			newPhotos[i].id = i + 1;
		}
		this.setState({photos: newPhotos});
	}

	addPhoto = () => {
		const { photos, id, photoID } = this.state;
		photos.push({
			id: id,
			photoID: photoID,
			order: '',
			title: '',
			description: '',
			caption: '',
			image: null
		});
		this.setState({photos: photos, id: id + 1, photoID: photoID + 1});
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleCheckboxChange = () => {
		this.setState({
			publicAlbum: !this.state.publicAlbum
		})
	}

	handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.id === tagsList[i].id) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSubmit = () => {
		this.setState({uploadingAlbum: true});
		const { title, description, publicAlbum, categoriesList, tagsList, categoryName } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const album = { title, description, category, publicAlbum, tags };
		if (window.confirm("Create New Album?")) {
			this.props.createNewPhotoAlbum(album);
			this.setState({uploadingAlbum: true});
		}
	}

	handleShowAlbumForm = () => {
		// this.setState({
		// 	showAlbum: true,
		// 	showPhotos: false
		// })
		alert("Select 'Upload Photos' to complete your Album");
	}

	handleShowPhotosForm = () => {
		// this.setState({
		// 	showAlbum: false,
		// 	showPhotos: true
		// })
		alert("Create a new Album before uploading Photos");
	}

	componentDidMount() {
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
		this.addPhoto();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.newPhotoAlbumFail !== null && lastProps.newPhotoAlbumFail === null) {
			this.props.clearNewPhotoAlbumFail();
			this.setState({uploadingAlbum: false});
		}
		if (this.props.newPhotoFail !== null && lastProps.newPhotoFail === null) {
			this.props.clearNewPhotoFail();
			this.setState({uploadingPhotos: false});
		}
		if (this.props.newPhoto !== null && lastProps.newPhoto === null) {
			this.props.clearNewPhoto();
			if (this.state.stagePhotoID > this.state.photos.length) {
				this.setState({uploadingPhotos: false, photos: [], newAlbumTitle: '', newAlbumSlug: '', showAlbum: true, showPhotos: false})
				this.addPhoto();
			} else {
				this.uploadNewPhoto();
			}
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: "select"
			});
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			this.setState({
				categoryName: 'select',
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.newPhotoAlbum !== null && lastProps.newPhotoAlbum === null) {
			const { newPhotoAlbum } = this.props;
			this.setState({
				title: '',
				description: '',
				publicAlbum: true,
				categoriesList: [],
				displayCategoriesList: false, 
				category: null,
				tags: [],
				tagsList: [],
				displayTagsList: false,
				categoryName: '',
				newAlbumTitle: newPhotoAlbum.title,
				newAlbumSlug: newPhotoAlbum.slug,
				showAlbum: false,
				showPhotos: true,
				uploadingAlbum: false
			})
			this.props.clearNewPhotoAlbum();
			this.props.clearDashboardCategories();
			this.props.clearDashboardTags();
			this.props.getDashboardCategories();
			this.props.getDashboardTags();
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { title, description, publicAlbum } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;
		const { showAlbum, showPhotos, uploadingAlbum, uploadingPhotos } = this.state;
		const { photos, newAlbumTitle } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid container style={styles.formTypeButtonsContainer}>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showAlbum ? styles.formTypeButtonSelected : styles.formTypeButton } onClick={this.handleShowAlbumForm}>Album</Button>
					</Grid>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showPhotos ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowPhotosForm}>Photos</Button>
					</Grid>
				</Grid>
				{showAlbum &&
					<div>
						{uploadingAlbum && 
							<div>
								<UploadModal />
								<Backdrop />
							</div>
						}
						<Grid container style={{paddingTop: 10}}>
		        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
								<Grid item xs={12} style={styles.formRow}>
									<span style={styles.formTitle}>New Photo Album:</span>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<Input 
										type="text" 
										name="title" 
										onChange={this.handleChange} 
										value={title} 
										placeholder="Title"
										style={styles.inputField}
									/>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<TextField 
										name="description" 
										onChange={this.handleChange} 
										value={description} 
										placeholder="Description"
										style={styles.textField}
										multiline={true}
										rows="8"
									/>
								</Grid>
								{displayCategoriesList ? 
									<Grid container item xs={12} style={styles.formRow}>
										<Grid item xs={4}>
											<span style={{fontSize: 18}}>Category:</span>
										</Grid>
										<Grid item xs={8}>
											<Select
			                  value={categoryName} 
			                  onChange={this.handleItemChange.bind(this)}
			                  inputProps={{
			                    name: `Categories`,
			                    id: "Categories"
			                  }} 
			                  style={styles.selectItemName} 
			                >
			                	{categoriesList.map((category, index) => (
													<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
												))}
			                </Select>
		                </Grid>
									</Grid> :
									<Grid item xs={12} style={styles.formRow}>
										<span>Loading Categories <Dots /></span>
									</Grid>
								}
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={1}>
										<Checkbox 
											name="publicAlbum" 
											checked={publicAlbum} 
											onChange={this.handleCheckboxChange} 
										/>
									</Grid>
									<Grid item xs={10}>
										<Button onClick={this.handleCheckboxChange}>Public</Button>
									</Grid>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<Button style={styles.submitButton} onClick={this.handleSubmit}>Create Photo Album</Button>
								</Grid>
							</Grid>
							<Grid item md={4} style={styles.formRow}>
								<div>
									<span style={styles.tagsTitle}>Tags:</span>
								</div>
								{displayTagsList ? 
									<Grid container>
										{tagsList.map((tag, index) => (
											<Grid container item xs={12} style={{padding: 5}} key={index}>
												<Grid item xs={2}>
													<Checkbox 
														name={tag.tag} 
														checked={tag.checked} 
														onChange={this.handleSelectTag.bind(this, tag)}
													/>
												</Grid>
												<Grid item xs={10}>
													<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
												</Grid>
											</Grid>
										))}
									</Grid> :
									<div>
										<span style={styles.tagsTitle}>Loading Tags <Dots /></span>
									</div>
								}
							</Grid>
						</Grid>
					</div>
				}
				{showPhotos && 
					<div style={{paddingTop: 10}}>
						{uploadingPhotos && 
							<div>
								<UploadModal />
								<Backdrop />
							</div>
						}
						<div style={{padding: 10}}><span style={{fontSize: 18, fontWeight: 'bold'}}>{newAlbumTitle}:</span></div>
						{photos.map((photo, index) => (
							<NewPhotoAlbumPhoto photo={photo} changeText={this.handleChangePhotoText} changeImage={this.handleChangePhotoImage} cancel={this.removePhoto} key={index} />
						))}
						<Grid container style={styles.formTypeButtonsContainer}>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.addPhotoButton} onClick={this.addPhoto}>Add Photo</Button>
							</Grid>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.uploadPhotosButton} onClick={this.uploadNewPhoto}>Upload Photos</Button>
							</Grid>
						</Grid>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newPhotoAlbum: state.addNew.newPhotoAlbum, 
		newPhoto: state.addNew.newPhoto, 

		newPhotoAlbumFail: state.addNew.newPhotoAlbumFail,
		newPhotoFail: state.addNew.newPhotoFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { createNewPhotoAlbum, clearNewPhotoAlbum, createNewPhoto, clearNewPhoto, clearNewPhotoAlbumFail, clearNewPhotoFail, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(NewPhotoAlbum);












