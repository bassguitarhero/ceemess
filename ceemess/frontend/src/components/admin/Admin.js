import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import Dashboard from './Dashboard';
import AddNew from './AddNew';
import BlogSettings from './BlogSettings';

import { logout } from '../../actions/admin';

class Admin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showDashboard: true,
			showAddNew: false,
			showSettings: false
		}
	}

	handleShowAddNew = () => {
		this.setState({
			showDashboard: false,
			showAddNew: true,
			showSettings: false
		})
	}

	handleShowDashboard = () => {
		this.setState({
			showDashboard: true,
			showAddNew: false,
			showSettings: false
		})
	}

	handleShowSettings = () => {
		this.setState({
			showDashboard: false,
			showAddNew: false,
			showSettings: true
		})
	}

	handleShowBlog = () => {
		this.props.showBlog();
	}

	handleLogOut = () => {
		// console.log('Log Out');
		this.props.logout();
		// this.props.showBlog();
	}

	componentDidMount() {
		if (this.props.isAuthenticated === false) {
			this.handleShowBlog();
		}
	}

	componentDidUpdate(lastProps) {
		// if (this.props.isAuthenticated === false && lastProps.isAuthenticated === true) {
		// 	this.handleShowBlog();
		// }
	}

	render() {
		const { showDashboard, showAddNew, showSettings } = this.state;

		return (
			<div>
				<Grid container style={{flex: 1}}>
					<Grid md={1} item />
	        <Grid container md={10} item xs style={{flex: 1}}>
	        	<Grid item container xs={12} style={styles.adminTypeButtonsContainer}>
	        		<Grid item xs={3} style={styles.adminTypeButtonContainer}>
	        			<Button style={showDashboard ? styles.adminTypeButtonSelected : styles.adminTypeButton} onClick={this.handleShowDashboard}>Dashboard</Button>
	        		</Grid>
	        		<Grid item xs={3} style={styles.adminTypeButtonContainer}>
	        			<Button style={showAddNew ? styles.adminTypeButtonSelected : styles.adminTypeButton} onClick={this.handleShowAddNew}>Add New</Button>
	        		</Grid>
	        		<Grid item xs={3} style={styles.adminTypeButtonContainer}>
	        			<Button style={showSettings ? styles.adminTypeButtonSelected : styles.adminTypeButton} onClick={this.handleShowSettings}>Settings</Button>
	        		</Grid>
	        		<Grid item xs={3} style={styles.adminTypeButtonContainer}>
	        			<Button style={styles.logoutButton} onClick={this.handleLogOut}>Log Out</Button>
	        		</Grid>
	        	</Grid>
	        	<Grid item container xs={12}>
		        	{showDashboard && 
		        		<Dashboard />
		        	}
		        	{showAddNew && 
		        		<AddNew />
		        	}
		        	{showSettings && 
		        		<BlogSettings />
		        	}
	        	</Grid>
	        </Grid>
	      </Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		isAuthenticated: state.admin.isAuthenticated
	}
}

export default connect(mapStateToProps, { logout })(Admin);












