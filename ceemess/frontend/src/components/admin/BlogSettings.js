import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

import Users from './settings/Users';
import Profile from './settings/Profile';
import ImportForm from './settings/ImportForm';
import ExportForm from './settings/ExportForm';
import BlogSettingsHome from './settings/BlogSettingsHome';

class BlogSettings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showSettingsForm: true,
			showProfile: false,
			showImportForm: false,
			showExportForm: false,
			showUsers: false,
		}
	}

	handleShowSettingsForm = () => {
		this.setState({
			showSettingsForm: true,
			showProfile: false,
			showImportForm: false,
			showExportForm: false,
			showUsers: false,
		})
	}

	handleShowProfile = () => {
		this.setState({
			showSettingsForm: false,
			showProfile: true,
			showImportForm: false,
			showExportForm: false,
			showUsers: false,
		})
	}

	handleShowImportForm = () => {
		this.setState({
			showSettingsForm: false,
			showProfile: false,
			showImportForm: true,
			showExportForm: false,
			showUsers: false,
		})
	}

	handleShowExportForm = () => {
		this.setState({
			showSettingsForm: false,
			showProfile: false,
			showImportForm: false,
			showExportForm: true,
			showUsers: false,
		})
	}

	handleShowUsers = () => {
		this.setState({
			showSettingsForm: false,
			showProfile: false,
			showImportForm: false,
			showExportForm: false,
			showUsers: true,
		})
	}

	render() {
		const { showSettingsForm, showProfile, showImportForm, showExportForm, showUsers } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid container style={{flex: 1}}>

					<Grid item sm={10} xs={12}>
						{showSettingsForm && 
							<BlogSettingsHome />
						}
						{showProfile && 
							<Profile />
						}
						{showImportForm && 
							<ImportForm />
						}
						{showExportForm && 
							<Grid container item xs={10}>
								Export Form
							</Grid>
						}
						{showUsers && 
							<Grid container item xs={10}>
								<Users />
							</Grid>
						}
					</Grid>
					<Grid container item sm={2} xs={12} style={{padding: 5}}>
						<div>
							<div style={{padding: 10}}>
								<span style={{fontSize: 18, fontWeight: 'bold'}}>View:</span>
							</div>
							<div style={{padding: 5}}>
								<Button style={showSettingsForm ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowSettingsForm}>Settings</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showProfile ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowProfile}>Profile</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showUsers ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowUsers}>Users</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showImportForm ? styles.dashboardTypeButtonSelected : styles.dashboardTypeButton} onClick={this.handleShowImportForm}>Import</Button>
							</div>
						</div>
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default BlogSettings;












