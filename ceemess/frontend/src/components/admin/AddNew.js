import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import NewPost from './new/NewPost';
import NewPhotoAlbum from './new/NewPhotoAlbum';
import NewMusicAlbum from './new/NewMusicAlbum';
import NewVideo from './new/NewVideo';
import NewImage from './new/NewImage';
import NewLink from './new/NewLink';
import NewCategory from './new/NewCategory';
import NewTag from './new/NewTag';

class AddNew extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showNewPost: true,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: false
		}
	}

	handleShowNewPost = () => {
		this.setState({
			showNewPost: true,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: false
		})
	}

	handleShowNewPhotoAlbum = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: true,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: false
		})
	}

	handleShowNewMusicAlbum = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: true,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: false
		})
	}

	handleShowNewVideo = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: true,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: false
		})
	}

	handleShowNewImage = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: true,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: false
		})
	}

	handleShowNewLink = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: true,
			showNewCategory: false,
			showNewTag: false
		})
	}

	handleShowNewCategory = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: true,
			showNewTag: false
		})
	}

	handleShowNewTag = () => {
		this.setState({
			showNewPost: false,
			showNewPhotoAlbum: false,
			showNewMusicAlbum: false,
			showNewVideo: false,
			showNewImage: false,
			showNewLink: false,
			showNewCategory: false,
			showNewTag: true
		})
	}
	
	render() {
		const { showNewPost, showNewPhotoAlbum, showNewMusicAlbum, showNewVideo, showNewImage, showNewLink, showNewCategory, showNewTag } = this.state;
		return (
			<div style={styles.container}>
				<Grid container>
					<Grid container item sm={10} xs={12} style={{padding: 5}}>
						{showNewPost && 
							<NewPost />
						}
						{showNewPhotoAlbum && 
							<NewPhotoAlbum />
						}
						{showNewMusicAlbum && 
							<NewMusicAlbum />
						}
						{showNewVideo && 
							<NewVideo />
						}
						{showNewImage && 
							<NewImage />
						}
						{showNewLink && 
							<NewLink />
						}
						{showNewCategory && 
							<NewCategory />
						}
						{showNewTag && 
							<NewTag />
						}
					</Grid>
					<Grid container item sm={2} xs={12} style={{padding: 5}}>
						<div>
							<div style={{paddingLeft: 10, paddingTop: 2,paddingBottom: 10}}>
								<span style={{fontSize: 18, fontWeight: 'bold'}}>Add New:</span>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewPost ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewPost}>Post</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewPhotoAlbum ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewPhotoAlbum}>Photo</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewMusicAlbum ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewMusicAlbum}>Music</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewVideo ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewVideo}>Video</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewImage ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewImage}>Image</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewLink ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewLink}>Link</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewCategory ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewCategory}>Category</Button>
							</div>
							<div style={{padding: 5}}>
								<Button style={showNewTag ? styles.addNewTypeButtonSelected : styles.addNewTypeButton} onClick={this.handleShowNewTag}>Tag</Button>
							</div>
						</div>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps, { })(AddNew);












