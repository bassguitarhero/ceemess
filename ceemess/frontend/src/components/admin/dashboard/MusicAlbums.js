import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import MusicAlbum from './MusicAlbum';
import MusicAlbumInline from './MusicAlbumInline';

import { getDashboardMusicAlbums, getMoreDashboardMusicAlbums, clearDashboardMusicAlbums } from '../../../actions/dashboard';

class MusicAlbums extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicAlbums: true,
			showMusicAlbum: false,
			musicAlbum: null
		}
	}

	handleShowMusicAlbums = () => {
		this.setState({
			showMusicAlbums: true,
			showMusicAlbum: false,
			musicAlbum: null
		})
	}

	handleShowMusicAlbum = (album) => {
		this.setState({
			showMusicAlbums: false,
			showMusicAlbum: true,
			musicAlbum: album
		})
	}

	handleGetMoreDashboardMusicAlbums = () => {
		const { dashboardMusicAlbumsNext } = this.props;
		let fields = dashboardMusicAlbumsNext.split('=');
		this.props.getMoreDashboardMusicAlbums(fields[1]);
	}

	componentDidMount() {
		this.props.getDashboardMusicAlbums();
	}

	componentWillUnmount() {
		this.props.clearDashboardMusicAlbums();
	}

	render() {
		const { showMusicAlbums, showMusicAlbum, musicAlbum } = this.state;
		const { dashboardMusicAlbums, dashboardMusicAlbumsNext, loadingDashboardMusicAlbums } = this.props;

		return(
			<div>
				{showMusicAlbums && 
					<div>
						{!loadingDashboardMusicAlbums ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Music Albums:</span>
								</div>
								{dashboardMusicAlbums.map((album, index) => (
									<MusicAlbumInline select={this.handleShowMusicAlbum.bind(this, album)} album={album} key={index} />
								))}
								{dashboardMusicAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleGetMoreDashboardMusicAlbums}>Load More</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										...
									</div>
								}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>
									Loading Music Albums <Dots />
								</span>
							</div>
						}
					</div>
				}
				{showMusicAlbum && 
					<MusicAlbum musicAlbum={musicAlbum} closeAlbum={this.handleShowMusicAlbums} />
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardMusicAlbums: state.dashboard.dashboardMusicAlbums,
		dashboardMusicAlbumsNext: state.dashboard.dashboardMusicAlbumsNext,
		loadingDashboardMusicAlbums: state.dashboard.loadingDashboardMusicAlbums
	}
}

export default connect(mapStateToProps, { getDashboardMusicAlbums, getMoreDashboardMusicAlbums, clearDashboardMusicAlbums })(MusicAlbums);












