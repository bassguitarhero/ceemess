import React from 'react';
import { Grid, Button } from '@material-ui/core';

const ImageInline = (props) => (
	<Grid container style={{flex: 1, padding: 5}}>
		<Grid item xs={2} style={{padding: 5}}>
			<Button onClick={props.select}><img style={{width: '100%'}} alt={props.image.caption} src={props.image.thumbnail} /></Button>
		</Grid>
		<Grid item xs={10} style={{padding: 5}}>
			<Button onClick={props.select}>{props.image.title}</Button>
		</Grid>
	</Grid>
);

export default ImageInline;