import React from 'react';
import { Button, Grid } from '@material-ui/core';

const LinkInline = (props) => (
	<div style={{paddingLeft: 10}}>
		<Button onClick={props.select}>{props.link.description}</Button>
	</div>
);

export default LinkInline;