import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, TextField } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { uploadMusicTrack, clearUploadMusicTrack, editMusicTrack, clearEditMusicTrack, deleteMusicTrack, clearDeleteMusicTrack, clearUploadMusicTrackFail, clearEditMusicTrackFail, clearDeleteMusicTrackFail } from '../../../actions/dashboard';

class MusicTrack extends Component {
	constructor(props) {
		super(props);
		this.state = {
			order: '',
			title: '',
			description: '',
			image: null,
			file: null,
			soundcloud: '',
			audio: null,
			youtube_url: '',
			slug: '',
			uploading: false
		}
	}

	handleSelectFile = (e) => {
		this.setState({
			file: e.target.files[0]
		});
	}

	handleSelectAudio = (e) => {
		this.setState({
			audio: e.target.files[0]
		});
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleUploadNewTrack = () => {
		const { order, title, description, caption, file, soundcloud, audio, youtube_url, } = this.state;
		const { albumSlug } = this.props;
		const track = {	order, title, description, caption, file, soundcloud, audio, youtube_url, albumSlug };
		if (title !== '') {
			if (window.confirm("Upload track?")) {
				this.props.uploadMusicTrack(track);
				this.setState({uploading: true});
			}
		} else {
			alert("Tracks must have a Title.");
		}
	}

	handleSubmit = () => {
		const { order, title, description, caption, file, soundcloud, audio, youtube_url, slug } = this.state;
		const track = {	order, title, description, caption, file, soundcloud, audio, youtube_url, slug };
		if (title !== '') {
			if (window.confirm("Save changes?")) {
				this.props.editMusicTrack(track);
				this.setState({uploading: true});
			}
		} else {
			alert("Tracks must have a Title.");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.props.deleteMusicTrack(this.props.musicTrack);
			this.props.handleAlbumTrackDelete(this.props.musicTrack);
			this.setState({uploading: true});
		}
	}

	componentDidMount() {
		const { musicTrack } = this.props;
		// console.log('Track: ', musicTrack);
		if (musicTrack.newTrack === true) {
			this.setState({
				id: musicTrack.id,
				order: musicTrack.order,
				title: musicTrack.title,
				description: musicTrack.description,
				image: musicTrack.thumbnail,
				soundcloud: musicTrack.soundcloud,
				audio: musicTrack.audio,
				youtube_url: musicTrack.youtube_url,
				slug: musicTrack.slug,
				newTrack: musicTrack.newTrack
			});
		} else {
			this.setState({
				id: musicTrack.id,
				order: musicTrack.order,
				title: musicTrack.title,
				description: musicTrack.description,
				image: musicTrack.thumbnail,
				soundcloud: musicTrack.soundcloud,
				audio: musicTrack.audio,
				youtube_url: musicTrack.youtube_url,
				slug: musicTrack.slug,
				newTrack: musicTrack.newTrack
			})
		}
	}

	componentDidUpdate(lastProps) {
		if (this.props.editedSongFail !== null && lastProps.editedSongFail === null) {
			this.props.clearEditMusicTrackFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedSongFail !== null && lastProps.deletedSongFail === null) {
			this.props.clearDeleteMusicTrackFail();
			this.setState({uploading: false});
		}
		if (this.props.uploadedSongFail !== null && lastProps.uploadedSongFail === null) {
			this.props.clearUploadMusicTrackFail();
			this.setState({uploading: false});
		}
		if (this.props.editedSong !== null && lastProps.editedSong === null) {
			this.props.clearEditMusicTrack();
			this.props.handleAlbumTrackEdit(this.props.editedSong);
			this.setState({uploading: false});
		}
		if (this.props.deletedSong !== null && lastProps.deletedSong === null) {
			this.props.clearDeleteMusicTrack();
			this.setState({uploading: false});
		}
		if (this.props.uploadedSong !== null && lastProps.uploadedSong === null) {
			const { uploadedSong } = this.props;
			console.log('Uploaded Song: ', uploadedSong);
			this.props.clearUploadMusicTrack();
			this.props.handleAlbumTrackCreate(uploadedSong);
			this.setState({
				uploading: false,
				title: uploadedSong.title,
				description: uploadedSong.description,
				image: uploadedSong.thumbnail,
				soundcloud: uploadedSong.soundcloud_embed,
				audio: uploadedSong.audio_file,
				youtube_url: uploadedSong.youtube_url,
				slug: uploadedSong.slug,
				newTrack: false,
			});
		}
	}

	render() {
		const { id, order, title, description, caption, image, file, soundcloud, audio, youtube_url, slug, newTrack, uploading } = this.state;
		
		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
					<Grid item xs={1} style={styles.formRow}>
						<span>{id}</span>
					</Grid>
					<Grid container item xs={11} style={styles.formRow}>
						<Grid item xs={3} style={styles.formRow}>
							<span style={{fontSize: 18}}>Title:</span>
						</Grid>
						<Grid item xs={9} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={3} style={styles.formRow}>
							<span style={{fontSize: 18}}>Description:</span>
						</Grid>
						<Grid item xs={9} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="Description"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						{image !== null && 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={3}>
									<span style={{fontSize: 18}}>Current Image:</span>
								</Grid>
								<Grid item xs={9}>
									<img src={image} alt="Music Track Thumbnail" style={styles.currentImage} />
								</Grid>
							</Grid>
						}
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>
									{newTrack ? <span>Image:</span> : <span>Replace:</span>}
								</label>
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="file" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						{(audio === null && youtube_url === '') && 
							<Grid item xs={12} style={styles.formRow}>
								<Input 
									type="text" 
									name="soundcloud" 
									onChange={this.handleChange} 
									value={soundcloud} 
									placeholder="Soundcloud URL"
									style={styles.inputField}
								/>
							</Grid>
						}
						{(audio !== null && newTrack === false) && 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={3}>
									<span style={{fontSize: 18}}>Audio:</span>
								</Grid>
								<Grid item xs={9}>
									<audio 
			              src={audio} 
			              style={styles.audioFilePlayer}  
			              controls 
			            />
			           </Grid>
							</Grid>
						}
						{(soundcloud === '' && youtube_url === '') && 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={3} style={{flex: 1}}>
									<label htmlFor="audio" style={{fontSize: 18}}>
										{newTrack ? <span>Audio:</span> : <span>Replace:</span>}
									</label>
								</Grid>
								<Grid item xs={9} style={{flex: 1}}>
									<Input 
										type="file" 
										name="audio" 
										onChange={this.handleSelectAudio}
									/>
								</Grid>
							</Grid>
						}
						{(audio === null && soundcloud === '') && 
							<Grid item xs={12} style={styles.formRow}>
								<Input 
									type="text" 
									name="youtube_url" 
									onChange={this.handleChange} 
									value={youtube_url} 
									placeholder="YouTube URL"
									style={styles.inputField}
								/>
							</Grid>
						}
					</Grid>
					{newTrack === false ? 
						<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Track</Button>
							</Grid>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Track</Button>
							</Grid>
						</Grid> :
						<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleUploadNewTrack}>Upload New Track</Button>
							</Grid>
						</Grid>
					}
				</Grid>
				<hr style={styles.bottomLine} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		uploadedSong: state.dashboard.uploadedSong,
		editedSong: state.dashboard.editedSong,
		deletedSong: state.dashboard.deletedSong,
		uploadedSongFail: state.dashboard.uploadedSongFail,
		editedSongFail: state.dashboard.editedSongFail,
		deletedSongFail: state.dashboard.deletedSongFail
	}
}

export default connect(mapStateToProps, { uploadMusicTrack, clearUploadMusicTrack, editMusicTrack, clearEditMusicTrack, deleteMusicTrack, clearDeleteMusicTrack, clearUploadMusicTrackFail, clearEditMusicTrackFail, clearDeleteMusicTrackFail })(MusicTrack);












