import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import Photo from './Photo';

import { editPhotoAlbum, clearEditPhotoAlbum, deletePhotoAlbum, clearDeletePhotoAlbum, clearEditPhotoAlbumFail, clearDeletePhotoAlbumFail } from '../../../actions/dashboard';
import { getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';
import { getDashboardPhotoAlbums, clearDashboardPhotoAlbums } from '../../../actions/dashboard';

class PhotoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPhotoAlbum: true,
			showPhoto: false,
			photo: null,
			// edit
			title: '',
			description: '',
			publicAlbum: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			uploading: false,
			photos: [],
			showAlbumForm: true,
			showPhotosForm: false,
			photoAlbum: null,
			slug: '',
			forceRefresh: false
		}
	}

	handleAlbumPhotoAdd = () => {
		const { photos } = this.state;
		// console.log('Tracks: ', tracks);
		var id = photos[photos.length -1].id + 1;
		// console.log('id: ', id);
		photos.push({
			id: id,
			title: '',
			image: null,
			thumbnail: null,
			file: null,
			slug: '',
			newPhoto: true
		});
		this.setState({photos: photos});
	}

	handleAlbumPhotoCreate = (uploadedPhoto) => {
		// console.log('Uploaded Photo: ', uploadedPhoto);
		this.setState({forceRefresh: true});
	}

	handleAlbumPhotoEdit = (editedPhoto) => {
		console.log('Edited Photo: ', editedPhoto);
		// const { photos } = this.state;
		// for (var i = 0; i < photos.length; i++) {
		// 	if (photos[i].slug == editedPhoto.slug) {
		// 		photos[i].title = editedPhoto.title;
		// 		photos[i].image = editedPhoto.image;
		// 		photos[i].thumbnail = editedPhoto.thumbnail;
		// 		photos[i].slug = editedPhoto.slug;
		// 		photos[i].newPhoto = false;
		// 	}
		// }
		this.setState({forceRefresh: true});
	}

	handleAlbumPhotoDelete = (deletedPhoto) => {
		// console.log('Deleted Track: ', deletedTrack);
		const { photos } = this.state;
		const newPhotos = photos.filter(photo => (photo.slug !== deletedPhoto.slug));
		this.setState({photos: newPhotos, forceRefresh: true});
	}

	handleShowAlbumForm = () => {
		this.setState({
			showAlbumForm: true,
			showPhotosForm: false,
		})
	}

	handleShowPhotosForm = () => {
		this.setState({
			showAlbumForm: false,
			showPhotosForm: true,
		})
	}

	handleShowPhotoAlbum = () => {
		this.setState({
			showPhotoAlbum: true,
			showPhoto: false,
			photo: null
		})
	}

	handleShowPhoto = (photo) => {
		this.setState({
			showPhotoAlbum: false,
			showPhoto: true,
			photo: photo
		})
	}

	handleCloseAlbum = () => {
		this.props.closeAlbum();
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleCheckboxChange = () => {
		this.setState({
			publicAlbum: !this.state.publicAlbum
		})
	}

	handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.slug === tagsList[i].slug) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSubmit = () => {
		const { title, description, publicAlbum, categoriesList, tagsList, categoryName, slug } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName) {
				category = categoriesList[i];
			}
		}
		const album = { title, description, category, publicAlbum, tags, slug };
		if (title !== '') {
			if (window.confirm("Save changes?")) {
				this.props.editPhotoAlbum(album);
				this.setState({uploading: true});
			}
		} else {
			alert("Albums must have a Title.");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.props.deletePhotoAlbum(this.props.photoAlbum);
			this.setState({uploading: true});
		}
	}

	componentDidMount() {
		// console.log('Album: ', this.props.photoAlbum);
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
		const { photoAlbum } = this.props;
		const photos = [];
		var id = 1;
		for (var i = 0; i < photoAlbum.album_photos.length; i++) {
			photos.push({
				id: id,
				image: photoAlbum.album_photos[i].image,
				thumbnail: photoAlbum.album_photos[i].thumbnail,
				title: photoAlbum.album_photos[i].title,
				slug: photoAlbum.album_photos[i].slug,
				newPhoto: false
			});
			id += 1;
		}
		this.setState({
			title: photoAlbum.title,
			description: photoAlbum.description,
			publicAlbum: photoAlbum.public,
			photoAlbum: photoAlbum,
			slug: photoAlbum.slug,
			photos: photos
		})
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.editedPhotoAlbumFail !== null && lastProps.editedPhotoAlbumFail === null) {
			this.props.clearEditPhotoAlbumFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedPhotoAlbumFail !== null && lastProps.deletedPhotoAlbumFail === null) {
			this.props.clearDeletePhotoAlbumFail();
			this.setState({uploading: false});
		}
		if (this.props.editedPhotoAlbum !== null && lastProps.editedPhotoAlbum === null) {
			this.props.clearEditPhotoAlbum();
			this.props.clearDashboardPhotoAlbums();
			this.props.getDashboardPhotoAlbums();
			this.setState({uploading: false});
		}
		if (this.props.deletedPhotoAlbum !== null && lastProps.deletedPhotoAlbum === null) {
			this.props.clearDeletePhotoAlbum();
			this.props.clearDashboardPhotoAlbums();
			this.props.getDashboardPhotoAlbums();
			this.setState({uploading: false});
			this.handleCloseAlbum();
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			const { photoAlbum } = this.props;
			let categoryName = 'select';
			if (photoAlbum.category !== null) {
				categoryName = photoAlbum.category.slug
			}
			this.setState({
				categoryName: categoryName,
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const { photoAlbum } = this.props;
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					// id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			for (var i = 0; i < tagsList.length; i++) {
				for (var j = 0; j < photoAlbum.tags.length; j++) {
					if (tagsList[i].slug === photoAlbum.tags[j].slug) {
						tagsList[i].checked = true;
					}
				}
			}
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
		if (this.state.forceRefresh === true) {
			this.props.clearDashboardPhotoAlbums();
			this.props.getDashboardPhotoAlbums();
		}
	}

	render() {
		const { showPhotoAlbum, showPhoto, photo, photoAlbum, slug } = this.state;
		const { title, description, publicAlbum, uploading, photos, showAlbumForm, showPhotosForm } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container style={styles.formTypeButtonsContainer}>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showAlbumForm ? styles.formTypeButtonSelected : styles.formTypeButton } onClick={this.handleShowAlbumForm}>Album</Button>
					</Grid>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showPhotosForm ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowPhotosForm}>Photos</Button>
					</Grid>
				</Grid>
				{showAlbumForm && 
					<div style={{flex: 1}}>
						<Grid container>
							<Grid item xs={11} style={styles.formRow}>
								<span style={styles.formTitle}>Edit Photo Album:</span>
							</Grid>
							<Grid item xs={1}>
								<Button style={styles.closeButton} onClick={this.handleCloseAlbum}>X</Button>
							</Grid>
		        	<Grid container item xs={8} style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
								<Grid item xs={12} style={styles.formRow}>
									<Input 
										type="text" 
										name="title" 
										onChange={this.handleChange} 
										value={title} 
										placeholder="Title"
										style={styles.inputField}
									/>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<TextField 
										name="description" 
										onChange={this.handleChange} 
										value={description} 
										placeholder="Description"
										style={styles.textField}
										multiline={true}
										rows="8"
									/>
								</Grid>
								{displayCategoriesList ? 
									<Grid container item xs={12} style={styles.formRow}>
										<Grid item xs={4}>
											<span style={{fontSize: 18}}>Category:</span>
										</Grid>
										<Grid item xs={8}>
											<Select
			                  value={categoryName} 
			                  onChange={this.handleItemChange.bind(this)}
			                  inputProps={{
			                    name: `Categories`,
			                    id: "Categories"
			                  }} 
			                  style={styles.selectItemName} 
			                >
			                	{categoriesList.map((category, index) => (
													<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
												))}
			                </Select>
		                </Grid>
									</Grid> :
									<Grid container item xs={12} style={styles.formRow}>
										<span>Loading Categories <Dots /></span>
									</Grid>
								}
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={1}>
										<Checkbox 
											name="publicAlbum" 
											checked={publicAlbum} 
											onChange={this.handleCheckboxChange} 
										/>
									</Grid>
									<Grid item xs={10}>
										<Button onClick={this.handleCheckboxChange}>Public</Button>
									</Grid>
								</Grid>
								<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
									<Grid item xs={6} style={styles.formTypeButtonContainer}>
										<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Album</Button>
									</Grid>
									<Grid item xs={6} style={styles.formTypeButtonContainer}>
										<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Album</Button>
									</Grid>
								</Grid>
							</Grid>
							<Grid item md={4} style={styles.formRow}>
								<div>
									<span style={styles.tagsTitle}>Tags:</span>
								</div>
								{displayTagsList ? 
									<Grid container>
										{tagsList.map((tag, index) => (
											<Grid container item xs={12} style={{padding: 5}} key={index}>
												<Grid item xs={2}>
													<Checkbox 
														name={tag.tag} 
														checked={tag.checked} 
														onChange={this.handleSelectTag.bind(this, tag)}
													/>
												</Grid>
												<Grid item xs={10}>
													<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
												</Grid>
											</Grid>
										))}
									</Grid> :
									<div>
										<span>Loading Tags <Dots /></span>
									</div>
								}
							</Grid>
						</Grid>
					</div>
				}
				{showPhotosForm && 
					<div style={{flex: 1}}>
						<div style={styles.formRow}>
							<span style={styles.formTitle}>{title}:</span>
						</div>
						{photos.map((photo, index) => (
							<Photo albumSlug={slug} photo={photo} handleAlbumPhotoCreate={this.handleAlbumPhotoCreate} handleAlbumPhotoEdit={this.handleAlbumPhotoEdit} handleAlbumPhotoDelete={this.handleAlbumPhotoDelete} key={index} />
						))}
						<div style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleAlbumPhotoAdd}>Add New Photo</Button>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedPhotoAlbum: state.dashboard.editedPhotoAlbum,
		deletedPhotoAlbum: state.dashboard.deletedPhotoAlbum,

		editedPhotoAlbumFail: state.dashboard.editedPhotoAlbumFail,
		deletedPhotoAlbumFail: state.dashboard.deletedPhotoAlbumFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { editPhotoAlbum, deletePhotoAlbum, clearEditPhotoAlbum, clearDeletePhotoAlbum, clearEditPhotoAlbumFail, clearDeletePhotoAlbumFail, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags, getDashboardPhotoAlbums, clearDashboardPhotoAlbums })(PhotoAlbum);












