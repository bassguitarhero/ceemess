import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import Category from './Category';
import CategoryInline from './CategoryInline';

import { getDashboardCategories, clearDashboardCategories } from '../../../actions/dashboard';

class Categories extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showCategories: true,
			showCategory: false,
			category: null
		}
	}

	handleShowCategories = () => {
		this.setState({
			showCategories: true,
			showCategory: false,
			category: null
		})
	}

	handleShowCategory = (category) => {
		this.setState({
			showCategories: false,
			showCategory: true,
			category: category
		})
	}

	componentDidMount() {
		this.props.getDashboardCategories();
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
	}

	render() {
		const { showCategories, showCategory, category } = this.state;
		const { dashboardCategories, loadingDashboardCategories } = this.props;

		return(
			<div>
				{showCategories && 
					<div>
						{!loadingDashboardCategories ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Categories:</span>
								</div>
								{dashboardCategories.map((category, index) => (
									<CategoryInline category={category} select={this.handleShowCategory.bind(this, category)} key={index} />
								))}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Categories <Dots /></span>
							</div>
						}
					</div>
				}
				{showCategory && 
					<Category category={category} closeCategory={this.handleShowCategories} />
				}	
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories
	}
}

export default connect(mapStateToProps, { getDashboardCategories, clearDashboardCategories })(Categories);












