import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import Link from './Link';
import LinkInline from './LinkInline';

import { getDashboardLinks, clearDashboardLinks } from '../../../actions/dashboard';

class Links extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showLinks: true,
			showLink: false,
			link: null
		}
	}

	handleShowLinks = () => {
		this.setState({
			showLinks: true,
			showLink: false,
			link: null
		})
	}

	handleShowLink = (link) => {
		this.setState({
			showLinks: false,
			showLink: true,
			link: link
		})
	}

	componentDidMount() {
		this.props.getDashboardLinks();
	}

	componentWillUnmount() {
		this.props.clearDashboardLinks();
	}

	render() {
		const { showLinks, showLink, link } = this.state;
		const { dashboardLinks, loadingDashboardLinks } = this.props;

		return(
			<div>
				{showLinks && 
					<div>
						{!loadingDashboardLinks ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Links:</span>
								</div>
								{dashboardLinks.map((link, index) => (
									<LinkInline link={link} key={index} select={this.handleShowLink.bind(this, link)} />
								))}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Links <Dots /></span>
							</div>
						}
					</div>
				}
				{showLink && 
					<Link link={link} closeLink={this.handleShowLinks} />
				}	
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardLinks: state.dashboard.dashboardLinks,
		loadingDashboardLinks: state.dashboard.loadingDashboardLinks
	}
}

export default connect(mapStateToProps, { getDashboardLinks, clearDashboardLinks })(Links);












