import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { editTag, clearEditTag, deleteTag, clearDeleteTag, clearEditTagFail, clearDeleteTagFail } from '../../../actions/dashboard';
import { getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class Tag extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			uploading: false
		}
	}

	handleCloseTag = () => {
		this.props.closeTag();
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { name } = this.state;
		const { tag } = this.props;
		const updateTag = { name: name, slug: tag.slug };
		if (name !== '') {
			if (window.confirm("Save changes?")) {
				this.props.editTag(updateTag);
				this.setState({uploading: true});
			}
		} else {
			alert("Tags must have a name.");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.setState({uploading: true});
			this.props.deleteTag(this.props.tag);
		}
	}

	componentDidMount() {
		this.setState({
			name: this.props.tag.tag
		})
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.editedTagFail !== null && lastProps.editedTagFail === null) {
			this.props.clearEditTagFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedTagFail !== null && lastProps.deletedTagFail === null) {
			this.props.clearDeleteTagFail();
			this.setState({uploading: false});
		}
		if (this.props.editedTag !== null && lastProps.editedTag === null) {
			this.props.clearEditTag();
			this.props.clearDashboardTags();
			this.props.getDashboardTags();
			this.setState({
				uploading: false,
				name: this.props.editedTag.tag
			});
		}
		if (this.props.deletedTag !== null && lastProps.deletedTag === null) {
			this.props.clearDeleteTag();
			this.props.clearDashboardTags();
			this.props.getDashboardTags();
			this.setState({
				uploading: false
			});
			this.handleCloseTag();
		}
	}

	render() {
		const { name, uploading } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
      	<Grid container style={{alignContent: 'flex-start', alignItems: 'flex-start', flex: 1}}>
					<Grid item xs={11} style={styles.formRow}>
						<span style={styles.formTitle}>Edit Tag:</span>
					</Grid>
					<Grid item xs={1}>
						<Button style={styles.closeButton} onClick={this.handleCloseTag}>X</Button>
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						<Input 
							type="text" 
							name="name" 
							onChange={this.handleChange} 
							value={name} 
							placeholder="Name"
							style={styles.inputField}
						/>
					</Grid>
					<Grid container style={{paddingTop: 20, flex: 1}}>
						<Grid item xs={6} style={styles.formTypeButtonContainer}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Tag</Button>
						</Grid>
						<Grid item xs={6} style={styles.formTypeButtonContainer}>
							<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Tag</Button>
						</Grid>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedTag: state.dashboard.editedTag,
		deletedTag: state.dashboard.deletedTag,
		editedTagFail: state.dashboard.editedTagFail,
		deletedTagFail: state.dashboard.deletedTagFail
	}
}

export default connect(mapStateToProps, { editTag, clearEditTag, deleteTag, clearDeleteTag, clearEditTagFail, clearDeleteTagFail, getDashboardTags, clearDashboardTags })(Tag);












