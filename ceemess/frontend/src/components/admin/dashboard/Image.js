import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { editImage, clearEditImage, deleteImage, clearDeleteImage, clearEditImageFail, clearDeleteImageFail } from '../../../actions/dashboard';
import { getDashboardImages, clearDashboardImages, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class Image extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			description: '',
			caption: '',
			file: null,
			publicImage: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			uploading: false,
			slug: ''
		}
	}

	handleCloseImage = () => {
		this.props.closeImage();
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title' || key === 'caption'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.slug === tagsList[i].slug) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSelectFile = e => this.setState({
		file: e.target.files[0]
	});

	handleCheckboxChange = () => {
		this.setState({
			publicImage: !this.state.publicImage
		})
	}

	handleSubmit = () => {
		const { title, description, caption, file, publicImage, tagsList, categoriesList, categoryName, slug } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const image = { title, description, caption, file, publicImage, category, tags, slug };
		if (title !== '') {
			if (window.confirm("Save changes?")) {
				this.setState({uploading: true});
				this.props.editImage(image);
			}
		} else {
			alert("Images must have a Title.");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.setState({uploading: true});
			this.props.deleteImage(this.props.image);
		}
	}

	componentDidMount() {
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
		// console.log('Image: ', this.props.image);
		const { image } = this.props;
		this.setState({
			title: image.title,
			description: image.description,
			caption: image.caption,
			publicImage: image.public,
			image: image.image,
			slug: image.slug
		})
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.editedImageFail !== null && lastProps.editedImageFail === null) {
			this.props.clearEditImageFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedImageFail !== null && lastProps.deletedImageFail === null) {
			this.props.clearDeleteImageFail();
			this.setState({uploading: false});
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: "select"
			});
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			const { image } = this.props;
			let categoryName = '';
			if (image.category !== null) {
				categoryName = image.category.slug;
			} else {
				categoryName = 'select';
			}
			this.setState({
				categoryName: categoryName,
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const { image } = this.props;
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					// id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			});
			for (var i = 0; i < tagsList.length; i++) {
				for (var j = 0; j < image.tags.length; j++) {
					if (tagsList[i].slug === image.tags[j].slug) {
						tagsList[i].checked = true;
					}
				}
			}
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.editedImage !== null && lastProps.editedImage === null) {
			this.props.clearEditImage();
			this.props.clearDashboardImages();
			this.props.getDashboardImages();
			this.setState({uploading: false, image: this.props.editedImage.image});
		}
		if (this.props.deletedImage !== null && lastProps.deletedImage === null) {
			this.props.clearDeleteImage();
			this.props.clearDashboardImages();
			this.props.getDashboardImages();
			this.setState({uploading: false});
			this.handleCloseImage();
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { title, description, caption, file, publicImage, uploading, image } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
					<Grid container item xs={12}>
						<Grid item xs={11} style={styles.formRow}>
							<span style={styles.formTitle}>Edit Image:</span>
						</Grid>
						<Grid item xs={1}>
							<Button style={styles.closeButton} onClick={this.handleCloseImage}>X</Button>
						</Grid>
					</Grid>
        	<Grid container item xs={8} style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="Description"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="caption" 
								onChange={this.handleChange} 
								value={caption} 
								placeholder="Caption"
								style={styles.inputField}
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={4}>
								<span style={{fontSize: 18}}>Current Image:</span>
							</Grid>
							<Grid item xs={8}>
								<img src={image} alt="Thumbnail" style={styles.currentImage} />
							</Grid>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={4} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>Replace:</label>
							</Grid>
							<Grid item xs={8} style={{flex: 1}}>
								<Input 
									type="file" 
									name="file" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						{displayCategoriesList ? 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={4}>
									<span style={{fontSize: 18}}>Category:</span>
								</Grid>
								<Grid item xs={8}>
									<Select
	                  value={categoryName} 
	                  onChange={this.handleItemChange.bind(this)}
	                  inputProps={{
	                    name: `Categories`,
	                    id: "Categories"
	                  }} 
	                  style={styles.selectItemName} 
	                >
	                	{categoriesList.map((category, index) => (
											<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
										))}
	                </Select>
                </Grid>
							</Grid> :
							<Grid container item xs={12} style={styles.formRow}>
								<span>Loading Categories <Dots /></span>
							</Grid>
						}
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={1}>
								<Checkbox 
									name="publicImage" 
									checked={publicImage} 
									onChange={this.handleCheckboxChange} 
								/>
							</Grid>
							<Grid item xs={10}>
								<Button onClick={this.handleCheckboxChange}>Public</Button>
							</Grid>
						</Grid>
						<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Image</Button>
							</Grid>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Image</Button>
							</Grid>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						<div>
							<span style={styles.tagsTitle}>Tags:</span>
						</div>
						{displayTagsList ? 
							<Grid container>
								{tagsList.map((tag, index) => (
									<Grid container item xs={12} style={{padding: 5}} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={tag.tag} 
												checked={tag.checked} 
												onChange={this.handleSelectTag.bind(this, tag)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<div>
								<span>Loading Tags <Dots /></span>
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedImage: state.dashboard.editedImage, 
		deletedImage: state.dashboard.deletedImage, 
		editedImageFail: state.dashboard.editedImageFail,
		deletedImageFail: state.dashboard.deletedImageFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { editImage, clearEditImage, deleteImage, clearDeleteImage, clearEditImageFail, clearDeleteImageFail, getDashboardImages, clearDashboardImages, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(Image);












