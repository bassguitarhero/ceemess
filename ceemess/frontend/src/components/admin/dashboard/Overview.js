import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import { getDashboardBlogPostsAll, clearDashboardBlogPostsAll, getDashboardPhotoAlbumsAll, clearDashboardPhotoAlbumsAll, getDashboardMusicAlbumsAll, clearDashboardMusicAlbumsAll, getDashboardVideosAll, clearDashboardVideosAll, getDashboardImagesAll, clearDashboardImagesAll, getDashboardLinks, clearDashboardLinks, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

// in dashboard
class Overview extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	componentDidMount() {
		this.props.getDashboardBlogPostsAll();
		this.props.getDashboardPhotoAlbumsAll();
		this.props.getDashboardMusicAlbumsAll();
		this.props.getDashboardVideosAll();
		this.props.getDashboardImagesAll();
		this.props.getDashboardLinks();
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
	}

	componentWillUnmount() {
		this.props.clearDashboardBlogPostsAll();
		this.props.clearDashboardPhotoAlbumsAll();
		this.props.clearDashboardMusicAlbumsAll();
		this.props.clearDashboardVideosAll();
		this.props.clearDashboardImagesAll();
		this.props.clearDashboardLinks();
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { dashboardBlogPostsAll, loadingDashboardBlogPostsAll } = this.props;
		const { dashboardPhotoAlbumsAll, loadingDashboardPhotoAlbumsAll, dashboardMusicAlbumsAll, loadingDashboardMusicAlbumsAll } = this.props; 
		const { dashboardVideosAll, loadingDashboardVideosAll, dashboardImagesAll, loadingDashboardImagesAll, dashboardLinks, loadingDashboardLinks } = this.props;
		const { dashboardCategories, loadingDashboardCategories, dashboardTags, loadingDashboardTags } = this.props;


		return (
			<div style={{flex: 1}}>
				<Grid container style={{flex: 1}}>
					<Grid item xs={12} style={styles.formRow}>
						<span style={{fontSize: 20, fontWeight: 'bold'}}>Overview:</span>
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardBlogPostsAll === false ? 
								<span style={styles.text}>{dashboardBlogPostsAll.length} Blog Posts</span> : 
								<span style={styles.text}>Loading Posts <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardPhotoAlbumsAll === false ? 
								<span style={styles.text}>{dashboardPhotoAlbumsAll.length} Photo Albums</span> : 
								<span style={styles.text}>Loading Photos <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardMusicAlbumsAll === false ? 
								<span style={styles.text}>{dashboardMusicAlbumsAll.length} Music Albums</span> : 
								<span style={styles.text}>Loading Music <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardVideosAll === false ? 
								<span style={styles.text}>{dashboardVideosAll.length} Videos</span> : 
								<span style={styles.text}>Loading Videos <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardImagesAll === false ? 
								<span style={styles.text}>{dashboardImagesAll.length} Images</span> : 
								<span style={styles.text}>Loading Images <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardLinks === false ? 
								<span style={styles.text}>{dashboardLinks.length} Links</span> : 
								<span style={styles.text}>Loading Links <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardCategories === false ? 
								<span style={styles.text}>{dashboardCategories.length} Categories</span> : 
								<span style={styles.text}>Loading Categories <Dots /></span>
						}
					</Grid>
					<Grid item xs={12} style={styles.formRow}>
						{loadingDashboardTags === false ? 
								<span style={styles.text}>{dashboardTags.length} Tags</span> : 
								<span style={styles.text}>Loading Tags <Dots /></span>
						}
					</Grid>
				</Grid>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardBlogPostsAll: state.dashboard.dashboardBlogPostsAll,
		loadingDashboardBlogPostsAll: state.dashboard.loadingDashboardBlogPostsAll,
		dashboardPhotoAlbumsAll: state.dashboard.dashboardPhotoAlbumsAll,
		loadingDashboardPhotoAlbumsAll: state.dashboard.loadingDashboardPhotoAlbumsAll,
		dashboardMusicAlbumsAll: state.dashboard.dashboardMusicAlbumsAll,
		loadingDashboardMusicAlbumsAll: state.dashboard.loadingDashboardMusicAlbumsAll,
		dashboardVideosAll: state.dashboard.dashboardVideosAll,
		loadingDashboardVideosAll: state.dashboard.loadingDashboardVideosAll,
		dashboardImagesAll: state.dashboard.dashboardImagesAll,
		loadingDashboardImagesAll: state.dashboard.loadingDashboardImagesAll,
		dashboardLinks: state.dashboard.dashboardLinks,
		loadingDashboardLinks: state.dashboard.loadingDashboardLinks,
		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { getDashboardBlogPostsAll, clearDashboardBlogPostsAll, getDashboardPhotoAlbumsAll, clearDashboardPhotoAlbumsAll, getDashboardMusicAlbumsAll, clearDashboardMusicAlbumsAll, getDashboardVideosAll, clearDashboardVideosAll, getDashboardImagesAll, clearDashboardImagesAll, getDashboardLinks, clearDashboardLinks, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(Overview);












