import React from 'react';
import { Button } from '@material-ui/core';

const TagInline = (props) => (
	<div style={{paddingLeft: 10}}>
		<Button onClick={props.select}>{props.tag.tag}</Button>
	</div>
);

export default TagInline;