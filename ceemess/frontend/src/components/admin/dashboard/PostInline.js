import React from 'react';
import { Grid, Button } from '@material-ui/core';

function truncateBody(s) {
		if (s.length > 250) {
			return s.substring(0, 250) + '...';
		} else {
			return s
		}
	}

const PostInline = (props) => (

	<Grid container style={{flex: 1, padding: 5}}>
		<Grid item xs={2} style={{padding: 5}}>
			<Button onClick={props.select}>
				{props.post.image_highlight !== null ? 
					<img style={{width: '100%'}} alt={props.post.image_highlight.caption} src={props.post.image_highlight.thumbnail} /> : 
					<div>
						{props.post.image !== null ? 
							<img style={{width: '100%'}} alt="Post Thumbnail" src={props.post.thumbnail} /> :
							<div>
								{props.post.photo_album !== null ? 
									<div>
										{props.post.photo_album.album_photos.length > 0 ? 
											<div>
												<img style={{width: '100%'}} alt={props.post.photo_albums.album_photos[0].title} src={props.post.photo_album.album_photos[0].thumbnail} />
											</div> : 
											null
										}
									</div> : 
									<div>
										{props.post.image_gallery.length > 0 ? 
											<div>
												<img style={{width: '100%'}} alt={props.post.image_gallery[0].title} src={props.post.image_gallery[0].thumbnail} />
											</div> :
											null
										}
									</div>
								}
							</div>
						}
					</div>
				}
			</Button>
		</Grid>
		<Grid item xs={10} style={{padding: 5}}>
			<Button onClick={props.select}>{props.post.title}</Button>
			<div style={{paddingLeft: 10}}><span style={{fontSize: 14}}>{truncateBody(props.post.body)}</span></div>
			<div style={{padding: 10}}>{props.post.published ? <span style={{fontSize: 14}}>Published</span> : <span style={{fontSize: 14}}>Draft</span> }</div>
		</Grid>
	</Grid>
);

export default PostInline;