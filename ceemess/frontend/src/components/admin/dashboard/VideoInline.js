import React from 'react';
import { Grid, Button } from '@material-ui/core';

const VideoInline = (props) => (
	<Grid container style={{flex: 1, padding: 5}}>
		<Grid item xs={2} style={{padding: 5}}>
			<Button onClick={props.select}>
				{props.video.thumbnail !== null ? 
					<img style={{width: '100%'}} alt="Video Thumbnail" src={props.video.thumbnail} /> :
					<div>
						{props.video.image !== null ? 
							<img style={{width: '100%'}} alt="Video Thumbnail" src={props.video.image} /> : 
							null
						}
					</div>
				}
			</Button>
		</Grid>
		<Grid item xs={10} style={{padding: 5}}>
			<Button onClick={props.select}>{props.video.title}</Button>
		</Grid>
	</Grid>
);

export default VideoInline;