import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { editCategory, clearEditCategory, deleteCategory, clearDeleteCategory, clearEditCategoryFail, clearDeleteCategoryFail } from '../../../actions/dashboard';
import { getDashboardCategories, clearDashboardCategories } from '../../../actions/dashboard';

class Category extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			uploading: false
		}
	}

	handleCloseCategory = () => {
		this.props.closeCategory();
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSubmit = () => {
		const { name } = this.state;
		const updateCategory = { name: name, slug: this.props.category.slug };
		if (name !== '') {
			if (window.confirm("Save changes?")) {
				this.props.editCategory(updateCategory);
				this.setState({uploading: true});
			}
		} else {
			alert("Categories must have a name.");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.setState({uploading: true});
			this.props.deleteCategory(this.props.category);
		}
	}

	componentDidMount() {
		// console.log('Category: ', this.props.category);
		this.setState({
			name: this.props.category.category
		});
	}

	componentDidUpdate(lastProps) {
		if (this.props.editedCategoryFail !== null && lastProps.editedCategoryFail === null) {
			this.props.clearEditCategoryFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedCategoryFail !== null && lastProps.deletedCategoryFail === null) {
			this.props.clearDeleteCategoryFail();
			this.setState({uploading: false});
		}
		if (this.props.editedCategory !== null && lastProps.editedCategory === null) {
			this.props.clearEditCategory();
			this.props.clearDashboardCategories();
			this.props.getDashboardCategories();
			this.setState({uploading: false});
		}
		if (this.props.deletedCategory !== null && lastProps.deletedCategory === null) {
			this.props.clearDeleteCategory();
			this.props.clearDashboardCategories();
			this.props.getDashboardCategories();
			this.setState({uploading: false});
			this.handleCloseCategory();
		}
	}

	render() {
		const { name, uploading } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
        	<Grid container item xs={12} style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={11} style={styles.formRow}>
							<span style={styles.formTitle}>Edit Category:</span>
						</Grid>
						<Grid item xs={1}>
							<Button style={styles.closeButton} onClick={this.handleCloseCategory}>X</Button>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="name" 
								onChange={this.handleChange} 
								value={name} 
								placeholder="Name"
								style={styles.inputField}
							/>
						</Grid>
					</Grid>
					<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
						<Grid item xs={6} style={styles.formTypeButtonContainer}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Category</Button>
						</Grid>
						<Grid item xs={6} style={styles.formTypeButtonContainer}>
							<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Category</Button>
						</Grid>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedCategory: state.dashboard.editedCategory,
		deletedCategory: state.dashboard.deletedCategory,
		editedCategoryFail: state.dashboard.editedCategoryFail,
		deletedCategoryFail: state.dashboard.deletedCategoryFail
	}
}

export default connect(mapStateToProps, { editCategory, clearEditCategory, deleteCategory, clearDeleteCategory, clearEditCategoryFail, clearDeleteCategoryFail, getDashboardCategories, clearDashboardCategories })(Category);












