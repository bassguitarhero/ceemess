import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { editLink, clearEditLink, deleteLink, clearDeleteLink, clearEditLinkFail, clearDeleteLinkFail } from '../../../actions/dashboard';
import { getDashboardLinks, clearDashboardLinks } from '../../../actions/dashboard';

class Link extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			linkURL: '',
			socialMediaLink: false,
			slug: '',
			uploading: false
		}
	}

	handleCloseLink = () => {
		this.props.closeLink();
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'linkURL'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleCheckboxChange = () => {
		this.setState({
			socialMediaLink: !this.state.socialMediaLink
		})
	}

	handleSubmit = () => {
		const { name, linkURL, socialMediaLink, slug } = this.state;
		const link = { name, linkURL, socialMediaLink, slug };
		if (name !== '' && linkURL !== '') {
			if (window.confirm("Save changes?")) {
				this.props.editLink(link);
				this.setState({uploading: true});
			}
		} else {
			alert("Links must have a Name and a URL");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.setState({uploading: true});
			this.props.deleteLink(this.props.link);
		}
	}

	componentDidMount() {

		this.setState({
			name: this.props.link.description,
			linkURL: this.props.link.link,
			socialMediaLink: this.props.link.social_media,
			slug: this.props.link.slug
		})
	}

	componentDidUpdate(lastProps) {
		if (this.props.editedLinkFail !== null && lastProps.editedLinkFail === null) {
			this.props.clearEditLinkFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedLinkFail !== null && lastProps.deletedLinkFail === null) {
			this.props.clearDeleteLinkFail();
			this.setState({uploading: false});
		}
		if (this.props.editedLink !== null && lastProps.editedLink === null) {
			this.props.clearEditLink();
			this.props.clearDashboardLinks();
			this.props.getDashboardLinks();
			this.setState({uploading: false});
		}
		if (this.props.deletedLink !== null && lastProps.deletedLink === null) {
			this.props.clearDeleteLink();
			this.props.clearDashboardLinks();
			this.props.getDashboardLinks();
			this.setState({uploading: false});
			this.handleCloseLink();
		}
	}

	render() {
		const { name, linkURL, socialMediaLink, uploading } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
        	<Grid container item xs={12} style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={11} style={styles.formRow}>
							<span style={styles.formTitle}>Edit Link:</span>
						</Grid>
						<Grid item xs={1}>
							<Button style={styles.closeButton} onClick={this.handleCloseLink}>X</Button>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="name" 
								onChange={this.handleChange} 
								value={name} 
								placeholder="Name"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="linkURL" 
								onChange={this.handleChange} 
								value={linkURL} 
								placeholder="URL"
								style={styles.inputField}
							/>
						</Grid>
					</Grid>
					<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={1}>
								<Checkbox 
									name="socialMediaLink" 
									checked={socialMediaLink} 
									onChange={this.handleCheckboxChange} 
								/>
							</Grid>
							<Grid item xs={10}>
								<Button onClick={this.handleCheckboxChange}>Social Media Link</Button>
							</Grid>
						</Grid>
					<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
						<Grid item xs={6} style={styles.formTypeButtonContainer}>
							<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Link</Button>
						</Grid>
						<Grid item xs={6} style={styles.formTypeButtonContainer}>
							<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Link</Button>
						</Grid>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedLink: state.dashboard.editedLink,
		deletedLink: state.dashboard.deletedLink,
		editedLinkFail: state.dashboard.editedLinkFail,
		deletedLinkFail: state.dashboard.deletedLinkFail
	}
}

export default connect(mapStateToProps, { editLink, clearEditLink, deleteLink, clearDeleteLink, clearEditLinkFail, clearDeleteLinkFail, getDashboardLinks, clearDashboardLinks })(Link);












