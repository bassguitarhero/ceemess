import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import Post from './Post';
import PostInline from './PostInline';

import { getDashboardBlogPosts, getMoreDashboardBlogPosts, clearDashboardBlogPosts } from '../../../actions/dashboard';

class Posts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPosts: true,
			showPost: false,
			post: null
		}
	}

	handleShowPosts = () => {
		this.setState({
			showPosts: true,
			showPost: false,
			post: null
		})
	}

	handleShowPost = (post) => {
		this.setState({
			showPosts: false,
			showPost: true,
			post: post
		})
	}

	handleGetMoreDashboardPosts = () => {
		const { dashboardBlogPostsNext } = this.props;
		let fields = dashboardBlogPostsNext.split('=');
		this.props.getMoreDashboardBlogPosts(fields[1]);
	}

	componentDidMount() {
		this.props.getDashboardBlogPosts();
	}

	componentWillUnmount() {
		this.props.clearDashboardBlogPosts();
	}

	render() {
		const { showPosts, showPost, post } = this.state;
		const { dashboardBlogPosts, dashboardBlogPostsNext, loadingDashboardBlogPosts } = this.props;

		return(
			<div>
				{showPosts && 
					<div>
						{!loadingDashboardBlogPosts ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Posts:</span>
								</div>
								{dashboardBlogPosts.map((post, index) => (
									<PostInline select={this.handleShowPost.bind(this, post)} post={post} key={index} />
								))}
								{dashboardBlogPostsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleGetMoreDashboardPosts}>Load More</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										...
									</div>
								}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Posts <Dots /></span>
							</div>
						}
					</div>
				}
				{showPost && 
					<div>
						<Post post={post} closePost={this.handleShowPosts} />
					</div>
				}	
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardBlogPosts: state.dashboard.dashboardBlogPosts,
		dashboardBlogPostsNext: state.dashboard.dashboardBlogPostsNext,
		loadingDashboardBlogPosts: state.dashboard.loadingDashboardBlogPosts
	}
}

export default connect(mapStateToProps, { getDashboardBlogPosts, getMoreDashboardBlogPosts, clearDashboardBlogPosts })(Posts);












