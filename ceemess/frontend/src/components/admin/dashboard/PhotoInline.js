import React from 'react';
import { Grid, Button } from '@material-ui/core';

const PhotoInline = (props) => (
	<Grid container style={{flex: 1, padding: 5}}>
		<Grid item xs={2} style={{padding: 5}}>
			<Button onClick={props.select}><img style={{width: '100%'}} src={props.album.album_photos.length > 0 ? props.album.album_photos[0].thumbnail : ''} /></Button>
		</Grid>
		<Grid item xs={10} style={{padding: 5}}>
			<Button onClick={props.select}>{props.album.title}</Button>
		</Grid>
	</Grid>
);

export default PhotoInline;