import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import Image from './Image';
import ImageInline from './ImageInline';

import { getDashboardImages, getMoreDashboardImages, clearDashboardImages } from '../../../actions/dashboard';

class Images extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showImages: true,
			showImage: false,
			image: null
		}
	}

	handleShowImages = () => {
		this.setState({
			showImages: true,
			showImage: false,
			image: null
		})
	}

	handleShowImage = (image) => {
		this.setState({
			showImages: false,
			showImage: true,
			image: image
		})
	}

	handleGetMoreDashboardImages = () => {
		const { dashboardImagesNext } = this.props;
		let fields = dashboardImagesNext.split('=');
		this.props.getMoreDashboardImages(fields[1]);
	}

	componentDidMount() {
		this.props.getDashboardImages();
	}

	componentWillUnmount() {
		this.props.clearDashboardImages();
	}

	render() {
		const { showImages, showImage, image } = this.state;
		const { dashboardImages, dashboardImagesNext, loadingDashboardImages } = this.props;

		return(
			<div>
				{showImages && 
					<div>
						{!loadingDashboardImages ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Images:</span>
								</div>
								{dashboardImages.map((image, index) => (
									<ImageInline select={this.handleShowImage.bind(this, image)} image={image} key={index} />
								))}
								{dashboardImagesNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleGetMoreDashboardImages}>Load More</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										...
									</div>
								}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Images <Dots /></span>
							</div>
						}
					</div>
				}
				{showImage && 
					<Image image={image} closeImage={this.handleShowImages} />
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardImages: state.dashboard.dashboardImages,
		dashboardImagesNext: state.dashboard.dashboardImagesNext,
		loadingDashboardImages: state.dashboard.loadingDashboardImages
	}
}

export default connect(mapStateToProps, { getDashboardImages, getMoreDashboardImages, clearDashboardImages })(Images);












