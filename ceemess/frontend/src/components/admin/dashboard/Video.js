import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, Select, MenuItem, TextField } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { editVideo, clearEditVideo, deleteVideo, clearDeleteVideo, clearEditVideoFail, clearDeleteVideoFail } from '../../../actions/dashboard';
import { getDashboardVideos, clearDashboardVideos, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class Video extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			youtubeID: '',
			vimeoID: '',
			description: '',
			image: null,
			file: null,
			aspectRatio: '',
			width: '',
			height: '',
			publicVideo: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			slug: '',
			uploading: false
		}
	}

	handleCloseVideo = () => {
		this.props.closeVideo();
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

  handleSelectFile = e => this.setState({
		file: e.target.files[0]
	});

  handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.slug === tagsList[i].slug) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title' || key === 'youtubeID' || key === 'vimeoID'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleCheckboxChange = () => {
		this.setState({
			publicVideo: !this.state.publicVideo
		})
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.setState({uploading: true});
			this.props.deleteVideo(this.props.video);
		}
	}

	handleSubmit = () => {
		const { title, youtubeID, vimeoID, description, file, aspectRatio, width, height, publicVideo, categoriesList, tagsList, categoryName, slug } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName && categoryName !== "select") {
				category = categoriesList[i];
			}
		}
		const video = { title, youtubeID, vimeoID, description, file, aspectRatio, width, height, publicVideo, category, tags, slug };
		if (title !== '') { 
			if (window.confirm("Save changes?")) {
				this.props.editVideo(video);
				this.setState({uploading: true});
			}
		} else {
			alert("Videos must have a Title.");
		}
	}

	componentDidMount() {
		// console.log('Video: ', this.props.video);
		const { video } = this.props;
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
		this.setState({
			title: video.title,
			youtubeID: video.youtube_id ? video.youtube_id !== null : '',
			vimeoID: video.vimeo_id ? video.vimeo_id !== null : '',
			description: video.description,
			image: video.image,
			publicVideo: video.public,
			slug: video.slug
		});
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				category: "Select:",
				slug: "select"
			});
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			const { video } = this.props;
			let categoryName = ''
			if (video.category !== null) {
				categoryName = video.category.slug;
			} else {
				categoryName = 'select';
			}
			this.setState({
				categoryName: categoryName,
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			// console.log('Tags: ', this.props.dashboardTags);
			const { video } = this.props;
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					// id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			for (var i = 0; i < tagsList.length; i++) {
				for (var j = 0; j < video.tags.length; j++) {
					if (tagsList[i].slug === video.tags[j].slug) {
						tagsList[i].checked = true;
					}
				}
			}
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
		if (this.props.editedVideo !== null && lastProps.editedVideo === null) {
			this.props.clearEditVideo();
			this.props.clearDashboardVideos();
			this.props.getDashboardVideos();
			this.setState({uploading: false, image: this.props.editedVideo.image});
		}
		if (this.props.deletedVideo !== null && lastProps.deletedVideo === null) {
			this.props.clearDeleteVideo();
			this.props.clearDashboardVideos();
			this.props.getDashboardVideos();
			this.setState({uploading: false});
			this.handleCloseVideo();
		}
		if (this.props.editedVideoFail !== null && lastProps.editedVideoFail === null) {
			this.props.clearEditVideoFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedVideoFail !== null && lastProps.deletedVideoFail === null) {
			this.props.clearDeleteVideoFail();
			this.setState({uploading: false});
		}
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
	}

	render() {
		const { title, youtubeID, vimeoID, description, image, file, publicVideo, uploading } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
					<Grid container item xs={12}>
						<Grid item xs={11} style={styles.formRow}>
							<span style={styles.formTitle}>Edit Video:</span>
						</Grid>
						<Grid item xs={1}>
							<Button style={styles.closeButton} onClick={this.handleCloseVideo}>X</Button>
						</Grid>
					</Grid>
        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<TextField 
								name="description" 
								onChange={this.handleChange} 
								value={description} 
								placeholder="Description"
								style={styles.textField}
								multiline={true}
								rows="8"
							/>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={4}>
								<span style={{fontSize: 18}}>Current Image:</span>
							</Grid>
							<Grid item xs={8}>
								<img src={image} alt="Video Thumbnail" style={styles.currentImage} />
							</Grid>
						</Grid>
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={4} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>Replace:</label>
							</Grid>
							<Grid item xs={8} style={{flex: 1}}>
								<Input 
									type="file" 
									name="file" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="youtubeID" 
								onChange={this.handleChange} 
								value={youtubeID} 
								placeholder="YouTube ID"
								style={styles.inputField}
							/>
						</Grid>
						<Grid item xs={12} style={styles.formRow}>
							<Input 
								type="text" 
								name="vimeoID" 
								onChange={this.handleChange} 
								value={vimeoID} 
								placeholder="Vimeo ID"
								style={styles.inputField}
							/>
						</Grid>
						{displayCategoriesList ? 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={4}>
									<span style={{fontSize: 18}}>Category:</span>
								</Grid>
								<Grid item xs={8}>
									<Select
	                  value={categoryName} 
	                  onChange={this.handleItemChange.bind(this)}
	                  inputProps={{
	                    name: `Categories`,
	                    id: "Categories"
	                  }} 
	                  style={styles.selectItemName} 
	                >
	                	{categoriesList.map((category, index) => (
											<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
										))}
	                </Select>
                </Grid>
							</Grid> :
							<Grid container item xs={12} style={styles.formRow}>
								<span>Loading Categories <Dots /></span>
							</Grid>
						}
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={1}>
								<Checkbox 
									name="publicVideo" 
									checked={publicVideo} 
									onChange={this.handleCheckboxChange} 
								/>
							</Grid>
							<Grid item xs={10}>
								<Button onClick={this.handleCheckboxChange}>Public</Button>
							</Grid>
						</Grid>
						<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Video</Button>
							</Grid>
							<Grid item xs={6} style={styles.formTypeButtonContainer}>
								<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Video</Button>
							</Grid>
						</Grid>
					</Grid>
					<Grid item md={4} style={styles.formRow}>
						<div>
							<span style={styles.tagsTitle}>Tags:</span>
						</div>
						{displayTagsList ? 
							<Grid container>
								{tagsList.map((tag, index) => (
									<Grid container item xs={12} style={{padding: 5}} key={index}>
										<Grid item xs={2}>
											<Checkbox 
												name={tag.tag} 
												checked={tag.checked} 
												onChange={this.handleSelectTag.bind(this, tag)}
											/>
										</Grid>
										<Grid item xs={10}>
											<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
										</Grid>
									</Grid>
								))}
							</Grid> :
							<div>
								<span>Loading Tags <Dots /></span>
							</div>
						}
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedVideo: state.dashboard.editedVideo,
		deletedVideo: state.dashboard.deletedVideo, 

		editedVideoFail: state.dashboard.editedVideoFail,
		deletedVideoFail: state.dashboard.deletedVideoFail, 

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { editVideo, clearEditVideo, deleteVideo, clearDeleteVideo, clearEditVideoFail, clearDeleteVideoFail, getDashboardVideos, clearDashboardVideos, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags })(Video);












