import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import PhotoAlbum from './PhotoAlbum';
import PhotoAlbumInline from './PhotoAlbumInline';

import { getDashboardPhotoAlbums, getMoreDashboardPhotoAlbums, clearDashboardPhotoAlbums } from '../../../actions/dashboard';

class PhotoAlbums extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPhotoAlbums: true,
			showPhotoAlbum: false,
			photoAlbum: null
		}
	}

	handleShowPhotoAlbums = () => {
		this.setState({
			showPhotoAlbums: true,
			showPhotoAlbum: false,
			photoAlbum: null
		})
	}

	handleShowPhotoAlbum = (album) => {
		this.setState({
			showPhotoAlbums: false,
			showPhotoAlbum: true,
			photoAlbum: album
		})
	}

	handleGetMoreDashboardPhotoAlbums = () => {
		const { dashboardPhotoAlbumsNext } = this.props;
		let fields = dashboardPhotoAlbumsNext.split('=');
		this.props.getMoreDashboardPhotoAlbums(fields[1]);
	}

	componentDidMount() {
		this.props.getDashboardPhotoAlbums();
	}

	// componentDidUpdate(lastProps) {
	// 	if (this.props.loadingDashboardPhotoAlbums === false && lastProps.loadingDashboardPhotoAlbums === true) {
	// 		console.log('dashboardPhotoAlbums: ', this.props.dashboardPhotoAlbums);
	// 	}
	// }

	componentWillUnmount() {
		this.props.clearDashboardPhotoAlbums();
	}

	render() {
		const { showPhotoAlbums, showPhotoAlbum, photoAlbum } = this.state;
		const { dashboardPhotoAlbums, dashboardPhotoAlbumsNext, loadingDashboardPhotoAlbums } = this.props;

		return(
			<div>
				{showPhotoAlbums && 
					<div>
						{!loadingDashboardPhotoAlbums ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Photo Albums:</span>
								</div>
								{dashboardPhotoAlbums.map((album, index) => (
									<PhotoAlbumInline select={this.handleShowPhotoAlbum.bind(this, album)} album={album} key={index} />
								))}
								{dashboardPhotoAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleGetMoreDashboardPhotoAlbums}>Load More</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										...
									</div>
								}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Photo Albums <Dots /></span>
							</div>
						}
					</div>
				}
				{showPhotoAlbum && 
					<PhotoAlbum photoAlbum={photoAlbum} closeAlbum={this.handleShowPhotoAlbums} />
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardPhotoAlbums: state.dashboard.dashboardPhotoAlbums,
		dashboardPhotoAlbumsNext: state.dashboard.dashboardPhotoAlbumsNext,
		loadingDashboardPhotoAlbums: state.dashboard.loadingDashboardPhotoAlbums
	}
}

export default connect(mapStateToProps, { getDashboardPhotoAlbums, getMoreDashboardPhotoAlbums, clearDashboardPhotoAlbums })(PhotoAlbums);












