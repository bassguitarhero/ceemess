import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import Video from './Video';
import VideoInline from './VideoInline';

import { getDashboardVideos, getMoreDashboardVideos, clearDashboardVideos } from '../../../actions/dashboard';

class Videos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showVideos: true,
			showVideo: false,
			video: null
		}
	}

	handleShowVideos = () => {
		this.setState({
			showVideos: true,
			showVideo: false,
			video: null
		})
	}

	handleShowVideo = (video) => {
		this.setState({
			showVideos: false,
			showVideo: true,
			video: video
		})
	}

	handleGetMoreDashboardVideos = () => {
		const { dashboardVideosNext } = this.props;
		let fields = dashboardVideosNext.split('=');
		this.props.getMoreDashboardVideos(fields[1]);
	}

	componentDidMount() {
		this.props.getDashboardVideos();
	}

	componentWillUnmount() {
		this.props.clearDashboardVideos();
	}

	render() {
		const { showVideos, showVideo, video } = this.state;
		const { dashboardVideos, dashboardVideosNext, loadingDashboardVideos } = this.props;

		return(
			<div>
				{showVideos && 
					<div>
						{!loadingDashboardVideos ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Videos:</span>
								</div>
								{dashboardVideos.map((video, index) => (
									<VideoInline select={this.handleShowVideo.bind(this, video)} video={video} key={index} />
								))}
								{dashboardVideosNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleGetMoreDashboardVideos}>Load More</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										...
									</div>
								}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Videos <Dots /></span>
							</div>
						}
					</div>
				}
				{showVideo && 
					<Video video={video} closeVideo={this.handleShowVideos} />
				}	
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardVideos: state.dashboard.dashboardVideos,
		dashboardVideosNext: state.dashboard.dashboardVideosNext,
		loadingDashboardVideos: state.dashboard.loadingDashboardVideos
	}
}

export default connect(mapStateToProps, { getDashboardVideos, getMoreDashboardVideos, clearDashboardVideos })(Videos);












