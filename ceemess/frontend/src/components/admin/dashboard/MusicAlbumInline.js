import React from 'react';
import { Grid, Button } from '@material-ui/core';

const MusicAlbumInline = (props) => (
	<Grid container style={{flex: 1, padding: 5}}>
		<Grid item xs={2} style={{padding: 5}}>
			<Button onClick={props.select}><img style={{width: '100%'}} alt="Music Album Thumbnail" src={props.album.thumbnail} /></Button>
		</Grid>
		<Grid item xs={10} style={{padding: 5}}>
			<div><Button onClick={props.select}>{props.album.band_name}</Button></div>
			<div><Button onClick={props.select}>{props.album.title}</Button></div>
		</Grid>
	</Grid>
);

export default MusicAlbumInline;