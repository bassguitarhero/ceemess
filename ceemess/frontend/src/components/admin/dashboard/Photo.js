import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input } from '@material-ui/core';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import { uploadPhoto, clearUploadPhoto, editPhoto, clearEditPhoto, deletePhoto, clearDeletePhoto, clearUploadPhotoFail, clearEditPhotoFail, clearDeletePhotoFail } from '../../../actions/dashboard';

class Photo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: 1,
			title: '',
			image: null,
			file: null,
			slug: '',
			newPhoto: false,
			uploading: false
		}
	}

	handleSelectFile = (e) => {
		this.setState({
			file: e.target.files[0]
		});
	}

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleUploadPhoto = () => {
		const { title, file } = this.state;
		const { albumSlug } = this.props;
		const photo = { title, file, albumSlug };
		if (title !== '') {
			if (window.confirm("Save changes?")) {
				this.props.uploadPhoto(photo);
				this.setState({uploading: true});
			}
		} else {
			alert("Photos must have a Title.");
		}
	}

	handleSubmit = () => {
		const { title, file, slug } = this.state;
		const photo = { title, file, slug };
		if (window.confirm("Save changes?")) {
			this.setState({uploading: true});
			this.props.editPhoto(photo);
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.props.deletePhoto(this.props.photo);
			this.props.handleAlbumPhotoDelete(this.props.photo);
			this.setState({uploading: true});
		}
	}

	componentDidMount() {
		const { photo } = this.props;
		// console.log('Photo: ', photo);
		this.setState({
			id: photo.id,
			image: photo.thumbnail, 
			title: photo.title,
			slug: photo.slug,
			newPhoto: photo.newPhoto
		});
	}

	componentDidUpdate(lastProps) {
		if (this.props.editedPhotoFail !== null && lastProps.editedPhotoFail === null) {
			this.props.clearEditPhotoFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedPhotoFail !== null && lastProps.deletedPhotoFail === null) {
			this.props.clearDeletePhotoFail();
			this.setState({uploading: false});
		}
		if (this.props.uploadedPhotoFail !== null && lastProps.uploadedPhotoFail === null) {
			this.props.clearUploadPhotoFail();
			this.setState({uploading: false});
		}
		if (this.props.editedPhoto !== null && lastProps.editedPhoto === null) {
			const { editedPhoto } = this.props;
			this.props.clearEditPhoto();
			this.props.handleAlbumPhotoEdit(editedPhoto);
			this.setState({
				uploading: false,
				title: editedPhoto.title,
				image: editedPhoto.image,
				file: '',
				slug: editedPhoto.slug,
				newPhoto: false
			});
		}
		if (this.props.deletedPhoto !== null && lastProps.deletedPhoto === null) {
			this.props.clearDeletePhoto();
			this.setState({uploading: false});
		}
		if (this.props.uploadedPhoto !== null && lastProps.uploadedPhoto === null) {
			const { uploadedPhoto } = this.props;
			this.props.clearUploadPhoto();
			this.props.handleAlbumPhotoCreate(uploadedPhoto);
			this.setState({
				uploading: false,
				title: uploadedPhoto.title,
				image: uploadedPhoto.image,
				file: '',
				slug: uploadedPhoto.slug,
				newPhoto: false
			});
		}
	}

	render() {
		const { id, title, image, file, uploading, newPhoto } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container>
					<Grid item xs={1} style={styles.formRow}>
						<span>{id}</span>
					</Grid>
					<Grid container item xs={11} style={styles.formRow}>
						<Grid item xs={3} style={styles.formRow}>
							<span style={{fontSize: 18}}>Title:</span>
						</Grid>
						<Grid item xs={9} style={styles.formRow}>
							<Input 
								type="text" 
								name="title" 
								onChange={this.handleChange} 
								value={title} 
								placeholder="Title"
								style={styles.inputField}
							/>
						</Grid>
						{image !== null && 
							<Grid container item xs={12} style={styles.formRow}>
								<Grid item xs={3}>
									<span style={{fontSize: 18}}>Current Image:</span>
								</Grid>
								<Grid item xs={9}>
									<img src={image} alt="Thumbnail" style={styles.currentImage} />
								</Grid>
							</Grid>
						}
						<Grid container item xs={12} style={styles.formRow}>
							<Grid item xs={3} style={{flex: 1}}>
								<label htmlFor="file" style={{fontSize: 18}}>
									{newPhoto === true ? <span>Image:</span> : <span>Replace:</span>}
								</label>
							</Grid>
							<Grid item xs={9} style={{flex: 1}}>
								<Input 
									type="file" 
									name="file" 
									onChange={this.handleSelectFile}
								/>
							</Grid>
						</Grid>
						{newPhoto === false ? 
							<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
								<Grid item xs={6} style={styles.formTypeButtonContainer}>
									<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Photo</Button>
								</Grid>
								<Grid item xs={6} style={styles.formTypeButtonContainer}>
									<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Photo</Button>
								</Grid>
							</Grid> :
							<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
								<Grid item xs={6} style={styles.formTypeButtonContainer}>
									<Button style={styles.submitButton} onClick={this.handleUploadPhoto}>Upload New Photo</Button>
								</Grid>
							</Grid>
						}
					</Grid>
				</Grid>
				<hr style={styles.bottomLine} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		uploadedPhoto: state.dashboard.uploadedPhoto,
		editedPhoto: state.dashboard.editedPhoto,
		deletedPhoto: state.dashboard.deletedPhoto,
		uploadedPhotoFail: state.dashboard.uploadedPhotoFail,
		editedPhotoFail: state.dashboard.editedPhotoFail,
		deletedPhotoFail: state.dashboard.deletedPhotoFail
	}
}

export default connect(mapStateToProps, {  uploadPhoto, clearUploadPhoto, editPhoto, clearEditPhoto, deletePhoto, clearDeletePhoto, clearUploadPhotoFail, clearEditPhotoFail, clearDeletePhotoFail })(Photo);











