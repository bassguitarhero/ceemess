import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Checkbox, TextField, Select, MenuItem } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../../Styles';

import UploadModal from '../../common/UploadModal';
import Backdrop from '../../common/Backdrop';

import MusicTrack from './MusicTrack';

import { editMusicAlbum, clearEditMusicAlbum, deleteMusicAlbum, clearDeleteMusicAlbum, clearEditMusicAlbumFail, clearDeleteMusicAlbumFail } from '../../../actions/dashboard';
import { getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';
import { getDashboardMusicAlbums, clearDashboardMusicAlbums } from '../../../actions/dashboard';

class MusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMusicAlbumForm: true,
			showMusicTracksForm: false,
			musicTrack: null,
			// edit 
			bandName: '',
			title: '',
			description: '',
			file: null,
			image: null,
			publicAlbum: true,
			categoriesList: [],
			displayCategoriesList: false, 
			category: null,
			tags: [],
			tagsList: [],
			displayTagsList: false,
			categoryName: '',
			uploading: false,
			tracks: [],
			slug: '',
			forceRefresh: false
		}
	}

	handleAlbumTrackAdd = () => {
		const { tracks } = this.state;
		// console.log('Tracks: ', tracks);
		var id = tracks[tracks.length -1].id + 1;
		// console.log('id: ', id);
		tracks.push({
			id: id,
			order: '',
			title: '',
			description: '',
			image: null,
			thumbnail: null,
			file: null,
			soundcloud: '',
			audio: null,
			youtube_url: '',
			slug: '',
			newTrack: true
		});
		this.setState({tracks: tracks});
	}

	handleAlbumTrackCreate = (uploadedSong) => {
		console.log('Uploaded Song: ', uploadedSong);
		this.setState({forceRefresh: true});
	}

	handleAlbumTrackEdit = (editedTrack) => {
		console.log('Edited Track: ', editedTrack);
		const { tracks } = this.state;
		for (var i = 0; i < tracks.length; i++) {
			if (tracks[i].slug === editedTrack.slug) {
				tracks[i].order = editedTrack.order;
				tracks[i].title = editedTrack.title;
				tracks[i].description = editedTrack.description;
				tracks[i].image = editedTrack.image;
				tracks[i].thumbnail = editedTrack.thumbnail;
				tracks[i].soundcloud = editedTrack.soundcloud_embed;
				tracks[i].audio = editedTrack.audio_file;
				tracks[i].youtube_url = editedTrack.youtube_url;
				tracks[i].slug = editedTrack.slug;
				tracks[i].newTrack = false;
			}
		}
		this.setState({tracks: tracks, forceRefresh: true});
	}

	handleAlbumTrackDelete = (deletedTrack) => {
		// console.log('Deleted Track: ', deletedTrack);
		const { tracks } = this.state;
		const newTracks = tracks.filter(track => (track.slug !== deletedTrack.slug));
		this.setState({tracks: newTracks, forceRefresh: true});
	}

	handleShowAlbumForm = () => {
		this.setState({
			showMusicAlbumForm: true,
			showMusicTracksForm: false,
		})
	}

	handleShowTracksForm = () => {
		this.setState({
			showMusicAlbumForm: false,
			showMusicTracksForm: true,
		})
	}

	handleCloseAlbum = () => {
		this.props.closeAlbum();
	}

	handleItemChange = (e) => {
    this.setState({
      categoryName: e.target.value
    });
  }

	handleChange = (e) => {
		e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'bandName' || key === 'title'){
			if (value.length > 250){
				alert("Your input is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
	}

	handleSelectTag = (tag) => {
		const { tagsList } = this.state;
		for (var i = 0; i < tagsList.length; i++) {
			if (tag.slug === tagsList[i].slug) {
				tagsList[i].checked = !tagsList[i].checked
			}
		}
		this.setState({
			tagsList: tagsList
		});
	}

	handleSubmit = () => {
		const { bandName, title, description, file, publicAlbum, categoriesList, tagsList, categoryName, slug } = this.state;
		const tags = [];
		for (var i = 0; i < tagsList.length; i++) {
			if (tagsList[i].checked) {
				tags.push(tagsList[i]);
			}
		}
		let category = null;
		for (var i = 0; i < categoriesList.length; i++) {
			if (categoriesList[i].slug === categoryName) {
				category = categoriesList[i];
			}
		}
		const album = { bandName, title, description, file, category, publicAlbum, tags, slug };
		if (title !== '') {
			if (window.confirm("Save changes?")) {
				this.props.editMusicAlbum(album);
				this.setState({uploading: true});
			}
		} else {
			alert("Albums must have a Title.");
		}
	}

	handleDelete = () => {
		if (window.confirm("Delete?")) {
			this.props.deleteMusicAlbum(this.props.musicAlbum);
			this.setState({uploading: true});
		}
	}

	handleSelectFile = e => this.setState({
		file: e.target.files[0]
	});

	handleCheckboxChange = () => {
		this.setState({
			publicAlbum: !this.state.publicAlbum
		})
	}

	componentDidMount() {
		// console.log('Album: ', this.props.musicAlbum);
		this.props.getDashboardCategories();
		this.props.getDashboardTags();
		const { musicAlbum } = this.props;
		const tracks = [];
		var id = 1;
		for (var i = 0; i < musicAlbum.album_songs.length; i++) {
			tracks.push({
				id: id,
				title: musicAlbum.album_songs[i].title,
				description: musicAlbum.album_songs[i].description,
				image: musicAlbum.album_songs[i].image,
				thumbnail: musicAlbum.album_songs[i].thumbnail,
				soundcloud: musicAlbum.album_songs[i].soundcloud_embed,
				audio: musicAlbum.album_songs[i].audio_file,
				youtube_url: musicAlbum.album_songs[i].youtube_url,
				slug: musicAlbum.album_songs[i].slug,
				newTrack: false
			});
			id += 1;
		}
		this.setState({
			bandName: musicAlbum.band_name,
			title: musicAlbum.title,
			description: musicAlbum.description,
			image: musicAlbum.thumbnail,
			publicAlbum: musicAlbum.public,
			musicAlbum: musicAlbum,
			slug: musicAlbum.slug,
			tracks: tracks
		})
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.editedMusicAlbumFail !== null && lastProps.editedMusicAlbumFail === null) {
			this.props.clearEditMusicAlbumFail();
			this.setState({uploading: false});
		}
		if (this.props.deletedMusicAlbumFail !== null && lastProps.deletedMusicAlbumFail === null) {
			this.props.clearDeleteMusicAlbumFail();
			this.setState({uploading: false});
		}
		if (this.props.editedMusicAlbum !== null && lastProps.editedMusicAlbum === null) {
			this.props.clearEditMusicAlbum();
			this.props.clearDashboardMusicAlbums();
			this.props.getDashboardMusicAlbums();
			this.setState({uploading: false});
		}
		if (this.props.deletedMusicAlbum !== null && lastProps.deletedMusicAlbum === null) {
			this.props.clearDeleteMusicAlbum();
			this.props.clearDashboardMusicAlbums();
			this.props.getDashboardMusicAlbums();
			this.setState({uploading: false});
			this.handleCloseAlbum();
		}
		if (this.props.loadingDashboardCategories === false && lastProps.loadingDashboardCategories === true) {
			const categoriesList = [];
			categoriesList.push({
				id: 0,
				slug: 'select',
				category: "Select:"
			})
			this.props.dashboardCategories.map(category => {
				categoriesList.push(category);
			})
			const { musicAlbum } = this.props;
			let categoryName = 'select';
			if (musicAlbum.category !== null) {
				categoryName = musicAlbum.category.slug
			}
			this.setState({
				categoryName: categoryName,
				categoriesList: categoriesList,
				displayCategoriesList: true
			})
		}	
		if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
			const { musicAlbum } = this.props;
			const tagsList = [];
			this.props.dashboardTags.map(tag => {
				tagsList.push({
					// id: tag.id,
					tag: tag.tag,
					slug: tag.slug,
					checked: false
				});
			})
			for (var i = 0; i < tagsList.length; i++) {
				for (var j = 0; j < musicAlbum.tags.length; j++) {
					if (tagsList[i].slug === musicAlbum.tags[j].slug) {
						tagsList[i].checked = true;
					}
				}
			}
			this.setState({
				tagsList: tagsList,
				displayTagsList: true
			})
		}	
	}

	componentWillUnmount() {
		this.props.clearDashboardCategories();
		this.props.clearDashboardTags();
		if (this.state.forceRefresh === true) {
			this.props.clearDashboardMusicAlbums();
			this.props.getDashboardMusicAlbums();
		}
	}

	render() {
		const { showMusicAlbumForm, showMusicTracksForm, musicTrack, uploading, musicAlbum } = this.state;
		const { bandName, title, description, file, image, publicAlbum, tracks, slug } = this.state;
		const { tags, tagsList, displayTagsList, category, categoriesList, displayCategoriesList, categoryName } = this.state;

		return (
			<div style={{flex: 1}}>
				{uploading && 
					<div>
						<UploadModal />
						<Backdrop />
					</div>
				}
				<Grid container style={styles.formTypeButtonsContainer}>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showMusicAlbumForm ? styles.formTypeButtonSelected : styles.formTypeButton } onClick={this.handleShowAlbumForm}>Album</Button>
					</Grid>
					<Grid item xs={6} style={styles.formTypeButtonContainer}>
						<Button style={showMusicTracksForm ? styles.formTypeButtonSelected : styles.formTypeButton} onClick={this.handleShowTracksForm}>Tracks</Button>
					</Grid>
				</Grid>
				{showMusicAlbumForm && 
					<div style={{flex: 1}}>
						<Grid container>
							<Grid item xs={11} style={styles.formRow}>
								<span style={styles.formTitle}>Edit Music Album:</span>
							</Grid>
							<Grid item xs={1}>
								<Button style={styles.closeButton} onClick={this.handleCloseAlbum}>X</Button>
							</Grid>
		        	<Grid container item md={8} xs style={{alignContent: 'flex-start', alignItems: 'flex-start'}}>
								<Grid item xs={12} style={styles.formRow}>
									<Input 
										type="text" 
										name="bandName" 
										onChange={this.handleChange} 
										value={bandName} 
										placeholder="Band Name"
										style={styles.inputField}
									/>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<Input 
										type="text" 
										name="title" 
										onChange={this.handleChange} 
										value={title} 
										placeholder="Title"
										style={styles.inputField}
									/>
								</Grid>
								<Grid item xs={12} style={styles.formRow}>
									<TextField 
										name="description" 
										onChange={this.handleChange} 
										value={description} 
										placeholder="Description"
										style={styles.textField}
										multiline={true}
										rows="8"
									/>
								</Grid>
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={3}>
										<span style={{fontSize: 18}}>Current Image:</span>
									</Grid>
									<Grid item xs={9}>
										<img src={image} alt="Music Album Thumbnail" style={styles.currentImage} />
									</Grid>
								</Grid>
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={3} style={{flex: 1}}>
										<label htmlFor="file" style={{fontSize: 18}}>Replace:</label>
									</Grid>
									<Grid item xs={9} style={{flex: 1}}>
										<Input 
											type="file" 
											name="file" 
											onChange={this.handleSelectFile}
										/>
									</Grid>
								</Grid>
								{displayCategoriesList ? 
									<Grid container item xs={12} style={styles.formRow}>
										<Grid item xs={3}>
											<span style={{fontSize: 18}}>Category:</span>
										</Grid>
										<Grid item xs={9}>
											<Select
			                  value={categoryName} 
			                  onChange={this.handleItemChange.bind(this)}
			                  inputProps={{
			                    name: `Categories`,
			                    id: "Categories"
			                  }} 
			                  style={styles.selectItemName} 
			                >
			                	{categoriesList.map((category, index) => (
													<MenuItem value={category.slug} key={index}>{category.category}</MenuItem>
												))}
			                </Select>
		                </Grid>
									</Grid> :
									<Grid container item xs={12} style={styles.formRow}>
										<span>Loading Categories <Dots /></span>
									</Grid>
								}
								<Grid container item xs={12} style={styles.formRow}>
									<Grid item xs={1}>
										<Checkbox 
											name="publicAlbum" 
											checked={publicAlbum} 
											onChange={this.handleCheckboxChange} 
										/>
									</Grid>
									<Grid item xs={10}>
										<Button onClick={this.handleCheckboxChange}>Public</Button>
									</Grid>
								</Grid>
								<Grid container item xs={12} style={{paddingTop: 20, flex: 1}}>
									<Grid item xs={6} style={styles.formTypeButtonContainer}>
										<Button style={styles.submitButton} onClick={this.handleSubmit}>Edit Album</Button>
									</Grid>
									<Grid item xs={6} style={styles.formTypeButtonContainer}>
										<Button style={styles.submitButton} onClick={this.handleDelete}>Delete Album</Button>
									</Grid>
								</Grid>
							</Grid>
							<Grid item md={4} style={styles.formRow}>
								<div>
									<span style={styles.tagsTitle}>Tags:</span>
								</div>
								{displayTagsList && 
									<Grid container>
										{tagsList.map((tag, index) => (
											<Grid container item xs={12} style={{padding: 5}} key={index}>
												<Grid item xs={2}>
													<Checkbox 
														name={tag.tag} 
														checked={tag.checked} 
														onChange={this.handleSelectTag.bind(this, tag)}
													/>
												</Grid>
												<Grid item xs={10}>
													<Button onClick={this.handleSelectTag.bind(this, tag)}>{tag.tag}</Button>
												</Grid>
											</Grid>
										))}
									</Grid>
								}
							</Grid>
						</Grid>
					</div>
				}
				{showMusicTracksForm && 
					<div style={{flex: 1}}>
						<div><span style={styles.formTitle}>{title}:</span></div>
						{tracks.map((musicTrack, index) => (
							<MusicTrack albumSlug={slug} musicTrack={musicTrack} handleAlbumTrackCreate={this.handleAlbumTrackCreate} handleAlbumTrackEdit={this.handleAlbumTrackEdit} handleAlbumTrackDelete={this.handleAlbumTrackDelete} key={index} />
						))}
						<div style={styles.formRow}>
							<Button style={styles.submitButton} onClick={this.handleAlbumTrackAdd}>Add New Track</Button>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		editedMusicAlbum: state.dashboard.editedMusicAlbum,
		deletedMusicAlbum: state.dashboard.deletedMusicAlbum,
		editedMusicAlbumFail: state.dashboard.editedMusicAlbumFail,
		deletedMusicAlbumFail: state.dashboard.deletedMusicAlbumFail,

		dashboardCategories: state.dashboard.dashboardCategories,
		loadingDashboardCategories: state.dashboard.loadingDashboardCategories, 
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { editMusicAlbum, clearEditMusicAlbum, deleteMusicAlbum, clearDeleteMusicAlbum, clearEditMusicAlbumFail, clearDeleteMusicAlbumFail, getDashboardCategories, clearDashboardCategories, getDashboardTags, clearDashboardTags, getDashboardMusicAlbums, clearDashboardMusicAlbums })(MusicAlbum);












