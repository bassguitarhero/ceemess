import React from 'react';
import { Button } from '@material-ui/core';

const CategoryInline = (props) => (
	<div style={{paddingLeft: 10}}>
		<Button onClick={props.select}>{props.category.category}</Button>
	</div>
);

export default CategoryInline;