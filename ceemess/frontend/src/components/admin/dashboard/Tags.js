import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import Tag from './Tag';
import TagInline from './TagInline';

import { getDashboardTags, clearDashboardTags } from '../../../actions/dashboard';

class Tags extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showTags: true,
			showTag: false,
			tag: null
		}
	}

	handleShowTags = () => {
		this.setState({
			showTags: true,
			showTag: false,
			tag: null
		})
	}

	handleShowTag = (tag) => {
		this.setState({
			showTags: false,
			showTag: true,
			tag: tag
		})
	}

	componentDidMount() {
		this.props.getDashboardTags();
	}

	componentDidUpdate(lastProps) {
		// if (this.props.loadingDashboardTags === false && lastProps.loadingDashboardTags === true) {
		// 	console.log('Tags: ', this.props.dashboardTags);
		// }
	}

	componentWillUnmount() {
		this.props.clearDashboardTags();
	}

	render() {
		const { showTags, showTag, tag } = this.state;
		const { dashboardTags, loadingDashboardTags } = this.props;

		return(
			<div style={{flex: 1}}>
				{showTags && 
					<div>
						{!loadingDashboardTags ? 
							<div>
								<div style={{padding: 10}}>
									<span style={{fontSize: 20, fontWeight: 'bold'}}>Tags:</span>
								</div>
								{dashboardTags.map((tag, index) => (
									<TagInline select={this.handleShowTag.bind(this, tag)} tag={tag} key={index} />
								))}
							</div> :
							<div style={{padding: 10}}>
								<span style={{fontSize: 20, fontWeight: 'bold'}}>Loading Tags <Dots /></span>
							</div>
						}
					</div>
				}
				{showTag && 
					<Tag tag={tag} closeTag={this.handleShowTags} />
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dashboardTags: state.dashboard.dashboardTags,
		loadingDashboardTags: state.dashboard.loadingDashboardTags
	}
}

export default connect(mapStateToProps, { getDashboardTags, clearDashboardTags })(Tags);












