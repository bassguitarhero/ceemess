import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import PhotoAlbum from './PhotoAlbum';
import PhotoAlbumInline from './PhotoAlbumInline';
import PhotoInline from './PhotoInline';

import { getPhotoAlbums, getMorePhotoAlbums, clearPhotoAlbums, getTagPhotos, getCategoryPhotos } from '../../actions/photos';

class PhotosHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPhotoAlbums: true,
			showPhotoAlbum: false,
			showPhotoAlbumPhoto: false,
			photoAlbum: null,
			photoAlbumPhoto: null
		}
	}

	handleGetTagPhotos = (tag) => {
		this.props.getTagPhotos(tag);
		this.props.viewTagPhotos(tag);
	}

	handleGetCategoryPhotos = (category) => {
		this.props.getCategoryPhotos(category);
		this.props.viewCategoryPhotos(category);
	}

	handleLoadMorePhotoAlbums = () => {
		const { photoAlbumsNext } = this.props;
		var fields = photoAlbumsNext.split('=');
		this.props.getMorePhotoAlbums(fields[1]);
	}

	handleClosePhotoAlbumPhoto = () => {
		this.setState({
			photoAlbumPhoto: null,
			showPhotoAlbums: true,
			showPhotoAlbumPhoto: false
		});
	}

	handleShowPhoto = (photo) => {
		this.setState({
			photoAlbumPhoto: photo
		});
	}

	handleClosePhotoAlbum = () => {
		this.setState({
			photoAlbum: null,
			showPhotoAlbums: true,
			showPhotoAlbum: false
		});
	}

	handleShowPhotoAlbum = (album) => {
		this.setState({
			photoAlbum: album
		});
	}

	componentDidMount() {
		this.props.getPhotoAlbums();
		this.setState({
			photoAlbum: null,
			photoAlbumPhoto: null
		});
	}

	componentDidUpdate(lastState) {
		if (this.state.photoAlbum != null && lastState.photoAlbum == null && this.state.showPhotoAlbum === false) {
			this.setState({
				showPhotoAlbums: false,
				showPhotoAlbum: true
			});
		}
		if (this.state.photoAlbumPhoto != null && lastState.photoAlbumPhoto == null && this.state.showPhotoAlbumPhoto === false) {
			this.setState({
				showPhotoAlbums: false,
				showPhotoAlbumPhoto: true
			});
		}
	}

	componentWillUnmount() {
		this.props.clearPhotoAlbums();
	}

	render() {
		const { photoAlbums, loadingPhotoAlbums, photoAlbumsNext } = this.props;
		const { showPhotoAlbums, showPhotoAlbum, showPhotoAlbumPhoto, photoAlbum, photoAlbumPhoto } = this.state;

		return (
			<div style={{flex: 1}}>
				{showPhotoAlbums && 
					<div style={{flex: 1}}>
						{loadingPhotoAlbums === false ? 
							<div style={{flex: 1}}>
								{photoAlbums.map((album, index) => (
									<div key={index}>
										{album.album_photos.length > 0 && 
											<PhotoAlbumInline key={index} showPhoto={this.handleShowPhoto} showPhotoAlbum={this.handleShowPhotoAlbum} album={album} viewCategoryPhotos={this.handleGetCategoryPhotos} viewTagPhotos={this.handleGetTagPhotos} />
										}
									</div>
								))}
								{photoAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMorePhotoAlbums}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}><span style={{fontSize: 16}}>Loading Photos</span> <Dots /></div>
								</Grid>
							</Grid>
						}
					</div>
				}
				{showPhotoAlbum && 
					<PhotoAlbum closePhotoAlbum={this.handleClosePhotoAlbum} album={photoAlbum} viewCategoryPhotos={this.handleGetCategoryPhotos} viewTagPhotos={this.handleGetTagPhotos} />
				}
				{showPhotoAlbumPhoto && 
					<PhotoInline closePhotoAlbumPhoto={this.handleClosePhotoAlbumPhoto} photo={photoAlbumPhoto} />
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		photoAlbums: state.photos.photoAlbums,
		loadingPhotoAlbums: state.photos.loadingPhotoAlbums,
		photoAlbumsNext: state.photos.photoAlbumsNext
	}
}

export default connect(mapStateToProps, { getPhotoAlbums, getMorePhotoAlbums, clearPhotoAlbums, getTagPhotos, getCategoryPhotos })(PhotosHome);












