import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import PhotoAlbumInline from './PhotoAlbumInline';
import PhotoAlbum from './PhotoAlbum';
import PhotoInline from './PhotoInline';

import { getMoreSearchPhotos, clearSearchPhotos, getTagPhotos, getCategoryPhotos } from '../../actions/photos';

class SearchPhotos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPhotoAlbumPhoto: false,
			showPhotoAlbums: true,
			showPhotoAlbum: false,
			photoAlbumPhoto: null,
			album: null
		}
	}

	handleGetTagPhotos = (tag) => {
		this.props.getTagPhotos(tag);
		this.props.viewTagPhotos(tag);
	}

	handleGetCategoryPhotos = (category) => {
		this.props.getCategoryPhotos(category);
		this.props.viewCategoryPhotos(category);
	}

	handleLoadMorePhotos = () => {
		const { searchText, searchPhotoAlbumsNext } = this.props;
		var fields = searchPhotoAlbumsNext.split('=');
		this.props.getMoreSearchPhotos(searchText, fields[1]);
	}

	handleClosePhotoAlbumPhoto = () => {
		this.setState({
			photoAlbumPhoto: null,
			showPhotoAlbums: true,
			showPhotoAlbumPhoto: false
		});
	}

	handleShowPhoto = (photo) => {
		this.setState({
			photoAlbumPhoto: photo
		});
	}

	handleClosePhotos = () => {
		this.setState({
			showPhotoAlbums: true,
			showPhotoAlbum: false,
			album: null
		});
	}

	handleShowPhotos = (album) => {
		this.setState({
			album: album
		});
	}

	componentDidMount() {

	}

	componentDidUpdate(lastState) {
		if (this.state.album !== null && lastState.album === null && this.state.showPhotoAlbum === false) {
			this.setState({
				showPhotoAlbums: false,
				showPhotoAlbum: true,
			});
		}
		if (this.state.photoAlbumPhoto !== null && lastState.photoAlbumPhoto === null && this.state.showPhotoAlbumPhoto === false) {
			this.setState({
				showPhotoAlbums: false,
				showPhotoAlbumPhoto: true
			});
		}
	}

	componentWillUnmount() {
		this.props.clearSearchPhotos();
	}

	render() {
		const { searchPhotoAlbums, loadingSearchPhotoAlbums, searchPhotoAlbumsNext } = this.props;
		const { searchText } = this.props;
		const { showPhotoAlbumPhoto, showPhotoAlbums, showPhotoAlbum, album, photoAlbumPhoto } = this.state;

		return (
			<div style={{flex: 1}}>
				<Grid item xs={11}>
					<div style={{flex: 1, padding: 20}}><span style={{fontSize: 18, fontWeight: 'bold'}}>Search: {searchText}</span></div>
				</Grid>
				{showPhotoAlbumPhoto && 
					<PhotoInline closePhotoAlbumPhoto={this.handleClosePhotoAlbumPhoto} photo={photoAlbumPhoto} />
				}
				{showPhotoAlbum && 
					<div>
						<PhotoAlbum closePhotoAlbum={this.handleClosePhotos} album={album} viewCategoryPhotos={this.handleGetCategoryPhotos} viewTagPhotos={this.handleGetTagPhotos} />
					</div>
				}
				{showPhotoAlbums && 
					<div style={{flex: 1}}>
						{loadingSearchPhotoAlbums === false ? 
							<div style={{flex: 1}}>
								{searchPhotoAlbums.map((album, index) => (
									<PhotoAlbumInline key={index} showPhoto={this.handleShowPhoto} showPhotoAlbum={this.handleShowPhotos} album={album} viewCategoryPhotos={this.handleGetCategoryPhotos} viewTagPhotos={this.handleGetTagPhotos} />
								))}
								{searchPhotoAlbumsNext ? 
									<div style={styles.loadMoreContainer}>
										<Button style={styles.loadMoreButton} onClick={this.handleLoadMorePhotos}>
											Load More
										</Button>
									</div> :
									<div style={styles.loadMoreContainer}>
										<span style={styles.text}>...</span>
									</div>
								}
							</div> :
							<Grid container style={{flex: 1}}>
								<Grid item md={2} />
				        <Grid item md={8} sm={12} style={{flex: 1}}>
									<div style={styles.loadingContainer}>
										<span style={styles.loadingText}>Loading Photos</span> <Dots />
									</div>
								</Grid>
							</Grid>
						}
					</div>
				}
			</div>
		);
	}	
}

const mapStateToProps = (state) => {
	return {
		searchPhotoAlbums: state.photos.searchPhotoAlbums,
		loadingSearchPhotoAlbums: state.photos.loadingSearchPhotoAlbums,
		searchPhotoAlbumsNext: state.photos.searchPhotoAlbumsNext
	}
}

export default connect(mapStateToProps, { getMoreSearchPhotos, clearSearchPhotos, getTagPhotos, getCategoryPhotos })(SearchPhotos);












