import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

class PhotoAlbumInline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			thumbnail: '',
			firstFour: [],
			displayReady: false
		}
	}

	handleGetTagPhotos = (tag) => {
		this.props.viewTagPhotos(tag);
	}

	handleGetCategoryPhotos = (category) => {
		this.props.viewCategoryPhotos(category);
	}

	handleGetThumbnail = () => {
		const { album } = this.props;
		if (album.album_photos.length > 0) {
			const thumbnail = album.album_photos[album.album_photos.length -1].thumbnail;
			this.setState({
				thumbnail: thumbnail
			});
		}
	}

	handleGetFirstFour = () => {
		const { album } = this.props;
		const { firstFour } = this.state;
		if (album.album_photos.length > 0) {
			if (album.album_photos.length >= 4) {
				for (var i = 0; i < 4; i++) {
					// console.log('Photo: ', album.album_photos[i]);
					firstFour.push(album.album_photos[i]);
				}
			} else {
				for (var i = 0; i < album.album_photos.length; i++) {
					// console.log('Photo: ', album.album_photos[i]);
					firstFour.push(album.album_photos[i]);
				}
			}
		}
		this.setState({displayReady: true})
	}

	handleSelectPhoto = (photo) => {
		this.props.showPhoto(photo);
	}

	handleSelectAlbum = (album) => {
		this.props.showPhotoAlbum(album);
	}

	componentDidMount() {
		// console.log('Album: ', this.props.album);
		this.handleGetThumbnail();
		this.handleGetFirstFour();
	}

	render() {
		function truncateDescription(s) {
			if (s.length > 400) {
				return s.substring(0, 300) + '...';
			} else {
				return s;
			}
		}

		const { album } = this.props;
		const { thumbnail, firstFour, displayReady } = this.state;

		return (
			<Grid container style={{flex: 1}}>
				<Grid item sm={3}>
					<div style={styles.imageContainer}>
						<Button onClick={this.handleSelectAlbum.bind(this, album)}>
							<img src={thumbnail} alt="Album Thumbnail" style={styles.albumImage} />
						</Button>
					</div>
				</Grid>
				<Grid container item sm={9}>
					<Grid item xs={12}>
						<div style={styles.titleContainer}><Button style={styles.inlinePostTitleText} onClick={this.handleSelectAlbum.bind(this, album)}>{album.title}</Button></div>
						<div style={styles.descriptionContainer}><span style={styles.descriptionText}>{truncateDescription(album.description)}</span></div>
					</Grid>
					{displayReady && 
						<Grid container item xs={12} style={{flex: 1, paddingLeft: 10, paddingRight: 10}}>
							{firstFour.map((photo, index) => (
								<Grid item xs={3} key={index}>
									<Button onClick={this.handleSelectPhoto.bind(this, photo)}>
										<div><img src={photo.thumbnail} alt="Thumbnail" style={{width: '100%'}} /></div>
									</Button>
								</Grid>
							))}
							{album.album_photos.length > 4 && 
								<Grid item xs={12}>
									<div style={styles.readMoreContainer}><span style={{fontSize: 16}}>{album.album_photos.length} photos</span></div>
								</Grid>
							}
							<Grid item xs={12}>
								<div style={styles.readMoreContainer}>
									<Button style={styles.readMoreButton} onClick={this.handleSelectAlbum.bind(this, album)}>Read More</Button>
								</div>
							</Grid>
						</Grid>
					}
				</Grid>
				<Grid container item xs={12}>
					<Grid item sm={6} xs={12} style={styles.categoryContainer}>
						{album.category !== null && 
							<Button style={styles.categoryButton} onClick={this.handleGetCategoryPhotos.bind(this, album.category)}>{album.category.category}</Button>
						}
					</Grid>
					<Grid item sm={6} xs={12} style={styles.tagsContainer}>
						{album.tags.map((tag, index) => (
							<Button key={index} style={styles.tagButton} onClick={this.handleGetTagPhotos.bind(this, tag)} >{tag.tag}</Button>
						))}
					</Grid>
				</Grid>
				<Grid item xs={12}><hr style={styles.bottomLine} /></Grid>
			</Grid>
		);
	}
}

export default PhotoAlbumInline;












