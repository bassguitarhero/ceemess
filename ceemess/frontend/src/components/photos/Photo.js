import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class Photo extends Component {
	handleClosePhoto = () => {
		this.props.closePhoto();
	}

	componentDidMount() {
		// console.log('Photo: ', this.props.photo);
	}

	render() {
		const { photo } = this.props;

		return (
			<div>
				<div style={styles.imageContainer}>
					<img src={photo.image} alt={photo.title} style={styles.image} />
				</div>
				<div style={styles.captionContainer}>
					<span style={styles.captionText}>{photo.caption}</span>
				</div>
				<div style={styles.closeImagePlaceHolder}>
					<div style={styles.closeImagePosition}>
						<div style={styles.closeImageCircle}>
							<Button onClick={this.handleClosePhoto} style={styles.closeImageText}> X </Button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Photo;

