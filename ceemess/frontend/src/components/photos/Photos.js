import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import PhotosHome from './PhotosHome';
import CategoryPhotos from './CategoryPhotos';
import TagPhotos from './TagPhotos';
import SearchPhotos from './SearchPhotos';
import PhotosSidebar from './PhotosSidebar';

class Photos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPhotosHome: true,
			showCategoryPhotos: false,
			showTagPhotos: false,
			showSearchPhotos: false,
			category: null,
			tag: null,
			searchText: ''
		}
	}

	handleCloseCategoryPhotos = () => {
		this.setState({
			category: null,
			showPhotosHome: true,
			showCategoryPhotos: false,
			showTagPhotos: false,
			showSearchPhotos: false,
		})
	}

	handleShowCategoryPhotos = (category) => {
		this.setState({
			category: category,
			showPhotosHome: false,
			showCategoryPhotos: true,
			showTagPhotos: false,
			showSearchPhotos: false,
		})
	}

	handleCloseTagPhotos = () => {
		this.setState({
			tag: null,
			showPhotosHome: true,
			showCategoryPhotos: false,
			showTagPhotos: false,
			showSearchPhotos: false,
		})
	}

	handleShowTagPhotos = (tag) => {
		this.setState({
			tag: tag,
			showPhotosHome: false,
			showCategoryPhotos: false,
			showTagPhotos: true,
			showSearchPhotos: false,
		})
	}

	handleCloseSearchPhotos = () => {
		this.setState({
			searchText: '',
			showPhotosHome: true,
			showCategoryPhotos: false,
			showTagPhotos: false,
			showSearchPhotos: false,
		})
	}

	handleShowSearchPhotos = (searchText) => {
		this.setState({
			searchText: searchText,
			showPhotosHome: false,
			showCategoryPhotos: false,
			showTagPhotos: false,
			showSearchPhotos: true,
		})
	}

	render() {
		const { showPhotosHome, showCategoryPhotos, showTagPhotos, showSearchPhotos } = this.state;
		const { category, tag, searchText } = this.state;

		return (
			<div>
				<Grid container style={{flex: 1}}>
					<Grid md={1} item />
	        <Grid container md={8} item xs style={{flex: 1}}>
						{showPhotosHome && 
							<PhotosHome viewCategoryPhotos={this.handleShowCategoryPhotos} viewTagPhotos={this.handleShowTagPhotos} />
						}
						{showCategoryPhotos && 
							<CategoryPhotos viewCategoryPhotos={this.handleShowCategoryPhotos} viewTagPhotos={this.handleShowTagPhotos} closeCategoryPhotos={this.handleCloseCategoryPhotos} category={category} />
						}
						{showTagPhotos && 
							<TagPhotos viewCategoryPhotos={this.handleShowCategoryPhotos} viewTagPhotos={this.handleShowTagPhotos} closeTagPhotos={this.handleCloseTagPhotos} tag={tag} />
						}
						{showSearchPhotos  && 
							<SearchPhotos viewCategoryPhotos={this.handleShowCategoryPhotos} viewTagPhotos={this.handleShowTagPhotos} closeSearchPhotos={this.handleCloseSearchPhotos} searchText={searchText} />
						}
					</Grid>
					<Grid container md={2} item xs style={{flex: 1}}>
						<PhotosSidebar viewCategoryPhotos={this.handleShowCategoryPhotos} viewTagPhotos={this.handleShowTagPhotos} viewSearchPhotos={this.handleShowSearchPhotos} />
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Photos;












