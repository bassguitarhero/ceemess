import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import { styles } from '../../Styles';

import Photo from './Photo';

class PhotoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			thumbnail: '',
			showPhotoAlbum: true,
			showPhotoAlbumPhoto: false,
			photo: null
		}
	}

	handleGetTagPhotos = (tag) => {
		this.props.viewTagPhotos(tag);
	}

	handleGetCategoryPhotos = (category) => {
		this.props.viewCategoryPhotos(category);
	}

	handleClosePhoto = () => {
		this.setState({
			photo: null,
			showPhotoAlbum: true,
			showPhotoAlbumPhoto: false
		});
	}

	handleShowPhoto = (photo) => {
		this.setState({
			photo: photo
		});
	}

	handleClosePhotoAlbum = () => {
		this.props.closePhotoAlbum();
	}

	handleGetThumbnail = () => {
		const { album } = this.props;
		if (album.album_photos.length > 0) {
			const thumbnail = album.album_photos[album.album_photos.length -1].thumbnail;
			this.setState({
				thumbnail: thumbnail
			});
		}
	}

	handleGetFirstFour = () => {
		const { album } = this.props;
		const { firstFour } = this.state;
		if (album.album_photos.length > 0) {
			for (var i = 0; i < 4; i++) {
				firstFour.push(album.album_photos[i]);
			}
		}
	}

	handleSelectAlbum = (album) => {
		this.props.showPhotoAlbum(album);
	}

	componentDidMount() {
		this.handleGetThumbnail();
		this.handleGetFirstFour();
	}

	componentDidUpdate(lastState) {
		if (this.state.photo != null && lastState.photo == null && this.state.showPhotoAlbumPhoto === false) {
			this.setState({
				showPhotoAlbum: false,
				showPhotoAlbumPhoto: true
			});
		}
	}

	render() {
		function truncateDescription(s) {
			if (s.length > 400) {
				return s.substring(0, 300) + '...';
			} else {
				return s;
			}
		}

		const { album } = this.props;
		const { thumbnail, showPhotoAlbum, showPhotoAlbumPhoto, photo } = this.state;

		return (
			<div style={{flex: 1, padding: 10}}>
				{showPhotoAlbum && 
					<Grid container style={{flex: 1}}>
						<Grid item sm={3}>
							<div style={styles.imageContainer}>
									<img src={thumbnail} alt="Album Thumbnail" style={styles.image} />
							</div>
						</Grid>
						<Grid container item sm={9}>
							<Grid container item xs={12}>
								<Grid item xs={10}>
									<div style={styles.titleContainer}><span style={styles.titleText}>{album.title}</span></div>
								</Grid>
								<Grid item xs={2}>
									<Button onClick={this.handleClosePhotoAlbum} style={styles.closeButton}>X</Button>
								</Grid>
								<Grid item xs={12}>
									<div style={styles.descriptionContainer}><span style={styles.descriptionText}>{truncateDescription(album.description)}</span></div>
								</Grid>
							</Grid>
							<Grid container item xs={12} style={{paddingLeft: 10, paddingRight: 10}}>
								{album.album_photos.map((photo, index) => (
									<Grid item xs={3} key={index}>
										<Button onClick={this.handleShowPhoto.bind(this, photo)}>
											<div><img src={photo.thumbnail} alt={photo.title} style={{width: '100%'}} /></div>
										</Button>
									</Grid>
								))}
							</Grid>
						</Grid>
						<Grid container item xs={12}>
							<Grid item sm={6} xs={12}>
								{album.category !== null && 
									<Button style={styles.categoryButton} onClick={this.handleGetCategoryPhotos.bind(this, album.category)}>{album.category.category}</Button>
								}
							</Grid>
							<Grid item sm={6} xs={12} style={styles.tagsContainer}>
								{album.tags.map((tag, index) => (
									<Button key={index} style={styles.tagButton} onClick={this.handleGetTagPhotos.bind(this, tag)} >{tag.tag}</Button>
								))}
							</Grid>
						</Grid>
					</Grid>
				}
				{showPhotoAlbumPhoto &&
					<Photo closePhoto={this.handleClosePhoto} photo={photo} />
				}			
			</div>
		);
	}
}

export default PhotoAlbum;












