import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

import CategoryPostPhotoAlbum from './CategoryPostPhotoAlbum';
import CategoryPostMusicAlbum from './CategoryPostMusicAlbum';
import CategoryLink from './CategoryLink';
import CategoryVideo from './CategoryVideo';

class CategoryPost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showCategoryPost: true,
			showCategoryPhoto: false,
			photo: null
		}
	}

	handleCloseCategoryPhoto = () => {
		this.setState({
			showCategoryPost: true,
			showCategoryPhoto: false,
			photo: null
		});
	}

	handleShowCategoryPhoto = (photo) => {
		this.setState({
			showCategoryPost: false,
			showCategoryPhoto: true,
			photo: photo
		});
	}

	handleCloseCategoryPost = () => {
		this.props.closeCategoryPost();
	}

	componentDidMount() {
		console.log('Post: ', this.props.post);
	}

	render() {
		function formatDateTime(s) {
			var fields 			= s.split('T');
			var date 				= fields[0];
			var time 				= fields[1];
			var dateFields 	= date.split('-');
			var timeFields 	= time.split(':');
			return dateFields[1]+'/'+dateFields[1]+'/'+dateFields[0]+', '+timeFields[0]+':'+timeFields[1];
		}

		const { showCategoryPost, showCategoryPhoto, photo } = this.state;
		const { post } = this.props;

		return (
			<div>
				{showCategoryPost && 
					<Grid container>
	          <Grid item md={2} />
	          <Grid container item md={8} xs={12}>
							{post.image_highlight !== null ?
								<Grid item sm={3} xs={12}>
									<div style={styles.imageContainer}>
										<img src={post.image_highlight.thumbnail} alt={post.image_highlight.caption} style={styles.image} />
									</div>
								</Grid> : 
								<Grid item sm={3} xs={12}>
									<div style={styles.imageContainer}>
										<img src={post.image} alt="Post Thumbnail" style={styles.image} />
									</div>
								</Grid>
							}
							<Grid container item sm={9} xs={12}>
								<div style={styles.post}>
									<Grid container item xs={12}>
										<Grid item xs={10}>
											<div style={styles.titleContainer}>
												<span style={styles.titleText}>{post.title}</span>
											</div>
										</Grid>
										<Grid item xs={2} style={{textAlign: 'right'}}>
											<Button onClick={this.handleCloseCategoryPost} style={styles.closeButton}>X</Button>
										</Grid>
									</Grid>
									<Grid item xs={12}>
										<div style={styles.bodyContainer}>
											<p style={styles.bodyText}>{post.body}</p>
										</div>
									</Grid>
									{post.photo_album !== null && 
										<Grid item xs={12}>
											<CategoryPostPhotoAlbum selectCategoryPostPhoto={this.handleShowCategoryPhoto} categoryPostPhotoAlbumPhotos={post.photo_album.album_photos} />
										</Grid>
									}
									{post.music_album !== null &&
										<Grid item xs={12}>
											<CategoryPostMusicAlbum categoryPostMusicAlbum={post.music_album.album_songs} />
										</Grid>
									}
									{post.links.length > 0 && 
										<div>
											{post.links.map((link, index) => (
												<CategoryLink key={index} link={link} />
											))}
										</div>
									}
									{post.videos.length > 0 &&
										<div>
											{post.videos.map((video, index) => (
												<CategoryVideo key={index} video={video} />
											))}
										</div>
									}
									<div style={styles.publishedContainer}>
										<span style={styles.publishedAt}>
											Published: {formatDateTime(post.created_at)}
										</span>
									</div>
									<Grid container item xs={12}>
										<Grid item xs={6}>
												<Button style={styles.categoryButton}>{post.category.category}</Button>
										</Grid>
										<Grid item xs={6} style={styles.tagsContainer}>
											{post.tags.map((tag, index) => (
												<Button key={index} style={styles.tagButton}>{tag.tag}</Button>
											))}
										</Grid>
									</Grid>
								</div>
							</Grid>
						</Grid>
					</Grid>
				}
				{showCategoryPhoto && 
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={photo.image} alt={photo.title} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{photo.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseCategoryPhoto} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}
}

export default CategoryPost;

