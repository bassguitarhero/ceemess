import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class CategoryPostInlineMusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFour: []
		}
	}

	componentDidMount() {
		const { categoryPostInlineMusicAlbum } = this.props;
		const { firstFour } = this.state;
		for (var i = 0; i < 4; i++) {
			console.log('Track: ', categoryPostInlineMusicAlbum[i]);
			if (categoryPostInlineMusicAlbum[i] !== undefined) {
				firstFour.push(categoryPostInlineMusicAlbum[i])
			}
		}
		this.setState({
			firstFour: firstFour
		})
	}

	render() {
		const { firstFour } = this.state;

		return (
			<div style={styles.container}>
				{firstFour.map((song, index) => (
					<div key={index}>
						{song.audio_file !== null && 
							<Grid container spacing={2} style={styles.songContainer}>
								<Grid item sm={4} xs={12}>
									<span>{song.title}</span>
								</Grid>
								<Grid item sm={8} xs={12}>
									<audio 
			              src={song.audio_file} 
			              style={styles.audioFilePlayer}  
			              controls 
			            />
								</Grid>
							</Grid>
						}
						{song.soundcloud_embed && 
							<div>
								Sound Cloud
							</div>
						}
					</div>
				))}
			</div>
		);
	}
}

export default CategoryPostInlineMusicAlbum;

