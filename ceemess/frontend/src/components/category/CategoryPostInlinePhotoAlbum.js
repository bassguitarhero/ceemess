import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class CategoryPostInlinePhotoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstFourPhotos: []
		}
	}

	handleSelectPhoto = (photo) => {
		this.props.selectPhoto(photo);
	}

	getFirstFourPhotoAlbumPhotos = () => {
		const { categoryPostInlinePhotoAlbumPhotos } = this.props;
		const { firstFourPhotos } = this.state;
		let reverseAlbum = categoryPostInlinePhotoAlbumPhotos.reverse();
		for (var i = 0; i < 4; i++) {
			firstFourPhotos.push(reverseAlbum[i]);
		}
		this.setState({
			firstFourPhotos: firstFourPhotos
		})
	}

	componentDidMount() {
		this.getFirstFourPhotoAlbumPhotos(this.props.categoryPostInlinePhotoAlbumPhotos);
	}

	render() {
		const { firstFourPhotos } = this.state;

		return (
			<Grid container>
				{firstFourPhotos.map((photo, index) => (
					<Grid item xs={3} key={index}>
						<Button onClick={this.handleSelectPhoto.bind(this, photo)} style={styles.photoButton}>
							<img src={photo.thumbnail} alt={photo.title} style={styles.inlineAlbumImage} />
						</Button>
					</Grid>
				))}
			</Grid>
		);
	}
}

export default CategoryPostInlinePhotoAlbum;


