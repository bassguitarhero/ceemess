import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { styles } from '../../Styles';

class CategoryPostMusicAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}

	handleButtonPush = (photo) => {
		// console.log('Photo: ', photo);
		this.props.selectBlogPostPhoto(photo);
	}

	render() {
		const { categoryPostMusicAlbum } = this.props;

		return (
			<Grid container>
				{categoryPostMusicAlbum.map((photo, index) => (
					<Grid item xs={3} key={index}>
						<Button onClick={this.handleButtonPush.bind(this, photo)}><img src={photo.thumbnail} alt="Music Album Cover" style={styles.inlineAlbumImage} /></Button>
					</Grid>
				))}
			</Grid>
		);
	}
}

export default CategoryPostMusicAlbum;

