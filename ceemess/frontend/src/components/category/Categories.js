import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { styles } from '../../Styles';

import { getCategories, clearCategories } from '../../actions/common';
import { getCategoryPosts, getMoreCategoryPosts } from '../../actions/blog';

import CategoryPostInline from './CategoryPostInline';
import CategoryPost from './CategoryPost';

class Categories extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showCategories: true,
			showCategoryPosts: false,
			showCategoryPost: false, 
			showCategoryPostPhoto: false,
			categoryName: '',
			categoryPost: null,
			categoryPostPhoto: null,
			category: null
		}
	}

	handleGetMoreCategoryPosts = () => {
		const { categoryPostsNext } = this.props;
		const { category } = this.state;
		const fields = categoryPostsNext.split('=');
		this.props.getMoreCategoryPosts(category, fields[1]);
	}

	handleCloseCategoryPostPhoto = () => {
		this.setState({
			showCategoryPosts: true,
			showCategoryPost: false, 
			showCategoryPostPhoto: false,
			categoryPostPhoto: null
		});
	}

	handleShowCategoryPostPhoto = (photo) => {
		this.setState({
			showCategoryPosts: false,
			showCategoryPost: false, 
			showCategoryPostPhoto: true,
			categoryPostPhoto: photo
		});
	}

	handleCloseCategoryPost = () => {
		this.setState({
			categoryPost: null,
			showCategoryPost: false,
			showCategoryPosts: true
		})
	}

	handleShowCategoryPost = (post) => {
		this.setState({
			categoryPost: post,
			showCategoryPost: true,
			showCategoryPosts: false
		});
	}

	handleSelectCategory = (category) => {
		this.props.getCategoryPosts(category);
		this.setState({
			categoryName: category.category,
			showCategoryPosts: true,
			category: category
		})
	}

	componentDidMount() {
		this.props.getCategories();
	}

	componentWillUnmount() {
		this.props.clearCategories();
	}

	render() {
		const { categories, loadingCategories, categoryPosts, loadingCategoryPosts, categoryPostsNext } = this.props;
		const { showCategories, showCategoryPosts, showCategoryPost, showCategoryPostPhoto, categoryName, categoryPost, categoryPostPhoto } = this.state;

		return (
			<div>
				{showCategories && 
					<Grid container>
						<Grid md={2} item />
            <Grid container md={8} item xs>
							{loadingCategories === false ?
								<div style={styles.categoriesContainer}>
									{categories.map((category, index) => (
										<Button key={index} style={styles.categoryButton} onClick={this.handleSelectCategory.bind(this, category)}>
											{category.category}
										</Button>
									))}
								</div> : 
								<div style={styles.loadingCategoriesContainer}>
									<span style={styles.title}>Loading Categories</span> <Dots />
								</div>
							}
						</Grid>
					</Grid>
				}
				{showCategoryPosts && 
					<Grid container>
						<Grid md={2} item />
            <Grid container md={8} item xs>
							{loadingCategoryPosts === false ?
								<div>
		            	<div style={styles.categoryTitleContainer}>
										<span style={styles.title}>Category: {categoryName}</span>
									</div>
									{categoryPosts.map((post, index) => (
										<CategoryPostInline key={index} post={post} showCategoryPost={this.handleShowCategoryPost} showCategoryPostPhoto={this.handleShowCategoryPostPhoto} />
									))}
									{categoryPostsNext ? 
										<div style={styles.textContainer}><Button style={styles.loadMoreButton} onClick={this.handleGetMoreCategoryPosts}>Load More</Button></div> :
										<div style={styles.textContainer}><span style={styles.text}>...</span></div>
									}
								</div> :
								<div style={styles.textContainer}>
									<span style={styles.text}>Loading Category Posts</span> <Dots />
								</div>
							}
						</Grid>
					</Grid>
				}
				{showCategoryPost &&
					<div>
						<CategoryPost closeCategoryPost={this.handleCloseCategoryPost} post={categoryPost} />
					</div>
				}
				{showCategoryPostPhoto && 
					<div>
						<div style={styles.blogPostPhotoImageContainer}>
							<img src={categoryPostPhoto.image} alt={categoryPostPhoto.caption} style={styles.blogPostPhoto} />
						</div>
						<div style={styles.blogPostPhotoCaptionContainer}>
							<span style={styles.blogPostPhotoCaption}>{categoryPostPhoto.caption}</span>
						</div>
						<div style={styles.closePhotoPlaceHolder}>
							<div style={styles.closePhotoPosition}>
								<div style={styles.closePhotoCircle}>
									<Button onClick={this.handleCloseCategoryPostPhoto} style={styles.closePhotoText}> X </Button>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
					
		);
	}
}

const mapStateToProps = (state) => {
	return {
		categories: state.common.categories,
		loadingCategories: state.common.loadingCategories,

		categoryPosts: state.blog.categoryPosts,
		loadingCategoryPosts: state.blog.loadingCategoryPosts,
		categoryPostsNext: state.blog.categoryPostsNext
	}
}

export default connect(mapStateToProps, { getCategories, getCategoryPosts, getMoreCategoryPosts, clearCategories })(Categories);












