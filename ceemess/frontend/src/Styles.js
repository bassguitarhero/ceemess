export const styles = {
	aboutMeImage: {
		width: '100%',
		padding: 10
	},
	addNewTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	addNewTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	addPhotoButton: {
		backgroundColor: '#4ab567',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	addTrackButton: {
		backgroundColor: '#4ab567',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	adminTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	adminTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	adminTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	adminTypeButtonsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flex: 1,
		justifyContent: 'center'
	},
	albumImage: {
		width: '90%',
		alignSelf: 'center',
		padding: 10,
		marginTop: 10
	},
	appContainer: {
    marginBottom: 40,
  },
	appWrapper: {
    display: 'flex',
    flex: 1
  },
  audioFilePlayer: {
		width: '100%'
	},
  blogPostPhoto: {
		margin: 20,
		width: '90%',
		alignSelf: 'center'
	},
	blogPostPhotoCaption: {
		fontSize: 20
	},
	blogPostPhotoCaptionContainer: {
		textAlign: 'center',
		padding: 20
	},
	blogPostPhotoImageContainer: {
		textAlign: 'center'
	},
	bodyContainer: {
		paddingLeft: 25,
		paddingRight: 20,
		paddingBottom: 10
	},
	bodyText: {
		fontSize: 16,
		whiteSpace: 'pre-line'
	},
	bodyTextContainer: {
		width: '100%',
		padding: 20
	},
	bottomLine: {
		marginTop: 30,
		backgroundColor: '#999',
		// width: 300,
		height: 2
	},
	buttonsWrapper: {
    marginBottom: 10
  },
  captionContainer: {
		textAlign: 'center',
		padding: 20
	},
	captionText: {
		fontSize: 20
	},
  categoriesContainer: {
		flex: 1,
		padding: 10,
		textAlign: 'center'
	},
  categoryButton: {
		backgroundColor: '#6b5b95',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
    margin: 5
	},
	categoryContainer: {
		padding: 10
	},
	categoryTitleContainer: {
		paddingLeft: 20,
		padding: 10
	},
  closeButton: {
		position: 'relative',
		borderRadius: '50%',
		backgroundColor: '#000',
		color: '#FFF',
		fontSize: 24,
		right: 30,
		border: '2px solid #666',
		// boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
	},
	closeImageCircle: {
		height: 80,
    width: 80,
    // -moz-border-radius: 50%;
    borderRadius: 40,
    backgroundColor: '#000',
    color: '#FFF',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	closeImagePlaceHolder: {
		position: 'relative'
	},
	closeImagePosition: {
		position: 'fixed',
		top: 55,
		right: 20
	},
	closeImageText: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	},
	closePhotoCircle: {
		margin: 0,
		height: 80,
    width: 80,
    // -moz-border-radius: 50%;
    borderRadius: 40,
    backgroundColor: '#000',
    color: '#FFF',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	closePhotoPlaceHolder: {
		position: 'relative'
	},
	closePhotoPosition: {
		position: 'fixed',
		top: 55,
		right: 20
	},
	closePhotoText: {
		justifyContent: 'center', 
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	},
	closePhotoTextWide: {
		justifyContent: 'center', 
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	},
	contactText: {
		fontSize: 18
	},
	contactTitle: {
		fontSize: 24,
		fontWeight: 'bold'
	},
	container: {
    flex: 1
  },
  currentImage: {
		width: '30%',
		padding: 10
	},
  dashboardTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	dashboardTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	descriptionContainer: {
		paddingLeft: 30,
		paddingBottom: 20,
		paddingRight: 10
	},
	descriptionText: {
		fontSize: 16
	},
	formRow: {
		padding: 10
	},
	formTitle: {
		fontSize: 20,
		fontWeight: 'bold'
	},
	formTypeButton: {
		backgroundColor: '#4ab567',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	formTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		paddingBottom: 10
	},
	formTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	formTypeButtonsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flex: 1,
		justifyContent: 'center'
	},
  galleryImage: {
		width: '100%'
	},
  homeButton: {
    color: 'white'
  },
  image: {
		width: '90%',
		alignSelf: 'center',
		padding: 10,
		marginTop: 10
	},
	imageCaptionText: {
		fontSize: 20
	},
	imageCaptionContainer: {
		textAlign: 'center',
		padding: 20
	},
	imageContainer: {
		paddingTop: 10,
		textAlign: 'center'
	},
	inlineAlbumImage: {
		width: '90%',
		padding: 10
	},
  inlinePostTitleText: {
		fontSize: 24,
		fontWeight: 'bold'
	},
	input: {
		width: '100%',
		fontSize: 18
		// marginRight: 40
	},
	inputField: {
		width: '90%'
	},
	itemSelect: {
    color: 'white',
    paddingRight: 10,
    paddingLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center'
  },
	linkContainer: {
		padding: 10,
		marginBottom: 10
	},
	linkText: {
		fontSize: 16,
		fontWeight: 'bold'
	},
	loadingCategoriesContainer: {
		paddingLeft: 20,
		padding: 10
	},
	loadingCategoriesText: {
		fontSize: 20,
		fontWeight: 'bold'
	}, 
	loadingContainer: {
		flex: 1, 
		padding: 20, 
		textAlign: 'center', 
		alignSelf: 'center'
	},
	loadingTagsContainer: {
		paddingLeft: 20,
		padding: 10
	},
	loadingTagsText: {
		fontSize: 20,
		fontWeight: 'bold'
	}, 
	loadMoreContainer: {
		textAlign: 'center',
		padding: 10
	},
	loadMoreButton: {
		backgroundColor: '#405d27',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	loginButton: {
		width: '100%',
		backgroundColor: '#405d27',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	logoutButton: {
		backgroundColor: '#c72a1c',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	playerText: {
		alignSelf: 'center',
		justifyContent: 'center'
	},
	post: {
		flex: 1
	},
	publishedAt: {
		fontSize: 14,
		fontStyle: 'italic'
	},
	publishedContainer: {
		paddingLeft: 10,
		paddingBottom: 10
	},
	readMoreButton: {
		backgroundColor: '#3e4444',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	readMoreContainer: {
		textAlign: 'center',
		padding: 10
	},
	rightAlign: {
    textAlign: 'right'
  },
  searchButton: {
		width: '100%',
		backgroundColor: '#3252a8',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 0 10px 0',
    margin: 5
	},
	searchButtonWide: {
		width: '100%',
		backgroundColor: '#3252a8',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
    margin: 5
	},
	searchInput: {
		width: '95%',
		fontSize: 18,
		margin: 5
	},
	sidebarHeaderText: {
		fontSize: 18,
		fontWeight: 'bold'
	},
  siteTitle: {
    justifyContent: 'center',
    paddingTop: 10
  },
  songContainer: {
		alignItems: 'center',
		padding: 10
	},
	submitButton: {
		backgroundColor: '#405d27',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
  tagButton: {
		backgroundColor: '#d64161',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
    margin: 5,
    fontSize: 12
	},
  tagsContainer: {
		textAlign: 'right',
		padding: 10
	},
	tagTitleContainer: {
		paddingLeft: 20,
		padding: 10
	},
  text: {
		fontSize: 18
	},
	textContainer: {
		padding: 20, 
		textAlign: 'center'
	},
	textField: {
		width: '90%',
		rows: 4
	},
	title: {
		fontSize: 20,
		fontWeight: 'bold'
	},
	titleContainer: {
		display: 'flex', 
		flex: 1,
		textAlign: 'center',
		padding: 20
	},
	uploadPhotosButton: {
		backgroundColor: '#4ab567',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	uploadTracksButton: {
		backgroundColor: '#4ab567',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
  videoContainer: {
		flex: 1,
		// width: '100%'
	},
	videoDescriptionContainer: {
		paddingLeft: 20,
		paddingBottom: 20,
		paddingRight: 10
	},
	videoInlineImage: {
		width: '90%',
		alignSelf: 'center',
		padding: 10
	},
	videoTitleContainer: {
		display: 'flex', 
		flex: 1,
		textAlign: 'center',
		padding: 20,
		marginTop: 10
	},
}










