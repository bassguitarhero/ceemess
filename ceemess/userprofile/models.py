from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.storage import default_storage as storage
from django.utils import timezone
from django.db.models.signals import pre_save
from django.core.files.storage import default_storage as storage

from PIL import Image
from PIL import _imaging
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose, ResizeToFill

from time import time

from api.utils import unique_email_code_generator

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class UserProfile(models.Model):
	user 					= models.OneToOneField(User, related_name='user_profile', on_delete=models.CASCADE)
	code 					= models.CharField(max_length=120, blank=True)
	verified 				= models.BooleanField(default=False)
	first_name				= models.CharField(max_length=140, null=True, blank=True)
	last_name				= models.CharField(max_length=140, null=True, blank=True)
	full_name				= models.CharField(max_length=140, null=True, blank=True)
	birthday				= models.DateField(null=True, blank=True)
	website					= models.URLField(null=True, blank=True)
	bio						= models.TextField(null=True, blank=True)
	public					= models.BooleanField(default=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	blog_writer				= models.BooleanField(default=False)
	following 				= models.ManyToManyField(User, related_name='users_following', blank=True)
	blocking				= models.ManyToManyField(User, related_name='users_blocking', blank=True)
	user_admin 				= models.BooleanField(default=False)
	user_staff 				= models.BooleanField(default=True)
	user_member 			= models.BooleanField(default=False)


	def __unicode__(self):
		return self.user.username

# User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

def pre_save_user_profile(sender, instance, *args, **kwargs):
	if not instance.code:
		instance.code = unique_email_code_generator(instance)

pre_save.connect(pre_save_user_profile, sender=UserProfile)
