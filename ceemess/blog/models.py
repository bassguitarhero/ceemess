from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import User
from time import time
from django.utils.text import slugify
from django.db.models.signals import pre_save
from django.core.files.storage import default_storage as storage

from PIL import Image
from PIL import _imaging
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose, ResizeToFill

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

def get_audio_file_name(instance, filename):
	return settings.AUDIO_UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class BlogSettings(models.Model):
	name 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	show_blog 				= models.BooleanField(default=True)
	default_blog 			= models.BooleanField(default=True)
	show_portfolio 			= models.BooleanField(default=True)
	default_portfolio		= models.BooleanField(default=False)
	show_photos 			= models.BooleanField(default=True)
	default_photos 			= models.BooleanField(default=False)
	show_music 				= models.BooleanField(default=True)
	default_music 			= models.BooleanField(default=False)
	show_videos 			= models.BooleanField(default=True)
	default_videos 			= models.BooleanField(default=False)
	show_images 			= models.BooleanField(default=True)
	default_images 			= models.BooleanField(default=False)
	description 			= models.TextField(null=True, blank=True)
	show_creator			= models.BooleanField(default=False)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ['name']

def create_blog_settings_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogSettings.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_blog_settings_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_blog_settings_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_blog_settings_slug(instance)

pre_save.connect(pre_save_blog_settings_receiver, sender=BlogSettings)

class BlogCountry(models.Model):
	name 					= models.CharField(max_length=256)
	long_name 				= models.CharField(max_length=256, null=True, blank=True)
	slug 					= models.SlugField(unique=True, max_length=255)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	description 			= models.TextField(null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ['name']

def create_country_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogCountry.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_country_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_country_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_country_slug(instance)

pre_save.connect(pre_save_country_receiver, sender=BlogCountry)

class BlogState(models.Model):
	name 					= models.CharField(max_length=256)
	long_name 				= models.CharField(max_length=256, null=True, blank=True)
	country 				= models.ForeignKey(BlogCountry, on_delete=models.CASCADE)
	slug 					= models.SlugField(unique=True, max_length=255)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	description 			= models.TextField(null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ['name']

def create_state_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogState.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_state_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_state_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_state_slug(instance)

pre_save.connect(pre_save_state_receiver, sender=BlogState)

class BlogLocation(models.Model):
	name 					= models.CharField(max_length=256)
	address 				= models.CharField(max_length=256, null=True, blank=True)
	slug 					= models.SlugField(unique=True, max_length=255)
	state 					= models.ForeignKey(BlogState, null=True, on_delete=models.CASCADE)
	country					= models.ForeignKey(BlogCountry, null=True, on_delete=models.CASCADE)
	gps_latitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	gps_longitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	description 			= models.TextField(null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['name']

def create_location_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogLocation.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_location_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_location_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_location_slug(instance)

pre_save.connect(pre_save_location_receiver, sender=BlogLocation)

class BlogVenue(models.Model):
	name 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	address 				= models.CharField(max_length=256, null=True, blank=True)
	gps_latitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	gps_longitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	location 				= models.ForeignKey(BlogLocation, on_delete=models.CASCADE)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	url 					= models.URLField(max_length=256, null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __unicode__(self):
		return self.name 

	class Meta:
		ordering = ['name']

def create_venue_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogVenue.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_venue_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_venue_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_venue_slug(instance)

pre_save.connect(pre_save_venue_receiver, sender=BlogVenue)

class BlogSite(models.Model):
	title 					= models.CharField(max_length=256, null=True, blank=True)
	slug 					= models.SlugField(unique=True, max_length=255)
	description				= models.CharField(max_length=256)
	link 					= models.URLField(max_length=256, null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['created_at']

def create_site_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogSite.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_site_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_site_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_site_slug(instance)

pre_save.connect(pre_save_site_receiver, sender=BlogSite)

class BlogCredit(models.Model):
	name 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	title 					= models.CharField(max_length=256, null=True, blank=True)
	link 					= models.URLField(max_length=256, null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ['-created_at']

def create_credit_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogCredit.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_credit_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_credit_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_credit_slug(instance)

pre_save.connect(pre_save_credit_receiver, sender=BlogCredit)

class BlogLink(models.Model):
	social_media 			= models.BooleanField(default=False)
	description				= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	link 					= models.URLField(max_length=256, null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.description

	class Meta:
		ordering = ['-created_at']

def create_link_slug(instance, new_slug=None):
	slug = slugify(instance.description)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogLink.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_link_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_link_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_link_slug(instance)

pre_save.connect(pre_save_link_receiver, sender=BlogLink)


class BlogTag(models.Model):
	tag 					= models.CharField(max_length=100)
	slug 					= models.SlugField(unique=True, max_length=255)
	css_name				= models.CharField(max_length=100, default="default")
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.tag

	class Meta:
		ordering = ['-created_at']

def create_tag_slug(instance, new_slug=None):
	slug = slugify(instance.tag)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogTag.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_tag_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_tag_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_tag_slug(instance)

pre_save.connect(pre_save_tag_receiver, sender=BlogTag)

class BlogCategory(models.Model):
	category 				= models.CharField(max_length=100)
	slug 					= models.SlugField(unique=True, max_length=255)
	css_name 				= models.CharField(max_length=100, default="default")
	tile_color 				= models.CharField(max_length=10, null=True, blank=True)
	link_color 				= models.CharField(max_length=10, null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.category

	class Meta:
		ordering = ['-created_at']

def create_category_slug(instance, new_slug=None):
	slug = slugify(instance.category)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogCategory.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_category_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_category_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_category_slug(instance)

pre_save.connect(pre_save_category_receiver, sender=BlogCategory)

class BlogImage(models.Model):
	title 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	description				= models.TextField(null=True, blank=True)
	caption 				= models.CharField(max_length=256, null=True, blank=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	image_cropped 			= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	image_thumbnail 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 100})
	home_thumb 				= ImageSpecField(source='image', processors=[ResizeToFill(200, 200)], format='JPEG', options={'quality': 60})
	crop_x_start 			= models.IntegerField(null=True, blank=True)
	crop_x_end 				= models.IntegerField(null=True, blank=True)
	crop_y_start 			= models.IntegerField(null=True, blank=True)
	crop_y_end				= models.IntegerField(null=True, blank=True)
	width 					= models.IntegerField(null=True, blank=True)
	height 					= models.IntegerField(null=True, blank=True)
	category 				= models.ForeignKey(BlogCategory, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, related_name='images_with_tag', blank=True)
	public 					= models.BooleanField(default=True)
	location 				= models.ForeignKey(BlogLocation, null=True, on_delete=models.CASCADE)
	venue 					= models.ForeignKey(BlogVenue, null=True, on_delete=models.CASCADE)
	gps_latitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	gps_longitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['-created_at']

	def save(self, **kwargs):
		super(BlogImage, self).save()
		if self.thumbnail:
			size = 400, 400
			image = Image.open(self.thumbnail)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.thumbnail.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()

	def get_thumbnail(self):
		thumb = str(self.thumbnail)
		if not settings.DEBUG:
			thumb = thumb.replace('assets/', '')
			return thumb

		game_thumb = str(self.game_thumbnail)
		if not settings.DEBUG:
			game_thumb = game_thumb.replace('assets/', '')
			return game_thumb

def create_image_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogImage.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_image_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_image_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_image_slug(instance)

pre_save.connect(pre_save_image_receiver, sender=BlogImage)

class BlogVideo(models.Model):
	title 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	description				= models.TextField(null=True, blank=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	image_thumbnail 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 100})
	home_thumb 				= ImageSpecField(source='image', processors=[ResizeToFill(200, 200)], format='JPEG', options={'quality': 60})
	video_id 				= models.CharField(max_length=100, null=True, blank=True)
	youtube_id 				= models.CharField(max_length=256, null=True, blank=True)
	vimeo_id				= models.CharField(max_length=256, null=True, blank=True)
	width 					= models.IntegerField(null=True, blank=True)
	height 					= models.IntegerField(null=True, blank=True)
	aspect_ratio 			= models.IntegerField(default=1)
	category 				= models.ForeignKey(BlogCategory, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	credits 				= models.ManyToManyField(BlogCredit, blank=True)
	public 					= models.BooleanField(default=True)
	location 				= models.ForeignKey(BlogLocation, null=True, on_delete=models.CASCADE)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['-created_at']

	def save(self, **kwargs):
		super(BlogVideo, self).save()
		if self.thumbnail:
			size = 400, 400
			image = Image.open(self.thumbnail)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.thumbnail.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()

def create_video_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogVideo.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_video_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_video_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_video_slug(instance)

pre_save.connect(pre_save_video_receiver, sender=BlogVideo)

class BlogPhotoAlbum(models.Model):
	title 					= models.CharField(max_length=256)
	description				= models.TextField(null=True, blank=True)
	slug 					= models.SlugField(unique=True, max_length=255)
	category 				= models.ForeignKey(BlogCategory, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	public 					= models.BooleanField(default=True)
	location 				= models.ForeignKey(BlogLocation, null=True, on_delete=models.CASCADE)
	venue 					= models.ForeignKey(BlogVenue, null=True, on_delete=models.CASCADE)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['-created_at']

	def album_order(self):
		return self.album_photos.order_by('order')

def create_photo_album_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogPhotoAlbum.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_photo_album_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_photo_album_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_photo_album_slug(instance)

pre_save.connect(pre_save_photo_album_receiver, sender=BlogPhotoAlbum)

class BlogPhotoPrintSize(models.Model):
	name 				= models.CharField(max_length=256)
	description 		= models.CharField(max_length=256)
	modifier 			= models.DecimalField(default=1.00, max_digits=100, decimal_places=2)
	created_by 			= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 			= models.DateTimeField(auto_now_add=True)
	last_edited_at 		= models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.name 

class BlogPhotoAlbumPhoto(models.Model):
	album 					= models.ForeignKey(BlogPhotoAlbum, related_name='album_photos', on_delete=models.CASCADE)
	order 					= models.IntegerField(default=1)
	position 				= models.IntegerField(default=1)
	title 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	description				= models.TextField(null=True, blank=True)
	caption 				= models.CharField(max_length=256, null=True, blank=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	home_thumb 				= ImageSpecField(source='image', processors=[ResizeToFill(200, 200)], format='JPEG', options={'quality': 60})
	blog_image 				= models.ForeignKey(BlogImage, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	gps_latitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	gps_longitude 			= models.DecimalField(max_digits=30, decimal_places=20, null=True, blank=True)
	location 				= models.ForeignKey(BlogLocation, null=True, on_delete=models.CASCADE)
	venue 					= models.ForeignKey(BlogVenue, null=True, on_delete=models.CASCADE)
	for_sale 				= models.BooleanField(default=True)
	price 					= models.DecimalField(max_digits=100, decimal_places=2, null=True, blank=True)
	shop_feature 			= models.BooleanField(default=False)
	print_sizes 			= models.ManyToManyField(BlogPhotoPrintSize, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['created_at']

def create_photo_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogPhotoAlbumPhoto.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_photo_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_photo_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_photo_slug(instance)

pre_save.connect(pre_save_photo_receiver, sender=BlogPhotoAlbumPhoto)

class BlogMusicAlbum(models.Model):
	band_name 				= models.CharField(max_length=256)
	title 					= models.CharField(max_length=256)
	description				= models.TextField(null=True, blank=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	home_thumb 				= ImageSpecField(source='image', processors=[ResizeToFill(200, 200)], format='JPEG', options={'quality': 60})
	slug 					= models.SlugField(unique=True, max_length=255)
	category 				= models.ForeignKey(BlogCategory, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	public 					= models.BooleanField(default=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	# def __unicode__(self):
	# 	return self.title 
	def __str__(self):
		return self.title

	class Meta:
		ordering = ['-created_at']

	def album_order(self):
		return self.album_songs.order_by('order')

def create_music_album_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogMusicAlbum.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_music_album_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_music_album_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_music_album_slug(instance)

pre_save.connect(pre_save_music_album_receiver, sender=BlogMusicAlbum)

class BlogMusicAlbumSong(models.Model):
	album 					= models.ForeignKey(BlogMusicAlbum, related_name='album_songs', on_delete=models.CASCADE)
	order 					= models.IntegerField(default=1)
	position 				= models.IntegerField(default=1)
	title 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	description				= models.TextField(null=True, blank=True)
	caption 				= models.CharField(max_length=256, null=True, blank=True)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	home_thumb 				= ImageSpecField(source='image', processors=[ResizeToFill(200, 200)], format='JPEG', options={'quality': 60})
	soundcloud_embed 		= models.TextField(null=True, blank=True)
	album_highlight 		= models.BooleanField(default=False)
	audio_file 				= models.FileField(upload_to=get_audio_file_name, null=True, blank=True)
	youtube_url 			= models.CharField(max_length=256, null=True, blank=True)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return self.title

	class Meta:
		ordering = ['created_at']

def create_song_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogMusicAlbumSong.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_song_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_song_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_song_slug(instance)

pre_save.connect(pre_save_song_receiver, sender=BlogMusicAlbumSong)

class BlogShow(models.Model):
	title 					= models.CharField(max_length=256, null=True, blank=True)
	slug 					= models.SlugField(unique=True, max_length=255)
	description				= models.CharField(max_length=256)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	venue 					= models.ForeignKey(BlogVenue, on_delete=models.CASCADE)
	date_start 				= models.DateField()
	url 					= models.URLField(max_length=256, null=True, blank=True)
	photos 					= models.ManyToManyField(BlogPhotoAlbumPhoto, blank=True)
	videos 					= models.ManyToManyField(BlogVideo, blank=True)
	songs 					= models.ManyToManyField(BlogMusicAlbumSong, blank=True)
	category 				= models.ForeignKey(BlogCategory, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)

	def __unicode__(self):
		return self.title 

	class Meta:
		ordering = ['-date_start']

def create_show_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogShow.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_show_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_show_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_show_slug(instance)

pre_save.connect(pre_save_show_receiver, sender=BlogShow)


class BlogPost(models.Model):
	blog 					= models.BooleanField(default=True)
	portfolio				= models.BooleanField(default=False)
	stream 					= models.BooleanField(default=False)
	title 					= models.CharField(max_length=256)
	slug 					= models.SlugField(unique=True, max_length=255)
	body 					= models.TextField(null=True, blank=True)
	image_highlight			= models.ForeignKey(BlogImage, related_name='image_highlight_posts', null=True, on_delete=models.CASCADE)
	video_highlight 		= models.ForeignKey(BlogVideo, related_name='video_highlight_posts', null=True, on_delete=models.CASCADE)
	image 					= ProcessedImageField(upload_to=get_upload_file_name, processors=[Transpose(),], format = 'JPEG', null=True, blank=True)
	thumbnail 				= ImageSpecField(source='image', processors=[ResizeToFill(400, 400)], format='JPEG', options={'quality': 60})
	image_gallery 			= models.ManyToManyField(BlogImage, blank=True)
	photo_album 			= models.ForeignKey(BlogPhotoAlbum, null=True, on_delete=models.CASCADE)
	music_album 			= models.ForeignKey(BlogMusicAlbum, null=True, on_delete=models.CASCADE)
	videos 					= models.ManyToManyField(BlogVideo, blank=True)
	# word_piece 				= models.ForeignKey(WordPiece, null=True, on_delete=models.CASCADE)
	location 				= models.ForeignKey(BlogLocation, null=True, on_delete=models.CASCADE)
	venue 					= models.ForeignKey(BlogVenue, null=True, on_delete=models.CASCADE)
	show 					= models.ForeignKey(BlogShow, null=True, on_delete=models.CASCADE)
	sticky 					= models.BooleanField(default=False)
	sticky_date 			= models.DateTimeField(null=True, blank=True)
	portfolio_sticky		= models.BooleanField(default=False)
	portfolio_sticky_date	= models.DateTimeField(null=True, blank=True)
	stream_sticky 			= models.BooleanField(default=False)
	stream_sticky_date 		= models.DateTimeField(null=True, blank=True)
	published				= models.BooleanField(default=False)
	published_at 			= models.DateTimeField(null=True, blank=True)
	likes 					= models.ManyToManyField(User, related_name='posts_liked', blank=True)
	category 				= models.ForeignKey(BlogCategory, null=True, on_delete=models.CASCADE)
	tags 					= models.ManyToManyField(BlogTag, blank=True)
	credits 				= models.ManyToManyField(BlogCredit, blank=True)
	links 					= models.ManyToManyField(BlogLink, blank=True)
	created_by 				= models.ForeignKey(User, on_delete=models.CASCADE)
	created_at 				= models.DateTimeField(default=timezone.now)
	last_edited				= models.DateTimeField(null=True, blank=True)
	test_field 				= models.CharField(max_length=256, null=True, blank=True)

	# def __unicode__(self):
	# 	return self.title 
	def __str__(self):
		return self.title

	class Meta:
		ordering = ['-created_at']

def create_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if len(slug)>50:
		slug = slug[:50]
	if new_slug is not None:
		slug = new_slug 
	query_set = BlogPost.objects.filter(slug=slug).order_by('-id')
	exists = query_set.exists()
	if exists:
		new_slug = "%s-%s" % (slug, query_set.first().id)
		return create_slug(instance, new_slug=new_slug)
	return slug 

def pre_save_post_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_post_receiver, sender=BlogPost)
