from django.contrib import admin
from blog.models import BlogSettings, BlogCountry, BlogState, BlogLocation, BlogVenue, BlogShow, BlogPost, BlogCategory, BlogTag, BlogVideo, BlogImage, BlogPhotoPrintSize, BlogPhotoAlbum, BlogPhotoAlbumPhoto, BlogMusicAlbum, BlogMusicAlbumSong, BlogCredit, BlogLink, BlogSite

# Register your models here.

admin.site.register(BlogSettings)
admin.site.register(BlogCountry)
admin.site.register(BlogState)
admin.site.register(BlogLocation)
admin.site.register(BlogVenue)
admin.site.register(BlogShow)
admin.site.register(BlogPost)
admin.site.register(BlogCategory)
admin.site.register(BlogTag)
admin.site.register(BlogVideo)
admin.site.register(BlogImage)
admin.site.register(BlogPhotoAlbum)
admin.site.register(BlogPhotoPrintSize)
admin.site.register(BlogPhotoAlbumPhoto)
admin.site.register(BlogMusicAlbum)
admin.site.register(BlogMusicAlbumSong)
admin.site.register(BlogCredit)
admin.site.register(BlogLink)
admin.site.register(BlogSite)